var base_url = window.location.protocol + '//' + window.location.host + '/' + window.location.pathname.split('/')[1];

function _regle_test(regle_id, test) {
    regle_elem = document.getElementById(regle_id);

    if (test) {
        regle_elem.classList.add('check');
        regle_elem.innerHTML = '<i class="fas fa-check"></i>' + regle_elem.innerText;
        return 0;
    } else {
        regle_elem.classList.remove('check');
        regle_elem.innerHTML = '<i class="fas fa-times"></i>' + regle_elem.innerText;
        return 1;
    }
}

function _password_test(mdp_str) {
	var result =	_regle_test('mdp-car', mdp_str.length > 7) +
					_regle_test('mdp-maj', /[A-Z]/.test(mdp_str)) +
					_regle_test('mdp-min', /[a-z]/.test(mdp_str)) +
					_regle_test('mdp-num', /[0-9]/.test(mdp_str)) +
					_regle_test('mdp-spe', /[^A-Za-z0-9]/.test(mdp_str));

	return result == 0;
}

function _password_compare(mdp1_str, mdp2_str, regles_elem) {
	if (mdp1_str == mdp2_str) {
		regles_elem.classList.remove('focus');
		return true;
	} else {
		regles_elem.classList.add('focus');
		return false;
	}
}

function _invalid_display(elem, test) {
    if (test) {
        elem.classList.remove('is-invalid');
    } else {
        elem.classList.add('is-invalid');
    }
}

// Mise en place des tableaux :
$(document).ready(function() {
	var dataSrc = [];
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash

	var tableSocietaires = $('#tableau-societaires').DataTable({
		pagingType: 'simple',
		stateSave: true,
		'columnDefs': [
			{ 'orderable': false, 'targets': [6,7]}
		],
		'language': {
			url: base_url + '/style/datatables/json/fr_fr.json'
		},

		'initComplete': function() {
			var api = this.api();

			// Récupération des données du tableau
			api.cells('tr', [0, 2, 3]).every(function() {
				var data = $('<div>').html(this.data()).text();
				if(dataSrc.indexOf(data) === -1) {
					dataSrc.push(data);
				}
			});

			dataSrc.sort();

			$('.dataTables_filter input[type="search"]', api.table().container()).typeahead({
				source: dataSrc,
				afterSelect: function(value) {
					api.search(value).draw();
				}
			});
		}
	});
	$('#tableau-societaires tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
	} );

	$('#allDisabled').click( function () {
		msg='Voulez-vous vraiment désactiver les sociétaires :\n'; 
		logins = [];
		listeSocs = tableSocietaires.rows('.selected').data();
		for(i = 0; i<listeSocs.length;i++) {
			if((listeSocs[i][5]=='Activé')||(listeSocs[i][5]=='Connecté')){
				msg += listeSocs[i][0] + '\n';
				logins[i] = listeSocs[i][0];
			}
		}
		if(logins.length>0) {
			var confirmdelete = (confirm(msg));
        	if (confirmdelete == true) {
           		$.ajax({
            		url: 'GestionSocietaires/activationListe',
            		type: "post",
					data : { logins: logins ,[csrfName]: csrfHash},
					dataType : 'json',
            		success: function(response){
						$('.txt_csrfname').val(response.token);
						location.reload(true);
               		},
					error: function (request, error) {
						console.log(arguments);
						alert(" Can't do because: " + error);
					}
            	});
        	}
		} else {
			alert('Il faut sélectionner des sociétaires activés ou connectés.')
		}
	} );

	$('#allEnabled').click( function () {
		msg='Voulez-vous vraiment activer les sociétaires :\n'; 
		logins = [];
		listeSocs = tableSocietaires.rows('.selected').data();
		for(i = 0; i<listeSocs.length;i++) {
			if(listeSocs[i][5]=='Désactivé'){
				msg += listeSocs[i][0] + '\n';
				logins[i] = listeSocs[i][0];
			}
		}
		if(logins.length>0) {
			var confirmdelete = (confirm(msg));
        	if (confirmdelete == true) {
           		$.ajax({
            		url: 'GestionSocietaires/activationListe',
            		type: "post",
					data : { logins: logins ,[csrfName]: csrfHash},
					dataType : 'json',
            		success: function(response){
						$('.txt_csrfname').val(response.token);
						location.reload(true);
               		},
					error: function (request, error) {
						console.log(arguments);
						alert(" Can't do because: " + error);
					}
            	});
        	}
		} else {
			alert('Il faut sélectionner des sociétaires désactivés.')
		}
	} );

	$('#allSupprimer').click( function () {
		msg='Voulez-vous vraiment supprimer les sociétaires :\n'; 
		logins = [];
		listeSocs = tableSocietaires.rows('.selected').data();
		for(i = 0; i<listeSocs.length;i++) {
			if(listeSocs[i][5]=='Désactivé'){
				msg += listeSocs[i][0] + '\n';
				logins[i] = listeSocs[i][0];
			}
		}
		if(logins.length>0) {
			var confirmdelete = (confirm(msg));
        	if (confirmdelete == true) {
           		$.ajax({
            		url: 'GestionSocietaires/supprimerListe',
            		type: "post",
					data : { logins: logins ,[csrfName]: csrfHash},
					dataType : 'json',
            		success: function(response){
						$('.txt_csrfname').val(response.token);
						location.reload(true);
               		},
					error: function (request, error) {
						console.log(arguments);
						alert(" Can't do because: " + error);
					}
            	});
        	}
		} else {
			alert('Il faut sélectionner des sociétaires désactivés.')
		}
	} );

	var tableSocietairesA = $('#tableau-societaires-a').DataTable({
		pagingType: 'simple',
		stateSave: true,
		'order': [[7, 'asc']],
		'columnDefs': [
			{ 'orderable': false, 'targets': [8, 9, 10]},
		],
		'language': {
			url: base_url + '/style/datatables/json/fr_fr.json'
		},

		'initComplete': function() {
			var api = this.api();

			// Récupération des données du tableau
			api.cells('tr', [0, 2, 3]).every(function() {
				var data = $('<div>').html(this.data()).text();
				if(dataSrc.indexOf(data) === -1) {
					dataSrc.push(data);
				}
			});

			dataSrc.sort();

			$('.dataTables_filter input[type="search"]', api.table().container()).typeahead({
				source: dataSrc,
				afterSelect: function(value) {
					api.search(value).draw();
				}
			});
		}
	});
	$('#tableau-societaires-a tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
	} );

	$('#allErase').click( function () {
		msg='Voulez-vous vraiment effacer les données des sociétaires :\n'; 
		logins = [];
		listeSocs = tableSocietairesA.rows('.selected').data();
		for(i = 0; i<listeSocs.length;i++) {
			msg += listeSocs[i][0] + '\n';
			logins[i] = listeSocs[i][0];
		}
		if(logins.length>0) {
			var confirmdelete = (confirm(msg));
        	if (confirmdelete == true) {
           		$.ajax({
            		url: 'effacerListe',
            		type: "post",
					data : { logins: logins ,[csrfName]: csrfHash},
					dataType : 'json',
            		success: function(response){
						$('.txt_csrfname').val(response.token);
						location.reload(true);
               		},
					error: function (request, error) {
						console.log(arguments);
						alert(" Can't do because: " + error);
					}
            	});
        	}
		} else {
			alert('Il faut sélectionner des sociétaires.')
		}
	} );

	$('#allOldSupprimer').click( function () {
		msg='Voulez-vous vraiment supprimer les sociétaires :\n'; 
		logins = [];
		listeSocs = tableSocietairesA.rows('.selected').data();
		for(i = 0; i<listeSocs.length;i++) {
			msg += listeSocs[i][0] + '\n';
			logins[i] = listeSocs[i][0];
		}
		if(logins.length>0) {
			var confirmdelete = (confirm(msg));
        	if (confirmdelete == true) {
           		$.ajax({
            		url: 'supprimerListe',
            		type: "post",
					data : { logins: logins ,[csrfName]: csrfHash},
					dataType : 'json',
            		success: function(response){
						$('.txt_csrfname').val(response.token);
						location.reload(true);
               		},
					error: function (request, error) {
						console.log(arguments);
						alert(" Can't do because: " + error);
					}
            	});
        	}
		} else {
			alert('Il faut sélectionner des sociétaires.')
		}
	} );

	var tableMouvements = $('#tableau-mouvements').DataTable({
		pagingType: 'simple',
		stateSave: true,
		'columnDefs': [
			{ 'orderable': false, 'targets': [8, 9]}
		],
		'language': {
			url: base_url + '/style/datatables/json/fr_fr.json'
		},

		'initComplete': function() {
			var api = this.api();

			// Récupération des données du tableau
			api.cells('tr', [0, 1, 2, 3, 7]).every(function() {
				var data = $('<div>').html(this.data()).text();
				if(dataSrc.indexOf(data) === -1) {
					dataSrc.push(data);
				}
			});

			dataSrc.sort();

			$('.dataTables_filter input[type="search"]', api.table().container()).typeahead({
				source: dataSrc,
				afterSelect: function(value) {
					api.search(value).draw();
				}
			});
		}
	});
	$('#tableau-mouvements tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
	} );

	$('#allMvtSupprimer').click( function () {
		msg='Voulez-vous vraiment supprimer les mouvements :\n'; 
		ids = [];
		listeMvts = tableMouvements.rows('.selected').data();
		for(i = 0; i<listeMvts.length;i++) {
			msg += listeMvts[i][0] + '\n';
			ids[i] = listeMvts[i][0];
		}
		if(ids.length>0) {
			var confirmdelete = (confirm(msg));
        	if (confirmdelete == true) {
           		$.ajax({
            		url: 'GestionMouvements/supprimerListeMouvements',
            		type: "post",
					data : { ids: ids ,[csrfName]: csrfHash},
					dataType : 'json',
            		success: function(response){
						$('.txt_csrfname').val(response.token);
						location.reload(true);
               		},
					error: function (request, error) {
						console.log(arguments);
						alert(" Can't do because: " + error);
					}
            	});
        	}
		} else {
			alert('Il faut sélectionner des mouvements.')
		}
	} );

	$('#tableau-indivisions').DataTable({
		pagingType: 'simple',
		stateSave: true,
		'columnDefs': [
			{ 'orderable': false, 'targets': 7}
		],
		'language': {
			url: base_url + '/style/datatables/json/fr_fr.json'
		},

		'initComplete': function() {
			var api = this.api();

			// Récupération des données du tableau
			api.cells('tr', [0, 2, 3]).every(function() {
				var data = $('<div>').html(this.data()).text();
				if(dataSrc.indexOf(data) === -1) {
					dataSrc.push(data);
				}
			});

			dataSrc.sort();

			$('.dataTables_filter input[type="search"]', api.table().container()).typeahead({
				source: dataSrc,
				afterSelect: function(value) {
					api.search(value).draw();
				}
			});
		}
	});

	$('[name="tableau-representes"]').DataTable({
		pagingType: 'simple',
		stateSave: true,
		'columnDefs': [
			{ 'orderable': false, 'targets': [7, 8]}
		],
		'language': {
			url: base_url + '/style/datatables/json/fr_fr.json'
		},

		'initComplete': function() {
			var api = this.api();

			// Récupération des données du tableau
			api.cells('tr', [1, 2]).every(function() {
				var data = $('<div>').html(this.data()).text();
				if(dataSrc.indexOf(data) === -1) {
					dataSrc.push(data);
				}
			});

			dataSrc.sort();

			$('.dataTables_filter input[type="search"]', api.table().container()).typeahead({
				source: dataSrc,
				afterSelect: function(value) {
					api.search(value).draw();
				}
			});
		}
	});
});

// Afficher ou non le mot de passe (yeux)
Array.from(document.querySelectorAll('.form-eye')).forEach(eye =>
    eye.addEventListener('click', function() {
        //------ Elements ------//
        var input_elem = document.querySelector('#' + eye.dataset.input);
        //------ Elements ------//

        //------ Actions ------//
        if (input_elem.getAttribute('type') == 'text') {
            input_elem.setAttribute('type', 'password');
            eye.innerHTML = '<i class="fas fa-eye-slash"></i>';
        } else {
            input_elem.setAttribute('type', 'text');
            eye.innerHTML = '<i class="fas fa-eye"></i>';
        }
        //------ Actions ------//
    })
);

// Menu
var menu_elem = document.getElementsByName('menu');
menu_elem.forEach(function(menu) {
	//------ Elements ------//
	var sub_menu_elem			= menu.querySelector('.nav');
	var sub_menu_button_elem	= menu.querySelector('a[role="button"]');
	//------ Elements ------//

	//------ Init ------//
	sub_menu_display();
	//------ Init ------//

	//------ Functions ------//
	function sub_menu_display() {
		if (sub_menu_button_elem.classList.contains('active') || sub_menu_elem.querySelector('a.active') != null) {
			sub_menu_button_elem.classList.add('active');
			sub_menu_elem.classList.add('active');
		} else {
			sub_menu_elem.classList.remove('active');
			sub_menu_button_elem.classList.remove('active');
		}
	}
	//------ Functions ------//

	//------ Events ------//
	sub_menu_button_elem.addEventListener('click', function() {
		this.classList.add('active');
		sub_menu_display();
	});
	//------ Events ------//
});

// Formulaire de connexion
var form_connexion_elem = document.getElementsByName('form-connexion');
form_connexion_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var inputs_elem = form.querySelectorAll('input.form-control');
	//------ Elements ------//

	//------ Init ------//
	button_elem.disabled = true;
	//------ Init ------//

	//------ Functions ------//
	//------ Functions ------//

	//------ Events ------//
	Array.from(inputs_elem).forEach(input =>
		input.addEventListener('input', function() {
			empty = 1;
			Array.from(inputs_elem).forEach(i => empty = empty * i.value.length);

			button_elem.disabled = empty == 0;
		})
	);
	//------ Events ------//
});


// Formulaire de modification de mot de passe
var form_modifierMdp_elem = document.getElementsByName('form-modifierMdp');
form_modifierMdp_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var oldmdp_elem		= form.querySelector('input[name="oldmdp"]');
	var newmdp1_elem	= form.querySelector('input[name="newmdp1"]');
	var newmdp2_elem	= form.querySelector('input[name="newmdp2"]');

	var newmdp1_regles_elem = form.querySelector('.form-regles-5');
	var newmdp2_regles_elem = form.querySelector('.form-regles-1');

	var oldmdp_check	= false;
	var newmdp1_check	= false;
	var newmdp2_check	= false;
	//------ Elements ------//

	//------ Init ------//
	button_elem.disabled = true;
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(oldmdp_check && newmdp1_check && newmdp2_check);
	}
	//------ Functions ------//

	//------ Events ------//
	oldmdp_elem.addEventListener('input', function() {
		oldmdp_check = this.value.length > 0;
		form_complete();
	});

	newmdp1_elem.addEventListener('focus', function() {
		newmdp1_regles_elem.classList.add('focus');
	});

	newmdp1_elem.addEventListener('blur', function() {
		if (this.value.length == 0) {
			newmdp1_regles_elem.classList.remove('focus');
		}
	});

	newmdp1_elem.addEventListener('input', function() {
		newmdp1_check = _password_test(this.value);
		newmdp2_check = _password_compare(this.value, newmdp2_elem.value, newmdp2_regles_elem);
		form_complete();
	});

	newmdp2_elem.addEventListener('input', function() {
		newmdp2_check = _password_compare(this.value, newmdp1_elem.value, newmdp2_regles_elem);
		form_complete();
	});
	//------ Events ------//
});

// Formulaire du nouveau mot de passe
var form_nouveauMdp_elem = document.getElementsByName('form-nouveauMdp');
form_nouveauMdp_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');
	
	var mdp1_elem = form.querySelector('input[name="mdp1"]');
	var mdp2_elem = form.querySelector('input[name="mdp2"]');

	var mdp1_regles_elem = form.querySelector('.form-regles-5');
	var mdp2_regles_elem = form.querySelector('.form-regles-1');

	var mdp1_check = false;
	var mdp2_check = false;
	//------ Elements ------//

	//------ Init ------//
	button_elem.disabled = true;
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(mdp1_check && mdp2_check);
	}
	//------ Functions ------//

	//------ Events ------//
	mdp1_elem.addEventListener('focus', function() {
		mdp1_regles_elem.classList.add('focus');
	});

	mdp1_elem.addEventListener('blur', function() {
		if (this.value.length == 0) {
			mdp1_regles_elem.classList.remove('focus');
		}
	});

	mdp1_elem.addEventListener('input', function() {
		mdp1_check = _password_test(this.value);
		mdp2_check = _password_compare(this.value, mdp2_elem.value, mdp2_regles_elem);
		form_complete();
	});

	mdp2_elem.addEventListener('input', function() {
		mdp2_check = _password_compare(this.value, mdp1_elem.value, mdp2_regles_elem);
		form_complete();
	});
	//------ Events ------//
});

// Formulaire de reinitialisation de mot de passe
var form_reinitialiserMdp_elem = document.getElementsByName('form-reinitialiserMdp');
form_reinitialiserMdp_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var login_elem	= form.querySelector('input[name="login"]');
	var email_elem	= form.querySelector('input[name="email"]');

	var login_check = login_elem.value.length > 0;
	var email_check = email_elem.value.length > 0;
	//------ Elements ------//

	//------ Init ------//
	button_elem.disabled = true;
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(login_check && email_check);
	}
	//------ Functions ------//

	//------ Events ------//
	login_elem.addEventListener('input', function() {
		login_check = this.value.length > 0;

		form_complete();
	});

	email_elem.addEventListener('input', function() {
		email_check = /^.*@.*\./.test(this.value);

		form_complete();
	});
	//------ Events ------//
});

// Formulaire de modification du site
var form_modifierSite_elem = document.getElementsByName('form-modifierSite');
form_modifierSite_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var nom_elem	= form.querySelector('input[name="nom"]');
	var email_elem	= form.querySelector('input[name="email"]');
	var image_elem	= form.querySelector('input[name="img"]');
	var icone_elem	= form.querySelector('input[name="ico"]');

	var nom_check	=	true;
	var email_check =	true;
	var image_check =	true;
	var icone_check =	true;
	//------ Elements ------//

	//------ Init ------//
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(nom_check && email_check && image_check && icone_check);
	}
	//------ Functions ------//

	//------ Events ------//
	nom_elem.addEventListener('input', function() {
		nom_check = this.value.length > 4 && this.value.length < 16;

		_invalid_display(this, nom_check);
		form_complete();
	});

	email_elem.addEventListener('input', function() {
		email_check = /^.*@.*\./.test(this.value);

		_invalid_display(this, email_check);
		form_complete();
	});

	image_elem.addEventListener('input', function() {
		var format = this.value.split('.').pop().toLowerCase();
		image_check = format == 'jpg' || format == 'jpeg';

		_invalid_display(this, image_check);
		form_complete();
	});

	icone_elem.addEventListener('input', function() {
		var format = this.value.split('.').pop().toLowerCase();
		icone_check = format == 'jpg' || format == 'jpeg';

		_invalid_display(this, icone_check);
		form_complete();
	});
	//------ Events ------//
});

// Formulaire de modification de l'administrateur
var form_modifierAdmin_elem = document.getElementsByName('form-modifierAdmin');
form_modifierAdmin_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var login_elem = form.querySelector('input[name="login"]');
	var email_elem = form.querySelector('input[name="mail"]');

	var login_check = true;
	var email_check = true;
	//------ Elements ------//

	//------ Init ------//
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(login_check && email_check);
	}
	//------ Functions ------//

	//------ Events ------//
	login_elem.addEventListener('input', function() {
		login_check = this.value.length > 0;

		_invalid_display(this, login_check);
		form_complete();
	});

	email_elem.addEventListener('input', function() {
		email_check = /^.*@.*\./.test(this.value);

		_invalid_display(this, email_check);
		form_complete();
	});
	//------ Events ------//
});

// Formulaire pour ajouter un sociétaire
var form_ajouterSocietaire_elem = document.getElementsByName('form-ajouterSocietaire');
form_ajouterSocietaire_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem		= form.querySelector('button');
	var pages_elem		= form.querySelectorAll('div[name="page"]');
	var num_pages_elem	= form.querySelector('p[name="num-page"]');
	var prec_elem		= form.querySelector('span[name="prec"]');
	var suiv_elem		= form.querySelector('span[name="suiv"]');

	var statut_elem			= form.querySelector('select[name="statut"]');
	var porte_fort_elem		= form.querySelector('input[name="porte-fort"]');
	var civilite_elem		= form.querySelector('select[name="civilite"]');
	var nom_elem			= form.querySelector('input[name="nom"]');
	var nom_usuel_elem		= form.querySelector('input[name="nom-usuel"]');
	var prenom_elem			= form.querySelector('input[name="prenom"]');
	var autres_prenoms_elem	= form.querySelector('input[name="autres-prenoms"]');
	var representants_elem	= form.querySelector('input[name="representants"]');
	var lieu_naissance_elem	= form.querySelector('input[name="lieu"]');
	var date_naissance_elem	= form.querySelector('input[name="date"]');
	var siret_elem			= form.querySelector('input[name="siret"]');
	var nationalite_elem	= form.querySelector('input[name="nationalite"]');
	var adresse_elem		= form.querySelector('input[name="adresse"]');
	var code_postal_elem	= form.querySelector('input[name="code"]');
	var ville_elem			= form.querySelector('input[name="ville"]');
	var pays_elem			= form.querySelector('input[name="pays"]');
	var email_elem			= form.querySelector('input[name="email"]');
	var tel_fixe_elem		= form.querySelector('input[name="fixe"]');
	var tel_portable_elem	= form.querySelector('input[name="portable"]');
	var regime_mat_elem		= form.querySelector('select[name="matrimonial"]');
	var regime_fis_elem		= form.querySelector('select[name="fiscal"]');

	var nom_check				= nom_elem.value.length > 0;
	var nom_usuel_check			= true;
	var prenom_check			= prenom_elem.value.length > 0;
	var autres_prenoms_check	= true;
	var representants_check		= true;
	var lieu_naissance_check	= lieu_naissance_elem.value.length > 0;
	var date_naissance_check	= date_naissance_elem.value.length > 0;
	var siret_check				= siret_elem.value.length > 0;
	var nationalite_check		= nationalite_elem.value.length > 0;
	var adresse_check			= adresse_elem.value.length > 0;
	var code_postal_check		= code_postal_elem.value.length > 0;
	var ville_check				= ville_elem.value.length > 0;
	var pays_check				= true;
	var email_check				= true;
	var tel_fixe_check			= true;
	var tel_portable_check		= true;
	//------ Elements ------//

	//------ Init ------//
	form_display();
	button_elem.disabled = true;
	prec_elem.classList.add('disabled');

	var index = 0;
	//------ Init ------//

	//------ Functions ------//
	function form_display() {
		var test = statut_elem.value == 0;
		
		porte_fort_elem.disabled					= !test;
		porte_fort_elem.parentElement.hidden		= !test;
		representants_elem.disabled					= !test;
		representants_elem.parentElement.hidden		= !test;
		lieu_naissance_elem.disabled				= !test;
		lieu_naissance_elem.parentElement.hidden	= !test;
		siret_elem.disabled							= test;
		siret_elem.parentElement.hidden				= test;
		regime_mat_elem.disabled					= !test;
		regime_mat_elem.parentElement.hidden		= !test;
		regime_fis_elem.disabled					= test;
		regime_fis_elem.parentElement.hidden		= test;
		prec_elem.hidden = true;

		if (test) {
			nom_elem.parentElement.querySelector('label').innerHTML				= 'Nom <span class="text-danger">*</span>';
			prenom_elem.parentElement.querySelector('label').innerHTML			= 'Prénom <span class="text-danger">*</span>';
			date_naissance_elem.parentElement.querySelector('label').innerHTML	= 'Date de naissance <span class="text-danger">*</span>';
		} else {
			nom_elem.parentElement.querySelector('label').innerHTML				= 'Nom du représentant légal <span class="text-danger">*</span>';
			prenom_elem.parentElement.querySelector('label').innerHTML			= 'Prénom du représentant légal <span class="text-danger">*</span>';
			date_naissance_elem.parentElement.querySelector('label').innerHTML	= 'Date de création <span class="text-danger">*</span>';
		}

		form_complete();
	}

	function form_complete() {
		var test =	nom_check && nom_usuel_check && prenom_check && autres_prenoms_check && date_naissance_check && nationalite_check &&
					adresse_check && code_postal_check && ville_check && pays_check && email_check && tel_fixe_check &&
					tel_portable_check;

		if (statut_elem.value == 0) {
			test = test && representants_check;
			test = test && lieu_naissance_check;
		} else {
			test = test && siret_check;
		}

		button_elem.disabled = !test;
	}
	//------ Functions ------//

	//------ Events ------//
	prec_elem.addEventListener('click', function() {
		pages_elem[index].hidden = true;
		index--;
		pages_elem[index].hidden = false;

		num_pages_elem.innerText = (index + 1) + ' / 3';

		if (index <= 0) {
			prec_elem.classList.add('disabled');
			suiv_elem.classList.remove('disabled');
			prec_elem.hidden = true;
			suiv_elem.hidden = false;
			button_elem.hidden = true;
		} else if (index < 2) {
			prec_elem.classList.remove('disabled');
			suiv_elem.classList.remove('disabled');
			prec_elem.hidden = false;
			suiv_elem.hidden = false;
			button_elem.hidden = true;
		} else {
			prec_elem.classList.remove('disabled');
			suiv_elem.classList.add('disabled');
			prec_elem.hidden = false;
			suiv_elem.hidden = true;
			button_elem.hidden = false;
		}
	});

	suiv_elem.addEventListener('click', function() {
		pages_elem[index].hidden = true;
		index++;
		pages_elem[index].hidden = false;

		num_pages_elem.innerText = (index + 1) + ' / 3';

		if (index <= 0) {
			prec_elem.classList.add('disabled');
			suiv_elem.classList.remove('disabled');
			prec_elem.hidden = true;
			suiv_elem.hidden = false;
			button_elem.hidden = true;
		} else if (index < 2) {
			prec_elem.classList.remove('disabled');
			suiv_elem.classList.remove('disabled');
			prec_elem.hidden = false;
			suiv_elem.hidden = false;
			button_elem.hidden = true;
		} else {
			prec_elem.classList.remove('disabled');
			suiv_elem.classList.add('disabled');
			prec_elem.hidden = false;
			suiv_elem.hidden = true;
			button_elem.hidden = false;
		}
	});

	statut_elem.addEventListener('input', function() {
		form_display();
		form_complete();
	});

	nom_elem.addEventListener('input', function() {
		nom_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])+$/.test(this.value);
		this.value = this.value.toUpperCase();

		_invalid_display(this, nom_check);
		form_complete();
	});

	nom_usuel_elem.addEventListener('input', function() {
		nom_usuel_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])*$/.test(this.value);
		this.value = this.value.toUpperCase();

		_invalid_display(this, nom_usuel_check);
		form_complete();
	});

	prenom_elem.addEventListener('input', function() {
		prenom_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, prenom_check);
		form_complete();
	});

	autres_prenoms_elem.addEventListener('input', function() {
		autres_prenoms_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])*$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, autres_prenoms_check);
		form_complete();
	});

	representants_elem.addEventListener('input', function() {
		representants_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])*$/.test(this.value);

		_invalid_display(this, representants_check);
		form_complete();
	});

	lieu_naissance_elem.addEventListener('input', function() {
		lieu_naissance_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, lieu_naissance_check);
		form_complete();
	});

	date_naissance_elem.addEventListener('input', function() {
		date_naissance_check = this.value.length > 0;

		_invalid_display(this, date_naissance_check);
		form_complete();
	});

	siret_elem.addEventListener('input', function() {
		siret_check = /^[0-9]{0,14}$/.test(this.value);
		_invalid_display(this, siret_check);
		form_complete();
	});

	nationalite_elem.addEventListener('input', function() {
		nationalite_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])*$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, nationalite_check);
		form_complete();
	});

	adresse_elem.addEventListener('input', function() {
		adresse_check = /^([A-Za-z]|[0-9]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);

		_invalid_display(this, adresse_check);
		form_complete();
	});

	code_postal_elem.addEventListener('input', function() {
		code_postal_check = /^([A-Za-z]|[0-9])+$/.test(this.value);

		_invalid_display(this, code_postal_check);
		form_complete();
	});

	ville_elem.addEventListener('input', function() {
		ville_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, ville_check);
		form_complete();
	});

	pays_elem.addEventListener('input', function() {
		pays_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, pays_check);
		form_complete();
	});

	email_elem.addEventListener('input', function() {
		email_check = this.value.length == 0 || /^.*@.*\./.test(this.value);

		_invalid_display(this, email_check);
		form_complete();
	});

	tel_fixe_elem.addEventListener('input', function() {
		tel_fixe_check = this.value.length == 0 || /^((\+?)(\s)?([0-9](\s)?)+)$/.test(this.value);

		_invalid_display(this, tel_fixe_check);
		form_complete();
	});

	tel_portable_elem.addEventListener('input', function() {
		tel_portable_check = this.value.length == 0 || /^((\+?)(\s)?([0-9](\s)?)+)$/.test(this.value);

		_invalid_display(this, tel_portable_check);
		form_complete();
	});

	//------ Events ------//
});

// Formulaire de modification des informations personnelles d'un sociétaire
var form_modifierProfilInfos_elem = document.getElementsByName('form-modifierProfilInfos');
form_modifierProfilInfos_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var statut_elem			= form.querySelector('select[name="statut"]');
	var porte_fort_elem		= form.querySelector('input[name="porte-fort"]');
	var civilite_elem		= form.querySelector('select[name="civilite"]');
	var nom_elem			= form.querySelector('input[name="nom"]');
	var nom_usuel_elem		= form.querySelector('input[name="nom-usuel"]');
	var prenom_elem			= form.querySelector('input[name="prenom"]');
	var autres_prenoms_elem	= form.querySelector('input[name="autres-prenoms"]');
	var representants_elem	= form.querySelector('input[name="representants"]');
	var lieu_naissance_elem	= form.querySelector('input[name="lieu"]');
	var date_naissance_elem	= form.querySelector('input[name="date"]');
	var siret_elem			= form.querySelector('input[name="siret"]');
	var nationalite_elem	= form.querySelector('input[name="nationalite"]');

	var nom_check				= true;
	var nom_usuel_check			= true;
	var prenom_check			= true;
	var autres_prenoms_check	= true;
	var representants_check		= true;
	var lieu_naissance_check	= true;
	var date_naissance_check	= true;
	var siret_check				= true;
	var nationalite_check		= true;
	//------ Elements ------//

	//------ Init ------//
	form_display();

	if (form.classList.contains('disabled')) {
		statut_elem.disabled=true;
		porte_fort_elem.disabled = true;
		civilite_elem.disabled = true;
		nom_elem.disabled=true;
		nom_usuel_elem.disabled=true;
		prenom_elem.disabled=true;
		autres_prenoms_elem.disabled=true;
		representants_elem.disabled=true;
		lieu_naissance_elem.disabled=true;
		date_naissance_elem.disabled=true;
		siret_elem.disabled=true;
		nationalite_elem.disabled = true;
		button_elem.disabled = true;
		button_elem.hidden = true;
	}
	//------ Init ------//

	//------ Functions ------//
	function form_display() {
		var test = statut_elem.value == 0;
		
		porte_fort_elem.disabled					= !test;
		porte_fort_elem.parentElement.hidden		= !test;
		representants_elem.disabled					= !test;
		representants_elem.parentElement.hidden		= !test;
		lieu_naissance_elem.disabled				= !test;
		lieu_naissance_elem.parentElement.hidden	= !test;
		siret_elem.disabled							= test;
		siret_elem.parentElement.hidden				= test;

		if (test) {
			nom_elem.parentElement.querySelector('label').innerHTML				= 'Nom <span class="text-danger">*</span>';
			prenom_elem.parentElement.querySelector('label').innerHTML			= 'Prénom <span class="text-danger">*</span>';
			date_naissance_elem.parentElement.querySelector('label').innerHTML	= 'Date de naissance <span class="text-danger">*</span>';
		} else {
			nom_elem.parentElement.querySelector('label').innerHTML				= 'Nom du représentant légal <span class="text-danger">*</span>';
			prenom_elem.parentElement.querySelector('label').innerHTML			= 'Prénom du représentant légal <span class="text-danger">*</span>';
			date_naissance_elem.parentElement.querySelector('label').innerHTML	= 'Date de création <span class="text-danger">*</span>';
		}

		form_complete();
	}

	function form_complete() {
		var test =	nom_check && nom_usuel_check && prenom_check && autres_prenoms_check && date_naissance_check && nationalite_check;

		if (statut_elem.value == 0) {
			test = test && representants_check;
			test = test && lieu_naissance_check;
		} else {
			test = test && siret_check;
		}

		button_elem.disabled = !test;
	}
	//------ Functions ------//

	//------ Events ------//
	statut_elem.addEventListener('input', function() {
		form_display();
		form_complete();
	});

	nom_elem.addEventListener('input', function() {
		nom_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])+$/.test(this.value);
		this.value = this.value.toUpperCase();

		_invalid_display(this, nom_check);
		form_complete();
	});

	nom_usuel_elem.addEventListener('input', function() {
		nom_usuel_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])*$/.test(this.value);
		this.value = this.value.toUpperCase();

		_invalid_display(this, nom_usuel_check);
		form_complete();
	});

	prenom_elem.addEventListener('input', function() {
		prenom_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, prenom_check);
		form_complete();
	});

	autres_prenoms_elem.addEventListener('input', function() {
		autres_prenoms_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])*$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, autres_prenoms_check);
		form_complete();
	});

	representants_elem.addEventListener('input', function() {
		representants_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])*$/.test(this.value);

		_invalid_display(this, representants_check);
		form_complete();
	});

	lieu_naissance_elem.addEventListener('input', function() {
		lieu_naissance_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, lieu_naissance_check);
		form_complete();
	});

	date_naissance_elem.addEventListener('input', function() {
		date_naissance_check = this.value.length > 0;

		_invalid_display(this, date_naissance_check);
		form_complete();
	});

	siret_elem.addEventListener('input', function() {
		siret_check = /^[0-9]{0,14}$/.test(this.value);

		_invalid_display(this, siret_check);
		form_complete();
	});
	//------ Events ------//
});

// Formulaire de modification des informations contact d'un sociétaire
var form_modifierProfilContact_elem = document.getElementsByName('form-modifierProfilContact');
form_modifierProfilContact_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');
	
	var adresse_elem		= form.querySelector('input[name="adresse"]');
	var code_postal_elem	= form.querySelector('input[name="code"]');
	var ville_elem			= form.querySelector('input[name="ville"]');
	var pays_elem			= form.querySelector('input[name="pays"]');
	var email_elem			= form.querySelector('input[name="email"]');
	var tel_fixe_elem		= form.querySelector('input[name="fixe"]');
	var tel_portable_elem	= form.querySelector('input[name="portable"]');

	var adresse_check			= adresse_elem.value.length > 0;
	var code_postal_check		= code_postal_elem.value.length > 0;
	var ville_check				= ville_elem.value.length > 0;
	var pays_check				= pays_elem.value.length > 0;
	var email_check				= true;
	var tel_fixe_check			= true;
	var tel_portable_check		= true;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		var test = adresse_check && code_postal_check && ville_check && pays_check && email_check && tel_fixe_check && tel_portable_check;
		button_elem.disabled = !test;
	}
	//------ Functions ------//

	//------ Events ------//
	adresse_elem.addEventListener('input', function() {
		adresse_check = /^([A-Za-z]|[0-9]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);

		_invalid_display(this, adresse_check);
		form_complete();
	});

	code_postal_elem.addEventListener('input', function() {
		code_postal_check = /^([A-Za-z]|[0-9])+$/.test(this.value);

		_invalid_display(this, code_postal_check);
		form_complete();
	});

	ville_elem.addEventListener('input', function() {
		ville_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, ville_check);
		form_complete();
	});

	pays_elem.addEventListener('input', function() {
		pays_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, pays_check);
		form_complete();
	});

	email_elem.addEventListener('input', function() {
		email_check = this.value.length == 0 || /^.*@.*\./.test(this.value);

		_invalid_display(this, email_check);
		form_complete();
	});

	tel_fixe_elem.addEventListener('input', function() {
		tel_fixe_check = this.value.length == 0 || /^((\+?)(\s)?([0-9](\s)?)+)$/.test(this.value);

		_invalid_display(this, tel_fixe_check);
		form_complete();
	});

	tel_portable_elem.addEventListener('input', function() {
		tel_portable_check = this.value.length == 0 || /^((\+?)(\s)?([0-9](\s)?)+)$/.test(this.value);

		_invalid_display(this, tel_portable_check);
		form_complete();
	});
	//------ Events ------//
});

// Formulaire de modification des informations sci d'un sociétaire
var form_modifierProfilSci_elem = document.getElementsByName('form-modifierProfilSci');
form_modifierProfilSci_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');
	
	var entree_elem			= form.querySelector('select[name="entree"]');
	var sortie_elem			= form.querySelector('select[name="sortie"]');
	var regime				= form.querySelector('select[name="regime"]');
	//------ Elements ------//

	//------ Init ------//
	sortie_elem.disabled = true;

	if (form.classList.contains('disabled')) {
		entree_elem.disabled = true;
		regime.disabled = true;
		button_elem.disabled = true;
		button_elem.hidden = true;
	}
	//------ Init ------//

	//------ Functions ------//
	//------ Functions ------//

	//------ Events ------//
	//------ Events ------//
});

// Formulaire pour modifier des données
var form_modifierDonnees_elem = document.getElementsByName('form-modifierDonnees');
form_modifierDonnees_elem.forEach(function(form) {
	//------ Elements ------//
	var input_elem = form.querySelector('.form-control');

	var modifier_elem	= form.querySelector('span[name="modifier"]');
	var supprimer_elem	= form.querySelector('a[name="supprimer"]');
	var valider_elem	= form.querySelector('span[name="valider"]');
	var annuler_elem	= form.querySelector('span[name="annuler"]');

	var input_check = true;
	//------ Elements ------//

	//------ Init ------//
	input_elem.disabled = true;
	valider_elem.hidden = true;
	annuler_elem.hidden = true;
	//------ Init ------//

	//------ Functions ------//
	//------ Functions ------//

	//------ Events ------//
	input_elem.addEventListener('input', function() {
		var name = input_elem.getAttribute('name');

		if (name == 'statut' || name == 'regime') {
			input_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		} else if (name == 'date') {
			input_check = this.value.length > 0;
		}

		_invalid_display(this, input_check);
	});

	modifier_elem.addEventListener('click', function() {
		input_elem.dataset.value = input_elem.value;
		input_elem.disabled = false;

		modifier_elem.hidden = true;
		supprimer_elem.hidden = true;
		valider_elem.hidden = false;
		annuler_elem.hidden = false;
	});

	valider_elem.addEventListener('click', function() {
		if (input_check) {
			form.submit();
		}
	});

	annuler_elem.addEventListener('click', function() {
		input_elem.value = input_elem.dataset.value;
		input_elem.disabled = true;
		input_check = true;
		_invalid_display(input_elem, input_check);

		modifier_elem.hidden = false;
		supprimer_elem.hidden = false;
		valider_elem.hidden = true;
		annuler_elem.hidden = true;
	});
	//------ Events ------//
});

// Formulaire pour ajouter des données
var form_ajouterDonnees_elem = document.getElementsByName('form-ajouterDonnees');
form_ajouterDonnees_elem.forEach(function(form) {
	//------ Elements ------//
	var group_elem = form.querySelector('.form-group');

	var input_elem = form.querySelector('.form-control');

	var plus_elem		= form.querySelector('span[name="plus"]');
	var valider_elem	= form.querySelector('span[name="valider"]');
	var annuler_elem	= form.querySelector('span[name="annuler"]');

	var input_check = false;
	//------ Elements ------//

	//------ Init ------//
	group_elem.hidden = true;
	input_elem.disabled = true;
	//------ Init ------//

	//------ Functions ------//
	//------ Functions ------//

	//------ Events ------//
	input_elem.addEventListener('input', function() {
		var name = input_elem.getAttribute('name');

		if (name == 'statut' || name == 'regime') {
			input_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		} else if (name == 'date') {
			input_check = this.value.length > 0;
		}

		_invalid_display(this, input_check);
	});

	plus_elem.addEventListener('click', function() {
		group_elem.hidden = false;
		input_elem.disabled = false;
		plus_elem.hidden = true;
	});

	valider_elem.addEventListener('click', function() {
		if (input_check) {
			form.submit();
		}
	});

	annuler_elem.addEventListener('click', function() {
		group_elem.hidden = true;
		plus_elem.hidden = false;
		input_elem.disabled = true;
		input_elem.value = '';
		input_check = false;
		_invalid_display(input_elem, true);
	});
	//------ Events ------//
});

// Formulaire pour ajouter / modifier un mouvement
var form_mouvement_elem = document.getElementsByName('form-mouvement');
form_mouvement_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');
	var paiements_nb;
	var parts;

	var ope_type_elem		= form.querySelector('select[name="ope-type"]');
	var titulaire_elem		= form.querySelector('select[name="titulaire"]');
	var beneficiaire_elem	= form.querySelector('select[name="beneficiaire"]');
	var nb_parts_elem		= form.querySelector('input[name="nb-parts"]');
	var parts_elem			= form.querySelector('input[name="parts"]');
	var prix_elem			= form.querySelector('input[name="prix"]');
	var paiements_elem		= form.querySelector('.paiements');
	var paiements_nb_elem	= form.querySelector('input[name="paiements-nb"]');
	var pai_type_elem		= form.querySelector('select[name="pai-type-1"]');
	var pai_num_elem		= form.querySelector('input[name="pai-num-1"');
	var plus_elem			= form.querySelector('span[name="plus"]');

	var titulaire_check		= titulaire_elem.value.length > 0;
	var beneficiaire_check	= beneficiaire_elem.value.length > 0;
	var parts_check			= parts_elem.value.length > 0;
	var prix_check			= prix_elem.value.length > 0;
	var pai_type_check		= pai_type_elem.value.length > 0;
	var pai_num_check		= pai_num_elem.value.length > 0;

	//------ Elements ------//

	//------ Init ------//
	parts = '';
	if (parts_elem.value.length > 0) {
		parts_update();
	} else {
		nb_parts_elem.value = 0;
	}

	paiements_nb = paiements_elem.querySelectorAll('.paiement').length;
	paiements_nb_elem.value = paiements_nb;
	if (paiements_nb > 1) {
		form.querySelectorAll('span[name="supprimer"').forEach(span => 
			span.addEventListener('click', function() {
				paiement_supprimer(span.parentElement.parentElement);
			})
		);
	}

	form_display();
	form_complete();
	nb_parts_elem.disabled = true;
	//------ Init ------//

	//------ Functions ------//
	function paiements_ajouter() {
		if (paiements_nb > 1) {
			paiements_elem.querySelectorAll('.paiement')[paiements_nb - 1].querySelector('span[name="supprimer"]').hidden = true;
		}

		paiements_nb++;

		var paiement_elem = paiements_elem.querySelector('.paiement').cloneNode(true);
		var nom = paiement_elem.querySelector('label');
		nom.innerHTML = 'Paiement n°' + paiements_nb + ' <span class="button" name="supprimer"><i class="fas fa-trash"></i></span>';
		nom.addEventListener('click', function() {paiement_supprimer(paiement_elem)});

		var type_label = paiement_elem.querySelector('label[for="pai-type-1"]');
		type_label.setAttribute('for', 'pai-type-' + paiements_nb);

		var type_input = paiement_elem.querySelector('select[name="pai-type-1"]');
		type_input.setAttribute('id', 'pai-type-' + paiements_nb);
		type_input.setAttribute('name', 'pai-type-' + paiements_nb);

		var num_label = paiement_elem.querySelector('label[for="pai-num-1"]');
		num_label.setAttribute('for', 'pai-num-' + paiements_nb);

		var num_input = paiement_elem.querySelector('input[name="pai-num-1"]');
		num_input.value = '';
		num_input.setAttribute('id', 'pai-num-' + paiements_nb);
		num_input.setAttribute('name', 'pai-num-' + paiements_nb);

		type_input.addEventListener('input', function() {
			pai_type_check = this.value.length > 0;
	
			_invalid_display(this, pai_type_check);
			form_complete();
		});
	
		num_input.addEventListener('input', function() {
			pai_num_check = this.value.length > 0;
	
			_invalid_display(this, pai_num_check);
			form_complete();
		});

		paiements_elem.appendChild(paiement_elem);
		paiements_nb_elem.value = paiements_nb;
	}

	function paiement_supprimer(elem) {
		paiements_nb--;

		if (paiements_nb > 1) {
			paiements_elem.querySelectorAll('.paiement')[paiements_nb - 1].querySelector('span[name="supprimer"]').hidden = false;
		}

		paiements_elem.removeChild(elem);
		paiements_nb_elem.value = paiements_nb;
	}

	function parts_update() {
		parts = '';
		var tmp = parts_elem.value.split(',');
		tmp.forEach(function(p) {
			if (/-/.test(p)) {
				var tmp2	= p.split('-');
				var start	= parseInt(tmp2[0]);
				var end		= parseInt(tmp2[1]);
				for(i = start; i <= end; i++) {
					parts = parts + i + ',';
				}
			} else {
				parts = parts + p + ',';
			}
		});

		nb_parts_elem.value = parts.split(',').length - 1;
	}

	function form_display() {
		var test = ope_type_elem.value == 0;
		titulaire_elem.disabled				= test;
		titulaire_elem.parentElement.hidden	= test;
	}

	function form_complete() {
		var test = beneficiaire_check && parts_check && prix_check && pai_type_check && pai_num_check;
		if (ope_type_elem.value != 0) {
			test = test && titulaire_check;
		}

		button_elem.disabled = !test;
	}
	//------ Functions ------//

	//------ Events ------//
	ope_type_elem.addEventListener('input', function() {
		form_display();
		form_complete();
	});

	titulaire_elem.addEventListener('input', function() {
		titulaire_check = this.value.length > 0;

		_invalid_display(this, titulaire_check);
		form_complete();
	});

	beneficiaire_elem.addEventListener('input', function() {
		beneficiaire_check = this.value.length > 0;

		_invalid_display(this, beneficiaire_check);
		form_complete();
	});

	parts_elem.addEventListener('input', function() {
		parts_check = /^[0-9]+([-,][0-9]+)*$/.test(this.value);

		if (parts_check) {
			parts_update();
		}

		_invalid_display(this, parts_check);
		form_complete();
	});

	prix_elem.addEventListener('input', function() {
		prix_check = /^[0-9]+([,\.][0-9]?[0-9]?)?$/.test(this.value);

		_invalid_display(this, prix_check);
		form_complete();
	});

	pai_type_elem.addEventListener('input', function() {
		pai_type_check = this.value.length > 0;

		_invalid_display(this, pai_type_check);
		form_complete();
	});

	pai_num_elem.addEventListener('input', function() {
		pai_num_check = this.value.length > 0;

		_invalid_display(this, pai_num_check);
		form_complete();
	});

	plus_elem.addEventListener('click', function() {paiements_ajouter()});

	form.addEventListener('submit', function() {
		prix_elem.value = prix_elem.value.replace(',', '.');
		parts_elem.value = parts.slice(0,-1);
	});
	//------ Events ------//
});

// Formulaire pour importer
var form_importer_elem = document.getElementsByName('form-importer');
form_importer_elem.forEach(function(form) {
	//------ Elements ------//
	var input_elem = form.querySelector('input[name="import"]');

	var input_check = false;
	//------ Elements ------//

	//------ Events ------//
	input_elem.addEventListener('input', function() {
		var format = this.value.split('.').pop().toLowerCase();
		input_check = format == 'ods' || format == 'xlsx';

		if (input_check) {
			form.submit();
		}
	});
	//------ Events ------//
});

// Formulaire pour ajouter / modifier un sociétaire représenté
var form_SocietaireRepresente_elem = document.getElementsByName('form-SocietaireRepresente');
form_SocietaireRepresente_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var nom_elem			= form.querySelector('input[name="nom"]');
	var prenom_elem			= form.querySelector('input[name="prenom"]');
	var lieu_naissance_elem	= form.querySelector('input[name="lieu"]');
	var date_naissance_elem	= form.querySelector('input[name="date"]');
	var adresse_elem		= form.querySelector('input[name="adresse"]');
	var code_postal_elem	= form.querySelector('input[name="code"]');	
	var ville_elem			= form.querySelector('input[name="ville"]');
	var pays_elem			= form.querySelector('input[name="pays"]');

	var nom_check				= nom_elem.value.length != 0;
	var prenom_check			= prenom_elem.value.length != 0;
	var lieu_naissance_check	= lieu_naissance_elem.value.length != 0;
	var date_naissance_check	= date_naissance_elem.value.length != 0;
	var adresse_check			= adresse_elem.value.length != 0;
	var code_postal_check		= code_postal_elem.value.length != 0;
	var ville_check				= ville_elem.value.length != 0;
	var pays_check				= pays_elem.value.length != 0;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(nom_check && prenom_check && lieu_naissance_check && date_naissance_check && adresse_check && code_postal_check && ville_check && pays_check);
	}
	//------ Functions ------//

	//------ Events ------//
	nom_elem.addEventListener('input', function() {
		nom_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])+$/.test(this.value);
		this.value = this.value.toUpperCase();

		_invalid_display(this, nom_check);
		form_complete();
	});

	prenom_elem.addEventListener('input', function() {
		prenom_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, prenom_check);
		form_complete();
	});

	lieu_naissance_elem.addEventListener('input', function() {
		lieu_naissance_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, lieu_naissance_check);
		form_complete();
	});

	date_naissance_elem.addEventListener('input', function() {
		date_naissance_check = this.value.length > 0;

		_invalid_display(this, date_naissance_check);
		form_complete();
	});

	adresse_elem.addEventListener('input', function() {
		adresse_check = /^([A-Za-z]|[0-9]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);

		_invalid_display(this, adresse_check);
		form_complete();
	});

	code_postal_elem.addEventListener('input', function() {
		code_postal_check = /^[0-9]+$/.test(this.value);

		_invalid_display(this, code_postal_check);
		form_complete();
	});

	ville_elem.addEventListener('input', function() {
		ville_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, ville_check);
		form_complete();
	});

	pays_elem.addEventListener('input', function() {
		pays_check = /^([A-Za-z]|[áàâãªäÁÀÂÃÄÍÌÎÏíìîïéèêëÉÈÊËóòôõºöÓÒÔÕÖúùûüÚÙÛÜçÇñÑÆŒæœÿ]|[-'\s,])+$/.test(this.value);
		this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);

		_invalid_display(this, pays_check);
		form_complete();
	});
	//------ Events ------//
});

// formulaire pour l'initialisation de la BDD
var form_initBDD_elem = document.getElementsByName('form-initialisationBDD');
form_initBDD_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var hostname_elem	= form.querySelector('input[name="hostname"]');
	var username_elem	= form.querySelector('input[name="username"]');
	var database_elem	= form.querySelector('input[name="database"]');

	form.querySelectorAll('input').forEach(function(input) {
		if (input.value == ' ') input.value = '';
	});

	var hostname_check	= hostname_elem.value.length > 0;
	var username_check	= username_elem.value.length > 0;
	var database_check	= database_elem.value.length > 0;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(hostname_check && username_check && database_check);
	}
	//------ Functions ------//

	//------ Events ------//
	hostname_elem.addEventListener('input', function() {
		hostname_check = this.value.length > 0;

		_invalid_display(this, hostname_check);
		form_complete();
	});

	username_elem.addEventListener('input', function() {
		username_check = this.value.length > 0;

		_invalid_display(this, username_check);
		form_complete();
	});

	database_elem.addEventListener('input', function() {
		database_check = this.value.length > 0;

		_invalid_display(this, database_check);
		form_complete();
	});
	//------ Events ------//
});

// formulaire pour l'initialisation de l'email
var form_initEmail_elem = document.getElementsByName('form-initialisationEmail');
form_initEmail_elem.forEach(function(form) {
	//------ Elements ------//
	var buttons_elem = form.querySelectorAll('button');

	var host_elem	= form.querySelector('input[name="host"]');
	var port_elem	= form.querySelector('input[name="port"]');
	var email_elem	= form.querySelector('input[name="email"]');
	var pass_elem	= form.querySelector('input[name="password"]');

	var host_check		= host_elem.value.length > 0;
	var port_check		= port_elem.value.length > 0;
	var email_check		= email_elem.value.length > 0;
	var pass_check		= pass_elem.value.length > 0;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		var test = host_check && port_check && email_check && pass_check;
		buttons_elem.forEach(function(button) {
			button.disabled = !test;
		});
	}
	//------ Functions ------//

	//------ Events ------//
	host_elem.addEventListener('input', function() {
		host_check = this.value.length > 0;

		_invalid_display(this, host_check);
		form_complete();
	});

	port_elem.addEventListener('input', function() {
		port_check = this.value.length > 0;

		_invalid_display(this, port_check);
		form_complete();
	});

	email_elem.addEventListener('input', function() {
		email_check = this.value.length > 0;

		_invalid_display(this, email_check);
		form_complete();
	});

	pass_elem.addEventListener('input', function() {
		pass_check = this.value.length > 0;

		_invalid_display(this, pass_check);
		form_complete();
	});
	//------ Events ------//
});

// formulaire pour l'initialisation des informations de base
var form_initInfos_elem = document.getElementsByName('form-initialisationInfos');
form_initInfos_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');
	
	var login_elem	= form.querySelector('input[name="login"]');
	var email_elem	= form.querySelector('input[name="email"]');
	var mdp1_elem	= form.querySelector('input[name="mdp1"]');
	var mdp2_elem	= form.querySelector('input[name="mdp2"]');
	var nom_elem	= form.querySelector('input[name="nom"]');

	var mdp1_regles_elem = form.querySelector('.form-regles-5');
	var mdp2_regles_elem = form.querySelector('.form-regles-1');

	var login_check	= login_elem.value.length > 0;
	var email_check	= email_elem.value.length > 0;
	var mdp1_check	= mdp1_elem.value.length > 0;
	var mdp2_check	= mdp2_elem.value.length > 0;
	var nom_check	= nom_elem.value.length > 0;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(login_check && email_check && mdp1_check && mdp2_check && nom_check);
	}
	//------ Functions ------//

	//------ Events ------//
	login_elem.addEventListener('input', function() {
		login_check = this.value.length > 0;

		_invalid_display(this, login_check);
		form_complete();
	});

	email_elem.addEventListener('input', function() {
		email_check = this.value.length > 0;

		_invalid_display(this, email_check);
		form_complete();
	});

	mdp1_elem.addEventListener('focus', function() {
		mdp1_regles_elem.classList.add('focus');
	});

	mdp1_elem.addEventListener('blur', function() {
		if (this.value.length == 0) {
			mdp1_regles_elem.classList.remove('focus');
		}
	});

	mdp1_elem.addEventListener('input', function() {
		mdp1_check = _password_test(this.value);
		mdp2_check = _password_compare(this.value, mdp2_elem.value, mdp2_regles_elem);
		form_complete();
	});

	mdp2_elem.addEventListener('input', function() {
		mdp2_check = _password_compare(this.value, mdp1_elem.value, mdp2_regles_elem);
		form_complete();
	});

	nom_elem.addEventListener('input', function() {
		nom_check = this.value.length > 4 && this.value.length < 16;

		_invalid_display(this, nom_check);
		form_complete();
	});
	//------ Events ------//
});

// formulaire pour le mailing
var form_mail_elem = document.getElementsByName('form-mail');
form_mail_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('#send');

	var destinataire_elem	= form.querySelectorAll('input[name="destinataire"]');
	var email_elem			= form.querySelector('select[name="email"]');
	var objet_elem			= form.querySelector('input[name="objet"]');
	var message_elem		= form.querySelector('textarea[name="message"]');

	var destinataire_value	= 0;
	var email_check			= email_elem.value.length > 0;
	var objet_check			= objet_elem.value.length > 0;
	var message_check		= message_elem.value.length > 0; 
	//------ Elements ------//

	//------ Init ------//
	form_display();
	form_complete();
	document.getElementById('rmvFile').onclick = function() { 
		var file = document.getElementById("attach");
		file.value = file.defaultValue;
	}
	//------ Init ------//

	//------ Functions ------//
	function form_display() {
		var test = destinataire_value == 2;

		email_elem.disabled = !test;
	}
	function form_complete() {
		var test = objet_check && message_check;
		if (destinataire_value == 2) {
			test = test && email_check;
		}

		button_elem.disabled = !test;
	}
	//------ Functions ------//

	//------ Events ------//
	destinataire_elem.forEach(dest =>
		dest.addEventListener('click', function() {
			destinataire_value = this.value;

			form_display();
			form_complete();
		}));

	email_elem.addEventListener('input', function() {
		email_check = this.value.length > 0;

		form_complete();
	});

	objet_elem.addEventListener('input', function() {
		objet_check = this.value.length > 0;

		form_complete();
	});

	message_elem.addEventListener('input', function() {
		message_check = this.value.length > 0;

		_invalid_display(this, message_check);
		form_complete();
	});
	//------ Events ------//
});

// Formulaire pour modifier la durée RGPD
var form_dureeRGPD_elem = document.getElementsByName('form-modifierDureeRGPD');
form_dureeRGPD_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var duree_elem = form.querySelector('input[name="duree"]');

	var duree_check = true;
	//------ Elements ------//

	//------ Init ------//
	//------ Init ------//

	//------ Functions ------//
	//------ Functions ------//

	//------ Events ------//
	duree_elem.addEventListener('input', function() {
		duree_check = /^[0-9]+$/.test(this.value) && this.value !== '0';

		_invalid_display(this, duree_check);
		button_elem.disabled = !duree_check;
	})
	//------ Events ------//
});

// Formulaire pour signaler une erreur avec champs
var form_signaler_elem = document.getElementsByName('form-signaler');
form_signaler_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var champs_elem		= form.querySelectorAll('input[type="checkbox"]');
	var message_elem	= form.querySelector('textarea[name="message"]');

	var champs_check	= false;
	var message_check	= message_elem.value.length > 0;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !(champs_check && message_check);
	}
	//------ Functions ------//

	//------ Events ------//
	champs_elem.forEach(champ => champ.addEventListener('input', function() {
		champs_check = false;
		champs_elem.forEach(c => champs_check = champs_check || c.checked);

		form_complete();
	}));

	message_elem.addEventListener('input', function() {
		message_check = this.value.length > 0;

		_invalid_display(this, message_check);
		form_complete();
	})
	//------ Events ------//
});

// Formulaire pour signaler une erreur sans champs
var form_signaler_elem = document.getElementsByName('form-signalerS');
form_signaler_elem.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var message_elem	= form.querySelector('textarea[name="message"]');

	var message_check	= message_elem.value.length > 0;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !message_check;
	}
	//------ Functions ------//

	//------ Events ------//

	message_elem.addEventListener('input', function() {
		message_check = this.value.length > 0;

		_invalid_display(this, message_check);
		form_complete();
	})
	//------ Events ------//
});

// Formulaire pour envoi de mail à la cogérance
var form_contact_cogerance = document.getElementsByName('form-contact-cogerance');
form_contact_cogerance.forEach(function(form) {
	//------ Elements ------//
	var button_elem = form.querySelector('button');

	var message_elem	= form.querySelector('textarea[name="message"]');

	var message_check	= message_elem.value.length > 0;
	//------ Elements ------//

	//------ Init ------//
	form_complete();
	//------ Init ------//

	//------ Functions ------//
	function form_complete() {
		button_elem.disabled = !message_check;
	}
	//------ Functions ------//

	//------ Events ------//
	message_elem.addEventListener('input', function() {
		message_check = this.value.length > 0;

		_invalid_display(this, message_check);
		form_complete();
	})
	//------ Events ------//
});