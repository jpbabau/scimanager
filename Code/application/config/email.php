<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['protocol'] = "smtp";
$config['smtp_host'] = 'smtp.XX';
$config['smtp_port'] = '587';
$config['smtp_user'] = 'XX';
$config['smtp_pass'] = 'XX';
$config['smtp_crypto'] = 'tls';
$config['charset'] = "utf-8";
$config['mailtype'] = "html";
$config['newline'] = "\r\n";