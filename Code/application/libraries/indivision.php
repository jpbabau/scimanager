<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Classe Indivision
 */
class Indivision {
	
	/**
	 * CI CodeIgniter super-object
	 *
	 * @access protected
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->database();
	}
	
	/**
	 * Ajoute un sociétaire représenté par une indivision
	 *
	 * @param  string	$porteFort le login du sociétaire porte-fort
	 * @param  int		$indivision le numéro de l'indivison
	 * @param  string	$prenom le prénom du sociétaire représenté
	 * @param  string	$nom le nom du sociétaire représenté
	 * @param  int		$civilite la civilité du sociétaire représenté
	 * @param  string	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string	$adresse l'adresse du sociétaire représenté
	 * @param  string	$codePostal le code postal du sociétaire représenté
	 * @param  string	$ville la ville du sociétaire représenté
	 * @param  string	$pays le pays du sociétaire représenté
	 * @return int
	 * 		- 0 : le sociétaire représenté a été ajouté
	 * 		- 1 : l'un des paramètres obligatoires (porteFort, indivision, prenom, nom, dateNaissance, lieuNaissance, adresse, codePostal, ville, pays) est vide
	 * 		- 2 : l'un des paramètres est trop long (> 45)
	 */
	public function ajouterSocietaireRepresente(string $porteFort, int $indivision, string $prenom, string $nom, int $civilite, string $dateNaissance, string $lieuNaissance, string $adresse, string $codePostal, string $ville, string $pays)
	{
		if (isset($porteFort) && strlen($porteFort) > 0 && isset($indivision) && isset($prenom) && strlen($prenom) > 0 && isset($nom) && strlen($nom) > 0 && isset($civilite) && isset($dateNaissance) && strlen($dateNaissance) > 0 && isset($lieuNaissance) && strlen($lieuNaissance) && isset($adresse) && strlen($adresse) > 0 && isset($codePostal) && isset($ville) && strlen($ville) > 0 && isset($pays) && strlen($pays) > 0) {
			if (strlen($porteFort) <= 45 && strlen($prenom) <= 45 && strlen($nom) <= 45 && strlen($dateNaissance) <= 45 && strlen($lieuNaissance) <= 45 && strlen($adresse) <= 45 && strlen($ville) <= 45 && strlen($pays) <= 45) {
				$this->_ajouterSocietaireRepresenteBdd($porteFort, $indivision, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
				return 0;
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}
	
	/**
	 * Modifie un sociétaire représenté
	 *
	 * @param  int		$id l'id du sociétaire représenté à modifier
	 * @param  string	$prenom le prénom du sociétaire représenté
	 * @param  string	$nom le nom du sociétaire représenté
	 * @param  int		$civilite la civilité du sociétaire représenté
	 * @param  string	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string	$adresse l'adresse du sociétaire représenté
	 * @param  string	$codePostal le code postal du sociétaire représenté
	 * @param  string	$ville la ville du sociétaire représenté
	 * @param  string	$pays le pays du sociétaire représenté
	 * @return int
	 * 		- 0 : le sociétaire représenté a été modifié
	 * 		- 1 : l'un des paramètres obligatoires (id, prenom, nom, civilite, dateNaissance, lieuNaissance, adresse, codePostal, ville, pays) est vide
	 * 		- 2 : l'un des paramètres est trop long (>45)
	 * 		- 3 : l'id n'existe pas
	 */
	public function modifierSocietaireRepresente(int $id, string $prenom, string $nom, int $civilite, string $dateNaissance, string $lieuNaissance, string $adresse, string $codePostal, string $ville, string $pays)
	{
		if (isset($id) && isset($prenom) && strlen($prenom) > 0 && isset($nom) && strlen($nom) > 0 && isset($civilite) && isset($dateNaissance) && strlen($dateNaissance) > 0 && isset($lieuNaissance) && strlen($lieuNaissance) && isset($adresse) && strlen($adresse) > 0 && isset($codePostal) && isset($ville) && strlen($ville) > 0 && isset($pays) && strlen($pays) > 0) {
			if (strlen($prenom) <= 45 && strlen($nom) <= 45 && strlen($dateNaissance) <= 45 && strlen($lieuNaissance) <= 45 && strlen($adresse) <= 45 && strlen($ville) <= 45 && strlen($pays) <= 45) {
				$result = $this->_obtenirInfosSocietaireRepresenteBdd($id);
				if (isset($result)) {
					$this->_modifierSocietaireRepresenteBdd($id, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
					return 0;
				} else {
					return 3;
				}
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}
	
	/**
	 * Supprime un sociétaire représenté
	 *
	 * @param  int $id l'id du sociétaire représenté à supprimer
	 * @return int
	 * 		- 0 : le sociétaire représenté a été supprimé
	 * 		- 1 : l'id n'existe pas
	 */
	public function supprimerSocietaireRepresente(int $id)
	{
		$result = $this->_obtenirInfosSocietaireRepresenteBdd($id);
		if (isset($result)) {
			$this->_supprimerSocietaireRepresenteBdd($id);
			return 0;
		} else {
			return 1;
		}
	}
	
	/**
	 * Récupère un sociétaire représenté
	 *
	 * @param  int $id l'id du sociétaire représenter à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations du sociétaire représenté
	 * 
	 * 		$infos = [<br>
	 * 			'socr_id'				=> id,<br>
	 * 			'socr_prenom'			=> prenom,<br>
	 * 			'socr_nom'				=> nom,<br>
	 * 			'socr_civilite'			=> civilite,<br>
	 * 			'socr_dateNaissance'	=> dateNaissance,<br>
	 * 			'socr_lieuNaissance'	=> lieuNaissance,<br>
	 * 			'socr_adresse'			=> adresse,<br>
	 * 			'socr_codePostal'		=> codePostal,<br>
	 * 			'socr_ville'			=> ville,<br>
	 * 			'socr_pays'				=> pays,<br>
	 * 			'soc_login'				=> porteFort,<br>
	 * 			'ind_num'				=> indivision<br>
	 * 		]
	 */
	public function obtenirInformationsSocietaireRepresente(int $id)
	{
		$infos = $this->_obtenirInfosSocietaireRepresenteBdd($id);
		return $infos;
	}
	
	/**
	 * Récupère tous les sociétaires représentés par un porte-fort
	 *
	 * @param string $porteFort le login du porte-fort
	 * @return array<int,array<string,string>> $infos un tableau qui contient les informations de tous les sociétaires représentés
	 * 
	 * 		$infos = [<br>
	 * 			[0] => [
	 * 				'socr_id'				=> id,<br>
	 * 				'socr_prenom'			=> prenom,<br>
	 * 				'socr_nom'				=> nom,<br>
	 * 				'socr_civilite'			=> civilite,<br>
	 * 				'socr_dateNaissance'	=> dateNaissance,<br>
	 * 				'socr_lieuNaissance'	=> lieuNaissance,<br>
	 * 				'socr_adresse'			=> adresse,<br>
	 * 				'socr_codePostal'		=> codePostal,<br>
	 * 				'socr_ville'			=> ville,<br>
	 * 				'socr_pays'				=> pays,<br>
	 * 				'soc_login'				=> porteFort,<br>
	 * 				'ind_num'				=> indivision<br>
	 * 			],<br>
	 * 			[1] => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function obtenirListeSocietaireRepresente(string $porteFort)
	{
		$infos = $this->_obtenirListeSocietaireRepresenteBdd($porteFort);
		return $infos;
	}

	public function obtenirListeIndivisions(string $porteFort)
	{
		$infos = $this->_obtenirListeIndivisions($porteFort);
		return $infos;
	}
	
	/**
	 * Ajoute un sociétaire représenté dans la table sociétaire représenté, ainsi que dans la table indivison
	 *
	 * @access private
	 * @param  string	$porteFort le login du sociétaire porte-fort
	 * @param  int		$indivision le numéro de l'indivison
	 * @param  string	$prenom le prénom du sociétaire représenté
	 * @param  string	$nom le nom du sociétaire représenté
	 * @param  int		$civilite la civilité du sociétaire représenté
	 * @param  string	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string	$adresse l'adresse du sociétaire représenté
	 * @param  string	$codePostal le code postal du sociétaire représenté
	 * @param  string	$ville la ville du sociétaire représenté
	 * @param  string	$pays le pays du sociétaire représenté
	 * @return void
	 */
	private function _ajouterSocietaireRepresenteBdd(string $porteFort, int $indivision, string $prenom, string $nom, int $civilite, string $dateNaissance, string $lieuNaissance, string $adresse, string $codePostal, string $ville, string $pays)
	{
		$req1 = '	INSERT INTO t_societaireRepresente_socr
					VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?);';

		$req2 = '	SET @socr_id := (SELECT MAX(socr_id) from t_societaireRepresente_socr);';

		$req3 = '	INSERT INTO t_indivision_ind
					VALUES (@socr_id, ?, ?);';

		$this->CI->db->query($req1, array($prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays));
		$this->CI->db->query($req2);
		$this->CI->db->query($req3, array($porteFort, $indivision));
	}
	
	/**
	 * Modifie un sociétaire représenté dans la table sociétaire représenté
	 *
	 * @access private
	 * @param  int		$id l'id du sociétaire représenté à modifier
	 * @param  string	$prenom le prénom du sociétaire représenté
	 * @param  string	$nom le nom du sociétaire représenté
	 * @param  int		$civilite la civilité du sociétaire représenté
	 * @param  string	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string	$adresse l'adresse du sociétaire représenté
	 * @param  string		$codePostal le code postal du sociétaire représenté
	 * @param  string	$ville la ville du sociétaire représenté
	 * @param  string	$pays le pays du sociétaire représenté
	 * @return void
	 */
	private function _modifierSocietaireRepresenteBdd(int $id, string $prenom, string $nom, int $civilite, string $dateNaissance, string $lieuNaissance, string $adresse, string $codePostal, string $ville, string $pays)
	{
		$req = 'UPDATE t_societaireRepresente_socr
				SET socr_prenom = ?,
				socr_nom = ?,
				socr_civilite = ?,
				socr_dateNaissance = ?,
				socr_lieuNaissance = ?,
				socr_adresse = ?,
				socr_codePostal = ?,
				socr_ville = ?,
				socr_pays = ?
				WHERE socr_id = ?;';

		$this->CI->db->query($req, array($prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays, $id));
	}
	
	/**
	 * Supprime un sociétaire représenté de la table sociétaire représenté
	 *
	 * @access private
	 * @param  int $id l'id du sociétaire représenté à supprimer
	 * @return void
	 */
	private function _supprimerSocietaireRepresenteBdd(int $id)
	{
		$req1 = '	DELETE FROM t_indivision_ind
					WHERE socr_id = ?;';
		
		$req2 = '	DELETE FROM t_societaireRepresente_socr
					WHERE socr_id = ?;';

		$this->CI->db->query($req1, array($id));
		$this->CI->db->query($req2, array($id));
	}
	
	/**
	 * Récupère les informations d'un sociétaire représenté
	 *
	 * @access private
	 * @param  int $id l'id du sociétaire représenter à récupèrer
	 * @return array<string,string>
	 */
	private function _obtenirInfosSocietaireRepresenteBdd(int $id)
	{
		$req = 'SELECT * FROM t_societaireRepresente_socr
				JOIN t_indivision_ind USING(socr_id)
				WHERE socr_id = ?;';

		$result = $this->CI->db->query($req, array($id))->row_array();
		return $result;
	}
	
	/**
	 * Récupère les informations de tous les sociétaires représentés par un porte-fort
	 *
	 * @access private
	 * @param string $porteFort le login du porte-fort
	 * @return array<int,array<string,string>>
	 */
	private function _obtenirListeSocietaireRepresenteBdd(string $porteFort)
	{
		$req = 'SELECT * FROM t_societaireRepresente_socr
				JOIN t_indivision_ind USING(socr_id)
				WHERE soc_id = ?
				ORDER BY ind_num;';

		$result = $this->CI->db->query($req, array($porteFort))->result_array();
		return $result;
	}

	private function _obtenirListeIndivisions(string $porteFort)
	{
		$req = 'SELECT ind_num FROM t_indivision_ind
				WHERE soc_id = ?
				ORDER BY ind_num;';

		$result = $this->CI->db->query($req, array($porteFort))->result_array();
		return $result;
	}
}
?>