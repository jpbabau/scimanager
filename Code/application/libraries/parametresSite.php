<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'libraries/administrateur.php';
require_once APPPATH . 'libraries/site.php';

/**
 * Classe Parametres Site
 */
class ParametresSite {
    
    /**
     * L'Administrateur
     *
     * @access protected
     * @var Administrateur
     */
    protected $administrateur;
        
    /**
     * Le Site
     *
     * @access protected
     * @var Site
     */
    protected $site;
    
    /**
     * Le constructeur de la classe
     *
     * @return void
     */
    public function __construct() {
        $this->administrateur = new Administrateur();
        $this->site = new Site();
    }

    /**
     * Modifie le login de l'administrateur
     *
     * @param  string $nouveauLogin Le nouveau login de l'administrateur
     * @return int
     *      - 0 : l'ancien login est remplacé par le nouveau login
     *      - 1 : le nouveau login est vide, l’ancien login est conservé
     *      - 2 : le nouveau login est trop long (>45), l’ancien login est conservé
     *      - 3 : le nouveau login est déjà utilisé par un utilisateur, l'ancien login est conservé
     */
    public function adminModifierLogin(string $nouveauLogin) {
        return $this->administrateur->modifierLogin($nouveauLogin);
    }

    /**
     * Modifie l'email de l'administrateur
     *
     * @param  string $nouvelEmail Le nouvel email de l'administrateur
     * @return int
     *      - 0 : l'ancien email est remplacé par le nouvel email
     *      - 1 : le nouvel email est vide, l'ancien email est conservé
     *      - 2 : le nouvel email est trop long (>45), l'ancien email est conservé
     *      - 3 : le format du nouvel email est incorrect, l'ancien email est conservé
     *      - 4 : le nouvel email est déjà utilisé par un utilisateur, l'ancien email est conservé
     */
    public function adminModifierEmail(string $nouvelEmail) {
        return $this->administrateur->modifierEmail($nouvelEmail);
    }

    /**
     * Modifie le mot de passe de l'administrateur
     *
     * @param  string $ancienMdp L'ancien mot de passe de l'administrateur
     * @param  string $nouveauMdp Le nouveau mot de passe de l'administrateur
     * @return int
     *      - 0 : l'ancien mot de passe est remplacé par le nouveau mot de passe
     *      - 1 : le nouveau ou l'ancien mot de passe est vide, l'ancien mot de passe est conservé
     *      - 2 : le nouveau ou l'ancien mot de passe est trop long, l'ancien mot de passe est conservé
     *      - 3 : l'ancien mot de passe est incorrect, l'ancien mot de passe est conservé
     *      - 4 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
     */
    public function adminModifierMdp(string $ancienMdp, string $nouveauMdp) {
        return $this->administrateur->modifierMdp($ancienMdp, $nouveauMdp);
    }

    /**
     * Récupére les informations de l'administrateur
     *
     * @return array<string,string> $info tableau qui contient l'email et le login de l'administrateur, retourne null si le résultat est vide
     */
    public function adminObtenirInformations() {
        return $this->administrateur->obtenirInformations();
    }

	/**
	 * Modifie le type de structure
	 *
	 * @param  int $structure le nouveau type de structure
	 * @return int
	 * 		- 0 : le type de structure a été modifié
	 * 		- 1 : le nouveau type de structure n'est pas valide, l'ancien type est conservé
	 * 		- 2 : le nouveau type de strucutre est vide, l'ancien type est conservé
	 */
	public function siteModifierStructure(int $structure) {
		return $this->site->modifierStructure($structure);
	}

    /**
     * Modifie le nom du site
     *
     * @param  string $nouveauNom Le nouveau nom du site
     * @return int
     *      - 0 : l'ancien nom est remplacé par le nouveau nom
     *      - 1 : le nouveau nom est vide, l’ancien nom est conservé
     *      - 2 : le nouveau nom est trop long (>15), ou trop court (<5). l’ancien nom est conservé
     */
    public function siteModifierNom(string $nouveauNom) {
        return $this->site->modifierNom($nouveauNom);
    }

    /**
     * Modifie l'email du site
     *
     * @param  string $email nouvel email du site
     * @return int 
     *      - 0 : l'ancien email est remplacé par le nouvel email
     *      - 1 : le nouvel email est vide, l'ancien email est conservé
     *      - 2 : le nouvel email est trop long (>45), l'ancien email est conservé
     *      - 3 : le format du nouvel email est incorrect, l'ancien email est conservé
     */
    public function siteModifierEmail(string $nouvelEmail) {
        return $this->site->modifierEmail($nouvelEmail);
    }

    /**
     * Modifie l'image du site
     *
     * @param  string $nouvelleImage La nouvelle image du site
     * @return int
     *      - 0 : le nom de l'ancienne image est remplacé par le nom de la nouvelle image
     *      - 1 : le nom de la nouvelle image est vide, le nom de l'ancienne image est conservé
     *      - 2 : le nom de la nouvelle image est trop longue (>45), le nom de l'ancienne image est conservé
     *      - 3 : le format de la nouvelle image n'est pas correct (jpg), le nom de l'ancienne image est conservé
     */
    public function siteModifierImage(string $nouvelleImage) {
        return $this->site->modifierImage($nouvelleImage);
    }

    /**
     * Modifie l'icône du site
     *
     * @param  string $nouvelleIcone La nouvelle icône du site
     * @return int
     *      - 0 : le nom de l'ancienne icone est remplacé par le nom de la nouvelle icone
     *      - 1 : le nom de la nouvelle icone est vide, le nom de l'ancienne icone est conservé
     *      - 2 : le nom de la nouvelle icone est trop longue (>45), le nom de l'ancienne icone est conservé
     *      - 3 : le format de la nouvelle icone n'est pas correct (jpg), le nom de l'ancienne icone est conservé
     */
    public function siteModifierIcone(string $nouvelleIcone) {
        return $this->site->modifierIcone($nouvelleIcone);
    }

	/**
	 * Modifie la durée RGPD
	 *
	 * @param  int $dureeRGPD la nouvelle durée RGPD en mois
	 * @return int
	 * 		- 0 : la durée RGPD a été modifiée
	 * 		- 1 : la nouvelle durée est soit vide soit négative, la durée RGPD n'est pas modifiée
	 */
	public function siteModifierDureeRGPD(int $dureeRGPD) {
		return $this->site->modifierDureeRGPD($dureeRGPD);
	}

	/**
	 * Modifie le type d'installation
	 *
	 * @param  int $local installation en local
	 * @return void
	 */
	public function siteModifierLocal(bool $local) {
		$this->site->siteModifierLocal($local);
	}

    /**
     * Récupére les informations du site
     *
     * @return array<string,string> $info tableau qui contient le nom, l'icone et l'image du site, retourne null si le résultat est vide
     */
    public function siteObtenirInformations() {
        return $this->site->obtenirInfos();
    }
	
	/**
	 * Désinstalle la base de données
	 *
	 * @return void
	 */
	public function siteDesinstallation() {
		return $this->site->desinstallation();
	}
}