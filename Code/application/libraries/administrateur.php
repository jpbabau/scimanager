<?php

require_once APPPATH . 'libraries/compte.php';
/**
 * Classe Administrateur 
 */
class Administrateur extends Compte
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Constructeur de la classe Administrateur
     *
     * @return void
     */
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->database();
    }

    /**
     * Modifie le login de l'administrateur
     *
     * @param  string $nouveauLogin nouveau login de l'administrateur
     * @return int 
     *      - 0 : l'ancien login est remplacé par le nouveau login
     *      - 1 : le nouveau login est vide, l’ancien login est conservé
     *      - 2 : le nouveau login est trop long (>45), l’ancien login est conservé
     *      - 3 : le nouveau login est déjà utilisé par un utilisateur, l'ancien login est conservé
     */
    public function modifierLogin(string $nouveauLogin)
    {
        if (isset($nouveauLogin) && strlen($nouveauLogin) > 0) {
            if (strlen($nouveauLogin) <= 45) {
                $resultUti = $this->_obtenirUtilisateurAvecLoginBdd($nouveauLogin);
                if (!isset($resultUti) || !($resultUti != null)) {
                    $this->_modifierLoginBdd($nouveauLogin);
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Modifie l'email de l'administrateur
     *
     * @param  string $nouvelEmail nouvel email de l'administrateur
     * @return int 
     *      - 0 : l'ancien email est remplacé par le nouvel email
     *      - 1 : le nouvel email est vide, l'ancien email est conservé
     *      - 2 : le nouvel email est trop long (>45), l'ancien email est conservé
     *      - 3 : le format du nouvel email est incorrect, l'ancien email est conservé
     */
    public function modifierEmail(string $nouvelEmail)
    {
        $login = $this->_obtenirInformationsBdd()->adm_login;
        $verification = $this->verifierEmail($login, $nouvelEmail);
        if ($verification !== 0) return $verification;

        $this->_modifierEmailBdd($nouvelEmail);
        return 0;
    }

    /**
     * Modifie le mot de passe de l'administrateur
     *
     * @param  string $ancienMdp ancien mot de passe de l'administrateur
     * @param  string $nouveauMdp nouveau mot de passe de l'administrateur
     * @return int
     *      - 0 : l'ancien mot de passe est remplacé par le nouveau mot de passe
     *      - 1 : le nouveau ou l'ancien mot de passe est vide, l'ancien mot de passe est conservé
     *      - 2 : le nouveau ou l'ancien mot de passe est trop long, l'ancien mot de passe est conservé
     *      - 3 : l'ancien mot de passe est incorrect, l'ancien mot de passe est conservé
     *      - 4 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
     */
    public function modifierMdp(string $ancienMdp, string $nouveauMdp)
    {
        $verification = $this->verifierMdp($ancienMdp, $nouveauMdp);
        if ($verification !== 0) return $verification;
        $ancienMdpHash1 = $this->_obtenirInformationsBdd()->adm_mot_de_passe;
        if (password_verify($ancienMdp, $ancienMdpHash1)) {
            //Vérifie le format du mot de passe (minuscule, majuscule, chiffre, caractères spéciaux, taille >=8)
            $nouveauMdpHash = password_hash($nouveauMdp, PASSWORD_BCRYPT);
            $this->_modifierMdpBdd($nouveauMdpHash);
            $this->_modifierValiditeBdd("A");
            return 0;
        } else {
            return 3;
        }
    }

    /**
     * Récupère toutes les informations de l'administrateur
     *
     * @return array<string, string> $info tableau qui contient l'email et le login de l'administrateur, retourne null si le résultat est vide
     * 
     *     $infos = [<br>
     *          "login"     =>  (string) login<br>
     *          "email"     =>  (string) email<br>
     *      ]
     */
    public function obtenirInformations()
    {
        $infosBdd = $this->_obtenirInformationsBdd();
        if (isset($infosBdd) && $infosBdd != null) {
            $infos = array(
                "login" => $infosBdd->adm_login,
                "email" => $infosBdd->adm_email,
            );
            return $infos;
        } else {
            return null;
        }
    }

    /**
     * Modifie le login de la table administrateur
     *
     * @access private
     * @param  string $login
     * @return void
     */
    private function _modifierLoginBdd($login)
    {
        $req = "UPDATE t_administrateur_adm
        SET adm_login = ?;";

        $this->CI->db->query($req, array($login));
    }

    /**
     * Modifie l'email de la table administrateur
     *
     * @access private
     * @param  string $email
     * @return void
     */
    private function _modifierEmailBdd($email)
    {
        $req = "UPDATE t_administrateur_adm
        SET adm_email = ?;";

        $this->CI->db->query($req, array($email));
    }

    /**
     * Modifie le mot de passe de la table administrateur
     *
     * @access private
     * @param  string $mdp
     * @return void
     */
    private function _modifierMdpBdd($mdp)
    {
        $req = "UPDATE t_administrateur_adm
        SET adm_mot_de_passe = ?;";

        $this->CI->db->query($req, array($mdp));
    }

    /**
     * Modifie la validite du compte administrateur de la table administrateur
     *
     * @param  string $validite
     * @return void
     */
    private function _modifierValiditeBdd($validite)
    {
        $req = "UPDATE t_administrateur_adm
        SET adm_validite = ?;";

        $this->CI->db->query($req, array($validite));
    }

    /**
     * Récupère toutes les données de la table administrateur
     *
     * @access private
     * @return object
     */
    private function _obtenirInformationsBdd()
    {
        $query = $this->CI->db->query("SELECT * 
        FROM t_administrateur_adm;");

        return $query->row();
    }

    /**
     * Récupère les informations d'un utilisateur
     *
     * @param  string $login
     * @return object
     */
    private function _obtenirUtilisateurAvecLoginBdd($login)
    {
        $req = "SELECT *
        FROM t_societaire_soc
        WHERE soc_login = ?;";

        $result = $this->CI->db->query($req, array($login))->row();
        return $result;
    }

    /**
     * Récupère les informations d'un utilisateur
     *
     * @param  string $email
     * @return void
     */
    private function _obtenirUtilisateurAvecEmailBdd($email)
    {
        $req = "SELECT *
        FROM t_societaire_soc
        WHERE soc_email = ?;";

        $result = $this->CI->db->query($req, array($email))->row();
        return $result;
    }
}
