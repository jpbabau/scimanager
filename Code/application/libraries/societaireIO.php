<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

use PhpOffice\PhpSpreadsheet\Style;

class SocietaireIO
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Le societaire
     *
     * @access protected
     * @var Societaire
     */
    protected $societaire;

    /**
     * La gestion des champs sociétaire
     *
     * @access protected
     * @var ParametresAvances
     */
    protected $parametresAvances;

    /**
     * L'assemblée générale
     *
     * @access protected
     * @var AssembleeGenerale
     */
    protected $assembleeGenerale;

    /**
     * les mouvements
     *
     * @access protected
     * @var Mouvement
     */
    protected $mouvement;

    /**
     * Format du fichier
     *
     * @var int
     */
    protected $format;

    /**
     * Constructeur de la classe
     *
     * @param  int $format Le format utilisé, 0 : .Csv, 1 : .Xlsx
     * @return void
     */
    public function __construct(int $format)
    {
        $this->CI = &get_instance();
        $this->CI->load->helper('download');

        $this->societaire = new Societaire();
        $this->parametresAvances = new parametresAvances();
        $this->assembleeGenerale = new AssembleeGenerale();
        $this->mouvement = new Mouvement();
        $this->format = $format;
    }

    /**
     * Exporte les données des sociétaires
     *
     * @return void
     */
    public function export()
    {
        $spreadsheet = new Spreadsheet();

        // Style par defaut
        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(12);

        // Style de l'entête
        $enteteStyle = [
            'font' => [
                'bold' => true
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => ['argb' => 'ff33cc99']
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ];

        // Style des données
        $donneesStyle = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ];

        // Création de l'entête de la feuille Personnes
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Identifiant')
            ->setCellValue('B1', 'Civilité')
            ->setCellValue('C1', 'Nom usuel')
            ->setCellValue('D1', 'Nom')
            ->setCellValue('E1', 'Prénom')
            ->setCellValue('F1', 'Représentants légaux')
            ->setCellValue('G1', 'Date de naissance')
            ->setCellValue('H1', 'Lieu de naissance')
            ->setCellValue('I1', 'Nationalité')
            ->setCellValue('J1', 'Adresse')
            ->setCellValue('K1', 'Code postal')
            ->setCellValue('L1', 'Ville')
            ->setCellValue('M1', 'Pays')
            ->setCellValue('N1', 'Téléphone fixe')
            ->setCellValue('O1', 'Téléphone portable')
            ->setCellValue('P1', 'Email')
            ->setCellValue('Q1', 'Accepte appel')
            ->setCellValue('R1', 'Accepte appel portable')
            ->setCellValue('S1', 'Accepte sms')
            ->setCellValue('T1', 'Accepte mail')
            ->setCellValue('U1', 'Cloud')
            ->setCellValue('V1', 'Date d\'entrée')
            ->setCellValue('W1', 'Date de sortie')
            ->setCellValue('X1', 'Nombre des parts attribuées')
            ->setCellValue('Y1', 'Numéro des parts attribuées')
            ->setCellValue('Z1', 'Régime matrimonial')
            ->setCellValue('AA1', 'Indivision');
        $spreadsheet->getActiveSheet()->setTitle('Personnes');
        $spreadsheet->getActiveSheet()->getStyle('A1:AA1')->applyFromArray($enteteStyle);
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($enteteStyle);
        foreach (range('A', 'Z') as $colonne) {
            $spreadsheet->getActiveSheet()->getColumnDimension($colonne)->setAutoSize(true);
        }

        // Création de l'entête de la feuille Sociétés
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1)
            ->setCellValue('A1', 'Identifiant')
            ->setCellValue('B1', 'Statut')
            ->setCellValue('C1', 'Numéro SIRET')
            ->setCellValue('D1', 'Raison Sociale')
            ->setCellValue('E1', 'Nom représentant légal')
            ->setCellValue('F1', 'Prénom représentant légal')
            ->setCellValue('G1', 'Civilité représentant légal')
            ->setCellValue('H1', 'Date de création')
            ->setCellValue('I1', 'Adresse')
            ->setCellValue('J1', 'Code postal')
            ->setCellValue('K1', 'Ville')
            ->setCellValue('L1', 'Pays')
            ->setCellValue('M1', 'Téléphone fixe')
            ->setCellValue('N1', 'Téléphone portable')
            ->setCellValue('O1', 'Email')
            ->setCellValue('P1', 'Accepte appel portable')
            ->setCellValue('Q1', 'Cloud')
            ->setCellValue('R1', 'Date d\'entrée')
            ->setCellValue('S1', 'Date de sortie')
            ->setCellValue('T1', 'Nombre de parts attribuées')
            ->setCellValue('U1', 'Numéro des parts attribuées')
            ->setCellValue('V1', 'Régime fiscal');
        $spreadsheet->getActiveSheet()->setTitle('Sociétés');
        $spreadsheet->getActiveSheet()->getStyle('A1:V1')->applyFromArray($enteteStyle);
        foreach (range('A', 'V') as $colonne) {
            $spreadsheet->getActiveSheet()->getColumnDimension($colonne)->setAutoSize(true);
        }

        // Ajout des données des sociétaires
        $listeSocietaires = $this->societaire->obtenirListeSocietaires();

        $personnesIndex = 1;
        $societesIndex = 1;
        foreach ($listeSocietaires as $soc) {

            // Civilité
            $civilite = '';
            switch ($soc['soc_civilite']) {
                case 1:
                    $civilite = 'Madame';
                    break;
                case 2:
                    $civilite = 'Non-binaire';
                    break;
                case 3:
                    $civilite = 'Non renseigné';
                    break;
                case 4:
                    $civilite = 'Autre';
                    break;
                default:
                    $civilite = 'Monsieur';
                    break;
            }

            // Autorisations
            $accepteAppel = 'OUI';
            if ($soc['soc_pasAppelerFixe']) {
                $accepteAppel = 'NON';
            }
            $accepteAppelPortable = 'OUI';
            if ($soc['soc_pasAppelerPortable']) {
                $accepteAppelPortable = 'NON';
            }
            $accepteSms = 'OUI';
            if ($soc['soc_pasEnvoyerSms']) {
                $accepteSms = 'NON';
            }
            $accepteMail = 'OUI';
            if ($soc['soc_pasEnvoyerEmail']) {
                $accepteMail = 'NON';
            }
            $cloud = 'NON';
            if ($soc['soc_autoriserCloud']) {
                $cloud = 'OUI';
            }

            //indivision
            if ($soc['soc_indivision']) {
                $indivision = "OUI";
            } else {
                $indivision = "NON";
            }

            //statut
            $statut = $this->parametresAvances->obtenirStatut($soc['sta_id'])['sta_nom'];

            //régime
            $regime = $this->parametresAvances->obtenirRegime($soc['reg_id'])['reg_nom'];

            //assemblée générale
            $dateEntree = $this->assembleeGenerale->obtenirDateAG($soc['ass_idEntree'])['ass_date'];
            if (isset($soc['ass_idSortie']) && $soc['ass_idSortie'] != null) {
                $dateSortie = $this->assembleeGenerale->obtenirDateAG($soc['ass_idSortie'])['ass_date'];
            } else {
                $dateSortie = '';
            }

            //mouvements
            $tabParts = $this->mouvement->obtenirPartSocietaire($soc['soc_login']);
            $nombreParts = count($tabParts);
            $Parts = implode(', ', $tabParts);

            // Date
            $dateNaissance = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($soc['soc_naissance']);
            $dateEntree = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($dateEntree);
            if (isset($dateSortie) && $dateSortie !== '') {
                $dateSortie = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($dateSortie);
            }

            if ($soc['sta_id'] === '0') {
                // Personne physique
                $personnesIndex = $personnesIndex + 1;

                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $personnesIndex, $soc['soc_login'])
                    ->setCellValue('B' . $personnesIndex, $civilite)
                    ->setCellValue('C' . $personnesIndex, $soc['soc_nomUsuel'])
                    ->setCellValue('D' . $personnesIndex, $soc['soc_nom'])
                    ->setCellValue('E' . $personnesIndex, $soc['soc_prenom'])
                    ->setCellValue('F' . $personnesIndex, $soc['soc_representantsLegaux'])
                    ->setCellValue('G' . $personnesIndex, $dateNaissance)
                    ->setCellValue('H' . $personnesIndex, $soc['soc_lieuNaissance'])
                    ->setCellValue('I' . $personnesIndex, $soc['soc_nationalite'])
                    ->setCellValue('J' . $personnesIndex, $soc['soc_adresse'])
                    ->setCellValue('K' . $personnesIndex, $soc['soc_codePostal'])
                    ->setCellValue('L' . $personnesIndex, $soc['soc_ville'])
                    ->setCellValue('M' . $personnesIndex, $soc['soc_pays'])
                    ->setCellValue('N' . $personnesIndex, $soc['soc_telephoneFixe'])
                    ->setCellValue('O' . $personnesIndex, $soc['soc_telephonePortable'])
                    ->setCellValue('P' . $personnesIndex, $soc['soc_email'])
                    ->setCellValue('Q' . $personnesIndex, $accepteAppel)
                    ->setCellValue('R' . $personnesIndex, $accepteAppelPortable)
                    ->setCellValue('S' . $personnesIndex, $accepteSms)
                    ->setCellValue('T' . $personnesIndex, $accepteMail)
                    ->setCellValue('U' . $personnesIndex, $cloud)
                    ->setCellValue('V' . $personnesIndex, $dateEntree)
                    ->setCellValue('W' . $personnesIndex, $dateSortie)
                    ->setCellValue('X' . $personnesIndex, $nombreParts)
                    ->setCellValue('Y' . $personnesIndex, $Parts)
                    ->setCellValue('Z' . $personnesIndex, $regime)
                    ->setCellValue('AA' . $personnesIndex, $indivision);
            } else {
                // Personne Morale
                $societesIndex = $societesIndex + 1;
                $spreadsheet->setActiveSheetIndex(1)
                    ->setCellValue('A' . $societesIndex, $soc['soc_login'])
                    ->setCellValue('B' . $societesIndex, $statut)
                    ->setCellValue('C' . $societesIndex, $soc['soc_siret'])
                    ->setCellValue('D' . $societesIndex, $soc['soc_nomUsuel'])
                    ->setCellValue('E' . $societesIndex, $soc['soc_nom'])
                    ->setCellValue('F' . $societesIndex, $soc['soc_prenom'])
                    ->setCellValue('G' . $societesIndex, $civilite)
                    ->setCellValue('H' . $societesIndex, $dateNaissance)
                    ->setCellValue('I' . $societesIndex, $soc['soc_adresse'])
                    ->setCellValue('J' . $societesIndex, $soc['soc_codePostal'])
                    ->setCellValue('K' . $societesIndex, $soc['soc_ville'])
                    ->setCellValue('L' . $societesIndex, $soc['soc_pays'])
                    ->setCellValue('M' . $societesIndex, $soc['soc_telephoneFixe'])
                    ->setCellValue('N' . $societesIndex, $soc['soc_telephonePortable'])
                    ->setCellValue('O' . $societesIndex, $soc['soc_email'])
                    ->setCellValue('P' . $societesIndex, $accepteAppelPortable)
                    ->setCellValue('Q' . $societesIndex, $cloud)
                    ->setCellValue('R' . $societesIndex, $dateEntree)
                    ->setCellValue('S' . $societesIndex, $dateSortie)
                    ->setCellValue('T' . $societesIndex, $nombreParts)
                    ->setCellValue('U' . $societesIndex, $Parts)
                    ->setCellValue('V' . $societesIndex, $regime);
            }
        }

        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->getStyle('A2:AA' . $personnesIndex)->applyFromArray($donneesStyle);
        // Format date
        $spreadsheet->getActiveSheet()->getStyle('G2:G' . $personnesIndex)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
        $spreadsheet->getActiveSheet()->getStyle('V2:W' . $personnesIndex)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

        $spreadsheet->setActiveSheetIndex(1);
        $spreadsheet->getActiveSheet()->getStyle('A2:V' . $societesIndex)->applyFromArray($donneesStyle);
        // Format date
        $spreadsheet->getActiveSheet()->getStyle('H2:H' . $personnesIndex)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
        $spreadsheet->getActiveSheet()->getStyle('R2:S' . $personnesIndex)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Sauvegarde du fichier
        switch ($this->format) {
            case 1:
                $writerFormat    = 'Xlsx';
                $saveFormat        = '.xlsx';
                break;
            default:
                $writerFormat    = 'Csv';
                $saveFormat        = '.csv';
                break;
        }


        $spreadsheet->setActiveSheetIndex(0);
        $writer = IOFactory::createWriter($spreadsheet, $writerFormat);
        $writer->save(FCPATH . 'registres/export/registreSocietaires' . $saveFormat);
        force_download(FCPATH . 'registres/export/registreSocietaires' . $saveFormat, NULL);
    }

    /**
     * Importe les données des sociétaire depuis un tableur
     *
     * @param  string $registreFile nom du fichier à importer
     * @return array<array<int,int>> tableau contenant le numéro d'une ligne associé à un numéro de retour
     *      code retour :
     *      - 0 : le compte sociétaire est créé
     *      - 1 : un des paramètre obligatoire (mdp, prenom, nom, civilite, statut, dateNaissance, adresse, codePostal, ville, pays, regime, agEntree) est vide, le compte societaire n'est pas créé
     *      - 2 : un des paramètre est trop long, le compte societaire n'est pas créé
     *      - 3 : le format de l'email est incorrect, le compte societaire n'est pas créé
     *      - 5 : le format d'un des numéros de téléphone est invalide, le compte societaire n'est pas créé
     *      - 6 : le format du mot de passe est incorrect, le compte societaire n'est pas créé
     *      - 7 : le mail n'a pas été envoyé, le compte societaire est créé
     *      - 8 : la civilité renseigné est incorrect, le compte societaire n'est pas créé
     *      - 9 : le statut renseigné est incorrect, le compte societaire n'est pas créé
     *      - 10 : le régime renseigné est incorrect, le compte societaire n'est pas créé
     *      - 11 : la date d'entrée renseigné est incorrect, le compte societaire n'est pas créé
     *      - 12 : la date de sortie renseigné est incorrect, le compte societaire n'est pas créé
     *      - 13 : le sociétaire est détecté comme étant un doublon, le compte societaire n'est pas créé
     */
    public function import(string $registreFile)
    {
        // Chargement du fichier
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(FCPATH . "registres/import/" . $registreFile);

        $tabErreurs = array();
        array_push($tabErreurs, array());
        array_push($tabErreurs, array());

        for ($i = 0; $i <= 1; $i++) {
            $spreadsheet->setActiveSheetIndex($i);
            // Lire les données et les stocker dans un tableau
            $data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $debutBool = true;
            foreach ($data as $nb => $row) {
                if ($debutBool) {
                    $debutBool = false;
                    continue;
                }

                if ($i === 0) {
                    $login = $row['A'];
                    $civilite = $row['B'];
                    $nomUsuel = $row['C'];
                    $nomUsuel = mb_strtoupper($nomUsuel, 'UTF-8');
                    $nom = $row['D'];
                    $prenom = $row['E'];
                    $representantLegaux = $row['F'];
                    $dateNaissance = $row['G'];
                    $lieuNaissance = $row['H'];
                    $nationalite = $row['I'];
                    $adresse = $row['J'];
                    $codePostal = $row['K'];
                    $ville = $row['L'];
                    $pays = $row['M'];
                    $telFixe = $row['N'];
                    $telPortable = $row['O'];
                    $email = $row['P'];
                    $pasFixe = $row['Q'];
                    $pasPortable = $row['R'];
                    $pasSms = $row['S'];
                    $pasEmail = $row['T'];
                    $cloud = $row['U'];
                    $dateEntree = $row['V'];
                    $dateSortie = $row['W'];
                    $regime = $row['Z'];
                    $indivision = $row['AA'];
                    $siret = null;
                } else {
                    $login = $row['A'];
                    $statut = $row['B'];
                    $siret = $row['C'];
                    $nomUsuel = $row['D'];
                    $nom = $row['E'];
                    $prenom = $row['F'];
                    $civilite = $row['G'];
                    $dateNaissance = $row['H'];
                    $adresse = $row['I'];
                    $codePostal = $row['J'];
                    $ville = $row['K'];
                    $pays = $row['L'];
                    $telFixe = $row['M'];
                    $telPortable = $row['N'];
                    $email = $row['O'];
                    $pasPortable = $row['P'];
                    $cloud = $row['Q'];
                    $dateEntree = $row['R'];
                    $dateSortie = $row['S'];
                    $regime = $row['V'];
                    $indivision = "NON";
                    if ($pays==="France") {
                        $nationalite = "Française";
                    } else {
                        $nationalite = "Etranger";
                    }
                }

                // Indivision
                if ($indivision === "OUI") {
                    $indivision = true;
                } else {
                    $indivision = false;
                }

                // Autorisations
                if (strcasecmp($pasFixe, "OUI") === 0) {
                    $pasFixe = 0;
                } else {
                    $pasFixe = 1;
                }
                if (strcasecmp($pasPortable, "OUI") === 0) {
                    $pasPortable = 0;
                } else {
                    $pasPortable = 1;
                }
                if (strcasecmp($pasSms, "OUI") === 0) {
                    $pasSms = 0;
                } else {
                    $pasSms = 1;
                }
                if (strcasecmp($pasEmail, "OUI") === 0) {
                    $pasEmail = 0;
                } else {
                    $pasEmail = 1;
                }
                if (strcasecmp($cloud, "OUI") === 0) {
                    $cloud = 1;
                } else {
                    $cloud = 0;
                }

                if ($i == 1) {
                    $pasSms = 0;
                    $pasEmail = 0;
                    $pasFixe = 0;
                }

                // Civilité
                switch (mb_strtolower($civilite, 'UTF-8')) {
                    case 'monsieur':
                        $civilite = 0;
                        break;
                    case 'madame':
                        $civilite = 1;
                        break;
                    case 'non-binaire':
                        $civilite = 2;
                        break;
                    case 'non renseigné':
                        $civilite = 3;
                        break;
                    case 'autre':
                        $civilite = 4;
                        break;
                    default:
                        $tabErreurs[$i][$nb] = 8;
                        continue 2;
                        break;
                }

                // Statut
                if ($i === 0) {
                    $statut = 0;
                } else {
                    $listeStatut = $this->parametresAvances->obtenirListeStatuts();
                    foreach ($listeStatut as $sta) {
                        if (strcasecmp($sta['sta_nom'], $statut) === 0) {
                            $statut = $sta['sta_id'];
                            break 1;
                        }
                    }
                    if (!ctype_digit($statut)) {
                        $tabErreurs[$i][$nb] = 9;
                        continue;
                    }
                }

                // Régime
                $listeRegime = $this->parametresAvances->obtenirListeRegimesParType($i);
                foreach ($listeRegime as $reg) {
                    if (strcasecmp($reg['reg_nom'], $regime) === 0) {
                        $regime = $reg['reg_id'];
                        break 1;
                    }
                }
                if (!ctype_digit($regime)) {
                    $tabErreurs[$i][$nb] = 10;
                    continue;
                }

                // Convertion dates
                $dt = DateTime::createFromFormat('m/j/Y', $dateNaissance);
                $dateNaissance = $dt->format('Y-m-d');
                $dt = DateTime::createFromFormat('m/j/Y', $dateEntree);
                $dateEntree = $dt->format('Y-m-d');
                if (isset($dateSortie) && strlen($dateSortie) > 0 && $dateSortie !== null) {
                    $dt = DateTime::createFromFormat('m/j/Y', $dateSortie);
                    $dateSortie = $dt->format('Y-m-d');
                }

                // AG
                $agEntree = '';
                $agSortie = '';
                $listeAG = $this->assembleeGenerale->obtenirListeDatesAG();
                foreach ($listeAG as $ag) {
                    if (strcasecmp($ag['ass_date'], $dateEntree) === 0) {
                        $agEntree = $ag['ass_id'];
                        break 1;
                    }
                }
                if (!ctype_digit($agEntree)) {
                    $tabErreurs[$i][$nb] = 11;
                    continue;
                }
                if ($dateSortie !== null) {
                    foreach ($listeAG as $ag) {
                        if (strcasecmp($ag['ass_date'], $dateSortie) === 0) {
                            $agSortie = $ag['ass_id'];
                            break 1;
                        }
                    }
                    if (!ctype_digit($agSortie)) {
                        $tabErreurs[$i][$nb] = 12;
                        continue;
                    }
                } else {
                    $agSortie = null;
                }

                //nom et prénom
                $nom = mb_strtoupper($nom, 'UTF-8');
                $prenom = ucfirst($prenom);

                // Vérification doublons
                $liste = $this->societaire->obtenirListeSocietaires();
                $login = $this->societaire->genererLogin($nom, $prenom, $dateNaissance);
                $testLogin = substr($login, -3);
                if (ctype_digit($testLogin)) {
                    foreach ($liste as $soc) {
                        if ($soc["soc_prenom"] === $prenom) {
                            $tabErreurs[$i][$nb] = 13;
                            break;
                        }
                    }
                    if ($tabErreurs[$i][$nb] === 13) {
                        continue;
                    }
                }

                $tabErreurs[$i][$nb] = $this->societaire->ajouter($prenom, null, $nom, $nomUsuel, $representantLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $regime, $agEntree, $indivision);
                if (isset($agSortie) && $agSortie !== null && strlen($agSortie) > 0) {
                    $this->societaire->modifierDateSortie($login, $agSortie);
                }
                if ($i === 0) {
                    $this->societaire->modifierAutorisation($login, $pasFixe, $pasPortable, $pasSms, $pasEmail, $cloud);
                } else {
                    $this->societaire->modifierAutorisation($login, true, $pasPortable, true, true, $cloud);
                }
            }
        }
        return $tabErreurs;
    }
}
