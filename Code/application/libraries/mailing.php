<?php

/**
 * Classe Installation 
 */
class Mailing
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Instance de la classe Site
     *
     * @var Site
     */
    private $Site;

    /**
     * Constructeur de la classe Societaire
     *
     * @return void
     */
    public function __construct()
    {
        $this->Site = new Site();
        $this->CI = &get_instance();
        $this->CI->load->config('email');
        $this->CI->load->library('email');
    }

    /**
     * Envoi un mail à une liste de destinataire
     *
     * @param  array $emails emails des destinataires
     * @param  string $objet objet de l'email
     * @param  string $message message de l'email
     * @param  $file_name le nom du fichier attaché
     * @return int
     *      - 0 : le mail est envoyé
     *      - 1 : un des paramètres est vide, le mail n'est pas envoyé
     *      - 2 : un problème est survenue lors de l'envoi de mail, le mail n'est pas envoyé
     *      - 3 : le fichier n'a pas pu etre été téléchargé
     */
    public function envoyerEmail(array $emails, string $objet, string $message,	$file_name)
    {
        if($file_data===3) {
            return 3;
        }
        if (isset($emails) && sizeof($emails) > 0 && isset($objet) && strlen($objet) > 0 && isset($message) && strlen($message) > 0) {
            array_push($emails, $this->CI->config->item('smtp_user'));
            $listeEmails = array_chunk($emails, 99);
            foreach ($listeEmails as $mails) {
                $nomSite = $this->Site->obtenirInfos()->nom;
                $this->CI->email->from($this->CI->config->item('smtp_user'), $nomSite);
                $this->CI->email->to($this->CI->config->item('smtp_user'));
                $this->CI->email->bcc($mails);
                $this->CI->email->subject($objet);
                $this->CI->email->message($message);
                if ($file_name!==NULL) {
                    $this->CI->email->attach('./uploads/'.$file_name);
                }
                if ($this->CI->email->send()) {
                    if ($file_name!==NULL) {
                        unlink('./uploads/'.$file_name);
                    }
                    return 0;
                } else {
                    return 2;
                }
            }
        } else {
            return 1;
        }
    }

 	/**
	 * Envoi un mail un sociétaire pour activation compte
	 *
	 * @param  string $login
	 * @return int 
	 *		0 : le mail est envoyé
     *		5 : le mail n'a pas pu être envoyé
	 */
    public function mailActivationCompte($societaire)
    {
        $infosSite = $this->Site->obtenirInfos();
        if ($infosSite[structure]) {
            $structure = "GFA";
            $pronom = "le";
        } else {
            $structure = "SCI";
            $pronom = "la";
        }
        $this->CI->email->from($this->CI->config->item('smtp_user'),  $structure .' '.$infosSite[nom]);
        $this->CI->email->to($societaire['soc_email']);
        $this->CI->email->subject('['.$structure.' '.$infosSite[nom].'] Création compte');
        $this->CI->email->message('
        <div style="font-family: helvetica">
                <p>Votre compte SciManager pour '.$pronom.' '.$structure.' '. $infosSite[nom].' vient d\'être créé,</p>
                <p>Utilisez les identifiants ci-dessous pour vous connecter</p>
                <br><hr>
                <ul style=" font-size: 18px;">
                    <li>identifiant : <strong>'. $societaire['soc_login'].'</strong></li>
                    <li>mot de passe : <strong>'.$societaire['soc_login'].'</strong></li>
                </ul>
                <hr><br>
                <p>Vous devrez changer votre mot de passe à la premiere <a href="'.base_url("index.php/accueil/connexion").'">connexion</a></p>
        </div>');
        if ($this->CI->email->send()) {
            return 0;
        } else {
            return 5;
        }
    }
}
