<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'vendor/autoload.php';
require 'utilString.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

use PhpOffice\PhpSpreadsheet\Style;

class MouvementIO
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Le societaire
     *
     * @access protected
     * @var Societaire
     */
    protected $societaire;

    /**
     * les mouvements
     *
     * @access protected
     * @var mouvement
     */
    protected $mouvement;

    /**
     * La gestion des champs sociétaire
     *
     * @access protected
     * @var ParametresAvances
     */
    protected $parametresAvances;

    /**
     * L'assemblée générale
     *
     * @access protected
     * @var AssembleeGenerale
     */
    protected $assembleeGenerale;

    /**
     * Le site
     *
     * @access protected
     * @var Site
     */
    protected $Site;

    /**
     * Format du fichier
     *
     * @var int
     */
    protected $format;

    /**
     * Constructeur de la classe
     *
     * @param  int $format Le format utilisé, 0 : .Csv, 1 : .Xlsx
     * @return void
     */
    public function __construct(int $format)
    {
        $this->CI = &get_instance();
        $this->CI->load->helper('download');

        $this->societaire = new Societaire();
        $this->mouvement = new Mouvement();
        $this->parametresAvances = new parametresAvances();
        $this->assembleeGenerale = new AssembleeGenerale();
        $this->Site = new Site();
        $this->format = $format;
    }

    /**
     * Exporte les mouvements
     *
     * @return void
     */
    public function export()
    {
        $spreadsheet = new Spreadsheet();

        // Style par defaut
        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(12);

        // Style de l'entête 1
        $enteteStyle1 = [
            'font' => [
                'bold' => true
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => ['argb' => 'ff33cc99']
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ];

        // Style de l'entête 2
        $enteteStyle2 = [
            'font' => [
                'bold' => true
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => ['argb' => 'ff33cc66']
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ];

        // Style des données
        $donneesStyle = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ];

        $infosSite = $this->Site->obtenirInfos();
        if ($infosSite[structure]) {
            $structure = "GFA";
        } else {
            $structure = "SCI";
        }

        // Création de l'entête
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Société')
            ->setCellValue('B1',$structure .' '.$infosSite[nom])
            ->setCellValue('A2', 'Nombre de parts')
            ->setCellValue('A3', 'Capital')
            ->setCellValue('B4', 'Titulaire')
            ->setCellValue('K4', 'Nature du Mouvement')
            ->setCellValue('L4', 'Bénéficiaire')
            ->setCellValue('P4', 'Organe social ayant agréé l\'opération')
            ->setCellValue('A5', 'Date d\'agrément')
            ->setCellValue('B5', 'Identifiant')
            ->setCellValue('C5', 'Nom')
            ->setCellValue('D5', 'Prénom')
            ->setCellValue('E5', 'Domicile')
            ->setCellValue('F5', 'Nombre de part')
            ->setCellValue('G5', 'N° des parts')
            ->setCellValue('H5', 'Prix d\'une part')
            ->setCellValue('I5', 'Type de paiement')
            ->setCellValue('J5', 'Numéro du paiement')
            ->setCellValue('L5', 'Identifiant')
            ->setCellValue('M5', 'Nom')
            ->setCellValue('N5', 'Prénom')
            ->setCellValue('O5', 'Domicile');
        $spreadsheet->getActiveSheet()->setTitle('Mouvements');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray($enteteStyle1);
        $spreadsheet->getActiveSheet()->getStyle('B1:B3')->applyFromArray($donneesStyle);
        $spreadsheet->getActiveSheet()->getStyle('B4:P4')->applyFromArray($enteteStyle1);
        $spreadsheet->getActiveSheet()->getStyle('B5:P5')->applyFromArray($enteteStyle2);

        //case grisé
        $spreadsheet->getActiveSheet()->getStyle('A4')->getFill()->getStartColor()->setARGB('FF555555');
        $spreadsheet->getActiveSheet()->getStyle('K5')->getFill()->getStartColor()->setARGB('FF555555');
        $spreadsheet->getActiveSheet()->getStyle('P5')->getFill()->getStartColor()->setARGB('FF555555');

        //fusion des cases
        $spreadsheet->getActiveSheet()->mergeCells('B4:J4');
        $spreadsheet->getActiveSheet()->mergeCells('L4:O4');

        foreach (range('A', 'P') as $colonne) {
            $spreadsheet->getActiveSheet()->getColumnDimension($colonne)->setAutoSize(true);
        }

        // Ajout des données des mouvements
        $listeMouvements = $this->mouvement->obtenirListeMouvement();

        $mouvementIndex = 5;
        $capital = 0;
        $nombreParts = 0;
        foreach ($listeMouvements as $mouv) {
            if (!isset($traite[$mouv["ope_id"]])) { //Si cet id n'a pas encore été traité
                $current_id = $mouv["ope_id"];

                // Nature du mouvement
                $natureMouvement = '';
                switch ($mouv['ope_type']) {
                    case 0:
                        $natureMouvement = 'Création';
                        $capital+=$mouv['ope_nombrePart']*$mouv['ope_valeurUnitaire'];
                        $nombreParts+=$mouv['ope_nombrePart'];
                        break;
                    case 1:
                        $natureMouvement = 'Transmission entre vifs';
                        break;
                    case 2:
                        $natureMouvement = 'Transmission par décès';
                        break;
                    case 3:
                        $natureMouvement = 'Vente';
                        break;
                    case 4:
                        $natureMouvement = 'Destruction';
                        $capital-=$mouv['ope_nombrePart']*$mouv['ope_valeurUnitaire'];
                        $nombreParts-=$mouv['ope_nombrePart'];
                        break;
                }

                // Sociétaires
                $beneficiaire = $this->societaire->obtenirInfos($mouv['soc_loginBeneficiaire']);
                $domicileBeneficiaire = $beneficiaire['soc_adresse'] . ', ' . $beneficiaire['soc_codePostal'] . ' ' . $beneficiaire['soc_ville'];
                $prenomBeneficiaire = $beneficiaire['soc_prenom'];
                $nomBeneficiaire = $beneficiaire['soc_nom'];

                if (isset($mouv['soc_loginTitulaire']) && $mouv['soc_loginTitulaire'] !== null) {
                    $titulaire = $this->societaire->obtenirInfos($mouv['soc_loginTitulaire']);
                    $domicileTitulaire = $titulaire['soc_adresse'] . ', ' . $titulaire['soc_codePostal'] . ' ' . $titulaire['soc_ville'];
                    $prenomTitulaire = $titulaire['soc_prenom'];
                    $nomTitulaire = $titulaire['soc_nom'];
                } else {
                    $domicileTitulaire = '';
                    $prenomTitulaire = '';
                    $nomTitulaire = '';
                }

                // Parts
                $tabParts = array();
                foreach ($listeMouvements as $mouv2) {
                    if (strcmp($current_id, $mouv2["ope_id"]) == 0) { //Si l'id de la ligne correspond au current_id
                        if (!in_array($mouv2["mou_numPart"], $tabParts)) {
                            array_push($tabParts, $mouv2["mou_numPart"]);
                        }
                    }
                }
                $numeroParts = implode(',', $tabParts);

                // numéros de paiement et types de paiement
                $tabType = array();
                $tabNum = array();
                foreach ($listeMouvements as $mouv3) {
                    if (strcmp($current_id, $mouv3["ope_id"]) == 0) { //Si l'id de la ligne correspond au current_id
                        if (!in_array($mouv3["pai_numero"], $tabNum)) {
                            array_push($tabNum, $mouv3["pai_numero"]);
                            array_push($tabType, $mouv3["moy_type"]);
                        }
                    }
                }
                $numeroPaiement = implode(',', $tabNum);
                $type = implode(',', $tabType);

                // Date
                $date = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($mouv['ass_date']);

                $mouvementIndex = $mouvementIndex + 1;

                $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $mouvementIndex, $date)
                    ->setCellValue('B' . $mouvementIndex, $mouv['soc_loginTitulaire'])
                    ->setCellValue('C' . $mouvementIndex, $nomTitulaire)
                    ->setCellValue('D' . $mouvementIndex, $prenomTitulaire)
                    ->setCellValue('E' . $mouvementIndex, $domicileTitulaire)
                    ->setCellValue('F' . $mouvementIndex, $mouv['ope_nombrePart'])
                    ->setCellValue('G' . $mouvementIndex, $numeroParts)
                    ->setCellValue('H' . $mouvementIndex, $mouv['ope_valeurUnitaire'])
                    ->setCellValue('I' . $mouvementIndex, $type)
                    ->setCellValue('J' . $mouvementIndex, $numeroPaiement)
                    ->setCellValue('K' . $mouvementIndex, $natureMouvement)
                    ->setCellValue('L' . $mouvementIndex, $mouv['soc_loginBeneficiaire'])
                    ->setCellValue('M' . $mouvementIndex, $nomBeneficiaire)
                    ->setCellValue('N' . $mouvementIndex, $prenomBeneficiaire)
                    ->setCellValue('O' . $mouvementIndex, $domicileBeneficiaire);

                $traite[$mouv["ope_id"]] = 1;
            }
        }

        $spreadsheet->setActiveSheetIndex(0)->setCellValue('B2', $nombreParts);
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('B3', $capital);

        $spreadsheet->getActiveSheet()->getStyle('A6:P' . $mouvementIndex)->applyFromArray($donneesStyle);

        // Format date
        $spreadsheet->getActiveSheet()->getStyle('A6:A' . $mouvementIndex)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

        // Sauvegarde du fichier
        switch ($this->format) {
            case 1:
                $writerFormat    = 'Xlsx';
                $saveFormat        = '.xlsx';
                break;
            default:
                $writerFormat    = 'Csv';
                $saveFormat        = '.csv';
                break;
        }

        $writer = IOFactory::createWriter($spreadsheet, $writerFormat);
        $writer->save(FCPATH . 'registres/export/registreMouvements' . $saveFormat);
        force_download(FCPATH . 'registres/export/registreMouvements' . $saveFormat, NULL);
    }

    /**
     * Importe les données des mouvements depuis un tableur
     *
     * @param  string $registreFile nom du fichier à importer
     * @return array<int,int> tableau contenant le numéro d'une ligne associé à un numéro de retour
     *      code retour :
     *      - 0 : le mouvement est ajouté
     *      - 1 : un des paramètre obligatoire est vide, le mouvement n'est pas créé
     *      - 2 : un des paramètre est trop long, le mouvement n'est pas créé
     *      - 3 : le titulaire ou le bénéficiaire n'éxiste pas, le mouvement n'est pas créé
     *      - 4 : la liste des parts n'est pas correct, elle doit être composé uniquement d'entier unique, le mouvement n'est pas créé
     *      - 5 : une des parts existe déjà, impossible de créer une part déjà éxistante, le mouvement n'est pas créé
     *      - 6 : le titulaire ne possède pas une des parts qu'il essaye de vendre ou transmettre, le mouvement n'est pas créé
     *      - 7 : la date d'assemblée générale renseigné est incorrect, le mouvement n'est pas créé
     *      - 8 : le type de mouvement renseigné est incorrect, le mouvement n'est pas créé
     *      - 9 : le mouvement est détecté comme étant un doublon, le mouvement n'est pas créé
     */
    public function import($registreFile)
    {
        // Chargement du fichier
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(FCPATH . "registres/import/" . $registreFile);

        // Lire les données et les stocker dans un tableau
        $data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $tabErreurs = array();
        $debutCount = 0;
        foreach ($data as $nb => $row) {
            if ($debutCount <= 4) {
                $debutCount = $debutCount + 1;
                continue;
            }

            $date = $row['A'];
            $loginTitulaire = $row['B'];
            $numPart = $row['G'];
            $prix = $row['H'];
            $typePaiement = $row['I'];
            $numPaiement = $row['J'];
            $typeMouvement = $row['K'];
            $loginBeneficiaire = $row['L'];
            $organeSocial = $row['P'];

            // Parts
            $traitString = new UtilString();
            $tabPart = $traitString->listeMouvement($numPart);
            if (!$traitString->isListeOk($listeParts)) {
                $tabPart = [];
            }
            // numéro paiement
            $numPaiement = explode(',', $numPaiement);

            // type de paiement
            $typePaiement = explode(',', $typePaiement);

            // Conversion date
            $dt = DateTime::createFromFormat('m/j/Y', $date);
            if ($dt!==false) {
                $date = $dt->format('Y-m-d');
            }

            // AG
            $agMouv = '';
            $listeAG = $this->assembleeGenerale->obtenirListeDatesAG();
            foreach ($listeAG as $ag) {
                if (strcasecmp($ag['ass_date'], $date) === 0) {
                    $agMouv = $ag['ass_id'];
                    break 1;
                }
            }
            if (!ctype_digit($agMouv)) {
                $tabErreurs[$nb] = 7;
                continue;
            }

            // Type de paiement
            $paiType = array();
            $listePaiement = $this->parametresAvances->obtenirListePaiements();
            foreach ($typePaiement as $type) {
                foreach ($listePaiement as $pai) {
                    if (strcasecmp($pai['moy_type'], $type) === 0) {
                        array_push($paiType, $pai['moy_id']);
                    }
                }
            }

            // Paiement
            $i = 0;
            $paiement = array();
            foreach ($paiType as $type) {
                array_push($paiement, array($type => $numPaiement[$i]));
                $i = $i + 1;
            }

            // Type de mouvement
            switch (mb_strtolower($typeMouvement, 'UTF-8')) {
                case 'création':
                    $typeMouvement = 0;
                    break;
                case 'transmission entre vifs':
                    $typeMouvement = 1;
                    break;
                case 'transmission par décès':
                    $typeMouvement = 2;
                    break;
                case 'vente':
                    $typeMouvement = 3;
                    break;
                default:
                    $tabErreurs[$nb] = 8;
                    continue 2;
                    break;
            }

            // Doublon
            $liste = $this->mouvement->obtenirListeMouvement();
            foreach ($liste as $mouv) {
                if (($mouv["soc_loginBeneficiaire"] === $loginBeneficiaire && $mouv["ass_id"] === $agMouv) && (($typeMouvement === 1 && $mouv["ope_type"] === 1) || ($mouv["soc_loginTitulaire"] === $loginTitulaire && $typeMouvement === $mouv["ope_type"]))) {
                    $tabErreurs[$nb] = 9;
                    continue;
                }
            }

            // Ajout du mouvement
            $tabErreurs[$nb] = $this->mouvement->ajouter($loginTitulaire, $loginBeneficiaire, $tabPart, $agMouv, $prix, $typeMouvement, $paiement);
        }
        return $tabErreurs;
    }
}
