<?php

// retourne vrai l'entier en entrée est strictmeent positif
function positif($var) {
    return ($var > 0);
}

class UtilString
{

    // la liste des id mouvements est bien constituée
    protected $isOk;

    public function __construct() {
        $this->isOk = true;
    }

    public function listeMouvement(string $listeMouvements=NULL) {
       $listePartL= explode(",",$listeMouvements);
       $listePart=[];
        foreach($listePartL as &$parts) {
            $this->isOk = $this->isOk && preg_match('#^([0-9]+(-[0-9]+)?)$#', $parts);
            $parts = explode("-",$parts);
            if (count($parts)===2) {
                $min=$parts[0];
                $max=$parts[1];
                for($i=$min;$i<=$max;$i++){
                    array_push($listePart, intval($i));
                }
            }
            if (count($parts)===1) {
                array_push($listePart, intval($parts[0]));
            }
        }	
        // liste d'entier strictement positifs sans doublons
        $this->isOk = $this->isOk 
            && (array_filter($listePart, 'is_int') === $listePart) 
            && (array_filter($listePart, "positif")=== $listePart)
            && (count($listePart) === count(array_unique($listePart)));
        return $listePart;
    }

    public function isListeOk() {
        return $this->isOk;
     }

}
?>