<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Classe Assemblée Générale
 */
class AssembleeGenerale {
	
	/**
	 * CodeIgniter super-object
	 *
	 * @access protected
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->database();
	}
	
	/**
	 * Ajoute une date d'AG
	 *
	 * @param  string $date la date d'AG
	 * @return int
	 * 		- 0 : la date d'AG a été ajoutée
	 * 		- 1 : la date est vide, la date d'AG n'est pas ajoutée
	 * 		- 2 : le format de la date n'est pas valide, la date d'AG n'est pas ajoutée
	 */
	public function ajouterDateAG(string $date)
	{
		if (isset($date) && strlen($date) > 0) {
			$dateArray = explode('-', $date);
			if (checkdate($dateArray[1], $dateArray[2], $dateArray[0])) {
				$this->_ajouterDateAG($date);
				return 0;
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}
	
	/**
	 * Modifie une date d'AG
	 *
	 * @param  int		$id l'id de la date d'AG à modifier
	 * @param  string	$nouvelleDate la nouvelle date d'AG
	 * @return int
	 * 		- 0 : la date d'AG a été modifiée
	 * 		- 1 : la nouvelle date est vide
	 * 		- 2 : la nouvelle date est trop longue
	 * 		- 3 : l'id n'existe pas
	 */
	public function modifierDateAG(int $id, string $nouvelleDate)
	{
		if (isset($id) && isset($nouvelleDate) && strlen($nouvelleDate) > 0) {
			$dateArray = explode('-', $nouvelleDate);
			if (checkdate($dateArray[1], $dateArray[2], $dateArray[0])) {
				$result = $this->_obtenirDateAG($id);
				if (isset($result)) {
					$this->_modifierDateAG($id, $nouvelleDate);
					return 0;
				} else {
					return 3;
				}
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}
	
	/**
	 * Récupère une date d'AG
	 *
	 * @param  int $id l'id de la date d'AG à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations d'une date d'AG
	 * 
	 * 		$infos = [<br>
	 * 			'ass_id'	=> id,<br>
	 * 			'ass_date'	=> date,<br>
	 *		]
	 */
	public function obtenirDateAG(int $id)
	{
		$infos = $this->_obtenirDateAG($id);
		return $infos;
	}
	
	/**
	 * Récupère toutes les dates d'AG
	 *
	 * @return array<int,array<string,string>> $infos un tableau qui contient les informations de toutes les dates d'AG
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'ass_id'	=> id,<br>
	 * 				'ass_date'	=> date,<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...
	 * 		]
	 */
	public function obtenirListeDatesAG()
	{
		$infos = $this->_obtenirToutesLesDatesAG();
		return $infos;
	}
    
    /**
     * Supprime une assemblée générale
     *
     * @param  int $id id de l'AG
     * @return int
     *      - 0 : L'AG est supprimé
     *      - 1 : L'AG n'éxiste pas, L'AG n'est pas supprimé
     *      - 2 : L'AG est utilisé, L'AG n'est pas supprimé
     */
    public function supprimerDateAG(int $id)
	{
		$infos = $this->_obtenirDateAG($id);
        if(isset($infos) && sizeof($infos) > 0){
            $verif = $this->_verifierDateAgNonUtilisee($id);
            if(!$verif){
                $this->_supprimerDateAG($id);
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
	}
	
	/**
	 * Ajoute une date d'AG dans la table assemblée générale
	 *
	 * @access private
	 * @param  string $date la date d'AG
	 * @return void
	 */
	private function _ajouterDateAG(string $date)
	{
		$req = 'INSERT INTO t_assembleeGenerale_ass
				VALUES (NULL, ?);';

		$this->CI->db->query($req, array($date));
	}
	
	/**
	 * Modifie une date d'AG dans la table assemblée générale
	 *
	 * @access private
	 * @param  int		$id l'id de la date d'AG à modifier
	 * @param  string	$nouvelleDate la nouvelle date d'AG
	 * @return void
	 */
	private function _modifierDateAG(int $id, string $nouvelleDate)
	{
		$req = 'UPDATE t_assembleeGenerale_ass
				SET ass_date = ?
				WHERE ass_id = ?;';

		$this->CI->db->query($req, array($nouvelleDate, $id));
	}
	
	/**
	 * Récupère les informations d'une date d'AG
	 *
	 * @access private
	 * @param  int $id l'id de la date d'AG à récupèrer
	 * @return mixed
	 */
	private function _obtenirDateAG(int $id)
	{
		$req = 'SELECT *
				FROM t_assembleeGenerale_ass
				WHERE ass_id = ?;';

		$result = $this->CI->db->query($req, array($id))->row_array();
		return $result;
	}
	
	/**
	 * Récupère toutes les dates d'assemblée générale
	 *
	 * @access private
	 * @return mixed
	 */
	private function _obtenirToutesLesDatesAG()
	{
		$req = 'SELECT *
				FROM t_assembleeGenerale_ass
				ORDER BY ass_date DESC;';

		$result = $this->CI->db->query($req)->result_array();
		return $result;
	}
    
    /**
     * supprime une assemblee générale
     *
     * @param  int $idAG id d'une AG
     * @return void
     */
    private function _supprimerDateAG(int $idAG)
	{
		$req = 'DELETE FROM t_assembleeGenerale_ass
        WHERE ass_id = ?;';

		$this->CI->db->query($req, array($idAG));
	}
    
    /**
     * Vérifie si il existe une contrainte empêchant la suppression d'une assemblée générale
     *
     * @param  int $idAG id d'une AG
     * @return boolean
     *      - true : il existe une contrainte empêchant la suppression
     *      - false : il n'éxiste aucune contrainte
     */
    private function _verifierDateAgNonUtilisee(int $idAG)
	{
		$req1 = 'SELECT ass_idEntree, ass_idSortie
                FROM t_societaire_soc
                WHERE ass_idEntree = ?
                OR ass_idSortie = ?;';

        $req2 = 'SELECT ass_id
                FROM t_operation_ope
                WHERE ass_id = ?;';

		$result1 = $this->CI->db->query($req1, array($idAG, $idAG))->result_array();
        $result2 = $this->CI->db->query($req2, array($idAG))->result_array();

        if((isset($result1) && $result1 !== null && sizeof($result1) > 0) || (isset($result2) && $result2 !== null && sizeof($result2) > 0)){
            return true;
        } else {
            return false;
        }
	}
}
?>
