<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

use PhpOffice\PhpSpreadsheet\Style;

class PartO
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Le societaire
     *
     * @access protected
     * @var Societaire
     */
    protected $societaire;

    /**
     * les mouvements
     *
     * @access protected
     * @var Mouvement
     */
    protected $mouvement;

    /**
     * Format du fichier
     *
     * @var int
     */
    protected $format;

    /**
     * Constructeur de la classe
     *
     * @param  int $format Le format utilisé, 0 : .Csv, 1 : .Xlsx
     * @return void
     */
    public function __construct(int $format)
    {
        $this->CI = &get_instance();
        $this->CI->load->helper('download');

        $this->societaire = new Societaire();
        $this->mouvement = new Mouvement();
        $this->format = $format;
    }

    /**
     * Exporte les données des sociétaires
     *
     * @return void
     */
    public function export()
    {
        $spreadsheet = new Spreadsheet();

        // Style par defaut
        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(12);

        // Style de l'entête 1
        $enteteStyle = [
            'font' => [
                'bold' => true
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => ['argb' => 'ff33cc99']
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ];

        // Style des données
        $donneesStyle = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ];

        // Création de l'entête
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Numéro de la part')
            ->setCellValue('B1', 'Nature du mouvement')
            ->setCellValue('C1', 'Date de l\'opération')
            ->setCellValue('D1', 'Valeur de la part')
            ->setCellValue('E1', 'Nom')
            ->setCellValue('F1', 'Prénom')
            ->setCellValue('G1', 'Identifiant');
        $spreadsheet->getActiveSheet()->setTitle('Parts');
        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($enteteStyle);

        foreach (range('A', 'G') as $colonne) {
            $spreadsheet->getActiveSheet()->getColumnDimension($colonne)->setAutoSize(true);
        }

        // Ajout des données des mouvements
        $listeMouvements = $this->mouvement->obtenirListeMouvementTriPart();

        $partIndex = 1;
        foreach ($listeMouvements as $mouv) {
            if (!isset($traite[$mouv["mou_id"]])){ //Si cet id n'a pas encore été traité
                $current_id = $mouv["mou_id"];

                // Nature du mouvement
                $natureMouvement = '';
                switch ($mouv['ope_type']) {
                    case 0:
                        $natureMouvement = 'Création';
                        break;
                    case 1:
                        $natureMouvement = 'Transmission entre vifs';
                        break;
                    case 2:
                        $natureMouvement = 'Transmission par décès';
                        break;
                    case 3:
                        $natureMouvement = 'Vente';
                        break;
                }

                // Sociétaires
                $beneficiaire = $this->societaire->obtenirInfos($mouv['soc_loginBeneficiaire']);
                $nomBeneficiaire = $beneficiaire['soc_nom'];
                $prenomBeneficiaire = $beneficiaire['soc_prenom'];
                $loginBeneficiaire = $mouv['soc_loginBeneficiaire'];

                $partIndex = $partIndex + 1;

                $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $partIndex, $mouv["mou_numPart"])
                    ->setCellValue('B' . $partIndex, $natureMouvement)
                    ->setCellValue('C' . $partIndex, $mouv['ass_date'])
                    ->setCellValue('D' . $partIndex, $mouv['ope_valeurUnitaire'])
                    ->setCellValue('E' . $partIndex, $nomBeneficiaire)
                    ->setCellValue('F' . $partIndex, $prenomBeneficiaire)
                    ->setCellValue('G' . $partIndex, $loginBeneficiaire);

                $traite[$mouv["mou_id"]]=1;
            }
        }

        $spreadsheet->getActiveSheet()->getStyle('A2:G' . $partIndex)->applyFromArray($donneesStyle);

        // Sauvegarde du fichier
        switch ($this->format) {
            case 1:
                $writerFormat    = 'Xlsx';
                $saveFormat        = '.xlsx';
                break;
            default:
                $writerFormat    = 'Csv';
                $saveFormat        = '.csv';
                break;
        }

        $writer = IOFactory::createWriter($spreadsheet, $writerFormat);
        $writer->save(FCPATH . 'registres/export/registreParts' . $saveFormat);
        force_download(FCPATH . 'registres/export/registreParts' . $saveFormat, NULL);
    }
}
