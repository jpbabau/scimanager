<?php

/**
 * Classe Mouvement
 */
class Mouvement
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Constructeur de la classe Mouvement
     *
     * @return void
     */
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->database();
    }

    /**
     * ajoute un mouvement
     *
     * @param  string $titulaire titulaire de la part
     * @param  string $beneficiaire acheteur d'une part
     * @param  array $listePart liste des parts du mouvement
     * @param  int $ag assemblée générale lié au mouvement
     * @param  float $valeurUnitaire valeur d'une part
     * @param  int $type type de mouvement 0, 1, 2 ou 3
     * @param array<int,<int,string>> tableau des paiements d'un mouvement, type de paiement (int) associé à un numéro de paiement (string)
     * @return int 
     *      - 0 : le mouvement est ajouté
     *      - 1 : un des paramètre obligatoire est vide, le mouvement n'est pas créé
     *      - 2 : un des paramètre est trop long, le mouvement n'est pas créé
     *      - 3 : le titulaire ou le bénéficiaire n'existe pas, le mouvement n'est pas créé
     *      - 4 : la liste des parts n'est pas correcte, elle doit être composé uniquement d'entiers uniques, le mouvement n'est pas créé
     *      - 5 : une des parts existe déjà, impossible de créer une part déjà éxistante, le mouvement n'est pas créé
     *      - 6 : le titulaire ne possède pas une des parts qu'il essaye de vendre ou transmettre, le mouvement n'est pas créé
     */
    public function ajouter($titulaire=NULL, string $beneficiaire=NULL, array $listePart=NULL, int $ag=NULL, float $valeurUnitaire=NULL, int $type=NULL, array $paiement=NULL)
    {
        if($type===1 || $type===2){
            $paiement = array();
			$nombrePaiements = 1;
			$paiType	= 5;
			$paiNum		= '0';
			$paiement[] = array($paiType => $paiNum);
        }
        if (isset($beneficiaire) && strlen($beneficiaire) > 0 && isset($listePart) && sizeof($listePart) > 0 && isset($ag) && $ag >= 0 && isset($valeurUnitaire) && $valeurUnitaire > 0.0 
                && ((isset($titulaire) && strlen($titulaire) > 0 && $type!==0  && $titulaire!==$beneficiaire) || (!isset($titulaire) && $type===0))
                && isset($type) && $type >= 0 && isset($paiement) && sizeof($paiement) > 0) {
            if (strlen($titulaire) <= 45 && strlen($beneficiaire) <= 45 && $type <= 3) {
                $loginTitulaire = $this->_obtenirLoginSocietaireBdd($titulaire);
                $loginBeneficiaire = $this->_obtenirLoginSocietaireBdd($beneficiaire);
                if ((!(isset($titulaire) && strlen($titulaire) > 0) || (isset($loginTitulaire) && $loginTitulaire !== null)) && isset($loginBeneficiaire) && $loginBeneficiaire !== null) {
                    if (array_filter($listePart, 'is_int') === $listePart && count($listePart) === count(array_unique($listePart))) {
                        if (($type === 0 && $this->_verifierExistancePartBdd($listePart) === 0) || $type !== 0) {
                            if ($type === 0 || $this->_verifierExistancePartSocietaireBdd($titulaire, $listePart) === 1) {
                                $nombrePart = count($listePart);
                                $this->_ajouterBdd($titulaire, $beneficiaire, $listePart, $ag, $nombrePart, $valeurUnitaire, $type, $paiement);
                                return 0;
                            } else {
                                return 6;
                            }
                        } else {
                            return 5;
                        }
                    } else {
                        return 4;
                    }
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Modifie un mouvement
     *
     * @param  string $titulaire titulaire de la part
     * @param  string $beneficiaire acheteur d'une part
     * @param  array $listePart liste des parts du mouvement
     * @param  int $ag assemblée générale lié au mouvement
     * @param  float $valeurUnitaire valeur d'une part
     * @param  array<int,<int,string>> tableau des paiements d'un mouvement, type de paiement (int) associé à un numéro de paiement (string)
     * @return int 
     *      - 0 : le mouvement est modifié
     *      - 1 : un des paramètre obligatoire est vide, le mouvement n'est pas modifié
     *      - 2 : un des paramètre est trop long, le mouvement n'est pas modifié
     *      - 3 : le titulaire ou le bénéficiaire n'éxiste pas, le mouvement n'est pas modifié
     *      - 4 : la liste des parts n'est pas correcte, elle doit être composé uniquement d'entier unique, le mouvement n'est pas modifié
     *      - 5 : une des parts existe déjà, impossible de créer une part déjà existante, le mouvement n'est pas modifié
     *      - 6 : le titulaire ne possède pas une des parts qu'il essaye de vendre ou de transmettre, le mouvement n'est pas modifié
     */
    public function modifierMouvement(int $mouvementId, $titulaire, string $beneficiaire, array $listePart, int $ag, float $valeurUnitaire, array $paiement)
    {
        $mouvement= $this->obtenirMouvement($mouvementId);
        $type = $mouvement[0][ope_type];
        if($type==1 || $type==2){
            $paiement = array();
			$nombrePaiements = 1;
			$paiType	= 5;
			$paiNum		= '0';
			$paiement[] = array($paiType => $paiNum);
        }
        if (isset($mouvementId) && strlen($mouvementId) > 0 && isset($beneficiaire) && strlen($beneficiaire) > 0 && isset($listePart) && sizeof($listePart) > 0 && isset($ag) && $ag >= 0 && isset($valeurUnitaire) && $valeurUnitaire > 0.0 && isset($type) && $type >= 0 && isset($paiement) && sizeof($paiement) > 0) {
            if (strlen($titulaire) <= 45 && strlen($beneficiaire) <= 45 && $type <= 3) {
                $loginTitulaire = $this->_obtenirLoginSocietaireBdd($titulaire);
                $loginBeneficiaire = $this->_obtenirLoginSocietaireBdd($beneficiaire);
                if ((!(isset($titulaire) && strlen($titulaire) > 0) || (isset($loginTitulaire) && $loginTitulaire !== null)) && isset($loginBeneficiaire) && $loginBeneficiaire !== null) {
                    if (array_filter($listePart, 'is_int') === $listePart && count($listePart) === count(array_unique($listePart))) {
                        if ($type == 0 ) {
                            if ($this->_verifierExistancePartFiltreBdd($mouvementId, $listePart) === 0) {
                                $nombrePart = count($listePart);
                                $this->_modifierBdd($mouvementId, $titulaire, $beneficiaire, $listePart, $ag, $nombrePart, $valeurUnitaire, $type, $paiement);
                                return 0;
                            } else {
                                return 5;
                            }
                        } else {
                            if ($this->_verifierExistancePartSocietaireFiltreBdd($mouvementId, $titulaire, $listePart) === 1) {
                                $nombrePart = count($listePart);
                                $this->_modifierBdd($mouvementId, $titulaire, $beneficiaire, $listePart, $ag, $nombrePart, $valeurUnitaire, $type, $paiement);
                                return 0;
                            } else {
                                return 6;
                            }
                        } 
                    } else {
                        return 4;
                    }
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Récupère toutes les information d'un mouvement
     *
     * @param  int $idMouvement
     * @return array<int,array<string,string>> tableau qui contient toutes les informations d'un mouvement, retourne null si le résultat est vide
     * 
     *     $infos = [<br>
     *          0 => [<br>
     *          "ope_id"            =>  (id) id du mouvement<br>
     *          "soc_loginTitulaire"    =>  (string) login du titulaire<br>
     *          "soc_loginBeneficiaire" =>  (string) login du bénéficiaire<br>
     *          "ass_date"          =>  (date) date ag du mouvement<br>
     *          "ope_nombrePart"    =>  (int) nombre de parts<br>
     *          "ope_valeurUnitaire"    =>  (double) valeur d'une part<br>
     *          "ope_type"          =>  (int) type d'opération (creation, destruction, vente)<br>
     *          "moy_id"            =>  (int) id du type de paiement<br>
     *          "pai_numero"    =>  (string) numéro de paiement<br>
     *          ]<br>
     *         1 => [ ... ]<br>
     *          ... <br>
     *      ]
     */
    public function obtenirMouvement(int $idMouvement)
    {
        $mouv = $this->_obtenirMouvementBdd($idMouvement);
        if (isset($mouv) && $mouv !== null && sizeof($mouv) >= 0) {
            return $mouv;
        } else {
            return null;
        }
    }

    /**
     * Récupère toutes les information de tous les mouvements avec tri par part
     *
     * @return array<int,array<string,string>> tableau qui contient toutes les informations de tous les mouvements, retourne null si le résultat est vide
     * 
     *     $infos = [<br>
     *          0 => [<br>
     *          "ope_id"            =>  (id) id du mouvement<br>
     *          "soc_loginTitulaire"    =>  (string) login du titulaire<br>
     *          "soc_loginBeneficiaire" =>  (string) login du bénéficiaire<br>
     *          "ass_date"          =>  (date) date ag du mouvement<br>
     *          "ope_nombrePart"    =>  (int) nombre de parts<br>
     *          "ope_valeurUnitaire"    =>  (double) valeur d'une part<br>
     *          "ope_type"          =>  (int) type d'opération (creation, destruction, vente)<br>
     *          "moy_id"            =>  (int) id du type de paiement<br>
     *          "pai_numero"    =>  (string) numéro de paiement<br>
     *          ]<br>
     *         1 => [ ... ]<br>
     *          ... <br>
     *      ]
     */
    public function obtenirListeMouvementTriPart()
    {
        $mouvs = $this->_obtenirListeMouvementTriPartBdd();
        if (isset($mouvs) && $mouvs !== null && sizeof($mouvs) >= 0) {
            return $mouvs;
        } else {
            return null;
        }
    }

    /**
     * Récupère toutes les information de tous les mouvements
     *
     * @return array<int,array<string,string>> tableau qui contient toutes les informations de tous les mouvements, retourne null si le résultat est vide
     * 
     *     $infos = [<br>
     *          0 => [<br>
     *          "ope_id"            =>  (id) id du mouvement<br>
     *          "soc_loginTitulaire"    =>  (string) login du titulaire<br>
     *          "soc_loginBeneficiaire" =>  (string) login du bénéficiaire<br>
     *          "ass_date"          =>  (date) date ag du mouvement<br>
     *          "ope_nombrePart"    =>  (int) nombre de parts<br>
     *          "ope_valeurUnitaire"    =>  (double) valeur d'une part<br>
     *          "ope_type"          =>  (int) type d'opération (creation, destruction, vente)<br>
     *          "moy_id"            =>  (int) id du type de paiement<br>
     *          "pai_numero"    =>  (string) numéro de paiement<br>
     *          ]<br>
     *         1 => [ ... ]<br>
     *          ... <br>
     *      ]
     */
    public function obtenirListeMouvement()
    {
        $mouvs = $this->_obtenirListeMouvementBdd();
        if (isset($mouvs) && $mouvs !== null && sizeof($mouvs) >= 0) {
            return $mouvs;
        } else {
            return null;
        }
    }

    /**
     * Récupère toutes les parts d'un sociétaire
     *
     * @param  string $login login d'un sociétaire
     * @return mixed tableau qui contient toutes les parts d'un sociétaire, retourne null si le résultat est vide
     *
     */
    public function obtenirPartSocietaire(string $login)
    {
        $parts = $this->_obtenirPartSocietaireBdd($login);
        if (isset($parts) && $parts !== null && sizeof($parts) >= 0) {
            $result = array();
            foreach ($parts as $part) {
                array_push($result, $part['mou_numPart']);
            }
            return $result;
        } else {
            return null;
        }
    }

    /**
     * Supprime un mouvement et toutes ses parts associés
     *
     * @param  int $idMouvement identifiant d'un mouvement
     * @return void
     */
    public function supprimerMouvement(int $idMouvement)
    {
        $mouvId = $this->_obtenirIdMouvementBdd($idMouvement);
        if (isset($mouvId->ope_id) && $mouvId->ope_id !== null) {
            $this->_supprimerBdd($idMouvement);
        }
    }

    /**
     * Récupère toutes les informations d'un mouvement
     *
     * @access private
     * @param int $idMouvement identifiant d'un mouvement
     * @return object
     */
    private function _obtenirMouvementBdd(int $idMouvement)
    {
        $req = "SELECT * FROM t_operation_ope
            JOIN t_mouvement_mou USING(ope_id)
            JOIN t_paiement_pai USING(ope_id)
            JOIN t_moyenPai_moy USING(moy_id)
            JOIN t_assembleeGenerale_ass USING(ass_id)
            WHERE ope_id = ?
            ORDER BY mou_numPart, moy_id, pai_numero;";

        $result = $this->CI->db->query($req, array($idMouvement))->result_array();
        return $result;
    }

    /**
     * Récupère toutes les informations de tous les mouvements
     *
     * @access private
     * @return object
     */
    private function _obtenirListeMouvementBdd()
    {
        $req = "SELECT * FROM t_operation_ope
        JOIN t_mouvement_mou USING(ope_id)
        JOIN t_paiement_pai USING(ope_id)
        JOIN t_moyenPai_moy USING(moy_id)
        JOIN t_assembleeGenerale_ass USING(ass_id)
        ORDER BY ope_id, mou_numPart, moy_id, pai_numero;";

        $result = $this->CI->db->query($req)->result_array();
        return $result;
    }

    /**
     * Récupère toutes les informations de tous les mouvements avec tri par part
     *
     * @access private
     * @return object
     */
    private function _obtenirListeMouvementTriPartBdd()
    {
        $req = "SELECT * FROM t_operation_ope
        JOIN t_mouvement_mou USING(ope_id)
        JOIN t_paiement_pai USING(ope_id)
        JOIN t_moyenPai_moy USING(moy_id)
        JOIN t_assembleeGenerale_ass USING(ass_id)
        ORDER BY mou_numPart, ope_id, moy_id, pai_numero;";

        $result = $this->CI->db->query($req)->result_array();
        return $result;
    }

    /**
     * Récupère toutes les parts détenus par un sociétaire
     *
     * @access private
     * @param string $login login d'un titulaire de part
     * @return object
     */
    private function _obtenirPartSocietaireBdd(string $login)
    {
        $req = "SELECT mou_numPart FROM(
            SELECT mou_numPart, soc_loginTitulaire, soc_loginBeneficiaire, ope_type FROM (
                SELECT * FROM t_operation_ope ope
                JOIN t_mouvement_mou USING(ope_id)
                ORDER BY ope_id DESC) tmp1
            GROUP BY mou_numPart) tmp2
        WHERE soc_loginBeneficiaire = ?
        ORDER BY mou_numPart;";

        $result = $this->CI->db->query($req, array($login))->result_array();
        return $result;
    }

    /**
     * Récupère toutes les parts détenus par un sociétaire en filtrant les parts d'un mouvement
     *
     * @access private
	 * @param int $ope_id l'id du mouvement à filtrer
     * @param string $login login d'un titulaire de part
     * @return object
     */
	private function _obtenirPartSocietaireFiltreBdd(int $ope_id, string $login)
    {
        $req = "SELECT mou_numPart FROM(
            SELECT mou_numPart, soc_loginTitulaire, soc_loginBeneficiaire, ope_type FROM (
                SELECT * FROM t_operation_ope ope
                JOIN t_mouvement_mou USING(ope_id)
				WHERE ope_id != ?
                ORDER BY ope_id DESC) tmp1
            GROUP BY mou_numPart) tmp2
        WHERE (soc_loginBeneficiaire = ?);";

        $result = $this->CI->db->query($req, array($ope_id, $login))->result_array();
        return $result;
    }

    /**
     * Récupère le login d'un sociétaire
     *
     * @access private
     * @param  string|null $login login d'un sociétaire
     * @return object
     */
    private function _obtenirLoginSocietaireBdd($login)
    {
        $req = "SELECT soc_login
        FROM t_societaire_soc
        WHERE soc_login = ?;";

        $result = $this->CI->db->query($req, array($login))->row();
        return $result;
    }

    /**
     * Récupère l'id d'un mouvement
     *
     * @access private
     * @param  int $idMouvement identifiant d'un mouvement
     * @return object
     */
    private function _obtenirIdMouvementBdd(int $idMouvement)
    {
        $req = "SELECT ope_id
        FROM t_operation_ope
        WHERE ope_id = ?;";

        $result = $this->CI->db->query($req, array($idMouvement))->row();
        return $result;
    }

    /**
     * vérifie l'éxistence d'une liste de part
     *
     * @access private
     * @param  array $listePart liste des parts
     * @return int
     *      - 0 : il n'existe aucune part
     *      - 1 : toutes les parts existent
     *      - 2 : au moins une part existe
     */
    private function _verifierExistancePartBdd(array $listePart)
    {
        $req = "SELECT mou_numPart FROM t_operation_ope ope
            JOIN t_mouvement_mou USING(ope_id)
            WHERE ope_type = 0";

        $result = $this->CI->db->query($req)->result_array();

        if (sizeof($result) <= 0) return 0;
        $nbPartExiste = 0;
        foreach ($result as $row) {
            if (in_array($row['mou_numPart'], $listePart)) {
                $nbPartExiste = $nbPartExiste + 1;
            }
        }
        if ($nbPartExiste === 0) {
            return 0;
        } else {
            if ($nbPartExiste === count($listePart)) {
                return 1; 
            } else {
                return 2;
            }
        }
    }
	
	/**
	 * vérifie l'existence d'une liste de parts en filtrant les parts d'un mouvement de création
	 *
	 * @access private
	 * @param  int $ope_id l'id du mouvement à filtrer
     * @param  array $listePart liste des parts
     * @return int
     *      - 0 : il n'existe aucune part
     *      - 1 : toutes les parts existent
     *      - 2 : au moins une part existe
	 */
	private function _verifierExistancePartFiltreBdd(int $ope_id, array $listePart)
    {
        $req = "SELECT mou_numPart FROM t_operation_ope ope
        JOIN t_mouvement_mou USING(ope_id)
        WHERE ope_type = 0 AND ope_id != $ope_id";

        $result = $this->CI->db->query($req)->result_array();

        if (sizeof($result) <= 0) return 0;
        $nbPartExiste = 0;
        foreach ($result as $row) {
            if (in_array($row['mou_numPart'], $listePart)) {
                $nbPartExiste = $nbPartExiste + 1;
            }
        }
        if ($nbPartExiste === 0) {
            return 0;
        } else {
            if ($nbPartExiste === count($listePart)) {
                return 1; 
            } else {
                return 2;
            }
        }
    }

    /**
     * vérifie l'éxistence d'une liste de part d'un sociétaire
     *
     * @access private
	 * @param  string $login le login du sociétaire
     * @param  array $listePart liste des parts
     * @return int
     *      - 0 : il n'existe aucune part
     *      - 1 : toutes les parts existent
     */
    private function _verifierExistancePartSocietaireBdd(string $login, array $listePart)
    {
        $result = $this->_obtenirPartSocietaireBdd($login);

        if (sizeof($result) <= 0) return 0;
        $nbPartExiste = 0;
        foreach ($result as $row) {
            if (in_array($row['mou_numPart'], $listePart)) {
                $nbPartExiste = $nbPartExiste + 1;
            }
        }
        if ($nbPartExiste === 0) {
            return 0;
        } elseif ($nbPartExiste === count($listePart)) {
            return 1;
        }
    }
	
	/**
	 * vérifie l'éxistence d'une liste de part d'un sociétaire en filtrant les parts d'un mouvement
	 *
	 * @access private
	 * @param  int $ope_id l'id du mouvement à filtrer
	 * @param  string $login le login du sociétaire
     * @param  array $listePart liste des parts
     * @return int
     *      - 0 : il n'éxiste aucune part
     *      - 1 : toute les parts éxiste
	 * @return void
	 */
	private function _verifierExistancePartSocietaireFiltreBdd(int $ope_id, string $login, array $listePart)
    {
        $result = $this->_obtenirPartSocietaireFiltreBdd($ope_id, $login);

        if (sizeof($result) <= 0) return 0;
        $nbPartExiste = 0;
        foreach ($result as $row) {
            if (in_array($row['mou_numPart'], $listePart)) {
                $nbPartExiste = $nbPartExiste + 1;
            }
        }
        if ($nbPartExiste === 0) {
            return 0;
        } elseif ($nbPartExiste === count($listePart)) {
            return 1;
        }
    }

    /**
     * modifie un mouvement et les parts associés
     *
     * @access private
     * @param  int $opeId id du mouvement
     * @param  string $titulaire titulaire de la part
     * @param  string $beneficiaire acheteur d'une part
     * @param  array $listePart liste des parts du mouvement
     * @param  int $ag assemblée générale lié au mouvement
     * @param  float $valeurUnitaire valeur d'une part
     * @param  int $type type de mouvement 0, 1 ou 2
     * @param array<int,<int,string>> tableau des paiements d'un mouvement, type de paiement (int) associé à un numéro de paiement (string)
     * @return void
     */
    private function _modifierBdd(int $opeId, $titulaire, $beneficiaire, array $listePart, int $ag, int $nombrePart, float $valeurUnitaire, int $type, array $paiement)
    {
        // mise à jour opération
        $req1 = "UPDATE t_operation_ope
            set soc_loginTitulaire = ?,
            soc_loginBeneficiaire = ?,
            ass_id = ?,
            ope_nombrePart = ?,
            ope_valeurUnitaire = ?,
            ope_type = ?
            WHERE ope_id = ?;";

        $this->CI->db->query($req1, array($titulaire, $beneficiaire, $ag, $nombrePart, $valeurUnitaire, $type, $opeId));

        // suppresion des anciens mouvements de parts liées au mouvement en cours
        $req2 = "DELETE FROM t_mouvement_mou
            WHERE ope_id = ?;";

        $this->CI->db->query($req2, array($opeId));

        // ajouts des nouveaux mouvements de parts liées au mouvement en cours
        $req3 = "INSERT INTO t_mouvement_mou
            VALUES (null, ?, ?);";

        foreach ($listePart as &$part) {
            $this->CI->db->query($req3, array($part, $opeId));
        }

        // suppression des ancien paiements liés au mouvement en cours
        $req4 = "DELETE FROM t_paiement_pai
        WHERE ope_id = ?;";

        $this->CI->db->query($req4, array($opeId));

        // ajout des nouveaux paiements liés au mouvement en cours
        $req5 = "INSERT INTO t_paiement_pai
        VALUES (?, ?, ?);";

        foreach ($paiement as $pai) {
            $this->CI->db->query($req5, array(key($pai), $opeId, $pai[key($pai)]));
        }
    }

    /**
     * ajoute un mouvement et les parts associés
     *
     * @access private
     * @param  string $titulaire titulaire de la part
     * @param  string $beneficiaire acheteur d'une part
     * @param  array $listePart liste des parts du mouvement
     * @param  int $ag assemblée générale lié au mouvement
     * @param  float $valeurUnitaire valeur d'une part
     * @param  int $type type de mouvement 0, 1 ou 2
     * @param array<int,<int,string>> tableau des paiements d'un mouvement, type de paiement (int) associé à un numéro de paiement (string)
     * @return void
     */
    private function _ajouterBdd($titulaire, $beneficiaire, array $listePart, int $ag, int $nombrePart, float $valeurUnitaire, int $type, array $paiement)
    {
        $req1 = "INSERT INTO t_operation_ope
        VALUES (null, ?, ?, ?, ?, ?, ?);";

        $req2 = "SET @id := (SELECT MAX(ope_id) from t_operation_ope);";

        $req3 = "INSERT INTO t_mouvement_mou
        VALUES (null, ?, @id);";

        $req4 = "INSERT INTO t_paiement_pai
        VALUES (?, @id, ?);";

        $this->CI->db->query($req1, array($titulaire, $beneficiaire, $ag, $nombrePart, $valeurUnitaire, $type));
        $this->CI->db->query($req2);

        foreach ($listePart as &$part) {
            $this->CI->db->query($req3, array($part));
        }

        foreach ($paiement as $pai) {
            $this->CI->db->query($req4, array(key($pai), $pai[key($pai)]));
        }
    }

    /**
     * Supprime toutes les information d'un mouvement et les parts associés
     *
     * @param  int $idMouvement identifiant d'un mouvement
     * @return void
     */
    private function _supprimerBdd(int $idMouvement)
    {
        $req1 = "DELETE FROM t_mouvement_mou
            WHERE ope_id = ?;";

        $req2 = "DELETE FROM t_paiement_pai
        WHERE ope_id = ?;";

        $req3 = "DELETE FROM t_operation_ope
            WHERE ope_id = ?;";

        $this->CI->db->query($req1, array($idMouvement));
        $this->CI->db->query($req2, array($idMouvement));
        $this->CI->db->query($req3, array($idMouvement));
    }
}
