<?php

require_once APPPATH . 'libraries/Array_Config_Writer.php';

/**
 * Classe Installation 
 */
class Installation
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Constructeur de la classe Societaire
     *
     * @return void
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /**
     * Modifie les paramètres d'envois de mails
     *
     * @param  string $host adresse de l'hote
     * @param  string $port numéro de port
     * @param  string $user nom d'utilisateur (généralement l'adresse email)
     * @param  string $password mot de passe (généralement le mot de passe du compte email)
     * @param  string $crypto méthode de chiffrage (ssl ou tls)
     * @return void
     */
    public function modifierEmail(string $host, string $port, string $user, string $password, string $crypto)
    {
        $config_writer = new Array_Config_Writer(APPPATH . '/config/email.php', 'config');
        $config_writer->write('smtp_host', $host)->write('smtp_port', $port)->write('smtp_user', $user)->write('smtp_pass', $password)->write('smtp_crypto', $crypto);
    }

    /**
     * Envoi un email de test
     *
     * @param  string $email adresse email du destinataire
     * @return bool
     *      - true : l'email a été envoyé
     *      - false : un problème est survenu, l'email n'est pas envoyé
     */
    public function envoyerEmail(string $email)
    {
		error_reporting(0);

        $this->CI->load->library('email');

        $this->CI->email->from($email, "SCIManager");
        $this->CI->email->to($email);
        $this->CI->email->subject('[SCIManager] Test envoi email');
        $this->CI->email->message('
        <div style="font-family: helvetica">
            <h2>Email de test</h2>
            <br>
            <p>La bonne réception de cet email indique que les paramètres d\'envois d\'email de votre site sont correct</p>
        </div>');
        $test = $this->CI->email->send();

		unset($this->CI->email);
		
		return $test;
    }

    /**
     * Modififie les paramètres de la base de données
     *
     * @param  string $hostname nom d'hote de la base de données
     * @param  string $username nom d'utilisateur de la base de données
     * @param  string $password mot de passe de la base de données
     * @param  string $databaseName nom de la base de données
     * @return void
     */
    public function modifierBD(string $hostname, string $username, string $password, string $databaseName)
    {
        $config_writer = new Array_Config_Writer(APPPATH . '/config/database.php', 'db');

        $config_writer->write(array('default'), array(
            'dsn'   => '',
            'hostname' => $hostname,
            'username' => $username,
            'password' => $password,
            'database' => $databaseName,
            'dbdriver' => 'mysqli',
            'dbprefix' => '',
            'pconnect' => TRUE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array()));
    }

    /**
     * Teste la connexion avec la base de données
     *
     * @return bool
     *      - true : Il y a une connexion avec la base de données
     *      - false : Impossible de se connecter à la base de données
     */
    public function testerConnexionBD()
    {
		error_reporting(0);
		if (file_exists($file_path = APPPATH.'config/database.php')) {
			include($file_path);
		}
		
		$config = $db[$active_group];

		if ($config['dbdriver'] === 'mysqli') {
			$mysqli = new mysqli( $config['hostname'] , $config['username'] , $config['password'] , $config['database'] );
			$test = !$mysqli->connect_error;
			$mysqli->close();
		}

		return $test;
    }
	
	/**
	 * Teste si la base de données est vide
	 *
	 * @return bool
     *      - true : La table site existe, donc la base de données a été initialisée
     *      - false : La table site n'existe pas, donc la base de données n'a pas été initialisée
	 */
	public function testerVideBD()
	{
        error_reporting(0);
		$this->CI->load->database();
		$result = $this->CI->db->table_exists('t_site_sit');
		$this->CI->db->close();

		return $result;
	}

    /**
     * Génére la base de données
     *
     * @param  string $login le login de l'administrateur
     * @param  string $mdp le mot de passe de l'administrateur
     * @param  string $email l'email de l'administrateur
     * @param  string $nom le nom du site
     * @return int
	 * 		- 0 : La base de données a été générée
	 * 		- 1 : L'un des paramètres est vide, la base de données n'est pas générées
	 * 		- 2 : L'un des paramètres est trop long, la base de données n'est pas générées
	 * 		- 3 : Le nom du site n'est pas compris entre 5 et 15 caractères, la base de données n'est pas générées
	 * 		- 4 : Le format du mot de passe n'est pas valide, la base de données n'est pas générées
	 * 		- 5 : Le format de l'email n'est pas valide, la base de données n'est pas générées
     */
    public function initBD(string $login, string $mdp, string $email, string $nom)
    {
		if (!isset($login) || strlen($login) === 0 || !isset($mdp) || strlen($mdp) === 0 || !isset($email) || strlen($email) === 0 || !isset($nom) || strlen($nom) === 0) {
			return 1;
		}

		if (strlen($login) > 45 || strlen($email) > 45 || strlen($mdp) > 50) {
			return 2;
		}

		if (strlen($nom) < 5 || strlen($nom) > 15) {
			return 3;
		}
		
		if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#', $mdp)) {
			return 4;
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return 5;
		}

		$password = password_hash($mdp, PASSWORD_BCRYPT);

        $this->CI->load->database();
        $database = $this->CI->db->database;
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_statut_sta` (
          `sta_id` INT NOT NULL AUTO_INCREMENT,
          `sta_nom` VARCHAR(45) NULL,
          PRIMARY KEY (`sta_id`))
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        INSERT INTO `t_statut_sta` (`sta_id`, `sta_nom`) VALUES
        (NULL, 'Physique'),
        (NULL, 'Association'),
        (NULL, 'GAEC'),
        (NULL, 'SARL'),
        (NULL, 'SCOOP'),
        (NULL, 'SCI'),
        (NULL, 'SCIC'),
        (NULL, 'SERL');");
        
        $this->CI->db->query("
        UPDATE `t_statut_sta`
        SET `sta_id` = 0
        WHERE `sta_id` = 1;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_regime_reg` (
          `reg_id` INT NOT NULL AUTO_INCREMENT,
          `reg_nom` VARCHAR(45) NULL,
          `reg_type` TINYINT NULL,
          PRIMARY KEY (`reg_id`))
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        INSERT INTO `t_regime_reg` (`reg_id`, `reg_nom`, `reg_type`) VALUES
        (NULL, 'Célibataire', 0),
        (NULL, 'Sous communauté de bien', 0),
        (NULL, 'Séparation de biens', 0),
        (NULL, 'micro', 1),
        (NULL, 'réel', 1);");
        
        $this->CI->db->query("
        UPDATE `t_regime_reg`
        SET `reg_id` = 0
        WHERE `reg_id` = 1;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_assembleeGenerale_ass` (
          `ass_id` INT NOT NULL AUTO_INCREMENT,
          `ass_date` DATE NULL,
          PRIMARY KEY (`ass_id`))
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        INSERT INTO `t_assembleeGenerale_ass` (`ass_id`, `ass_date`) VALUES
        (0, '2021-07-07');");
        
        $this->CI->db->query("
        UPDATE `t_assembleeGenerale_ass`
        SET `ass_id` = 0
        WHERE `ass_id` = 1;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_societaire_soc` (
          `soc_login` VARCHAR(45) NOT NULL,
          `soc_mot_de_passe` CHAR(60) NULL,
          `soc_prenom` VARCHAR(45) NULL,
          `soc_autrePrenom` VARCHAR(250) NULL,
          `soc_nom` VARCHAR(45) NULL,
          `soc_nomUsuel` VARCHAR(45) NULL,
          `soc_representantsLegaux` VARCHAR(128) NULL,
          `soc_civilite` INT NULL,
          `sta_id` INT NULL,
          `soc_siret` VARCHAR(15) NULL,
          `soc_naissance` DATE NULL,
          `soc_lieuNaissance` VARCHAR(45) NULL,
          `soc_nationalite` VARCHAR(45) NULL,
          `soc_email` VARCHAR(45) NULL,
          `soc_telephoneFixe` VARCHAR(20) NULL,
          `soc_telephonePortable` VARCHAR(20) NULL,
          `soc_adresse` VARCHAR(45) NULL,
          `soc_codePostal` VARCHAR(11) NULL,
          `soc_ville` VARCHAR(45) NULL,
          `soc_pays` VARCHAR(45) NULL,
          `reg_id` INT NOT NULL,
          `ass_idEntree` INT NOT NULL,
          `ass_idSortie` INT NULL,
          `soc_pasAppelerFixe` TINYINT NULL,
          `soc_pasAppelerPortable` TINYINT NULL,
          `soc_pasEnvoyerSms` TINYINT NULL,
          `soc_pasEnvoyerEmail` TINYINT NULL,
          `soc_autoriserCloud` TINYINT NULL,
          `soc_tentativeConnexion` INT NULL,
          `soc_indivision` TINYINT NULL,
          `soc_etat` CHAR(1) NULL,
          PRIMARY KEY (`soc_login`),
          INDEX `fk_t_societaire_soc_t_statut_sta1_idx` (`sta_id` ASC),
          INDEX `fk_t_societaire_soc_t_regime_reg1_idx` (`reg_id` ASC),
          INDEX `fk_t_societaire_soc_t_assembleeGenerale_ass1_idx` (`ass_idEntree` ASC),
          INDEX `fk_t_societaire_soc_t_assembleeGenerale_ass2_idx` (`ass_idSortie` ASC),
          CONSTRAINT `fk_t_societaire_soc_t_statut_sta1`
            FOREIGN KEY (`sta_id`)
            REFERENCES `" . $database . "`.`t_statut_sta` (`sta_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_societaire_soc_t_regime_reg1`
            FOREIGN KEY (`reg_id`)
            REFERENCES `" . $database . "`.`t_regime_reg` (`reg_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_societaire_soc_t_assembleeGenerale_ass1`
            FOREIGN KEY (`ass_idEntree`)
            REFERENCES `" . $database . "`.`t_assembleeGenerale_ass` (`ass_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_societaire_soc_t_assembleeGenerale_ass2`
            FOREIGN KEY (`ass_idSortie`)
            REFERENCES `" . $database . "`.`t_assembleeGenerale_ass` (`ass_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_administrateur_adm` (
          `adm_login` VARCHAR(45) NOT NULL,
          `adm_email` VARCHAR(45) NULL,
          `adm_mot_de_passe` CHAR(60) NULL,
          `adm_validite` CHAR(1) NULL,
          PRIMARY KEY (`adm_login`))
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        INSERT INTO `t_administrateur_adm` (`adm_login`, `adm_email`, `adm_mot_de_passe`, `adm_validite`) VALUES
        ('". $login ."', '". $email ."', '". $password ."', 'A');");

        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_site_sit` (
          `sit_nom` VARCHAR(15) NOT NULL,
          `sit_icone` VARCHAR(45) NULL,
          `sit_image` VARCHAR(45) NULL,
          `sit_email` VARCHAR(45) NULL,
          `sit_structure` TINYINT NULL,
		      `sit_dureeRGPD` INT     NULL,
          `sit_local` TINYINT(1) NOT NULL DEFAULT 1,
          PRIMARY KEY (`sit_nom`))
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        INSERT INTO `t_site_sit` (`sit_nom`, `sit_icone`, `sit_image`, `sit_email`, `sit_structure`, `sit_dureeRGPD`) VALUES
        ('". $nom ."', 'icone_defaut.jpg', 'image_defaut.jpg', 'emailsci@example.com', 0, 24);");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_token_tok` (
          `tok_id` INT NOT NULL AUTO_INCREMENT,
          `tok_token` VARCHAR(32) NULL,
          `tok_dateCreation` TIMESTAMP NULL,
          `soc_login` VARCHAR(45) NULL,
          `adm_login` VARCHAR(45) NULL,
          PRIMARY KEY (`tok_id`),
          INDEX `fk_t_reinitialisationMdp_rei_t_utilisateur_uti_idx` (`soc_login` ASC),
          INDEX `fk_t_reinitialisationMdp_rei_t_administrateur_adm1_idx` (`adm_login` ASC),
          CONSTRAINT `fk_t_reinitialisationMdp_rei_t_utilisateur_uti`
            FOREIGN KEY (`soc_login`)
            REFERENCES `" . $database . "`.`t_societaire_soc` (`soc_login`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_reinitialisationMdp_rei_t_administrateur_adm1`
            FOREIGN KEY (`adm_login`)
            REFERENCES `" . $database . "`.`t_administrateur_adm` (`adm_login`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_gerant_ger` (
          `ger_id` INT NOT NULL AUTO_INCREMENT,
          `ger_dateDebut` DATE NULL,
          `ger_dateFin` DATE NULL,
          `soc_login` VARCHAR(45) NOT NULL,
          PRIMARY KEY (`ger_id`),
          INDEX `fk_t_gerant_ger_t_utilisateur_uti1_idx` (`soc_login` ASC),
          CONSTRAINT `fk_t_gerant_ger_t_utilisateur_uti1`
            FOREIGN KEY (`soc_login`)
            REFERENCES `" . $database . "`.`t_societaire_soc` (`soc_login`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_operation_ope` (
          `ope_id` INT NOT NULL AUTO_INCREMENT,
          `soc_loginTitulaire` VARCHAR(45) NULL,
          `soc_loginBeneficiaire` VARCHAR(45) NOT NULL,
          `ass_id` INT NOT NULL,
          `ope_nombrePart` INT NULL,
          `ope_valeurUnitaire` DOUBLE NULL,
          `ope_type` INT NULL,
          PRIMARY KEY (`ope_id`),
          INDEX `fk_t_mouvement_mou_t_utilisateur_uti1_idx` (`soc_loginBeneficiaire` ASC),
          INDEX `fk_t_operation_ope_t_societaire_soc1_idx` (`soc_loginTitulaire` ASC),
          INDEX `fk_t_operation_ope_t_assembleeGenerale_ass1_idx` (`ass_id` ASC),
          CONSTRAINT `fk_t_mouvement_mou_t_utilisateur_uti1`
            FOREIGN KEY (`soc_loginBeneficiaire`)
            REFERENCES `" . $database . "`.`t_societaire_soc` (`soc_login`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_operation_ope_t_societaire_soc1`
            FOREIGN KEY (`soc_loginTitulaire`)
            REFERENCES `" . $database . "`.`t_societaire_soc` (`soc_login`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_operation_ope_t_assembleeGenerale_ass1`
            FOREIGN KEY (`ass_id`)
            REFERENCES `" . $database . "`.`t_assembleeGenerale_ass` (`ass_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_moyenPai_moy` (
          `moy_id` INT NOT NULL AUTO_INCREMENT,
          `moy_type` VARCHAR(45) NULL,
          PRIMARY KEY (`moy_id`))
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        INSERT INTO `t_moyenPai_moy` (`moy_id`, `moy_type`) VALUES
        (NULL, 'Chèque'),
        (NULL, 'Virement'),
        (NULL, 'Apport en nature'),
        (NULL, 'Compte courant associé'),
        (NULL, 'Autre');");
        
        $this->CI->db->query("
        UPDATE `t_moyenPai_moy`
        SET `moy_id` = 0
        WHERE `moy_id` = 1;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_mouvement_mou` (
          `mou_id` INT NOT NULL AUTO_INCREMENT,
          `mou_numPart` INT NULL,
          `ope_id` INT NOT NULL,
          PRIMARY KEY (`mou_id`),
          INDEX `fk_t_mouvement_mou_t_operation_ope1_idx` (`ope_id` ASC),
          CONSTRAINT `fk_t_mouvement_mou_t_operation_ope1`
            FOREIGN KEY (`ope_id`)
            REFERENCES `" . $database . "`.`t_operation_ope` (`ope_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_societaireRepresente_socr` (
          `socr_id` INT NOT NULL AUTO_INCREMENT,
          `socr_prenom` VARCHAR(45) NULL,
          `socr_nom` VARCHAR(45) NULL,
          `socr_civilite` INT NULL,
          `socr_dateNaissance` DATE NULL,
          `socr_lieuNaissance` VARCHAR(45) NULL,
          `socr_adresse` VARCHAR(45) NULL,
          `socr_codePostal` INT NULL,
          `socr_ville` VARCHAR(45) NULL,
          `socr_pays` VARCHAR(45) NULL,
          PRIMARY KEY (`socr_id`))
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_indivision_ind` (
          `socr_id` INT NOT NULL,
          `soc_id` VARCHAR(45) NOT NULL,
          `ind_num` INT NULL,
          PRIMARY KEY (`socr_id`, `soc_id`),
          INDEX `fk_t_societaireRepresente_socr_has_t_societaire_soc_t_socie_idx` (`soc_id` ASC),
          INDEX `fk_t_societaireRepresente_socr_has_t_societaire_soc_t_socie_idx1` (`socr_id` ASC),
          CONSTRAINT `fk_t_societaireRepresente_socr_has_t_societaire_soc_t_societa1`
            FOREIGN KEY (`socr_id`)
            REFERENCES `" . $database . "`.`t_societaireRepresente_socr` (`socr_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_societaireRepresente_socr_has_t_societaire_soc_t_societa2`
            FOREIGN KEY (`soc_id`)
            REFERENCES `" . $database . "`.`t_societaire_soc` (`soc_login`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;");
        
        $this->CI->db->query("
        CREATE TABLE IF NOT EXISTS `" . $database . "`.`t_paiement_pai` (
          `moy_id` INT NOT NULL,
          `ope_id` INT NOT NULL,
          `pai_numero` VARCHAR(45) NOT NULL,
          PRIMARY KEY (`moy_id`, `ope_id`, `pai_numero`),
          INDEX `fk_t_moyenPai_moy_has_t_operation_ope_t_operation_ope1_idx` (`ope_id` ASC),
          INDEX `fk_t_moyenPai_moy_has_t_operation_ope_t_moyenPai_moy1_idx` (`moy_id` ASC),
          CONSTRAINT `fk_t_moyenPai_moy_has_t_operation_ope_t_moyenPai_moy1`
            FOREIGN KEY (`moy_id`)
            REFERENCES `" . $database . "`.`t_moyenPai_moy` (`moy_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
          CONSTRAINT `fk_t_moyenPai_moy_has_t_operation_ope_t_operation_ope1`
            FOREIGN KEY (`ope_id`)
            REFERENCES `" . $database . "`.`t_operation_ope` (`ope_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)
        ENGINE = InnoDB;
        ");

		return 0;
    }
}
