<?php
defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Classe des paramètres avancés
 */
class parametresAvances {
	
	/**
	 * CodeIgniter super-object
	 *
	 * @access protected
	 * @var object
	 */
	protected $CI;
	
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->database();
	}
	
	/**
	 * Ajoute un statut
	 *
	 * @param  string	$nom le nom du statut
	 * @return int
	 * 		- 0 : le statut a été ajouté
	 * 		- 1 : le nom est vide, le statut n'est pas ajouté
	 * 		- 2 : le nom est trop long, le statut n'est pas ajouté
	 */
	public function ajouterStatut(string $nom)
	{
		if (isset($nom) && strlen($nom) > 0) {
			if (strlen($nom) <= 45) {
				$this->_ajouterStatutBdd($nom);
				return 0;
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}
	
	/**
	 * Modifie un statut
	 *
	 * @param  int		$id l'id du statut à modifier
	 * @param  string	$nouveauNom le nouveau nom du statut
	 * @return int
	 * 		- 0 : le statut a été modifié
	 * 		- 1 : l'un des arguments est vide, le statut n'est pas modifié
	 * 		- 2 : le nom est trop long, le statut n'est pas modifié
	 * 		- 3 : l'id n'existe pas
	 */
	public function modifierStatut(int $id, string $nouveauNom)
	{
		if (isset($id) && isset($nouveauNom) && strlen($nouveauNom) > 0) {
			if (strlen($nom) <= 45) {
				$result = $this->_obtenirStatutBdd($id);
				if (isset($result)) {
					$this->_modifierStatutBdd($id, $nouveauNom);
					return 0;
				} else {
					return 3;
				}
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}

	/**
     * Supprime un statut
     *
     * @param  int $id id de l'AG
     * @return int
     *      - 0 : Le statut est supprimé
     *      - 1 : Le statut n'éxiste pas, Le statut n'est pas supprimé
     *      - 2 : Le statut est utilisé, Le statut n'est pas supprimé
     */
    public function supprimerStatut(int $id)
	{
		$infos = $this->obtenirStatut($id);
        if(isset($infos) && sizeof($infos) > 0){
            $verif = $this->_verifierStatutNonUtiliseeBdd($id);
            if(!$verif){
                $this->_supprimerStatutBdd($id);
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
	}
	
	/**
	 * Récupère un statut
	 *
	 * @param  int $id l'id du statut à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations d'un statut
	 * 
	 * 		$infos = [<br>
	 * 			'sta_id'	=> id,<br>
	 * 			'sta_nom'	=> nom<br>
	 * 		]
	 */
	public function obtenirStatut(int $id)
	{
		$infos = $this->_obtenirStatutBdd($id);
		return $infos;
	}
	
	/**
	 * Récupère tous les statuts
	 *
	 * @return array<int,array<string,string>> $infos un tableau contenant la liste des informations des statuts
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'sta_id'	=> id,<br>
	 * 				'sta_nom'	=> nom<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function obtenirListeStatuts()
	{
		$infos = $this->_obtenirTousLesStatutsBdd();
		return $infos;
	}
	
	/**
	 * Ajoute un régime
	 *
	 * @param  string	$nom le nom du régime
	 * @param  bool		$type le type du régime
	 * @return int
	 * 		- 0 : le régime a été ajouté
	 * 		- 1 : l'un des champs est vide, le régime n'est pas ajouté
	 * 		- 2 : le nom est trop long, le régime n'est pas ajouté
	 */
	public function ajouterRegime(string $nom, bool $type)
	{
		if (isset($nom) && strlen($nom) > 0 && isset($type)) {
			if (strlen($nom) <= 45) {
				$this->_ajouterRegimeBdd($nom, $type);
				return 0;
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}
	
	/**
	 * Modifie un régime
	 *
	 * @param  int $id l'id du régime à modifier
	 * @param  string $nouveauNom le nouveau nom du régime
	 * @param  bool $nouveauType le nouveau type du régime
	 * @return int
	 * 		- 0 : le régime a été modifié
	 * 		- 1 : l'un des champs est vide, le régime n'est pas modifié
	 * 		- 2 : le nom est trop long, le régime n'est pas modifié
	 * 		- 3 : l'id n'existe pas
	 */
	public function modifierRegime(int $id, string $nouveauNom, bool $nouveauType)
	{
		if (isset($id) && isset($nouveauNom) && strlen($nouveauNom) > 0 && isset($nouveauType)) {
			if (strlen($nouveauNom) <= 45) {
				$result = $this->_obtenirRegimeBdd($id);
				if (isset($result)) {
					$this->_modifierRegimeBdd($id, $nouveauNom, $nouveauType);
					return 0;
				} else {
					return 3;
				}
			} else {
				return 2;
			}
		} else {
			return 1;
		}
	}
	
	/**
	 * Récupère un régime
	 *
	 * @param  int $id l'id du régime à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations d'un régime
	 * 
	 * 		$infos = [<br>
	 * 			'reg_id'	=> id,<br>
	 * 			'reg_nom'	=> nom,<br>
	 * 			'reg_type'	=> type,<br>
	 * 		]
	 */
	public function obtenirRegime(int $id)
	{
		$infos = $this->_obtenirRegimeBdd($id);
		return $infos;
	}
	
	/**
	 * Récupère tous les régimes ayant pour type $type
	 *
	 * @param  bool $type le type des régimes à récupèrer
	 * @return array<int,array<string,string>> $infos un tableau qui contient la liste des informations des régimes
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'reg_id'	=> id,<br>
	 * 				'reg_nom'	=> nom,<br>
	 * 				'reg_type'	=> type,<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function obtenirListeRegimesParType(bool $type)
	{
		$infos = $this->_obtenirTousLesRegimesParTypeBdd($type);
		return $infos;
	}
	
	/**
	 * Récupère tous les types de paiement
	 *
	 * @return array<int,array<string,string>> $infos un tableau qui contient la liste des types de paiement
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'pai_id'	=> id,<br>
	 * 				'pai_type'	=> type,<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function obtenirListePaiements()
	{
		$infos = $this->_obtenirTousLesPaiements();
		return $infos;
	}
	
	/**
	 * Ajoute un statut dans la table statut
	 *
	 * @access private
	 * @param  string $nom le nom du statut
	 * @return void
	 */
	private function _ajouterStatutBdd(string $nom)
	{
		$req = 'INSERT INTO t_statut_sta
				VALUES (NULL, ?);';

		$this->CI->db->query($req, array($nom));
	}
	
	/**
	 * Modifie un statut dans la table statut
	 *
	 * @access private
	 * @param  int		$id l'id du statut à modifier
	 * @param  string	$nouveauNom le nouveau nom du statut
	 * @return void
	 */
	private function _modifierStatutBdd(int $id, string $nouveauNom)
	{
		$req = 'UPDATE t_statut_sta
				SET sta_nom = ?
				WHERE sta_id = ?;';

		$this->CI->db->query($req, array($nouveauNom, $id));
	}
	
    /**
     * supprime un statut dans la BDD
     *
     * @param  int $id id du statut
     * @return void
     */
    private function _supprimerStatutBdd(int $id)
	{
		$req = 'DELETE FROM t_statut_sta
        WHERE sta_id = ?;';

		$this->CI->db->query($req, array($id));
	}

	/**
     * Vérifie si il existe une entreprise empêchant la suppression d'un statut
     *
     * @param  int $id id du statut
     * @return boolean
     *      - true : il existe une contrainte empêchant la suppression
     *      - false : il n'éxiste aucune contrainte
     */
    private function _verifierStatutNonUtiliseeBdd(int $id)
	{
		$req = 'SELECT sta_id
                FROM t_societaire_soc
                WHERE sta_id = ?;';

		$result = $this->CI->db->query($req, array($id))->result_array();

        if((isset($result) && $result !== null && sizeof($result) > 0) ){
            return true;
        } else {
            return false;
        }
	}

	/**
	 * Récupère les informations d'un statut
	 *
	 * @access private
	 * @param  int $id l'id du statut à récupèrer
	 * @return array<string,string>
	 */
	private function _obtenirStatutBdd(int $id)
	{
		$req = 'SELECT *
				FROM t_statut_sta
				WHERE sta_id = ?;';

		$result = $this->CI->db->query($req, array($id))->row_array();
		return $result;
	}
	
	/**
	 * Récupère les informations de tous les statuts
	 *
	 * @access private
	 * @return array<int,array<string,string>>
	 */
	private function _obtenirTousLesStatutsBdd()
	{
		$req = 'SELECT *
				FROM t_statut_sta;';

		$result = $this->CI->db->query($req)->result_array();
		return $result;
	}
	
	/**
	 * Ajoute un régime dans la table régime
	 *
	 * @access private
	 * @param  string	$nom le nom du régime
	 * @param  bool		$type le type du régime
	 * @return void
	 */
	private function _ajouterRegimeBdd(string $nom, bool $type)
	{
		$req = 'INSERT INTO t_regime_reg
				VALUES(NULL, ?, ?);';

		$this->CI->db->query($req, array($nom, $type));
	}
	
	/**
	 * Modifie un régime dans la table regime
	 *
	 * @access private
	 * @param  int		$id l'id du regime à modifier
	 * @param  string	$nouveauNom le nouveau nom du régime
	 * @param  bool		$nouveauType le nouveau type du régime
	 * @return void
	 */
	private function _modifierRegimeBdd(int $id, string $nouveauNom, bool $nouveauType)
	{
		$req = 'UPDATE t_regime_reg
				SET reg_nom = ?,
					reg_type = ?
				WHERE reg_id = ?;';

		$this->CI->db->query($req, array($nouveauNom, $nouveauType, $id));
	}
	
	/**
	 * Récupère les informations d'un régime
	 *
	 * @access private
	 * @param  int $id l'id du régime à récupèrer
	 * @return array<string,string>
	 */
	private function _obtenirRegimeBdd(int $id)
	{
		$req = 'SELECT *
				FROM t_regime_reg
				WHERE reg_id = ?;';

		$result = $this->CI->db->query($req, array($id))->row_array();
		return $result;
	}
	
	/**
	 * Récupère les informations de tous les régimes ayant pour type $type
	 *
	 * @access private
	 * @param  bool $type le type des régimes à récupèrer
	 * @return array<int,array<string,string>>
	 */
	private function _obtenirTousLesRegimesParTypeBdd(bool $type)
	{
		$req = 'SELECT *
				FROM t_regime_reg
				WHERE reg_type = ?;';

		$result = $this->CI->db->query($req, array($type))->result_array();
		return $result;
	}
	
	/**
	 * Récupère les informations de tous les types de paiement
	 *
	 * @access private
	 * @return array<int,array<string,string>>
	 */
	private function _obtenirTousLesPaiements()
	{
		$req = 'SELECT *
				FROM t_moyenPai_moy;';

		$result = $this->CI->db->query($req)->result_array();
		return $result;
	}
}