<?php

/**
 * Classe Compte
 */
class Compte
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Constructeur de la classe Compte
     *
     * @return void
     */
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->database();
        $this->CI->load->config('email');
        $this->CI->load->library('email');
    }

    /**
     * Récupère le statut et la validité d'un compte
     *
     * @param  string $login login de connexion
     * @param  string $mdp mot de passe de connexion
     * @return string|int
     *      - AA : les identifiants correspondent à un Administrateur Activé
     *      - AB : les identifiants correspondent à un Administrateur Bloqué
     *      - SA : les identifiants correspondent à un Sociétaire Activé
     *      - SB : les identifiants correspondent à un Sociétaire Bloqué
     * 
     *      - 1 : le login ou le mot de passe est vide
     *      - 2 : le couple d'identifiants login et mot de passe n'éxiste pas ou l'utilisateur est désactivé
     *      - 3 : le mot de passe est erroné
     *      - 4 : le compte est bloqué
     */
    public function connexion(string $login, string $mdp)
    {
        if (isset($login) && strlen($login) > 0 && isset($mdp) && strlen($mdp) > 0) {
            $resultAdmin = $this->_obtenirIdAdminBdd($login);
            $resultUtilisateur = $this->_obtenirIdUtilisateurBdd($login);
            if (isset($resultAdmin) && $resultAdmin !== null && password_verify($mdp, $resultAdmin->adm_mot_de_passe)) {
                return "A" . $resultAdmin->adm_validite;
            } elseif (isset($resultUtilisateur) && $resultUtilisateur !== null && $resultUtilisateur->soc_etat !== 'D') {
                if (password_verify($mdp, $resultUtilisateur->soc_mot_de_passe)) {
                    $this->_reinitialiserTentativeConnexionBdd($login);
                    return "S" . $resultUtilisateur->soc_etat;
                } else {
                    $this->_incrementerTentativeConnexionBdd($login);
                    if ($this->_obtenirTentativeConnexionBdd($login) >= 5) {
                        $this->_desactiverUtilisateurBdd($login);
                        $this->_reinitialiserTentativeConnexionBdd($login);
                        return 4;
                    } else {
                        return 3;
                    }
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Envoi un mail avec un lien pour changer le mot de passe du compte
     *
     * @param  string $email email du compte ou le mot de passe est oublié
     * @return int
     *      - 0 : l'email est envoyé
     *      - 1 : l'email est vide, aucun email n'est envoyé
     *      - 2 : il n'éxiste aucun compte avec cet email, aucun email n'est envoyé
     *      - 3 : le login ne correspond pas à l'email
     */
    public function reinitialiserMdp(string $login, string $email)
    {
        if (isset($email) && strlen($email) > 0) {
            $loginBD = $this->_obtenirLoginAvecEmailBdd($email);
            if (isset($loginBD) && $loginBD !== null) {
                $verifemail = $this->_verifEmailBdd($login, $email);
                if ($verifemail) {
                    $utiInfos = $this->_obtenirIdUtilisateurBdd($login);
                    $token = $this->_obtenirTokenBdd($login);
                    if (!isset($token) || $token === null) {
                        //Ce compte n'a pas encore de token
                        $token = bin2hex(random_bytes(16));
                        if (!isset($utiInfos) || $utiInfos === null) {
                            //C'est un admin
                            $this->_ajouterTokenAdministrateurBdd($login, $token);
                        } elseif ($utiInfos->soc_etat !== 'D') {
                            //C'est un utilisateur
                            $this->_ajouterTokenUtilisateurBdd($login, $token);
                        }
                    } else {
                        $token = $token->tok_token;
                    }
                    $nomSite = $this->_obtenirInfosSiteBdd()->sit_nom;
                    $structure = $this->_obtenirInfosSiteBdd()->sit_structure;
                    if ($structure === '0') {
                        $structure = "SCI";
                    } else {
                        $structure = "GFA";
                    }
                    $this->CI->email->from($this->CI->config->item('smtp_user'), $structure .' '.$nomSite);
                    $this->CI->email->bcc($this->CI->config->item('smtp_user'));
                    $this->CI->email->to($email);
                    $this->CI->email->subject('[' . $structure . ' ' . $nomSite . '] Réinitialisation du mot de passe');
                    $this->CI->email->message('
                    <div style="font-family: helvetica">
                        <h2>Il semble que vous ayez oublié votre mot de passe.</h2>
                        <h3>Si tel est le cas, cliquez sur le bouton ci-dessous pour réinitialiser votre mot de passe.</h3>
                        <p><a href="' . base_url("index.php/accueil/nouveauMdp/" . $token) . '" target="_blank"> <input style="border-width: 0px 0px; border-radius: 0px; background: #478b26; font-size: 20px; color: white; padding: 10px;" type="button" value="Réinitialiser mon mot de passe" /> </a></p>
                        <br>
                        <p>Si vous n\'avez pas oublié votre mot de passe, veuillez ignorer cet e-mail.</p>
                    </div>');
                    $this->CI->email->send();
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Récupère le login du compte correspondant au token
     *
     * @param  string $token
     * @return string login correspondant au token, ou null si le token n'éxiste pas
     */
    public function obtenirLogin(string $token)
    {
        $this->_supprimerTokenBdd("");
        $login = $this->_obtenirLoginAvecTokenBdd($token);
        if (isset($login) && $login !== null) {
            return $login->login;
        } else {
            return null;
        }
    }

    /**
     * Modifie le mot de passe du compte correspondant au login
     *
     * @param  string $login login du compte
     * @param  string $nouveauMdp nouveau mot de passe du compte
     * @return int
     *      - 0 : l'ancien mot de passe est remplacé par le nouveau mot de passe
     *      - 1 : le nouveau mot de passe est vide, l'ancien mot de passe est conservé
     *      - 2 : le nouveau mot de passe est trop long, l'ancien mot de passe est conservé
     *      - 3 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
     */
    public function modifierMdpOublie(string $login, string $nouveauMdp)
    {
        if (isset($nouveauMdp) && strlen($nouveauMdp) > 0) {
            if (strlen($nouveauMdp) <= 50) {
                //Vérifie le format du mot de passe (minuscule, majuscule, chiffre, caractères spéciaux, taille >=8)
                if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#', $nouveauMdp)) {
                    $admin = $this->_obtenirIdAdminBdd($login);
                    $nouveauMdpHash = password_hash($nouveauMdp, PASSWORD_BCRYPT);
                    if (isset($admin) && $admin !== null) {
                        //C'est un admin
                        $this->_modifierMdpAdminBdd($nouveauMdpHash);
                    } else {
                        //C'est un utilisateur
                        $this->_modifierMdpUtilisateurBdd($login, $nouveauMdpHash);
                    }
                    $this->_supprimerTokenBdd($login);
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Vérifier mot de passe
     *
     * @param  string $ancienMdp ancien mot de passe
     * @param  string $nouveauMdp nouveau mot de passe
     * @return int
     *      - 0 : le mot de passe est valide
     *      - 1 : le nouveau mot de passe, l'ancien mot de passe, ou le login est vide, l'ancien mot de passe est conservé
     *      - 2 : le nouveau mot de passe, l'ancien mot de passe ou le login est trop long, l'ancien mot de passe est conservé
     *      - 4 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
     */
    protected function verifierMdp(string $ancienMdp, string $nouveauMdp)
    {
        if (isset($nouveauMdp) && strlen($nouveauMdp) > 0 && isset($ancienMdp) && strlen($ancienMdp) > 0) {
            if (strlen($nouveauMdp) <= 50 && strlen($ancienMdp) <= 50) {
                //Vérifie le format du mot de passe (minuscule, majuscule, chiffre, caractères spéciaux, taille >=8)
                if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#', $nouveauMdp)) {
                    return 0;
                } else {
                    return 4;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Vérifier un email
     *
     * @param  string $email email de l'utilisateur
     * @return int 
     *      - 0 : l'email est valide
     *      - 1 : le nouvel email est vide
     *      - 2 : le nouvel email est trop long (>45)
     *      - 3 : le format du nouvel email est incorrect
     */
    protected function verifierEmail(string $login, string $email)
    {
        if (isset($email) && strlen($email) > 0) {
            if (strlen($email) <= 45) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Récupère le login, le mot de passe et le code de validite de l'administrateur
     *
     * @access private
     * @param  string $login login de l'administrateur
     * @return object
     */
    private function _obtenirIdAdminBdd(string $login)
    {
        $req = "SELECT adm_login, adm_mot_de_passe, adm_validite
        FROM t_administrateur_adm
        WHERE adm_login = ?;";

        $query = $this->CI->db->query($req, array($login));

        return $query->row();
    }

    /**
     * Récupère le login, le mot de passe et l'etat d'un utilisateur'
     *
     * @access private
     * @param  string $login login d'un utilisateur
     * @return object
     */
    private function _obtenirIdUtilisateurBdd(string $login)
    {
        $req = "SELECT soc_login, soc_mot_de_passe, soc_etat
        FROM t_societaire_soc
        WHERE soc_login = ?;";

        $query = $this->CI->db->query($req, array($login));

        return $query->row();
    }

    /**
     * Récupère le login correspondant à l'email en paramètre
     *
     * @access private
     * @param  string $email
     * @return object
     */
    private function _obtenirLoginAvecEmailBdd(string $email)
    {
        $req = "SELECT adm_login as login
        FROM t_administrateur_adm
        WHERE adm_email = ?
        UNION
        SELECT soc_login as login
        FROM t_societaire_soc
        WHERE soc_email = ?;";

        $query = $this->CI->db->query($req, array($email, $email));

        return $query->row();
    }

    /**
     * Récupère le login correspondant au token en paramètre
     *
     * @access private
     * @param  string $token
     * @return object
     */
    private function _obtenirLoginAvecTokenBdd(string $token)
    {
        $req = "SELECT adm_login as login
        FROM t_token_tok
        WHERE tok_token = ?
        AND adm_login is not NULL
        UNION
        SELECT soc_login as login
        FROM t_token_tok
        WHERE tok_token = ?
        AND soc_login is not NULL;";

        $query = $this->CI->db->query($req, array($token, $token));

        return $query->row();
    }

    /**
     * Récupère les infos du site
     *
     * @access private
     * @return object
     */
    private function _obtenirInfosSiteBdd()
    {
        $query = $this->CI->db->query("SELECT sit_nom, sit_structure
        FROM t_site_sit;");
        return $query->row();
    }

    /**
     * Récupère le token correspondant à un login
     *
     * @access private
     * @param  string $login
     * @return object
     */
    private function _obtenirTokenBdd(string $login)
    {
        $req = "SELECT tok_token
        FROM t_token_tok
        WHERE soc_login = ?
        OR adm_login = ?;";

        $query = $this->CI->db->query($req, array($login, $login));

        return $query->row();
    }

    /**
     * retourne le nombre de tentative de connexion d'un compte
     *
     * @param  string $login login du compte
     * @return int nombre de tentative
     */
    private function _obtenirTentativeConnexionBdd(string $login)
    {
        $req = "SELECT soc_tentativeConnexion
        FROM t_societaire_soc
        WHERE soc_login = ?;";

        $query = $this->CI->db->query($req, array($login));

        return $query->row()->soc_tentativeConnexion;
    }

    /**
     * Ajoute un token de réinitialisation du mot de passe pour un utilisateur
     *
     * @access private
     * @param  string $login
     * @param  string $token
     * @return void
     */
    private function _ajouterTokenUtilisateurBdd(string $login, string $token)
    {
        $req = "INSERT INTO t_token_tok
        VALUES (NULL, ?, CURRENT_TIMESTAMP, ?, NULL);";

        $this->CI->db->query($req, array($token, $login));
    }

    /**
     * Ajoute un token de réinitialisation du mot de passe pour un administrateur
     *
     * @access private
     * @param  string $login
     * @param  string $token
     * @return void
     */
    private function _ajouterTokenAdministrateurBdd(string $login, string $token)
    {
        $req = "INSERT INTO t_token_tok
        VALUES (NULL, ?, CURRENT_TIMESTAMP, NULL, ?);";

        $this->CI->db->query($req, array($token, $login));
    }

    private function _supprimerTokenBdd(string $login)
    {
        $req = "DELETE FROM t_token_tok
        WHERE soc_login = ?
        OR adm_login = ?
        OR CURRENT_TIMESTAMP - tok_dateCreation > 3600;";

        $this->CI->db->query($req, array($login, $login));
    }

    /**
     * Modifie le mot de passe de l'administrateur
     *
     * @access private
     * @param  string $mdp
     * @return void
     */
    private function _modifierMdpAdminBdd(string $mdp)
    {
        $req = "UPDATE t_administrateur_adm
        SET adm_mot_de_passe = ?;";

        $this->CI->db->query($req, array($mdp));
    }

    /**
     * Modifie le mot de passe d'un utilisateur
     *
     * @access private
     * @param  string $login
     * @param  string $mdp
     * @return void
     */
    private function _modifierMdpUtilisateurBdd(string $login, string $mdp)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_mot_de_passe = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($mdp, $login));
    }

    /**
     * incrémente de 1 le nombre de tentative de connexion pour un compte
     *
     * @param  string $login
     * @return void
     */
    private function _incrementerTentativeConnexionBdd($login)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_tentativeConnexion = soc_tentativeConnexion + 1
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($login));
    }

    /**
     * réinitialise à 0 le nombre de tentative de connexion pour un compte
     *
     * @param  string $login
     * @return void
     */
    private function _reinitialiserTentativeConnexionBdd($login)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_tentativeConnexion = 0
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($login));
    }

    /**
     * Désactive un utilisateur
     *
     * @access private
     * @param  string $login
     * @return object
     */
    private function _desactiverUtilisateurBdd($login)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_etat = 'D'
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($login));
    }

    /**
     * vérifie l'existence d'un couple email et login
     *
     * @param  string $login login d'un utilisateur
     * @param  string $email email d'un utilisateur
     * @return boolean
     *      - true : l'email et le login correspondent à un utilisateur
     *      - false : l'email et le login ne correspndent pas à un utilisateur
     */
    private function _verifEmailBdd(string $login, string $email)
    {
        $req = "SELECT adm_login as login
        FROM t_administrateur_adm
        WHERE adm_login = ?
        AND adm_email = ?
        UNION
        SELECT soc_login as login
        FROM t_societaire_soc
        WHERE soc_login = ?
        AND soc_email = ?;";

        $result = $this->CI->db->query($req, array($login, $email, $login, $email))->result_array();

        if (isset($result) && $result !== null && sizeof($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Récupère les informations d'un utilisateur ou admin à partir d'un email
     *
     * @access private
     * @param  string $login
     * @return object
     */
    private function _obtenirUtilisateurAvecEmailBdd($email)
    {
        $req = "SELECT adm_login as login
        FROM t_administrateur_adm
        WHERE adm_email = ?
        UNION
        SELECT soc_login as login
        FROM t_societaire_soc
        WHERE soc_email = ?;";

        $result = $this->CI->db->query($req, array($email, $email))->row();
        return $result;
    }
}
