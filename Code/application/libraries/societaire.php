<?php

require_once APPPATH . 'libraries/compte.php';

/**
 * Classe Societaire 
 */
class Societaire extends Compte
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Constructeur de la classe Societaire
     *
     * @return void
     */
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->database();
        $this->CI->load->config('email');
        $this->CI->load->library('email');
    }


    /**
     * Ajoute un compte societaire
     *
     * @param  string $login login du nouveau societaire
     * @param  string $mdp mot de passe du nouveau societaire
     * @param  string $prenom prénom du nouveau societaire
     * @param  string|null $autrePrenom autres prénoms du nouveau societaire
     * @param  string $nom nom du nouveau societaire
     * @param  string|null $nomUsuel nom du nouveau societaire
     * @param  string|null $representantsLegaux réprésentants légaux
     * @param  int    $civilite civilité du nouveau societaire
     * @param  int    $statut statut du nouveau societaire
     * @param  string|null $siret numéro de siret du nouveau societaire si personne morale
     * @param  string $dateNaissance date de naissance du nouveau societaire
     * @param  string|null $lieuNaissance lieu de naissance du nouveau societaire
     * @param  string|null $nationalite nationalité du nouveau societaire
     * @param  string|null $email courriel du nouveau societaire
     * @param  string|null $telFixe téléphone fixe du nouveau societaire
     * @param  string|null $telPortable téléphone portable du nouveau societaire
     * @param  string $adresse adresse du nouveau societaire
     * @param  string $codePostal code postal du nouveau societaire
     * @param  string $ville ville du nouveau societaire
     * @param  string $pays pays du nouveau societaire
     * @param  int    $regime régime matrimonial ou fiscal du nouveau societaire
     * @param  int $agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
     * @param  bool $indivision booléen pour déterminé si le sociétaire est le réprésentant d'une indivision
     * @return int
     *      - 0 : le compte societaire est crée
     *      - 1 : un des paramètre obligatoire (prenom, nom, nomUsuel, civilite, statut, dateNaissance, adresse, codePostal, ville, pays, regime, agEntree) est vide, le compte societaire n'est pas crée
     *      - 2 : un des paramètre est trop long, le compte societaire n'est pas crée
     *      - 3 : le format de l'email est incorrect, le compte societaire n'est pas crée
     *      - 5 : le format d'un des numéros de téléphone est invalide, le compte societaire n'est pas crée
     *      - 6 : il faut au moins un numéro de téléphone ou un mail
     *      - 7 : le mail n'a pas été envoyé, le compte societaire est crée
     */
    public function ajouter(string $prenom, $autrePrenom, string $nom, $nomUsuel, $representantsLegaux, int $civilite, int $statut, $siret, string $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, string $adresse = NULL, string $codePostal = NULL, string $ville = NULL, string $pays = NULL, int $regime, int $agEntree, bool $indivision)
    {
        if (isset($prenom) && strlen($prenom) > 0 && isset($nom) && strlen($nom) > 0 && isset($nomUsuel) && strlen($nomUsuel) > 0 && isset($civilite) && isset($statut) && isset($dateNaissance) && strlen($dateNaissance) > 0 && isset($adresse) && strlen($adresse) > 0 && isset($codePostal) && strlen($codePostal) > 0 && isset($ville) && strlen($ville) > 0 && isset($pays) && strlen($pays) > 0 && isset($regime) && strlen($regime) > 0 && isset($agEntree) && isset($indivision)) {
            if (strlen($prenom) <= 45 && strlen($autrePrenom) <= 45 && strlen($nom) <= 45 && strlen($nomUsuel) <= 45
            && strlen($representantsLegaux) <= 45 && $civilite <= 5 
            && (strlen($siret) ==0 || strlen($siret) ==9 || strlen($siret) == 14)
            && strlen($dateNaissance) <= 45 && strlen($lieuNaissance) <= 45 && strlen($nationalite) <= 45 && strlen($email) <= 45 && strlen($telFixe) <= 45 && strlen($telPortable) <= 45 && strlen($adresse) <= 45 && strlen($codePostal) <= 45 && strlen($ville) <= 45 && strlen($pays) <= 45 && strlen($regime) <= 45) {
                //vérification email
                if (!(isset($email) && strlen($email) > 0) || filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    //vérification autorisation email
                    if (isset($email) && strlen($email) > 0) {
                        $pasEmail = 0;
                    } else {
                        $pasEmail = 1;
                    }
                    // il faut au moins un point de contact
                    if (!(isset($telFixe)) && !(isset($telPortable)) && !(isset($email))) {
                        return 6;
                    } 
                    // vérification numéros téléphone
                    if ((!isset($telFixe) || preg_match('#^((\+?)(\s)?([0-9](\s)?)+)$#', $telFixe)) && (!isset($telPortable) || preg_match('#^((\+?)(\s)?([0-9](\s)?)+)$#', $telPortable))) {
                        $nomSite = $this->_obtenirInfosSiteBdd()->sit_nom;
                        $structure = $this->_obtenirInfosSiteBdd()->sit_structure;
                        if ($structure) {
                            $structure = "GFA";
                            $pronom = "le";
                        } else {
                           $structure = "SCI";
                           $pronom = "la";
                        }
                        // Login
                        $login = $this->genererLogin($nom, $prenom, $dateNaissance);
                        // 1er mdp = login
                        $mdp = $login;
                        $mdpHash = password_hash($mdp, PASSWORD_BCRYPT);
                        // Nom et prénom
                        $nom = mb_strtoupper($nom, 'UTF-8');
                        $prenom = ucfirst($prenom);
                        $this->_ajouterBdd($login, $mdpHash, $prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $regime, $agEntree, $indivision, $pasEmail);
                        return 0;
                    } else {
                        return 5;
                    }
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Crée un login
     * 
     * Le login est de la forme initiale prénom en minuscule + nomSociétaire en minuscule sans caractères spéciaux ni accents +
     * année de naissance (sur 2 chiffres) + numéro (si nécessaire)
     *
     * @param  string $nom nom de du societaire
     * @param  string $prenom prénom du societaire
     * @param  string $date date de naissance du societaire
     * @return string login du societaire
     */
    public function genererLogin(string $nom, string $prenom, string $date)
    {
        $login = substr(strtolower($this->_nettoyerChaine($prenom)), 0, 1) . substr(strtolower($this->_nettoyerChaine($nom)), 0, 8) . substr($date, 2, 2);
        $resultLogin = $this->_obtenirSocietaireAvecLoginBdd($login);
        $numero = 1; //numéro si le login existe déjà

        //si le login existe déjà
        while (isset($resultLogin) && $resultLogin !== null) {
            if ($numero > 1) { //si il existe au moins deux personnes avec ce login
                $login = substr($login, 0, -1) . $numero;
            } else {
                $login = $login . $numero;
            }
            $numero++;
            $resultLogin = $this->_obtenirSocietaireAvecLoginBdd($login);
        }
        return $login;
    }

    /**
     * Nettoie une chaine de caractère
     * 
     * Enlève tous les caractères spéciaux et remplace toutes les lettres accentués par leur équivalent
     *
     * @param  string $string chaine de caractères à nettoyer
     * @return string chaine de caractères nettoyé
     */
    private function _nettoyerChaine(string $chaine)
    {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'a',
            '/[ÍÌÎÏ]/u' => 'i',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'e',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'o',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'u',
            '/ç/' => 'c',
            '/Ç/' => 'c',
            '/ñ/' => 'n',
            '/Ñ/' => 'n',
            '/[«»]/u' => '',
            '/ /' => '',
        );

        $chaine =  preg_replace(array_keys($utf8), array_values($utf8), $chaine);
        return preg_replace('#([^a-z0-9])+#i', '', $chaine);
    }

    /**
     * Modifie les informations personnelles d'un societaire
     *
     * @param  string $login login du societaire
     * @param  string $prenom prénom du societaire
     * @param  string|null $autrePrenom autres prénoms du societaire
     * @param  string $nom nom du societaire
     * @param  string|null $nomUsuel nom du societaire
     * @param  string|null $representantsLegaux réprésentants légaux
     * @param  int    $civilite civilité du societaire
     * @param  int    $statut statut du societaire
     * @param  string|null $siret numéro de siret du nouveau sociétaire si personne morale
     * @param  string $dateNaissance date de naissance du societaire
     * @param  string|null $lieuNaissance lieu de naissance du societaire
     * @param  string $nationalite nationalité du nouveau societaire
     * @param  bool $indivision booléen pour déterminé si le sociétaire est le réprésentant d'une indivision
     * @return int
     *      - 0 : les informations personnelles sont modifiés
     *      - 1 : un des paramètre obligatoire (prenom, nom, nomUsuel, civilite, statut, dateNaissance, lieuNaissance) est vide, les informations personnelles ne sont pas modifiés
     *      - 2 : un des paramètre est trop long, les informations personnelles ne sont pas modifiés
     */
    public function modifierInfosPerso(string $login, string $prenom, $autrePrenom, string $nom, $nomUsuel, $representantsLegaux, int $civilite, int $statut, $siret, string $dateNaissance, $lieuNaissance, $nationalite, bool $indivision)
    {
        if (isset($prenom) && strlen($prenom) > 0 && isset($nom) && strlen($nom) > 0 && isset($nomUsuel) && strlen($nomUsuel) > 0&& isset($civilite) && isset($statut) && isset($dateNaissance) && strlen($dateNaissance) > 0 && isset($indivision)) {
            if (strlen($prenom) <= 45 && strlen($autrePrenom) <= 45 && strlen($nom) <= 45 && strlen($nomUsuel) <= 45 
            && strlen($representantsLegaux) <= 45 
            && (strlen($siret) ==0 || strlen($siret) ==9 || strlen($siret) == 14) 
            && strlen($dateNaissance) <= 45 && strlen($lieuNaissance) <= 45 && strlen($nationalite) <= 45) {
                // Nom et prénom
                $nom = mb_strtoupper($nom, 'UTF-8');
                $prenom = ucfirst($prenom);
                $this->_modifierInfosPersoBdd($login, $prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $indivision);
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * modifie les informations de contact d'un societaire
     *
     * @param  string $login login du societaire
     * @param  string|null $email courriel du societaire
     * @param  string|null $telFixe téléphone fixe du societaire
     * @param  string|null $telPortable téléphone portable du societaire
     * @param  string $adresse adresse du societaire
     * @param  string $codePostal code postal du societaire
     * @param  string $ville ville du societaire
     * @param  string $pays pays du societaire
     * @return int
     *      - 0 : les informations de contact sont modifiés
     *      - 1 : un des paramètre obligatoire (adresse, codePostal, ville, pays) est vide, les informations de contact ne sont pas modifiés
     *      - 2 : un des paramètre est trop long, les informations de contact ne sont pas modifiés
     *      - 3 : le format de l'email est incorrect, les informations de contact ne sont pas modifiés
     *      - 5 : le format d'un des numéros de téléphone est invalide, les informations de contact ne sont pas modifiés
     */
    public function modifierInfosContact(string $login, $email, $telFixe, $telPortable, string $adresse = NULL, string $codePostal = NULL, string $ville = NULL, string $pays = NULL)
    {
        if (isset($adresse) && strlen($adresse) > 0 && isset($codePostal) && strlen($codePostal) > 0 && isset($ville) && strlen($ville) > 0 && isset($pays) && strlen($pays) > 0) {
            if (strlen($email) <= 45 && strlen($telFixe) <= 45 && strlen($telPortable) <= 45 && strlen($adresse) <= 45 && strlen($codePostal) <= 11 && strlen($ville) <= 45 && strlen($pays) <= 45) {
                $verification = 0;
                if (isset($email) && strlen($email) > 0) {
                    $verification = $this->verifierEmail($login, $email);
                }
                // il faut au moins un point de contact
                if (!(isset($telFixe)) && !(isset($telPortable)) && !(isset($email))) {
                     return 6;
                } 
                if ($verification === 0) {
                    //vérification du téléphone
                    if ((!isset($telFixe) || preg_match('#^((\+?)(\s)?([0-9](\s)?)+)$#', $telFixe)) && (!isset($telPortable) || preg_match('#^((\+?)(\s)?([0-9](\s)?)+)$#', $telPortable))) {
                        $this->_modifierInfosContactBdd($login, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays);
                        return 0;
                    } else {
                        return 5;
                    }
                } else {
                    return $verification;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * modifie les informations sci d'un societaire
     *
     * @param  string $login login du societaire
     * @param  int    $regime régime matrimonial ou fiscal du societaire (Communauté, Contrat)
     * @param  int $agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
     * @return int
     *      - 0 : les informations de contact sont modifiés
     *      - 1 : un des paramètre obligatoire (adresse, codePostal, ville, pays) est vide, les informations de contact ne sont pas modifiés
     */
    public function modifierInfosSci(string $login, int $regime, int $agEntree)
    {
        if (isset($regime) && $regime >= 0 && isset($agEntree)) {
            $this->_modifierInfosSciBdd($login, $regime, $agEntree);
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * modifie les autorisations d'un societaire
     *
     * @access private
     * @param  bool $login login du societaire
     * @param  bool $pasFixe ne pas appeler sur le téléphone fixe
     * @param  bool $pasPortable ne pas appeler sur le téléphone portable
     * @param  bool $pasSms ne pas envoyer d'sms
     * @param  bool $pasEmail ne pas envoyer d'email
     * @param  bool $cloud autoriser le stockage dans le cloud
     * @return int
     *      - 0 : les informations de contact sont modifiés
     *      - 1 : un des paramètre obligatoire (pasFixe, pasPortable, pasSms, pasEmail, cloud) est vide, les autorisations ne sont pas modifié
     *      - 2 : un des paramètre n'est pas un booléen, les autorisations ne sont pas modifié
     */
    public function modifierAutorisation(string $login, bool $pasFixe, bool $pasPortable, bool $pasSms, bool $pasEmail, bool $cloud)
    {
        if (isset($pasFixe) && isset($pasPortable) && isset($pasSms) && isset($pasEmail) && isset($cloud)) {
            if (is_bool($pasFixe) && is_bool($pasPortable) && is_bool($pasSms) && is_bool($pasEmail) && is_bool($cloud)) {
                $this->_modifierAutorisationBdd($login, $pasFixe, $pasPortable, $pasSms, $pasEmail, $cloud);
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * modifie la date de sortie d'un sociétaire
     *
     * @param  string $login login du societaire
     * @param  int    $regime régime matrimonial ou fiscal du societaire (Communauté, Contrat)
     * @param  int    $agSortie assemblée générale pendant laquelle le sociétaire est entré dans la sci
     * @return int
     *      - 0 : les informations de contact sont modifiés
     *      - 1 : un des paramètre obligatoire (adresse, codePostal, ville, pays) est vide, les informations de contact ne sont pas modifiés
     */
    public function modifierDateSortie(string $login, int $agSortie)
    {
        if (isset($agSortie) && $agSortie >= 0 && isset($login)) {
            $this->_modifierDateSortieBdd($login, $agSortie);
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Modifie le mot de passe d'un societaire
     *
     * @param  string $ancienMdp ancien mot de passe du societaire
     * @param  string $nouveauMdp nouveau mot de passe du societaire
     * @return int
     *      - 0 : l'ancien mot de passe est remplacé par le nouveau mot de passe
     *      - 1 : le nouveau ou l'ancien mot de passe est vide, l'ancien mot de passe est conservé
     *      - 2 : le nouveau ou l'ancien mot de passe est trop long, l'ancien mot de passe est conservé
     *      - 3 : l'ancien mot de passe est incorrect, l'ancien mot de passe est conservé
     *      - 4 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
     */
    public function modifierMdp(string $login, string $ancienMdp, string $nouveauMdp)
    {
        $verification = $this->verifierMdp($ancienMdp, $nouveauMdp);
        if ($verification !== 0) return $verification;

        $ancienMdpHash = $this->_obtenirMdpBdd($login)->soc_mot_de_passe;
        if (password_verify($ancienMdp, $ancienMdpHash)) {
            //Vérifie le format du mot de passe (minuscule, majuscule, chiffre, caractères spéciaux, taille >=8)
            $nouveauMdpHash = password_hash($nouveauMdp, PASSWORD_BCRYPT);
            $this->_modifierMdpBdd($login, $nouveauMdpHash);
            $this->_modifierEtatBdd($login, "A");
            return 0;
        } else {
            return 3;
        }
    }

    /**
     * Récupère toutes les informations des societaires
     *
     * @return array<int,array<string,string>> tableau qui contient toutes les informations de tous les societaires, retourne null si le résultat est vide
     * 
     *     $infos = [<br>
     *          0 => [<br>
     *          "soc_login"         =>  (string) login<br>
     *          "soc_prenom"        =>  (string) prénom<br>
     *          "soc_autrePrenom"   =>  (string) autres prénoms<br>
     *          "soc_nom"           =>  (string) nom<br>
     *          "soc_nomUsuel"      =>  (string) nom usuel<br>
     *          "soc_civilite"      =>  (int) civilité<br>
     *          "sta_nom"        =>  (string) statut<br>
     *          "soc_siret"         =>  (string) numéro siret<br>
     *          "soc_naissance"     =>  (string) date de naissance<br>
     *          "soc_lieuNaissance" =>  (string) lieu de naissance<br>
     *          "soc_nationalite" =>  (string) nationalité<br>
     *          "soc_email"         =>  (string) adresse email<br>
     *          "soc_telephoneFixe" =>  (string) numéro de téléphone fixe<br>
     *          "soc_telephonePortable" =>  (string) numéro de téléphone portable<br>
     *          "soc_adresse"       =>  (string) adresse<br>
     *          "soc_codePostal"    =>  (int) code postal<br>
     *          "soc_ville"         =>  (string) ville<br>
     *          "soc_pays"          =>  (string) pays<br>
     *          "reg_nom"        =>  (string) régime matrimonial ou fiscal<br>
     *          "ass_dateEntree"    =>  (string) date d'entrée dans la société<br>
     *          "ass_dateSortie"    =>  (string) date de sortie de la société<br>
     *          "soc_pasAppelerFixe"    =>  (bool) ne pas appeler le téléphone fixe<br>
     *          "soc_pasAppelerPortable"    =>  (bool) ne pas appeler le téléphone portable<br>
     *          "soc_pasEnvoyerSms" =>  (bool) ne pas envoyer d'sms<br>
     *          "soc_pasEnvoyerEmail"   =>  (bool) ne pas envoyer d'email<br>
     *          "soc_autoriserCloud"    =>  (bool) autoriser le stockage dans le cloud<br>
     *          "soc_indivision"      =>  (bool) représentant d'une indivision<br>
     *          "soc_etat"      =>  (char) etat<br>
     *          ]<br>
     *         1 => [ ... ]<br>
     *          ... <br>
     *      ]
     */
    public function obtenirListeSocietaires()
    {
        $societairesBdd = $this->_obtenirTousLesSocietairesBdd();
        if (isset($societairesBdd) && $societairesBdd !== null && sizeof($societairesBdd) !== 0) {
            return $societairesBdd;
        } else {
            return null;
        }
    }

    /**
     * Récupère toutes les informations d'un societaire
     *
     * @param  string $login login du societaire
     * @return array<string,string> $info tableau qui contient toutes les informations d'un societaire, retourne null si le résultat est vide
     * 
     *     $infos = [<br>
     *          "soc_login"         =>  (string) login<br>
     *          "soc_prenom"        =>  (string) prénom<br>
     *          "soc_autrePrenom"   =>  (string) autres prénoms<br>
     *          "soc_nom"           =>  (string) nom<br>
     *          "soc_nomUsuel"      =>  (string) nom usuel<br>
     *          "soc_civilite"      =>  (int) civilité<br>
     *          "sta_nom"        =>  (string) statut<br>
     *          "soc_siret"         =>  (string) numéro siret<br>
     *          "soc_naissance"     =>  (string) date de naissance<br>
     *          "soc_lieuNaissance" =>  (string) lieu de naissance<br>
     *          "soc_nationalite" =>  (string) nationalité<br>
     *          "soc_email"         =>  (string) adresse email<br>
     *          "soc_telephoneFixe" =>  (string) numéro de téléphone fixe<br>
     *          "soc_telephonePortable" =>  (string) numéro de téléphone portable<br>
     *          "soc_adresse"       =>  (string) adresse<br>
     *          "soc_codePostal"    =>  (int) code postal<br>
     *          "soc_ville"         =>  (string) ville<br>
     *          "soc_pays"          =>  (string) pays<br>
     *          "reg_nom"        =>  (string) régime matrimonial ou fiscal<br>
     *          "ass_dateEntree"    =>  (string) date d'entrée dans la société<br>
     *          "ass_dateSortie"    =>  (string) date de sortie de la société<br>
     *          "soc_pasAppelerFixe"    =>  (bool) ne pas appeler le téléphone fixe<br>
     *          "soc_pasAppelerPortable"    =>  (bool) ne pas appeler le téléphone portable<br>
     *          "soc_pasEnvoyerSms" =>  (bool) ne pas envoyer d'sms<br>
     *          "soc_pasEnvoyerEmail"   =>  (bool) ne pas envoyer d'email<br>
     *          "soc_autoriserCloud"    =>  (bool) autoriser le stockage dans le cloud<br>
     *          "soc_indivision"      =>  (bool) représentant d'une indivision<br>
     *          "soc_etat"      =>  (char) état<br>
     *      ]
     */
    public function obtenirInfos(string $login)
    {
        $infos = $this->_obtenirInfosSocietaireBdd($login);
        $parts = $this->_obtenirPartSocietaireBdd($login);
        $infos += ["soc_nbParts" => count($parts)];

        return $infos;
    }

    /**
     * Envoi les données d'un sociétaire par email
     *
     * @param  string $login login du sociétaire
     * @param  string $email adresse email du sociétaire
     * @return int 
     *      - 0 : les données sont envoyé
     *      - 1 : le login est vide, les données ne sont pas envoyé
     *      - 2 : le login est trop long (>45), les données ne sont pas envoyé
     *      - 3 : le login n'éxiste pas, les données ne sont pas envoyé
     *      - 4 : le format de l'email est incorrect, les données ne sont pas envoyé
     *      - 5 : un problème est survenu lors de l'envoi du mail, les données ne sont pas envoyé
     */
    public function envoyerDonnees(string $login, string $email)
    {
        if (isset($login) && strlen($login) > 0 && isset($login) && strlen($login) > 0) {
            if (strlen($login) <= 45 && strlen($login) <= 45) {
                $result = $this->_obtenirInfosSocietaireBdd($login);
                if (isset($result) && $result !== null) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $nomSite = $this->_obtenirInfosSiteBdd()->sit_nom;
                        $structure = $this->_obtenirInfosSiteBdd()->sit_structure;
                        if ($structure) {
                            $structure = "GFA";
                            $pronom = "le";
                        } else {
                            $structure = "SCI";
                            $pronom = "la";
                        }
                        // Autorisations
                        $accepteAppel = 'OUI';
                        if ($result['soc_pasAppelerFixe']) {
                            $accepteAppel = 'NON';
                        }
                        $accepteAppelPortable = 'OUI';
                        if ($result['soc_pasAppelerPortable']) {
                            $accepteAppelPortable = 'NON';
                        }
                        $accepteSms = 'OUI';
                        if ($result['soc_pasEnvoyerSms']) {
                            $accepteSms = 'NON';
                        }
                        $accepteMail = 'OUI';
                        if ($result['soc_pasEnvoyerEmail']) {
                            $accepteMail = 'NON';
                        }
                        $cloud = 'NON';
                        if ($result['soc_autoriserCloud']) {
                            $cloud = 'OUI';
                        }
                        $this->CI->email->from($this->CI->config->item('smtp_user'), $nomSite);
                        $this->CI->email->bcc($this->CI->config->item('smtp_user'));
                        $this->CI->email->to($email);
                        $this->CI->email->subject('[' . $structure . ' ' . $nomSite . '] Communication des données personnelles');
                        $this->CI->email->message('
                        <head>
                            <style>
                                table {
                                    border: 3px solid black;
                                    border-collapse: collapse;
                                    table-layout: auto;
                                }
                                th, td {
                                    padding: 10px;
                                    text-align: left;
                                    border: 2px solid black;
                                    border-collapse: collapse;
                                }
                            </style>
                            </head>
                        <body>
                            <div style="font-family: helvetica">
                                <h2>Données personnelles stocké par ' . $pronom . ' ' . $structure . ' ' . $nomSite . '</h2>
                                <table style="width:50%; font-size: 1.1em;">
                                    <tr>
                                        <th>Login</th>
                                        <td>' . $result["soc_login"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Prénom</th>
                                        <td>' . $result["soc_prenom"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Autres Prénoms</th>
                                        <td>' . $result["soc_autrePrenom"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Nom</th>
                                        <td>' . $result["soc_nom"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Nom usuel</th>
                                        <td>' . $result["soc_nomUsuel"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Représentants Légaux</th>
                                        <td>' . $result["soc_representantsLegaux"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Civilité</th>
                                        <td>' . $result["soc_civilite"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Statut</th>
                                        <td>' . $result["sta_nom"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Numéro de siret</th>
                                        <td>' . $result["soc_siret"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Date de naissance</th>
                                        <td>' . $result["soc_naissance"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Lieu de naissance</th>
                                        <td>' . $result["soc_lieuNaissance"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Nationalité</th>
                                        <td>' . $result["soc_nationalite"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>' . $result["soc_email"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Téléphone fixe</th>
                                        <td>' . $result["soc_telephoneFixe"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Téléphone portable</th>
                                        <td>' . $result["soc_telephonePortable"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Adresse</th>
                                        <td>' . $result["soc_adresse"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Code Postal</th>
                                        <td>' . $result["soc_codePostal"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Ville</th>
                                        <td>' . $result["soc_ville"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Pays</th>
                                        <td>' . $result["soc_pays"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Régime</th>
                                        <td>' . $result["reg_nom"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Date d\'entrée dans la société</th>
                                        <td>' . $result["ass_dateEntree"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Date de sortie de la société</th>
                                        <td>' . $result["ass_dateSortie"] . '</td>
                                    </tr>
                                    <tr>
                                        <th>Autoriser les appels sur le téléphone fixe</th>
                                        <td>' . $accepteAppel . '</td>
                                    </tr>
                                    <tr>
                                        <th>Autoriser les appels sur le téléphone portable</th>
                                        <td>' . $accepteAppelPortable . '</td>
                                    </tr>
                                    <tr>
                                        <th>Autoriser les sms</th>
                                        <td>' . $accepteSms . '</td>
                                    </tr>
                                    <tr>
                                        <th>Autoriser les emails</th>
                                        <td>' . $accepteMail . '</td>
                                    </tr>
                                    <tr>
                                        <th>Autoriser le stockage des données dans le cloud</th>
                                        <td>' . $cloud . '</td>
                                    </tr>
                                </table>
                            </div>
                        </body>');
                        if ($this->CI->email->send()) {
                            return 0;
                        } else {
                            return 5;
                        }
                    } else {
                        return 4;
                    }
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Supprime un societaire
     *
     * @param  string $login
     * @return int 
     *      - 0 : le sociétaire est supprimé
     *      - 1 : le login est vide, le sociétaire n'est pas supprimé
     *      - 2 : le login est trop long (>45), le sociétaire n'est pas supprimé
     *      - 3 : le login n'éxiste pas, le sociétaire n'est pas supprimé
     */
    public function supprimerSocietaire(string $login)
    {
        if (isset($login) && strlen($login) > 0) {
            if (strlen($login) <= 45) {
                $resultLogin = $this->_obtenirInfosSocietaireBdd($login);
                if (isset($resultLogin) && $resultLogin !== null) {
                    $this->_supprimerSocietaireBdd($login);
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

	/**
	 * activation d'un societaire
	 *
	 * @param  string $login
	 * @return int 
	 *		- 0 : le sociétaire est désactivé si il est activé ou désactivé si il est activé
	 *		- 1 : le login est vide, le sociétaire n'est pas activé/désactivé
	 *		- 2 : le login est trop long (>45), le sociétaire n'est pas activé/désactivé
	 *		- 3 : le login n'éxiste pas, le sociétaire n'est pas activé/désactivé
     *      - 4 : le site est en local ou le sociétaire n'a pas d'email, le sociétaire est activé mais le mail n'est pas envoyé
     *      - 5 : le sociétaire est activé mais le mail n'a pas pu etre envoyé
	 */
    public function activationSocietaire(string $login)
    {
        if (isset($login) && strlen($login) > 0) {
            if (strlen($login) <= 45) {
                $societaire = $this->_obtenirInfosSocietaireBdd($login);
                if (isset($societaire) && $societaire !== null) {
                    if ($societaire['soc_etat']=='B' || $societaire['soc_etat']=='A') {
                        $this->_modifierEtatBdd($login,'D');
                        $nouveauMdpHash = password_hash($login, PASSWORD_BCRYPT);
                        $this->_modifierMdpBdd($login, $nouveauMdpHash);
                    } else {
                        $this->_modifierEtatBdd($login, 'B');
                        $isLocal = $this->_obtenirInfosSiteBdd()->sit_local;
                        if (!$isLocal && isset($societaire[soc_email])) {
                            $mail = new Mailing();
                            $societaire = $this->_obtenirInfosSocietaireBdd($login);
                            return ($mail->mailActivationCompte($societaire));
                        } else {
                            return 4;
                        }
                    }
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Supprime les données RPGD d'un sociétaire
     *
     * @param  string $login
     * @return int 
     *      - 0 : les données sont supprimées
     *      - 1 : le login est vide, les données ne sont pas supprimé
     *      - 2 : le login est trop long (>45), les données ne sont pas supprimé
     *      - 3 : le login n'éxiste pas, les données ne sont pas supprimé
     */
    public function supprimerRGPD(string $login)
    {
        if (isset($login) && strlen($login) > 0) {
            if (strlen($login) <= 45) {
                $resultLogin = $this->_obtenirInfosSocietaireBdd($login);
                if (isset($resultLogin) && $resultLogin !== null) {
                    $this->_supprimerRGPDBdd($login);
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Envoi un mail pour signaler un problème avec un formulaire
     *
     * @param  string $login login du sociétaire qui signale le problème
     * @param  array $champs liste des champs
     * @param  string $message commentaire de l'utilisateur
     * @return int
     *      - 0 : le mail est envoyé
     *      - 1 : un des paramètres est vide, le mail n'est pas envoyé
     *      - 2 : un problème est survenue lors de l'envoi de mail, le mail n'est pas envoyé
     */
    public function signalerProbleme(string $login, array $champs, string $message)
    {
        if (isset($champs) && sizeof($champs) > 0 && isset($login) && strlen($login) > 0 && isset($message) && strlen($message) > 0) {
            $societaire = $this->obtenirInfos($login);
            $nomSite = $this->_obtenirInfosSiteBdd()->sit_nom;
            $structure = $this->_obtenirInfosSiteBdd()->sit_structure;
            if ($structure) {
                $structure = "GFA";
            } else {
                $structure = "SCI";
            }
            if (!isset($societaire["soc_email"])) {
                return 3;
            }
            $this->CI->email->from($societaire["soc_email"]);
            $this->CI->email->to($this->CI->config->item('smtp_user'));
            $this->CI->email->subject('[' . $structure . ' ' . $nomSite . '] Erreur '. $societaire["soc_nom"]);
            $mess1 = '<p>L\'utilisateur '.$societaire["soc_prenom"].' '.$societaire["soc_nom"].' signale des problèmes sur les données :<br><br></p>';
            $mess2 = '';
            foreach ($champs as $ch) {
                switch ($ch[1]) {
                    case 1:$valeur=$societaire['sta_nom'];break;
                    case 2:
                        if ($societaire['sta_indivision'] == 1) {
                            $valeur='OUI'; 
                        } else {
                            $valeur='NON';
                        }break;
                    case 3:
                        switch ($societaire['soc_civilite']) {
                            case 1:
                                $valeur='Madame';
                                break;
                            case 2:
                                $valeur='Non binaire';
                                break;
                            case 3:
                                $valeur='Non renseigné';
                                break;
                            case 4:
                                $valeur='Autre';
                                break;
                            default:
                            $valeur='Monsieur';
                                break;
                        }
                        break;
                    case 4:$valeur=$societaire["soc_nom"];break;
                    case 5:$valeur=$societaire["soc_prenom"];break;
                    case 6:$valeur=$societaire["soc_nomUsuel"];break;
                    case 7:$valeur=$societaire["soc_autrePrenom"];break;
                    case 8:$valeur=$societaire["soc_representantsLegaux"];break;
                    case 9:$valeur= date('d-m-Y', strtotime($societaire['soc_naissance']));break;
                    case 10:$valeur=$societaire['soc_lieuNaissance'];break;
                    case 11:$valeur=$societaire['soc_siret'];break;
                    case 12:$valeur=$societaire['soc_nationalite'];break;
                    case 13:$valeur=$societaire['soc_adresse'];break;
                    case 14:$valeur=$societaire['soc_codePostal'];break;
                    case 15:$valeur=$societaire['soc_ville'];break;
                    case 16:$valeur=$societaire['soc_pays'];break;
                    case 17:$valeur=$societaire['reg_nom'];break;
                    case 18:$valeur=date('d-m-Y', strtotime($societaire['ass_dateEntree']));break;
                    default: break;
                }
                $mess2 = $mess2 .$ch[0].' : '.$valeur.'<br>';
            }
            $this->CI->email->message($mess1 . $mess2 .'<br><br><p>Messsage :<br></p>'. $message);
            if ($this->CI->email->send()) {
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Envoi un mail à la co-gérance
     *
     * @param  string $login login du sociétaire 
     * @param  string $message commentaire de l'utilisateur
     * @return int
     *      - 0 : le mail est envoyé
     *      - 1 : un des paramètres est vide, le mail n'est pas envoyé
     *      - 2 : un problème est survenue lors de l'envoi de mail, le mail n'est pas envoyé
     *      - 3 : le sociétaire n'a pas de mail
     */
    public function mailCogerance(string $login, string $message)
    {
        if (isset($login) && strlen($login) > 0 && isset($message) && strlen($message) > 0) {
            $societaire = $this->obtenirInfos($login);
            $nomSite = $this->_obtenirInfosSiteBdd()->sit_nom;
            $structure = $this->_obtenirInfosSiteBdd()->sit_structure;
            if ($structure) {
                $structure = "GFA";
            } else {
                $structure = "SCI";
            }
            if (!isset($societaire["soc_email"])) {
                return 3;
            }
            $this->CI->email->from($societaire["soc_email"]);
            $this->CI->email->to($this->CI->config->item('smtp_user'));
            $this->CI->email->subject('['.$structure.' '.$nomSite.'] Message de '.$societaire["soc_nom"]);
            $mess1 = 'Message du sociétaire '.$societaire["soc_prenom"].' '.$societaire["soc_nom"].' : <br><br>';
            $this->CI->email->message($mess1 . $message);
            if ($this->CI->email->send()) {
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Supprime un sociétaire
     *
     * @access private
     * @param  string $login login du sociétaire
     * @return object
     */
    private function _supprimerSocietaireBdd($login)
    {
        $req1 = "DELETE FROM t_gerant_ger
        WHERE soc_login = ?;";

        $req2 = "DELETE FROM t_mouvement_mou
        WHERE mou_id IN (
            SELECt * FROM (
                SELECT mou_id FROM t_operation_ope
                JOIN t_mouvement_mou using(ope_id)
                WHERE soc_loginTitulaire = ?
                OR soc_loginBeneficiaire = ?
            ) tmp
        );";

        $req21 = "DELETE FROM t_paiement_pai
        WHERE ope_id in (
            SELECT ope_id FROM t_operation_ope
            WHERE soc_loginTitulaire = ?
            OR soc_loginBeneficiaire = ?);";

        $req3 = "DELETE FROM t_operation_ope
        WHERE soc_loginTitulaire = ?
        OR soc_loginBeneficiaire = ?;";

        $req4 = "DELETE FROM t_token_tok
        WHERE soc_login = ?;";

        $req5 = "DELETE FROM t_societaire_soc
        WHERE soc_login = ?;";

        $this->CI->db->query($req1, array($login));
        $this->CI->db->query($req2, array($login, $login));
        $this->CI->db->query($req21, array($login, $login));
        $this->CI->db->query($req3, array($login, $login));
        $this->CI->db->query($req4, array($login));
        $this->CI->db->query($req5, array($login));
    }

    /**
     * Supprime les données inutilisé d'un sociétaire
     *
     * @access private
     * @param  string $login login du sociétaire 
     * @return object
     */
    private function _supprimerRGPDBdd($login)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_email = null,
        soc_telephoneFixe = null,
        soc_telephonePortable = null,
        soc_adresse = null,
        soc_codePostal = null,
        soc_ville = null,
        soc_pays = null
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($login));
    }

    /**
     * Modifie les informations personnelles d'un sociétaire
     *
     * @access private
     * @param  string $login login du societaire
     * @param  string $prenom prénom du societaire
     * @param  string|null $autrePrenom autres prénoms du societaire
     * @param  string $nom nom du societaire
     * @param  string|null $nomUsuel nom du societaire
     * @param  string|null $representantsLegaux réprésentants légaux
     * @param  int    $civilite civilité du societaire
     * @param  int    $statut statut du societaire
     * @param  string|null $siret numéro de siret du nouveau sociétaire si personne morale
     * @param  string $dateNaissance date de naissance du societaire
     * @param  string|null $lieuNaissance lieu de naissance du societaire
     * @param  string|null $nationalite nationalité du nouveau societaire
     * @param  bool $indivision booléen pour déterminé si le sociétaire est le réprésentant d'une indivision
     * @return void
     */
    private function _modifierInfosPersoBdd(string $login, string $prenom, $autrePrenom, string $nom, $nomUsuel, $representantsLegaux, int $civilite, int $statut, $siret, string $dateNaissance, $lieuNaissance, $nationalite, bool $indivision)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_prenom = ?,
        soc_autrePrenom = ?,
        soc_nom = ?,
        soc_nomUsuel = ?,
        soc_representantsLegaux = ?,
        soc_civilite = ?,
        sta_id = ?,
        soc_siret = ?,
        soc_naissance = ?,
        soc_lieuNaissance = ?,
        soc_nationalite = ?,
        soc_indivision = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $indivision, $login));
    }

    /**
     * modifie les informations de contact d'un sociétaire
     *
     * @access private
     * @param  string $login login du societaire
     * @param  string|null $email courriel du societaire
     * @param  string|null $telFixe téléphone fixe du societaire
     * @param  string|null $telPortable téléphone portable du societaire
     * @param  string $adresse adresse du societaire
     * @param  string $codePostal code postal du societaire
     * @param  string $ville ville du societaire
     * @param  string $pays pays du societaire
     * @return void
     */
    private function _modifierInfosContactBdd(string $login, $email, $telFixe, $telPortable, string $adresse, string $codePostal, string $ville, string $pays)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_email = ?,
        soc_telephoneFixe = ?,
        soc_telephonePortable = ?,
        soc_adresse = ?,
        soc_codePostal = ?,
        soc_ville = ?,
        soc_pays = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $login));
    }

    /**
     * modifie les informations sci d'un sociétaire
     *
     * @access private
     * @param  string $login login du societaire
     * @param  int $regime régime matrimonial ou fiscal du societaire (Communauté, Contrat)
     * @param  int $agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
     * @return void
     */
    private function _modifierInfosSciBdd(string $login, int $regime, int $agEntree)
    {
        $req = "UPDATE t_societaire_soc
        SET reg_id = ?,
        ass_idEntree = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($regime, $agEntree, $login));
    }

    /**
     * modifie la date de sortie d'un sociétaire
     *
     * @access private
     * @param  string $login login du societaire
     * @param  int $agSortie assemblée générale pendant laquelle le sociétaire est parti de la sci
     * @return void
     */
    private function _modifierDateSortieBdd(string $login, int $agSortie)
    {
        $req = "UPDATE t_societaire_soc
        SET ass_idSortie = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($agSortie, $login));
    }

    /**
     * modifie les autorisations d'un sociétaire
     *
     * @access private
     * @param  bool $login login du societaire
     * @param  bool $pasFixe ne pas appeler sur le téléphone fixe
     * @param  bool $pasPortable ne pas appeler sur le téléphone portable
     * @param  bool $pasSms ne pas envoyer d'sms
     * @param  bool $pasEmail ne pas envoyer d'email
     * @param  bool $cloud autoriser le stockage dans le cloud
     * @return void
     */
    private function _modifierAutorisationBdd(string $login, bool $pasFixe, bool $pasPortable, bool $pasSms, bool $pasEmail, bool $cloud)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_pasAppelerFixe = ?,
        soc_pasAppelerPortable = ?,
        soc_pasEnvoyerSms = ?,
        soc_pasEnvoyerEmail = ?,
        soc_autoriserCloud = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($pasFixe, $pasPortable, $pasSms, $pasEmail, $cloud, $login));
    }

    /**
     * Modifie le mot de passe d'un sociétaire
     *
     * @access private
     * @param  string $login
     * @param  string $mdp
     * @return void
     */
    private function _modifierMdpBdd($login, $mdp)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_mot_de_passe = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($mdp, $login));
    }

    /**
     * Modifie la validite d'un sociétaire
     *
     * @access private
     * @param  string $login
     * @param  string $etat
     * @return void
     */
    private function _modifierEtatBdd($login, $etat)
    {
        $req = "UPDATE t_societaire_soc
        SET soc_etat = ?
        WHERE soc_login = ?;";

        $this->CI->db->query($req, array($etat, $login));
    }

    /**
     * Récupère les informations d'un sociétaire ou admin à partir d'un email
     *
     * @access private
     * @param  string $login
     * @return object
     */
    private function _obtenirSocietaireAvecEmailBdd($email)
    {
        $req = "SELECT adm_login as login
        FROM t_administrateur_adm
        WHERE adm_email = ?
        UNION
        SELECT soc_login as login
        FROM t_societaire_soc
        WHERE soc_email = ?;";

        $result = $this->CI->db->query($req, array($email, $email))->row();
        return $result;
    }

    /**
     * Récupère les informations d'un sociétaire ou admin à partir d'un login
     *
     * @access private
     * @param  string $login
     * @return object
     */
    private function _obtenirSocietaireAvecLoginBdd($login)
    {
        $req = "SELECT adm_login
        FROM t_administrateur_adm
        WHERE adm_login = ?
        UNION
        SELECT soc_login 
        FROM t_societaire_soc
        WHERE soc_login = ?;";

        $result = $this->CI->db->query($req, array($login, $login))->row();
        return $result;
    }

    /**
     * Récupère les informations d'un sociétaire
     *
     * @access private
     * @param  string $login
     * @return object
     */
    private function _obtenirInfosSocietaireBdd($login)
    {
        $req = "SELECT *, t1.ass_date as ass_dateEntree, t2.ass_date as ass_dateSortie
        FROM t_societaire_soc
        JOIN t_regime_reg USING(reg_id)
        JOIN t_statut_sta USING(sta_id)
        JOIN t_assembleeGenerale_ass as t1 on ass_idEntree = t1.ass_id
        LEFT JOIN t_assembleeGenerale_ass as t2 on ass_idSortie = t2.ass_id
        WHERE soc_login = ?;";

        $result = $this->CI->db->query($req, array($login))->row_array();
        return $result;
    }

    /**
     * Récupère toute les informations de tous les sociétaires
     *
     * @access private
     * @return object
     */
    private function _obtenirTousLesSocietairesBdd()
    {
        $query = $this->CI->db->query("SELECT *, t1.ass_date as ass_dateEntree, t2.ass_date as ass_dateSortie
        FROM t_societaire_soc
        JOIN t_regime_reg USING(reg_id)
        JOIN t_statut_sta USING(sta_id)
        JOIN t_assembleeGenerale_ass as t1 on ass_idEntree = t1.ass_id
        LEFT JOIN t_assembleeGenerale_ass as t2 on ass_idSortie = t2.ass_id;");
        return $query->result_array();
    }

    /**
     * Récupère les infos du site
     *
     * @access private
     * @return object
     */
    private function _obtenirInfosSiteBdd()
    {
        $query = $this->CI->db->query("SELECT sit_nom, sit_structure, sit_local
        FROM t_site_sit;");
        return $query->row();
    }

    /**
     * Récupère le mot de passe hashé d'un sociétaire
     *
     * @access private
     * @param  string $login
     * @return object
     */
    private function _obtenirMdpBdd($login)
    {
        $req = "SELECT soc_mot_de_passe
        FROM t_societaire_soc
        WHERE soc_login = ?;";

        $result = $this->CI->db->query($req, array($login))->row();
        return $result;
    }

    /**
     * Récupère toutes les parts détenus par un sociétaire
     *
     * @access private
     * @param string $login login d'un titulaire de part
     * @return object
     */
    private function _obtenirPartSocietaireBdd(string $login)
    {
        $req = "SELECT mou_numPart FROM(
            SELECT mou_numPart, soc_loginTitulaire, soc_loginBeneficiaire, ope_type FROM (
                SELECT * FROM t_operation_ope ope
                JOIN t_mouvement_mou USING(ope_id)
                ORDER BY ope_id DESC) tmp1
            GROUP BY mou_numPart) tmp2
        WHERE (soc_loginBeneficiaire = ?);";

        $result = $this->CI->db->query($req, array($login))->result_array();
        return $result;
    }

    /**
     * Ajoute un societaire dans la table societaire
     *
     * @access private
     * @param  string $login login du nouveau sociétaire
     * @param  string $mdp mot de passe du nouveau sociétaire
     * @param  string $prenom prénom du nouveau sociétaire
     * @param  string|null $autrePrenom autres prénoms du nouveau sociétaire
     * @param  string $nom nom du nouveau sociétaire
     * @param  string|null $nomUsuel nom du nouveau sociétaire
     * @param  string|null $representantsLegaux réprésentants légaux
     * @param  int    $civilite civilité du nouveau sociétaire
     * @param  int    $statut statut du nouveau sociétaire
     * @param  string|null $siret numéro de siret du nouveau sociétaire si personne morale
     * @param  string $dateNaissance date de naissance du nouveau sociétaire
     * @param  string|null $lieuNaissance lieu de naissance du nouveau sociétaire
     * @param  string|null $nationalite nationalité du nouveau societaire
     * @param  string|null $email courriel du nouveau societaire
     * @param  string|null $telFixe téléphone fixe du nouveau societaire
     * @param  string|null $telPortable téléphone portable du nouveau societaire
     * @param  string $adresse adresse du nouveau societaire
     * @param  string $codePostal code postal du nouveau societaire
     * @param  string $ville ville du nouveau societaire
     * @param  string $pays pays du nouveau societaire
     * @param  int    $regime régime matrimonial ou fiscal du nouveau societaire
     * @param  int $agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
     * @param  bool $indivision booléen pour déterminé si le sociétaire est le réprésentant d'une indivision
     * @return void
     */
    private function _ajouterBdd(string $login, string $mdp, string $prenom, $autrePrenom, string $nom, $nomUsuel, $representantsLegaux, int $civilite, int $statut, $siret, string $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, string $adresse, string $codePostal, string $ville, string $pays, int $regime, int $agEntree, bool $indivision, $pasEmail)
    {
        $req = "INSERT INTO t_societaire_soc
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, null, '1', '1', '1', ?, '1', '0', ?, 'D');";

        $this->CI->db->query($req, array($login, $mdp, $prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $regime, $agEntree, $pasEmail, $indivision));
    }
}
