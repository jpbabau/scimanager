<?php

/**
 * Classe Site
 */
class Site
{

    /**
     * CodeIgniter super-object
     *
     * @access protected
     * @var object
     */
    protected $CI;

    /**
     * Constructeur de la classe Site
     *
     * @return void
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

	/**
	 * Modifie le type de structure
	 *
	 * @param  int $structure le nouveau type de structure
	 * @return int
	 * 		- 0 : le type de structure a été modifié
	 * 		- 1 : le nouveau type de structure n'est pas valide, l'ancien type est conservé
	 * 		- 2 : le nouveau type de strucutre est vide, l'ancien type est conservé
	 */
	public function modifierStructure(int $structure)
	{
		if (isset($structure)) {
			if ($structure === 0 || $structure === 1) {
				$this->_modifierStructureBdd($structure);
				return 0;
			} else {
				return 1;
			}
		} else {
			return 2;
		}
	}

    /**
     * Modifie le nom du site
     *
     * @param  string $nouveauNom nouveau nom du site
     * @return int 
     *      - 0 : l'ancien nom est remplacé par le nouveau nom
     *      - 1 : le nouveau nom est vide, l’ancien nom est conservé
     *      - 2 : le nouveau nom est trop long (>15), ou trop court (<5). l’ancien nom est conservé
     */
    public function modifierNom(string $nouveauNom)
    {
        if (isset($nouveauNom) && strlen($nouveauNom) > 0) {
            if (strlen($nouveauNom) <= 15 && strlen($nouveauNom) >= 5) {
                $this->_modifierNomBdd($nouveauNom);
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Modifie le nom de l'icone du site
     *
     * @param  string $nomNouvelleIcone nom de la nouvelle icone du site
     * @return int 
     *      - 0 : le nom de l'ancienne icone est remplacé par le nom de la nouvelle icone
     *      - 1 : le nom de la nouvelle icone est vide, le nom de l'ancienne icone est conservé
     *      - 2 : le nom de la nouvelle icone est trop longue (>45), le nom de l'ancienne icone est conservé
     *      - 3 : le format de la nouvelle icone n'est pas correct (jpg), le nom de l'ancienne icone est conservé
     */
    public function modifierIcone(string $nomNouvelleIcone)
    {
        if (isset($nomNouvelleIcone) && strlen($nomNouvelleIcone) > 0) {
            if (strlen($nomNouvelleIcone) <= 45) {
                $tempIconeFormat = explode(".", $nomNouvelleIcone);
                $iconeFormat = end($tempIconeFormat);
                if ($iconeFormat === "jpg" || $iconeFormat === "jpeg") {
                    $this->_modifierIconeBdd($nomNouvelleIcone);
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Modifie le nom de l'image du site
     *
     * @param  string $nomNouvelleImage nom de la nouvelle image du site
     * @return int 
     *      - 0 : le nom de l'ancienne image est remplacé par le nom de la nouvelle image
     *      - 1 : le nom de la nouvelle image est vide, le nom de l'ancienne image est conservé
     *      - 2 : le nom de la nouvelle image est trop longue (>45), le nom de l'ancienne image est conservé
     *      - 3 : le format de la nouvelle image n'est pas correct (jpg), le nom de l'ancienne image est conservé
     */
    public function modifierImage(string $nomNouvelleImage)
    {
        if (isset($nomNouvelleImage) && strlen($nomNouvelleImage) > 0) {
            if (strlen($nomNouvelleImage) <= 45) {
                $tempImageFormat = explode(".", $nomNouvelleImage);
                $imageFormat = end($tempImageFormat);
                if ($imageFormat === "jpg" || $imageFormat === "jpeg") {
                    $this->_modifierImageBdd($nomNouvelleImage);
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    /**
     * Modifie l'email du site
     *
     * @param  string $email nouvel email du site
     * @return int 
     *      - 0 : l'ancien email est remplacé par le nouvel email
     *      - 1 : le nouvel email ou le login est vide, l'ancien email est conservé
     *      - 2 : le nouvel email ou le login est trop long (>45), l'ancien email est conservé
     *      - 3 : le format du nouvel email est incorrect, l'ancien email est conservé
     */
    public function modifierEmail(string $email)
    {
        if (isset($email) && strlen($email) > 0) {
            if (strlen($email) <= 45) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $this->_modifierEmailBdd($email);
                    return 0;
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }
	
	/**
	 * Modifie la durée RGPD
	 *
	 * @param  int $dureeRGPD la nouvelle durée RGPD en mois
	 * @return int
	 * 		- 0 : la durée RGPD a été modifiée
	 * 		- 1 : la nouvelle durée est soit vide soit négative, la durée RGPD n'est pas modifiée
	 */
	public function modifierDureeRGPD(int $dureeRGPD)
	{
		if (isset($dureeRGPD) && $dureeRGPD > 0) {
			$this->_modifierDureeRGPDBdd($dureeRGPD);
			return 0;
		} else {
			return 1;
		}
	}

	/**
	 * Modifie le type d'installation
	 *
	 * @param  int $local installation en local
	 * @return void
	 */
	public function siteModifierLocal(bool $local) {
		$this->_modifierLocal($local);
	}

    /**
     * Récupère toutes les informations du site
     *
     * @return array<string, string> $info tableau qui contient le nom, l'icone et l'image du site, retourne null si le résultat est vide
     * 
     *     $infos = [<br>
     *          "nom"       =>  (string) nom<br>
     *          "icone"     =>  (string) icone<br>
     *          "image"     =>  (string) image<br>
     *          "structure" =>  (int) structure<br>
	 * 			"dureeRGPD" =>	(int) dureeRGPD<br>
     *      ]
     */
    public function obtenirInfos()
    {
        $infosBdd = $this->_obtenirInfosBdd();
        if (isset($infosBdd) && $infosBdd != null) {
            $infos = array(
                "nom" => $infosBdd->sit_nom,
                "icone" => $infosBdd->sit_icone,
                "image" => $infosBdd->sit_image,
                "email" => $infosBdd->sit_email,
                "structure" => $infosBdd->sit_structure,
				"dureeRGPD"	=> $infosBdd->sit_dureeRGPD,
                "isLocal"	=> $infosBdd->sit_local
            );
            return $infos;
        } else {
            return null;
        }
    }

    /**
     * Désinstalle la base de données
     *
     * @return void
     */
    public function desinstallation()
    {
        $this->_desinstallationBdd();
    }

    /**
     * Récupère les données du site
     *
     * @access private
     * @return mixed informations du site
     */
    private function _obtenirInfosBdd()
    {
        $query = $this->CI->db->query("SELECT *
        FROM t_site_sit;");

        return $query->row();
    }
	
	/**
	 * Modifie la structure du site dans la table site
	 *
	 * @access private
	 * @param  int $structure
	 * @return void
	 */
	private function _modifierStructureBdd($structure)
	{
		$req = "UPDATE t_site_sit
		SET sit_structure = ?;";

		$this->CI->db->query($req, array($structure));
	}

    /**
     * Modifie le nom du site dans la table site
     *
     * @access private
     * @param  string $nom
     * @return void
     */
    private function _modifierNomBdd($nom)
    {
        $req = "UPDATE t_site_sit
        SET sit_nom = ?;";

        $this->CI->db->query($req, array($nom));
    }

    /**
     * Modifie l'icone du site dans la table site
     *
     * @access private
     * @param  string $icone
     * @return void
     */
    private function _modifierIconeBdd($icone)
    {
        $req = "UPDATE t_site_sit
        SET sit_icone = ?;";

        $this->CI->db->query($req, array($icone));
    }

    /**
     * Modifie l'email du site
     *
     * @access private
     * @param  string $email
     * @return void
     */
    private function _modifierEmailBdd($email)
    {
        $req = "UPDATE t_site_sit
        SET sit_email = ?;";

        $this->CI->db->query($req, array($email));
    }

    /**
     * Modifie l'image du site dans la table site
     *
     * @access private
     * @param  string $image
     * @return void
     */
    private function _modifierImageBdd($image)
    {
        $req = "UPDATE t_site_sit
        SET sit_image = ?;";

        $this->CI->db->query($req, array($image));
    }
	
	/**
	 * Modifie la durée RGPD dans la table site
	 *
	 * @access private
	 * @param  int $dureeRGPD
	 * @return void
	 */
	private function _modifierDureeRGPDBdd($dureeRGPD)
	{
		$req = "UPDATE t_site_sit
		SET sit_dureeRGPD = ?;";

		$this->CI->db->query($req, array($dureeRGPD));
	}

	/**
	 * Modifie le type d'installation dans la table site
	 *
	 * @access private
	 * @param  bool $local
	 * @return void
	 */
	private function _modifierLocal($local)
	{
		$req = "UPDATE t_site_sit
		SET sit_local = ?;";

		$this->CI->db->query($req, array($local));
	}

    /**
     * Supprime toutes les tables de la base de données
     *
     * @access private
     * @return void
     */
    private function _desinstallationBdd()
    {
        $this->CI->db->query(
            "DROP TABLE IF EXISTS 
        t_site_sit, t_token_tok,
        t_administrateur_adm, t_gerant_ger,
        t_indivision_ind, t_societaireRepresente_socr,
        t_mouvement_mou, t_paiement_pai,
        t_moyenPai_moy, t_operation_ope,
        t_societaire_soc, t_regime_reg,
        t_assembleeGenerale_ass, t_statut_sta;"
        );
    }
}
