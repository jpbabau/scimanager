<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'libraries/parametresSite.php';
require_once APPPATH . 'libraries/compte.php';
require_once APPPATH . 'libraries/societaire.php';
require_once APPPATH . 'libraries/assembleeGenerale.php';
require_once APPPATH . 'libraries/parametresAvances.php';
require_once APPPATH . 'libraries/mouvement.php';
require_once APPPATH . 'libraries/indivision.php';
require_once APPPATH . 'libraries/installation.php';
require_once APPPATH . 'libraries/societaireIO.php';
require_once APPPATH . 'libraries/mouvementIO.php';
require_once APPPATH . 'libraries/partO.php';
require_once APPPATH . 'libraries/mailing.php';

/**
 * Classe de l'API SciManager
 */
class ApiSciManager {
	
	/**
	 * Les paramètres site
	 *
	 * @access protected
	 * @var ParametresSite
	 */
	protected $parametresSite;
	
	/**
	 * La gestion du compte
	 *
	 * @access protected
	 * @var Compte
	 */
	protected $gestionCompte;
	
	/**
	 * Le societiare
	 *
	 * @access protected
	 * @var Societaire
	 */
	protected $societaire;
	
	/**
	 * L'assemblée générale
	 *
	 * @access protected
	 * @var AssembleeGenerale
	 */
	protected $assembleeGenerale;
	
	/**
	 * Les paramètres avancés
	 *
	 * @access protected
	 * @var GestionChampsSocietaire
	 */
	protected $parametresAvances;
	
	/**
	 * Le mouvement
	 *
	 * @access protected
	 * @var Mouvement
	 */
	protected $mouvement;

	/**
	 * L'indivision
	 *
	 * @access protected
	 * @var Indivision
	 */
	protected $indivision;
	
	/**
	 * L'installation
	 *
	 * @access protected
	 * @var Installation
	 */
	protected $installation;
	
	/**
	 * La classe d'import / export des sociétaires
	 *
	 * @access protected
	 * @var SocietaireIO
	 */
	protected $societaireIO;
	
	/**
	 * La classe d'import / export des mouvements
	 *
	 * @access protected
	 * @var MouvementIO
	 */
	protected $mouvementIO;
	
	/**
	 * La classe d'export des parts
	 *
	 * @access protected
	 * @var PartO
	 */
	protected $partO;
	
	/**
	 * La classe de mailing
	 *
	 * @access protected
	 * @var Mailing
	 */
	protected $mailing;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		$this->parametresSite		= new ParametresSite();
		$this->gestionCompte		= new Compte();
		$this->societaire			= new Societaire();
		$this->assembleeGenerale	= new AssembleeGenerale();
		$this->parametresAvances	= new ParametresAvances();
		$this->mouvement			= new Mouvement();
		$this->indivision			= new Indivision();
		$this->installation			= new Installation();
		$this->societaireIO			= new SocietaireIO(1);	// Xlsx
		$this->mouvementIO			= new MouvementIO(1);	// Xlsx
		$this->partO				= new PartO(1);			// Xlsx
		$this->mailing				= new Mailing();
	}

	/**
	 * Modifie le login de l'administrateur
	 *
	 * @param  string $nouveauLogin Le nouveau login de l'administrateur
	 * @return int
	 *		- 0 : l'ancien login est remplacé par le nouveau login
	 *		- 1 : le nouveau login est vide, l’ancien login est conservé
	 *		- 2 : le nouveau login est trop long (>45), l’ancien login est conservé
	 *		- 3 : le nouveau login est déjà utilisé par un utilisateur, l'ancien login est conservé
	 */
	public function adminModifierLogin(string $nouveauLogin) {
		return $this->parametresSite->adminModifierLogin($nouveauLogin);
	}

	/**
	 * Modifie l'email de l'administrateur
	 *
	 * @param  string $nouvelEmail Le nouvel email de l'administrateur
	 * @return int
	 *		- 0 : l'ancien email est remplacé par le nouvel email
	 *		- 1 : le nouvel email est vide, l'ancien email est conservé
	 *		- 2 : le nouvel email est trop long (>45), l'ancien email est conservé
	 *		- 3 : le format du nouvel email est incorrect, l'ancien email est conservé
	 */
	public function adminModifierEmail(string $nouvelEmail) {
		return $this->parametresSite->adminModifierEmail($nouvelEmail);
	}

	/**
	 * Modifie le mot de passe de l'administrateur
	 *
	 * @param  string $ancienMdp L'ancien mot de passe de l'administrateur
	 * @param  string $nouveauMdp Le nouveau mot de passe de l'administrateur
	 * @return int
	 *		- 0 : l'ancien mot de passe est remplacé par le nouveau mot de passe
	 *		- 1 : le nouveau ou l'ancien mot de passe est vide, l'ancien mot de passe est conservé
	 *		- 2 : le nouveau ou l'ancien mot de passe est trop long, l'ancien mot de passe est conservé
	 *		- 3 : l'ancien mot de passe est incorrect, l'ancien mot de passe est conservé
	 *		- 4 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
	 */
	public function adminModifierMdp(string $ancienMdp, string $nouveauMdp) {
		return $this->parametresSite->adminModifierMdp($ancienMdp, $nouveauMdp);
	}

	/**
	 * Récupére les informations de l'administrateur
	 *
	 * @return array<string,string> $info tableau qui contient l'email et le login de l'administrateur, retourne null si le résultat est vide
	 */
	public function adminObtenirInformations() {
		return $this->parametresSite->adminObtenirInformations();
	}

	/**
	 * Modifie le type de structure
	 *
	 * @param  int $structure le nouveau type de structure
	 * @return int
	 * 		- 0 : le type de structure a été modifié
	 * 		- 1 : le nouveau type de structure n'est pas valide, l'ancien type est conservé
	 * 		- 2 : le nouveau type de strucutre est vide, l'ancien type est conservé
	 */
	public function siteModifierStructure(int $structure) {
		return $this->parametresSite->siteModifierStructure($structure);
	}

	/**
	 * Modifie le nom du site
	 *
	 * @param  string $nouveauNom Le nouveau nom du site
	 * @return int
	 *		- 0 : l'ancien nom est remplacé par le nouveau nom
	 *		- 1 : le nouveau nom est vide, l’ancien nom est conservé
	 *		- 2 : le nouveau nom est trop long (>15), ou trop court (<5). l’ancien nom est conservé
	 */
	public function siteModifierNom(string $nouveauNom) {
		return $this->parametresSite->siteModifierNom($nouveauNom);
	}

	/**
	 * Modifie l'email du site
	 *
	 * @param  string $email nouvel email du site
	 * @return int 
	 *		- 0 : l'ancien email est remplacé par le nouvel email
	 *		- 1 : le nouvel email est vide, l'ancien email est conservé
	 *		- 2 : le nouvel email est trop long (>45), l'ancien email est conservé
	 *		- 3 : le format du nouvel email est incorrect, l'ancien email est conservé
	 */
	public function siteModifierEmail(string $nouvelEmail) {
		return $this->parametresSite->siteModifierEmail($nouvelEmail);
	}

	/**
	 * Modifie l'image du site
	 *
	 * @param  string $nouvelleImage La nouvelle image du site
	 * @return int
	 *		- 0 : le nom de l'ancienne image est remplacé par le nom de la nouvelle image
	 *		- 1 : le nom de la nouvelle image est vide, le nom de l'ancienne image est conservé
	 *		- 2 : le nom de la nouvelle image est trop longue (>45), le nom de l'ancienne image est conservé
	 *		- 3 : le format de la nouvelle image n'est pas correct (jpg), le nom de l'ancienne image est conservé
	 */
	public function siteModifierImage(string $nouvelleImage) {
		return $this->parametresSite->siteModifierImage($nouvelleImage);
	}

	/**
	 * Modifie l'icône du site
	 *
	 * @param  string $nouvelleIcone La nouvelle icône du site
	 * @return int
	 *		- 0 : le nom de l'ancienne icone est remplacé par le nom de la nouvelle icone
	 *		- 1 : le nom de la nouvelle icone est vide, le nom de l'ancienne icone est conservé
	 *		- 2 : le nom de la nouvelle icone est trop longue (>45), le nom de l'ancienne icone est conservé
	 *		- 3 : le format de la nouvelle icone n'est pas correct (jpg), le nom de l'ancienne icone est conservé
	 */
	public function siteModifierIcone(string $nouvelleIcone) {
		return $this->parametresSite->siteModifierIcone($nouvelleIcone);
	}

	/**
	 * Modifie la durée RGPD
	 *
	 * @param  int $dureeRGPD la nouvelle durée RGPD en mois
	 * @return int
	 * 		- 0 : la durée RGPD a été modifiée
	 * 		- 1 : la nouvelle durée est soit vide soit négative, la durée RGPD n'est pas modifiée
	 */
	public function siteModifierDureeRGPD(int $dureeRGPD) {
		return $this->parametresSite->siteModifierDureeRGPD($dureeRGPD);
	}

	/**
	 * Modifie le type d'installation
	 *
	 * @param  int $local installation en local
	 * @return void
	 */
	public function siteModifierLocal(bool $local) {
		$this->parametresSite->siteModifierLocal($local);
	}

	/**
	 * Récupére les informations du site
	 *
	 * @return array<string,string> $info tableau qui contient le nom, l'icone et l'image du site, retourne null si le résultat est vide
	 */
	public function siteObtenirInformations() {
		return $this->parametresSite->siteObtenirInformations();
	}
	
	/**
	 * Désinstalle la base de données
	 *
	 * @return void
	 */
	public function siteDesinstallation() {
		return $this->parametresSite->siteDesinstallation();
	}

	/**
	 * Récupère le statut et la validité d'un compte
	 *
	 * @param  string $login login de connexion
	 * @param  string $mdp mot de passe de connexion
	 * @return string|int
	 *		- AA : les identifiants correspondent à un Administrateur Activé
	 *		- AB : les identifiants correspondent à un Administrateur Bloqué
	 *		- SA : les identifiants correspondent à un Sociétaire Activé
	 *		- SB : les identifiants correspondent à un Sociétaire Bloqué
	 *		 
	 *		- 1 : le login ou le mot de passe est vide
	 *		- 2 : le couple d'identifiants login et mot de passe n'éxiste pas ou l'utilisateur est désactivé
	 *		- 3 : le mot de passe est erroné
	 *		- 4 : le compte est bloqué
	 */
	public function connexion(string $login, string $mdp) {
		return $this->gestionCompte->connexion($login, $mdp);
	}

	/**
	 * Envoi un mail avec un lien pour changer le mot de passe du compte
	 *
	 * @param  string $email email du compte ou le mot de passe est oublié
	 * @return int
	 *		- 0 : l'email est envoyé
	 *		- 1 : l'email est vide, aucun email n'est envoyé
	 *		- 2 : il n'éxiste aucun compte avec cet email, aucun email n'est envoyé
	 *		- 3 : le login ne correspond pas à l'email, aucun email n'est envoyé
	 */
	public function reinitialiserMdp(string $login, string $email) {
		return $this->gestionCompte->reinitialiserMdp($login, $email);
	}

	/**
	 * Modifie le mot de passe du compte correspondant au login
	 *
	 * @param  string $login login du compte
	 * @param  string $nouveauMdp nouveau mot de passe du compte
	 * @return int
	 *		- 0 : l'ancien mot de passe est remplacé par le nouveau mot de passe
	 *		- 1 : le nouveau mot de passe est vide, l'ancien mot de passe est conservé
	 *		- 2 : le nouveau mot de passe est trop long, l'ancien mot de passe est conservé
	 *		- 3 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
	 */
	public function modifierMdpOublie(string $token, string $nouveauMdp) {
		$login = $this->gestionCompte->obtenirLogin($token);
		return $this->gestionCompte->modifierMdpOublie($login, $nouveauMdp);
	}

	/**
	 * Ajoute un compte societaire
	 *
	 * @param  string		$prenom prénom du nouveau societaire
	 * @param  string|null	$autrePrenom autres prénoms du nouveau societaire
	 * @param  string		$nom nom du nouveau societaire
	 * @param  string|null	$nomUsuel nom du nouveau societaire
	 * @param  string|null	$representantsLegaux réprésentants légaux
	 * @param  int 			$civilite civilité du nouveau societaire
	 * @param  int 			$statut statut du nouveau societaire
	 * @param  string|null	$siret numéro de siret du nouveau societaire si personne morale
	 * @param  string		$dateNaissance date de naissance du nouveau societaire
	 * @param  string|null	$lieuNaissance lieu de naissance du nouveau societaire
	 * @param  string|null	$email courriel du nouveau societaire
	 * @param  string|null	$telFixe téléphone fixe du nouveau societaire
	 * @param  string|null	$telPortable téléphone portable du nouveau societaire
	 * @param  string		$adresse adresse du nouveau societaire
	 * @param  string		$codePostal code postal du nouveau societaire
	 * @param  string		$ville ville du nouveau societaire
	 * @param  string		$pays pays du nouveau societaire
	 * @param  int 			$regime régime matrimonial ou fiscal du nouveau societaire
	 * @param  int 			$agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
	 * @return int
     *      - 0 : le compte societaire est crée
     *      - 1 : un des paramètre obligatoire (prenom, nom, nomUsuel, civilite, statut, dateNaissance, adresse, codePostal, ville, pays, regime, agEntree) est vide, le compte societaire n'est pas crée
     *      - 2 : un des paramètre est trop long, le compte societaire n'est pas crée
     *      - 3 : le format de l'email est incorrect, le compte societaire n'est pas crée
     *      - 5 : le format d'un des numéros de téléphone est invalide, le compte societaire n'est pas crée
     *      - 6 : il faut au moins un numéro de téléphone ou un mail
     *      - 7 : le mail n'a pas été envoyé, le compte societaire est crée
     */
	public function societaireAjouter(string $prenom, $autrePrenom, string $nom, $nomUsuel, $representantsLegaux, int $civilite, int $statut, $siret, string $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, string $adresse, string $codePostal, string $ville, string $pays, int $regime, int $agEntree, bool $indivision) {
		return $this->societaire->ajouter($prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $regime, $agEntree, $indivision);
	}

	/**
	 * Modifie les informations personnelles d'un societaire
	 *
	 * @param  string 		$login login du societaire
	 * @param  string 		$prenom prénom du societaire
	 * @param  string|null	$autrePrenom autres prénoms du societaire
	 * @param  string 		$nom nom du societaire
	 * @param  string|null	$nomUsuel nom du societaire
	 * @param  string|null	$representantsLegaux réprésentants légaux
	 * @param  int 			$civilite civilité du societaire
	 * @param  int 			$statut statut du societaire
	 * @param  string|null	$siret numéro de siret du nouveau sociétaire si personne morale
	 * @param  string 		$dateNaissance date de naissance du societaire
	 * @param  string|null	$lieuNaissance lieu de naissance du societaire
	 * @return int
	 *		- 0 : les informations personnelles sont modifiés
	 *		- 1 : un des paramètre obligatoire (prenom, nom, civilite, statut, dateNaissance, lieuNaissance) est vide, les informations personnelles ne sont pas modifiés
	 *		- 2 : un des paramètre est trop long, les informations personnelles ne sont pas modifiés
	 */
	public function societaireModifierInfosPerso(string $login, string $prenom, $autrePrenom, string $nom, $nomUsuel, $representantsLegaux, int $civilite, int $statut, $siret, string $dateNaissance, $lieuNaissance, $nationalite, bool $indivision) {
		return $this->societaire->modifierInfosPerso($login, $prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $indivision);
	}

	/**
	 * modifie les informations de contact d'un societaire
	 *
	 * @param  string		$login login du societaire
	 * @param  string|null	$email courriel du societaire
	 * @param  string|null	$telFixe téléphone fixe du societaire
	 * @param  string|null	$telPortable téléphone portable du societaire
	 * @param  string		$adresse adresse du societaire
	 * @param  string		$codePostal code postal du societaire
	 * @param  string		$ville ville du societaire
	 * @param  string		$pays pays du societaire
	 * @return int
	 *		- 0 : les informations de contact sont modifiés
	 *		- 1 : un des paramètre obligatoire (adresse, codePostal, ville, pays) est vide, les informations de contact ne sont pas modifiés
	 *		- 2 : un des paramètre est trop long, les informations de contact ne sont pas modifiés
	 *		- 3 : le format de l'email est incorrect, les informations de contact ne sont pas modifiés
	 *		- 5 : le format d'un des numéros de téléphone est invalide, les informations de contact ne sont pas modifiés
	 */
	public function societaireModifierInfosContact(string $login, $email, $telFixe, $telPortable, string $adresse, string $codePostal, string $ville, string $pays) {
		return $this->societaire->modifierInfosContact($login, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays);
	}

	/**
	 * modifie les informations sci d'un societaire
	 *
	 * @param  string	$login login du societaire
	 * @param  int		$regime régime matrimonial ou fiscal du societaire (Communauté, Contrat)
	 * @param  int		$agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
	 * @return int
	 *		- 0 : les informations de contact sont modifiés
	 *		- 1 : un des paramètre obligatoire (adresse, codePostal, ville, pays) est vide, les informations de contact ne sont pas modifiés
	 *		- 2 : un des paramètre est trop long, les informations de contact ne sont pas modifiés
	 */
	public function societaireModifierInfosSci(string $login, int $regime, int $agEntree) {
		return $this->societaire->modifierInfosSci($login, $regime, $agEntree);
	}

	/**
	 * modifie les autorisations d'un societaire
	 *
	 * @param  bool $login login du societaire
	 * @param  bool $pasFixe ne pas appeler sur le téléphone fixe
	 * @param  bool $pasPortable ne pas appeler sur le téléphone portable
	 * @param  bool $pasSms ne pas envoyer d'sms
	 * @param  bool $pasEmail ne pas envoyer d'email
	 * @param  bool $cloud autoriser le stockage dans le cloud
	 * @return int
	 *		- 0 : les informations de contact sont modifiés
	 *		- 1 : un des paramètre obligatoire (pasFixe, pasPortable, pasSms, pasEmail, cloud) est vide, les autorisations ne sont pas modifié
	 *		- 2 : un des paramètre n'est pas un booléen, les autorisations ne sont pas modifié
	 */
	public function societaireModifierAutorisation(string $login, bool $pasFixe, bool $pasPortable, bool $pasSms, bool $pasEmail, bool $cloud) {
		return $this->societaire->modifierAutorisation($login, $pasFixe, $pasPortable, $pasSms, $pasEmail, $cloud);
	}

	/**
	 * Modifie le mot de passe d'un societaire
	 *
	 * @param  string $ancienMdp ancien mot de passe du societaire
	 * @param  string $nouveauMdp nouveau mot de passe du societaire
	 * @return int
	 *		- 0 : l'ancien mot de passe est remplacé par le nouveau mot de passe
	 *		- 1 : le nouveau ou l'ancien mot de passe est vide, l'ancien mot de passe est conservé
	 *		- 2 : le nouveau ou l'ancien mot de passe est trop long, l'ancien mot de passe est conservé
	 *		- 3 : l'ancien mot de passe est incorrect, l'ancien mot de passe est conservé
	 *		- 4 : le format du nouveau mot de passe est incorrect, l'ancien mot de passe est conservé
	 */
	public function societaireModifierMdp(string $login, string $ancienMdp, string $nouveauMdp) {
		return $this->societaire->modifierMdp($login, $ancienMdp, $nouveauMdp);
	}

	/**
	 * modifie la date de sortie d'un sociétaire
	 *
	 * @param  string $login login du societaire
	 * @param  int    $dateSortie l'id de la date d'assemblée générale où le sociétaire à quitter la SCI / GFA
	 * @return int
	 *		- 0 : la date de sortie a été modifiée
	 *		- 1 : la date de sortie est vide, la date n'est pas modifiée
	 */
	public function societaireModifierDateSortie(string $login, int $dateSortie) {
		return $this->societaire->modifierDateSortie($login, $dateSortie);
	}

	/**
	 * Récupère toutes les informations d'un societaire
	 *
	 * @param  string $login login du societaire
	 * @return array<string, string> $info tableau qui contient toutes les informations d'un societaire, retourne null si le résultat est vide
	 * 
	 *		$infos = [<br>
	 *			"soc_login"					=>	(string) login<br>
	 *			"soc_prenom"				=>	(string) prénom<br>
	 *			"soc_autrePrenom"			=>	(string) autres prénoms<br>
	 *			"soc_nom"					=>	(string) nom<br>
	 *			"soc_nomUsuel"				=>	(string) nom usuel<br>
	 *			"soc_civilite"				=>	(int) civilité<br>
	 *			"sta_id"					=>	(int) statut<br>
	 *			"soc_siret"					=>	(string) numéro siret<br>
	 *			"soc_naissance"				=>	(string) date de naissance<br>
	 *			"soc_lieuNaissance"			=>	(string) lieu de naissance<br>
	 *			"soc_email"					=>	(string) adresse email<br>
	 *			"soc_telephoneFixe"			=>	(string) numéro de téléphone fixe<br>
	 *			"soc_telephonePortable"		=>	(string) numéro de téléphone portable<br>
	 *			"soc_adresse"				=>	(string) adresse<br>
	 *			"soc_codePostal"			=>	(int) code postal<br>
	 *			"soc_ville"					=>	(string) ville<br>
	 *			"soc_pays"					=>	(string) pays<br>
	 *			"reg_id" 					=>	(int) régime matrimonial ou fiscal<br>
	 *			"ass_idEntree"				=>	(int) date d'entrée dans la société<br>
	 *			"soc_pasAppelerFixe"		=>	(bool) ne pas appeler le téléphone fixe<br>
	 *			"soc_pasAppelerPortable"	=>	(bool) ne pas appeler le téléphone portable<br>
	 *			"soc_pasEnvoyerSms"			=>	(bool) ne pas envoyer d'sms<br>
	 *			"soc_pasEnvoyerEmail"		=>	(bool) ne pas envoyer d'email<br>
	 *			"soc_autoriserCloud"		=>	(bool) autoriser le stockage dans le cloud<br>
	 *			"soc_etat"				=>	(char) etat<br>
	 *		]
	 */
	public function societaireObtenirInformations(string $login) {
		return $this->societaire->obtenirInfos($login);
	}

	/**
	 * Récupère toutes les informations des societaires
	 *
	 * @return array<int, array<string, string>> tableau qui contient toutes les informations de tous les societaires, retourne null si le résultat est vide
	 * 
	 *		$infos = [<br>
	 *			0 => [<br>
	 *				"soc_login"					=>	(string) login<br>
	 *				"soc_prenom"				=>	(string) prénom<br>
	 *				"soc_autrePrenom"			=>	(string) autres prénoms<br>
	 *				"soc_nom"					=>	(string) nom<br>
	 *				"soc_nomUsuel"				=>	(string) nom usuel<br>
	 *				"soc_civilite"				=>	(int) civilité<br>
	 *				"sta_id"					=>	(int) statut<br>
	 *				"soc_siret"					=>	(string) numéro siret<br>
	 *				"soc_naissance"				=>	(string) date de naissance<br>
	 *				"soc_lieuNaissance"			=>	(string) lieu de naissance<br>
	 *				"soc_email"					=>	(string) adresse email<br>
	 *				"soc_telephoneFixe"			=>	(string) numéro de téléphone fixe<br>
	 *				"soc_telephonePortable"		=>	(string) numéro de téléphone portable<br>
	 *				"soc_adresse"				=>	(string) adresse<br>
	 *				"soc_codePostal"			=>	(int) code postal<br>
	 *				"soc_ville"					=>	(string) ville<br>
	 *				"soc_pays"					=>	(string) pays<br>
	 *				"reg_id"					=>	(int) régime matrimonial ou fiscal<br>
	 *				"ass_idEntree"				=>	(string) date d'entrée dans la société<br>
	 *				"soc_pasAppelerFixe"		=>	(bool) ne pas appeler le téléphone fixe<br>
	 *				"soc_pasAppelerPortable"	=>	(bool) ne pas appeler le téléphone portable<br>
	 *				"soc_pasEnvoyerSms"			=>	(bool) ne pas envoyer d'sms<br>
	 *				"soc_pasEnvoyerEmail"		=>	(bool) ne pas envoyer d'email<br>
	 *				"soc_autoriserCloud"		=>	(bool) autoriser le stockage dans le cloud<br>
	 *				"soc_etat"				=>	(char) etat<br>
	 * 			]<br>
	 *			1 => [ ... ]<br>
	 *			... <br>
	 *		]
	 */
	public function societaireObtenirListe() {
		return $this->societaire->obtenirListeSocietaires();
	}

	/**
	 * Supprime un societaire
	 *
	 * @param  string $login
	 * @return int 
	 *		- 0 : le sociétaire est supprimé
	 *		- 1 : le login est vide, le sociétaire n'est pas supprimé
	 *		- 2 : le login est trop long (>45), le sociétaire n'est pas supprimé
	 *		- 3 : le login n'éxiste pas, le sociétaire n'est pas supprimé
	 */
	public function societaireSupprimer(string $login) {
		return $this->societaire->supprimerSocietaire($login);
	}
	
	/**
	 * Supprime les données RGPD d'un sociétaire
	 *
	 * @param  string $login
	 * @return int 
	 *		- 0 : les données sont supprimées
	 *		- 1 : le login est vide, les données ne sont pas supprimé
	 *		- 2 : le login est trop long (>45), les données ne sont pas supprimé
	 *		- 3 : le login n'éxiste pas, les données ne sont pas supprimé
	 */
	public function societaireSupprimerRGPD(string $login) {
		return $this->societaire->supprimerRGPD($login);
	}

	/**
	 * activation d'un societaire
	 *
	 * @param  string $login
	 * @return int 
	 *		- 0 : le sociétaire est désactivé si il est activé ou désactivé si il est activé
	 *		- 1 : le login est vide, le sociétaire n'est pas activé/désactivé
	 *		- 2 : le login est trop long (>45), le sociétaire n'est pas activé/désactivé
	 *		- 3 : le login n'éxiste pas, le sociétaire n'est pas activé/désactivé
     *      - 4 : le site est en local ou le sociétaire n'a pas d'email, le sociétaire est activé mais le mail n'est pas envoyé
     *      - 5 : le sociétaire est activé mais le mail n'a pas pu etre envoyé
	 */
	public function societaireActivation(string $login) {
		return $this->societaire->activationSocietaire($login);
	}

	/**
	 * Envoi un mail pour signaler un problème avec un formulaire
	 *
	 * @param  string $login login du sociétaire qui signale le problème
	 * @param  array $champs liste des champs
	 * @param  string $message commentaire de l'utilisateur
	 * @return int
	 *		- 0 : le mail est envoyé
	 *		- 1 : un des paramètres est vide, le mail n'est pas envoyé
	 *		- 2 : un problème est survenue lors de l'envoi de mail, le mail n'est pas envoyé
	 */
	public function societaireSignalerProbleme(string $login, array $champs, string $message) {
		return $this->societaire->signalerProbleme($login, $champs, $message);
	}

	
	/**
	 * Envoi un mail à la cogérance
	 *
	 * @param  string $login login du sociétaire
	 * @param  string $message commentaire de l'utilisateur
	 * @return int
	 *		- 0 : le mail est envoyé
	 *		- 1 : un des paramètres est vide, le mail n'est pas envoyé
	 *		- 2 : un problème est survenue lors de l'envoi de mail, le mail n'est pas envoyé
	 */
	public function mailCogerance(string $login, string $message) {
		return $this->societaire->mailCogerance($login, $message);
	}

	/**
	 * Ajoute une date d'AG
	 *
	 * @param  string $date la date d'AG
	 * @return int
	 * 		- 0 : la date d'AG a été ajoutée
	 * 		- 1 : la date est vide, la date d'AG n'est pas ajoutée
	 * 		- 2 : le format de la date n'est pas valide, la date d'AG n'est pas ajoutée
	 */
	public function assembleeGeneraleAjouterDateAG(string $date) {
		return $this->assembleeGenerale->ajouterDateAG($date);
	}

	/**
	 * Modifie une date d'AG
	 *
	 * @param  int		$id l'id de la date d'AG à modifier
	 * @param  string	$nouvelleDate la nouvelle date d'AG
	 * @return int
	 * 		- 0 : la date d'AG a été modifiée
	 * 		- 1 : la nouvelle date est vide
	 * 		- 2 : la nouvelle date est trop longue
	 * 		- 3 : l'id n'existe pas
	 */
	public function assembleeGeneraleModifierDateAG(int $id, string $nouvelleDate) {
		return $this->assembleeGenerale->modifierDateAG($id, $nouvelleDate);
	}
	
	/**
	 * Supprime une date d'AG
	 *
	 * @param  int $id l'id de la date d'AG à modifier
	 * @return int
	 * 		- 0 : la date d'AG a été supprimée
	 * 		- 1 : l'id n'existe pas, la date n'est pas supprimée
	 *		- 2 : la date d'AG est utilisée, la date n'est pas supprimée
	 */
	public function assembleeGeneraleSupprimerDateAG(int $id) {
		return $this->assembleeGenerale->supprimerDateAG($id);
	}

	/**
	 * Récupère une date d'AG
	 *
	 * @param  int $id l'id de la date d'AG à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations d'une date d'AG
	 * 
	 * 		$infos = [<br>
	 * 			'ass_id'	=> id,<br>
	 * 			'ass_date'	=> date,<br>
	 *		]
	 */
	public function assembleeGeneraleObtenirDateAG(int $id) {
		return $this->assembleeGenerale->obtenirDateAG($id);
	}

	/**
	 * Récupère toutes les dates d'AG
	 *
	 * @return array<int,array<string,string>> $infos un tableau qui contient les informations de toutes les dates d'AG
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'ass_id'	=> id,<br>
	 * 				'ass_date'	=> date,<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...
	 * 		]
	 */
	public function assembleeGeneraleObtenirListeDatesAG() {
		return $this->assembleeGenerale->obtenirListeDatesAG();
	}

	/**
	 * Ajoute un statut
	 *
	 * @param  string	$nom le nom du statut
	 * @return int
	 * 		- 0 : le statut a été ajouté
	 * 		- 1 : le nom est vide, le statut n'est pas ajouté
	 * 		- 2 : le nom est trop long, le statut n'est pas ajouté
	 */
	public function ajouterStatut(string $nom) {
		return $this->parametresAvances->ajouterStatut($nom);
	}

	/**
	 * Modifie un statut
	 *
	 * @param  int		$id l'id du statut à modifier
	 * @param  string	$nouveauNom le nouveau nom du statut
	 * @return int
	 * 		- 0 : le statut a été modifié
	 * 		- 1 : l'un des arguments est vide, le statut n'est pas modifié
	 * 		- 2 : le nom est trop long, le statut n'est pas modifié
	 * 		- 3 : l'id n'existe pas
	 */
	public function modifierStatut(int $id, string $nouveauNom) {
		return $this->parametresAvances->modifierStatut($id, $nouveauNom);
	}

	/**
	 * Supprime un statut
	 *
	 * @param  int		$id l'id du statut à supprimer
	 * @return int
	 * 		- 0 : le statut a été supprimé
	 * 		- 1 : l'id n'existe pas, le statut n'est pas supprimé
	 *		- 2 : le statut est utilisé, le statut n'est pas supprimé
	 */
	public function supprimerStatut(int $id) {
		return $this->parametresAvances->supprimerStatut($id);
	}

	/**
	 * Récupère un statut
	 *
	 * @param  int $id l'id du statut à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations d'un statut
	 * 
	 * 		$infos = [<br>
	 * 			'sta_id'	=> id,<br>
	 * 			'sta_nom'	=> nom<br>
	 * 		]
	 */
	public function parametresAvancesObtenirStatut(int $id) {
		return $this->parametresAvances->obtenirStatut($id);
	}

	/**
	 * Récupère tous les statuts
	 *
	 * @return array<int,array<string,string>> $infos un tableau contenant la liste des informations des statuts
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'sta_id'	=> id,<br>
	 * 				'sta_nom'	=> nom<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function parametresAvancesObtenirListeStatuts() {
		return $this->parametresAvances->obtenirListeStatuts();
	}

	/**
	 * Ajoute un régime
	 *
	 * @param  string	$nom le nom du régime
	 * @param  bool		$type le type du régime
	 * @return int
	 * 		- 0 : le régime a été ajouté
	 * 		- 1 : l'un des champs est vide, le régime n'est pas ajouté
	 * 		- 2 : le nom est trop long, le régime n'est pas ajouté
	 */
	public function parametresAvancesAjouterRegime(string $nom, bool $type) {
		return $this->parametresAvances->ajouterRegime($nom, $type);
	}

	/**
	 * Modifie un régime
	 *
	 * @param  int $id l'id du régime à modifier
	 * @param  string $nouveauNom le nouveau nom du régime
	 * @param  bool $nouveauType le nouveau type du régime
	 * @return int
	 * 		- 0 : le régime a été modifié
	 * 		- 1 : l'un des champs est vide, le régime n'est pas modifié
	 * 		- 2 : le nom est trop long, le régime n'est pas modifié
	 * 		- 3 : l'id n'existe pas
	 */
	public function parametresAvancesModifierRegime(int $id, string $nouveauNom, bool $nouveauType) {
		return $this->parametresAvances->modifierRegime($id, $nouveauNom, $nouveauType);
	}

	/**
	 * Récupère un régime
	 *
	 * @param  int $id l'id du régime à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations d'un régime
	 * 
	 * 		$infos = [<br>
	 * 			'reg_id'	=> id,<br>
	 * 			'reg_nom'	=> nom,<br>
	 * 			'reg_type'	=> type,<br>
	 * 		]
	 */
	public function parametresAvancesObtenirRegime(int $id) {
		return $this->parametresAvances->obtenirRegime($id);
	}

	/**
	 * Récupère tous les régimes ayant pour type $type
	 *
	 * @param  bool $type le type des régimes à récupèrer
	 * @return array<int,array<string,string>> $infos un tableau qui contient la liste des informations des régimes
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'reg_id'	=> id,<br>
	 * 				'reg_nom'	=> nom,<br>
	 * 				'reg_type'	=> type,<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function parametresAvancesObtenirListeRegimesParType(bool $type) {
		return $this->parametresAvances->obtenirListeRegimesParType($type);
	}

	/**
	 * Récupère tous les types de paiement
	 *
	 * @return array<int,array<string,string>> $infos un tableau qui contient la liste des types de paiement
	 * 
	 * 		$infos = [<br>
	 * 			0 => [<br>
	 * 				'pai_id'	=> id,<br>
	 * 				'pai_type'	=> type,<br>
	 * 			],<br>
	 * 			1 => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function parametresAvancesObtenirListePaiements() {
		return $this->parametresAvances->obtenirListePaiements();
	}

	/**
	 * ajoute un mouvement
	 *
	 * @param  string $titulaire titulaire de la part
	 * @param  string $beneficiaire acheteur d'une part
	 * @param  array $listePart liste des parts du mouvement
	 * @param  int $ag assemblée générale lié au mouvement
	 * @param  float $valeurUnitaire valeur d'une part
	 * @param  int $type type de mouvement 0, 1, 2 ou 3
	 * @param array<int,<int,string>> tableau des paiements d'un mouvement, type de paiement (int) associé à un numéro de paiement (string)
	 * @return int 
	 *      - 0 : le mouvement est ajouté
	 *      - 1 : un des paramètre obligatoire est vide, le mouvement n'est pas créé
	 *      - 2 : un des paramètre est trop long, le mouvement n'est pas créé
	 *      - 3 : le titulaire ou le bénéficiaire n'éxiste pas, le mouvement n'est pas créé
	 *      - 4 : la liste des parts n'est pas correct, elle doit être composé uniquement d'entier unique, le mouvement n'est pas créé
	 *      - 5 : une des parts éxiste déjà, impossible de créer une part déjà éxistante, le mouvement n'est pas créé
	 *      - 6 : le titulaire ne possède pas une des parts qu'il essaye de vendre ou transmettre, le mouvement n'est pas créé
	 */
	public function mouvementAjouter($titulaire, string $beneficiaire, array $listePart, int $ag, float $valeurUnitaire, int $type, array $paiement) {
		return $this->mouvement->ajouter($titulaire, $beneficiaire, $listePart, $ag, $valeurUnitaire, $type, $paiement);
	}

	/**
	 * Modifie un mouvement
	 *
	 * @param  string $titulaire titulaire de la part
	 * @param  string $beneficiaire acheteur d'une part
	 * @param  array $listePart liste des parts du mouvement
	 * @param  int $ag assemblée générale lié au mouvement
	 * @param  float $valeurUnitaire valeur d'une part
	 * @param  array<int,<int,string>> tableau des paiements d'un mouvement, type de paiement (int) associé à un numéro de paiement (string)
	 * @return int 
	 *      - 0 : le mouvement est ajouté
	 *      - 1 : un des paramètre obligatoire est vide, le mouvement n'est pas créé
	 *      - 2 : un des paramètre est trop long, le mouvement n'est pas créé
	 *      - 3 : le titulaire ou le bénéficiaire n'éxiste pas, le mouvement n'est pas créé
	 *      - 4 : la liste des parts n'est pas correct, elle doit être composé uniquement d'entier unique, le mouvement n'est pas créé
	 *      - 5 : une des parts existe déjà, impossible de créer une part déjà éxistante, le mouvement n'est pas créé
	 *      - 6 : le titulaire ne possède pas une des parts qu'il essaye de vendre ou de transmettre, le mouvement n'est pas créé
	 */
	public function mouvementModifier(int $mouvementId, $titulaire, string $beneficiaire, array $listePart, int $ag, float $valeurUnitaire, array $paiement) {
		return $this->mouvement->modifierMouvement($mouvementId, $titulaire, $beneficiaire, $listePart, $ag, $valeurUnitaire, $paiement);
	}

	/**
	 * Récupère toutes les information d'un mouvement
	 *
	 * @param  int $idMouvement
	 * @return array<int,array<string,string>> tableau qui contient toutes les informations d'un mouvement, retourne null si le résultat est vide
	 * 
	 *		$infos = [<br>
	 *			0 => [<br>
	 *			"ope_id"            =>  (id) id du mouvement<br>
	 *			"soc_loginTitulaire"    =>  (string) login du titulaire<br>
	 *			"soc_loginBeneficiaire" =>  (string) login du bénéficiaire<br>
	 *			"ass_date"          =>  (date) date ag du mouvement<br>
	 *			"ope_nombrePart"    =>  (int) nombre de parts<br>
	 *			"ope_valeurUnitaire"    =>  (double) valeur d'une part<br>
	 *			"ope_type"          =>  (int) type d'opération (creation, destruction, vente)<br>
	 *			"moy_id"            =>  (int) id du type de paiement<br>
	 *			"pai_numero"    =>  (string) numéro de paiement<br>
	 *			]<br>
	 *			1 => [ ... ]<br>
	 *			... <br>
	 *		]
	 */
	public function mouvementObtenirInformations(int $mouvementId) {
		return $this->mouvement->obtenirMouvement($mouvementId);
	}

	/**
	 * Récupère toutes les information de tous les mouvements
	 *
	 * @return array<int,array<string,string>> tableau qui contient toutes les informations de tous les mouvements, retourne null si le résultat est vide
	 * 
	 *		$infos = [<br>
	 *			0 => [<br>
	 *			"ope_id"            =>  (id) id du mouvement<br>
	 *			"soc_loginTitulaire"    =>  (string) login du titulaire<br>
	 *			"soc_loginBeneficiaire" =>  (string) login du bénéficiaire<br>
	 *			"ass_date"          =>  (date) date ag du mouvement<br>
	 *			"ope_nombrePart"    =>  (int) nombre de parts<br>
	 *			"ope_valeurUnitaire"    =>  (double) valeur d'une part<br>
	 *			"ope_type"          =>  (int) type d'opération (creation, destruction, vente)<br>
	 *			"moy_id"            =>  (int) id du type de paiement<br>
	 *			"pai_numero"    =>  (string) numéro de paiement<br>
	 *			]<br>
	 *			1 => [ ... ]<br>
	 *			... <br>
	 *		]
	 */
	public function mouvementObtenirListe() {
		return $this->mouvement->obtenirListeMouvement();
	}

	/**
	 * Supprime un mouvement et toutes ses parts associés
	 *
	 * @param  int $idMouvement identifiant d'un mouvement
	 * @return void
	 */
	public function mouvementSupprimer(int $mouvementId) {
		return $this->mouvement->supprimerMouvement($mouvementId);
	}

	/**
	 * Ajoute un sociétaire représenté par une indivision
	 *
	 * @param  string	$porteFort le login du sociétaire porte-fort
	 * @param  int		$indivision le numéro de l'indivison
	 * @param  string	$prenom le prénom du sociétaire représenté
	 * @param  string	$nom le nom du sociétaire représenté
	 * @param  int		$civilite la civilité du sociétaire représenté
	 * @param  string	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string	$adresse l'adresse du sociétaire représenté
	 * @param  string	$codePostal le code postal du sociétaire représenté
	 * @param  string	$ville la ville du sociétaire représenté
	 * @param  string	$pays le pays du sociétaire représenté
	 * @return int
	 * 		- 0 : le sociétaire représenté a été ajouté
	 * 		- 1 : l'un des paramètres obligatoires (porteFort, indivision, prenom, nom, dateNaissance, lieuNaissance, adresse, codePostal, ville, pays) est vide
	 * 		- 2 : l'un des paramètres est trop long (> 45)
	 */
	public function indivisionAjouterSocietaireRepresente(string $porteFort, int $indivision, string $prenom, string $nom, int $civilite, string $dateNaissance, string $lieuNaissance, string $adresse, string $codePostal, string $ville, string $pays) {
		return $this->indivision->ajouterSocietaireRepresente($porteFort, $indivision, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
	}

	/**
	 * Modifie un sociétaire représenté
	 *
	 * @param  int		$id l'id du sociétaire représenté à modifier
	 * @param  string	$prenom le prénom du sociétaire représenté
	 * @param  string	$nom le nom du sociétaire représenté
	 * @param  int		$civilite la civilité du sociétaire représenté
	 * @param  string	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string	$adresse l'adresse du sociétaire représenté
	 * @param  string	$codePostal le code postal du sociétaire représenté
	 * @param  string	$ville la ville du sociétaire représenté
	 * @param  string	$pays le pays du sociétaire représenté
	 * @return int
	 * 		- 0 : le sociétaire représenté a été modifié
	 * 		- 1 : l'un des paramètres obligatoires (id, prenom, nom, civilite, dateNaissance, lieuNaissance, adresse, codePostal, ville, pays) est vide
	 * 		- 2 : l'un des paramètres est trop long (>45)
	 * 		- 3 : l'id n'existe pas
	 */
	public function indivisionModifierSocietaireRepresente(int $id, string $prenom, string $nom, int $civilite, string $dateNaissance, string $lieuNaissance, string $adresse, string $codePostal, string $ville, string $pays) {
		return $this->indivision->modifierSocietaireRepresente($id, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
	}

	/**
	 * Supprime un sociétaire représenté
	 *
	 * @param  int $id l'id du sociétaire représenté à supprimer
	 * @return int
	 * 		- 0 : le sociétaire représenté a été supprimé
	 * 		- 1 : l'id n'existe pas
	 */
	public function indivisionSupprimerSocietaireRepresente(int $id) {
		return $this->indivision->supprimerSocietaireRepresente($id);
	}

	/**
	 * Récupère un sociétaire représenté
	 *
	 * @param  int $id l'id du sociétaire représenter à récupèrer
	 * @return array<string,string> $infos un tableau qui contient les informations du sociétaire représenté
	 * 
	 * 		$infos = [<br>
	 * 			'socr_id'				=> id,<br>
	 * 			'socr_prenom'			=> prenom,<br>
	 * 			'socr_nom'				=> nom,<br>
	 * 			'socr_civilite'			=> civilite,<br>
	 * 			'socr_dateNaissance'	=> dateNaissance,<br>
	 * 			'socr_lieuNaissance'	=> lieuNaissance,<br>
	 * 			'socr_adresse'			=> adresse,<br>
	 * 			'socr_codePostal'		=> codePostal,<br>
	 * 			'socr_ville'			=> ville,<br>
	 * 			'socr_pays'				=> pays,<br>
	 * 			'soc_login'				=> porteFort,<br>
	 * 			'ind_num'				=> indivision<br>
	 * 		]
	 */
	public function indivisionObtenirInformationsSocietaireRepresente(int $id) {
		return $this->indivision->obtenirInformationsSocietaireRepresente($id);
	}

	/**
	 * Récupère tous les sociétaires représentés par un porte-fort
	 *
	 * @param string $porteFort le login du porte-fort
	 * @return array<int,array<string,string>> $infos un tableau qui contient les informations de tous les sociétaires représentés
	 * 
	 * 		$infos = [<br>
	 * 			[0] => [
	 * 				'socr_id'				=> id,<br>
	 * 				'socr_prenom'			=> prenom,<br>
	 * 				'socr_nom'				=> nom,<br>
	 * 				'socr_civilite'			=> civilite,<br>
	 * 				'socr_dateNaissance'	=> dateNaissance,<br>
	 * 				'socr_lieuNaissance'	=> lieuNaissance,<br>
	 * 				'socr_adresse'			=> adresse,<br>
	 * 				'socr_codePostal'		=> codePostal,<br>
	 * 				'socr_ville'			=> ville,<br>
	 * 				'socr_pays'				=> pays,<br>
	 * 				'soc_login'				=> porteFort,<br>
	 * 				'ind_num'				=> indivision<br>
	 * 			],<br>
	 * 			[1] => [...],<br>
	 * 			...<br>
	 * 		]
	 */
	public function indivisionObtenirListeSocietaireRepresente(string $porteFort) {
		return $this->indivision->obtenirListeSocietaireRepresente($porteFort);
	}
	
	/**
	 * indivisionObtenirListeIndivisions
	 *
	 * @param  string $porteFort le login du porte-fort
	 * @return void
	 */
	public function indivisionObtenirListeIndivisions(string $porteFort) {
		return $this->indivision->obtenirListeIndivisions($porteFort);
	}
	
	/**
	 * Modifie les paramètres d'envoi de mails
	 *
	 * @param  string $host adresse de l'hote
	 * @param  string $port numéro de port
	 * @param  string $user nom d'utilisateur (généralement l'adresse email)
	 * @param  string $password mot de passe (généralement le mot de passe du compte email)
	 * @param  string $crypto méthode de chiffrage (ssl ou tls)
	 * @return void
	 */
	public function installationModifierEmail(string $host, string $port, string $email, string $password, string $crypto) {
		return $this->installation->modifierEmail($host, $port, $email, $password, $crypto);
	}
	
	/**
	 * Teste l'email en envoyant un email
	 *
	 * @param  string $email l'email du destinataire
	 * @return int
	 * 		- 0 : l'email a été envoyé
	 * 		- 1 : une erreur s'est produite lors de l'envoi de l'email
	 */
	public function installationTesterEmail(string $email) {
		return $this->installation->envoyerEmail($email);
	}

	/**
	 * Exporte les informations de tous les sociétaires
	 *
	 * @return void
	 */
	public function societaireIOExporter() {
		$this->societaireIO->export();
	}
	
	/**
	 * Importe les données des sociétaire depuis un tableur
	 *
	 * @param  string $registreFile nom du fichier à importer
	 * @return array<array<int,int>> tableau contenant le numéro d'une ligne associé à un numéro de retour
	 *		code retour :
	 *		- 0 : le compte sociétaire est créé
	 *		- 1 : un des paramètre obligatoire (mdp, prenom, nom, civilite, statut, dateNaissance, adresse, codePostal, ville, pays, regime, agEntree) est vide, le compte societaire n'est pas créé
	 *		- 2 : un des paramètre est trop long, le compte societaire n'est pas créé
	 *		- 3 : le format de l'email est incorrect, le compte societaire n'est pas créé
	 *		- 5 : le format d'un des numéros de téléphone est invalide, le compte societaire n'est pas créé
	 *		- 6 : le format du mot de passe est incorrect, le compte societaire n'est pas créé
	 *		- 7 : le mail n'a pas été envoyé, le compte societaire est créé
	 *		- 8 : la civilité renseigné est incorrect, le compte societaire n'est pas créé
	 *		- 9 : le statut renseigné est incorrect, le compte societaire n'est pas créé
	 *		- 10 : le régime renseigné est incorrect, le compte societaire n'est pas créé
	 *		- 11 : la date d'entrée renseigné est incorrect, le compte societaire n'est pas créé
	 *		- 12 : la date de sortie renseigné est incorrect, le compte societaire n'est pas créé
	 *		- 13 : le sociétaire est détecté comme étant un doublon, le compte societaire n'est pas créé
	 */
	public function societaireIOImporter(string $fichier) {
		return $this->societaireIO->import($fichier);
	}
	
	/**
	 * Exporte les informations de tous les mouvements
	 *
	 * @return void
	 */
	public function mouvementIOExporter() {
		$this->mouvementIO->export();
	}
	
	/**
	 * Importe les données des mouvements depuis un tableur
	 *
	 * @param  string $registreFile nom du fichier à importer
	 * @return array<int,int> tableau contenant le numéro d'une ligne associé à un numéro de retour
	 *		code retour :
	 *		- 0 : le mouvement est ajouté
	 *		- 1 : un des paramètre obligatoire est vide, le mouvement n'est pas créé
	 *		- 2 : un des paramètre est trop long, le mouvement n'est pas créé
	 *		- 3 : le titulaire ou le bénéficiaire n'éxiste pas, le mouvement n'est pas créé
	 *		- 4 : la liste des parts n'est pas correct, elle doit être composé uniquement d'entier unique, le mouvement n'est pas créé
	 *		- 5 : une des parts éxiste déjà, impossible de créer une part déjà éxistante, le mouvement n'est pas créé
	 *		- 6 : le titulaire ne possède pas une des parts qu'il essaye de vendre ou transmettre, le mouvement n'est pas créé
	 *		- 7 : la date d'assemblée générale renseigné est incorrect, le mouvement n'est pas créé
	 *		- 8 : le type de mouvement renseigné est incorrect, le mouvement n'est pas créé
	 *		- 9 : le mouvement est détecté comme étant un doublon, le mouvement n'est pas créé
	 */
	public function mouvementIOImporter(string $fichier) {
		return $this->mouvementIO->import($fichier);
	}
	
	/**
	 * Exporte les informations de toutes les parts (historique des parts)
	 *
	 * @return void
	 */
	public function partOExporter() {
		$this->partO->export();
	}

	/**
	 * Envoi un mail à une liste de destinataire
	 *
	 * @param  array $emails emails des destinataires
	 * @param  string $objet objet de l'email
	 * @param  string $message message de l'email
	 * @param  $file_name le nom du fichier attaché
	 * @return int
	 *		- 0 : le mail est envoyé
	 *		- 1 : un des paramètres est vide, le mail n'est pas envoyé
	 *		- 2 : un problème est survenue lors de l'envoi de mail, le mail n'est pas envoyé
	 *      - 3 : le fichier n'a pas pu etre été téléchargé
	 */
	public function mailEnvoyer(array $emails, string $objet, string $message, $file_name) {
		return $this->mailing->envoyerEmail($emails, $objet, $message, $file_name);
	}
}

?>