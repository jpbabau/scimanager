<header class="container-fluid header">
    <div class="row justify-content-center align-items-center">
        <div class="col-auto mr-auto logo">
            <img src="<?php echo base_url() .'style/core/img/'. $siteInfos['icone'];?>" alt="">
            <h1>
				<?php
					if ($siteInfos['structure'] == 0) {
						echo 'SCI ';
					} else {
						echo 'GFA ';
					}

					echo $siteInfos['nom'];
				?>
			</h1>
        </div>
        <div class="col-auto card">
            <div class="row justify-content-center align-items-center">
                <div class="col">
                    <h4 class="font-weight-bold">
						<?php
							if ($this->session->statut === 'Admin') {
								echo $this->session->login;
							} else {
								echo $societaireInfos['soc_prenom'];
							}
						?>
					</h4>
                    <span class="font-italic"><?php echo $this->session->statut;?></span>
                </div>
                <div class="col">
                    <a href="<?php echo base_url() . 'index.php/accueil/deconnexion';?>" class="text-danger"><i class="fas fa-sign-out-alt fa-2x"></i></a>
                </div>
            </div>
        </div>
    </div>
</header>