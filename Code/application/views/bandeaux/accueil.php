<header class="container-fluid header">
    <div class="row justify-content-center align-items-center">
        <div class="col-auto mr-auto logo">
            <img src="<?php echo base_url() .'style/core/img/'. $siteInfos['icone'];?>" alt="">
            <h1>
				<?php
					if ($siteInfos['structure'] == 0) {
						echo 'SCI ';
					} else {
						echo 'GFA ';
					}

					echo $siteInfos['nom'];
				?>
			</h1>
        </div>
        <div class="col-auto">
            <a href="<?php echo base_url() . 'index.php/accueil/connexion';?>" class="btn btn-lg btn-primary">Connexion</a>
        </div>
    </div>
</header>