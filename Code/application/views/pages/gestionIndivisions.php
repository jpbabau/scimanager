<div class="col-lg-9">
    <div class="block">
        <?php
			$nbSocieteaires = 0;
			foreach($listeSocietaires as $soc) {
				if ($soc['soc_indivision'] === '1') {
					$nbSocieteaires++;
				} 
			}
		?>
        <h2 class="font-weight-bold"><i class="fas fa-users"></i> Nombre d'indivisions : <?php echo $nbSocieteaires;?></h2>
    </div>

    <div class="block">
        <h3>Sociétaires porte-forts</h3>
        <div class="table-responsive">
            <table class="table table-striped display" id="tableau-indivisions">
                <thead>
                    <tr>
                        <th scope="col">Identifiant</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Email</th>
                        <th scope="col">Date d'entrée</th>
                        <th scope="col">État du compte</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						if (isset($listeSocietaires)) {
							foreach($listeSocietaires as $soc) {
								if ($soc['soc_indivision'] === '1') {
									echo	'<tr>'.
												'<td>'. $soc['soc_login'] .'</td>'.
												'<td>'. $soc['sta_nom'] .'</td>'.
												'<td>'. $soc['soc_nom'] .'</td>'.
												'<td>'. $soc['soc_prenom'] .'</td>'.
												'<td>'. $soc['soc_email'] .'</td>'.
												'<td>'. date('d-m-Y', strtotime($soc['ass_dateEntree'])) .'</td>'.
												'<td>'. $soc['soc_etat'] .'</td>'.
												'<td><a href="'. base_url() .'index.php/gestionIndivisions/indivision/'. $soc['soc_login'] .'"><i class="fas fa-users"></i></a></td>'.
											'</tr>';
								}
							}
						}
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</section>