<div class="col-lg-9">
	<input type="text" class="txt_csrfname" hidden name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>"><br>   
    <div class="block">
        <div class="row justify-content-center align-items-center">
            <div class="col-auto mr-auto">
				<?php
				$nbSocieteaires = 0;
                $nbSocieteairesB = 0;
                $nbSocieteairesA = 0;
                $nbSocieteairesD = 0;
				foreach($listeSocietaires as $soc) {
					if (($soc['soc_etat']==='D') && !(isset($soc['ass_dateSortie']))) {
						$nbSocieteairesD++;
                        $nbSocieteaires++;
					} 
                    if ($soc['soc_etat']==='A') {
						$nbSocieteairesA++;
                        $nbSocieteaires++;
					} 
                    if ($soc['soc_etat']==='B') {
						$nbSocieteairesB++;
                        $nbSocieteaires++;
					} 
				}
				?>
                <h2 class="font-weight-bold"><i class="fas fa-users"></i> Nombre de sociétaires : <?php echo $nbSocieteaires.'   ('.$nbSocieteairesA.','.$nbSocieteairesB.','.$nbSocieteairesD.')';?></h2>
            </div>
            <div class="col-auto">
                <a title="Ajouter un sociétaire" href="<?php echo base_url() . 'index.php/gestionSocietaires/ajouter';?>"><i class="fas fa-user-plus fa-3x"></i></a>
            </div>
        </div>
		<?php
			if (isset($result)) {
				if ($result === -1) {
					echo '<p class="text-danger">Le fichier n\'a pas pu être téléversé.</p>';
				} else {
					for ($numPage = 0; $numPage <= 1; $numPage++) {
						$erreurs = $result[$numPage];

						if ($numPage === 0) {
							$nomPage = 'Personnes';
						} else {
							$nomPage = 'Sociétés';
						}

						foreach ($erreurs as $ligne=>$erreur) {
                            if ($erreur !== 0 && $erreur !== 7) {
                                echo '<p class="text-danger">Il y a une erreur sur la page '. $nomPage .' à la ligne '. $ligne .' : ';
                                switch ($erreur) {
                                    case 1:
                                        echo 'L\'un des champs obligatoires est vide.</p>';
                                        break;
                                    case 2:
                                        echo 'L\'un des champs est trop long.</p>';
                                        break;
                                    case 3:
                                        echo 'Le format de l\'email est incorrect.</p>';
                                        break;
                                    case 5:
                                        echo 'Le format d\'un des numéros de téléphone est incorrect.</p>';
                                        break;
                                    case 6:
                                        echo 'Il faut au moins un mail ou un numéro de téléphone.</p>';
                                        break;
                                    case 8:
                                        echo 'La civilité renseignée est incorrecte.</p>';
                                        break;
                                    case 9:
                                        echo 'Le statut renseigné est incorrect.</p>';
                                        break;
                                    case 10:
                                        echo 'Le régime renseigné est incorrect.</p>';
                                        break;
                                    case 11:
                                        echo 'La date d\'entrée renseignée est incorrecte.</p>';
                                        break;
                                    case 12:
                                        echo 'La date de sortie renseignée est incorrecte.</p>';
                                        break;
                                    case 13:
                                        echo 'Le sociétaire est un doublon.</p>';
                                        break;
                                    default:
										echo 'erreur inconnue';
                                        break;
                                }
                            } else {
								if ($erreur == 0) {
									echo '<p class="text-success">le sociétaire sur la page '. $nomPage . ' à la ligne '. $ligne .' a été ajouté.</p>';
								} 
							}
						}
					}
				}
			}
		?>
    </div>

    <div class="block">
        <h3>Sociétaires</h3>
        <div class="table-responsive">
           <table class="table table-striped display" style="width:100%" id="tableau-societaires">
                <thead>
                    <tr>
                        <th scope="col">Identifiant</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Date d'entrée</th>
                        <th scope="col">Etat</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						if (isset($listeSocietaires)) {
							foreach($listeSocietaires as $soc) {
                                if (!isset($soc['ass_dateSortie'])) {
									switch ($soc['soc_etat']) {
										case 'A':
											$connexion = 'Connecté';
                                            $msg="désactiver";
                                            $msg1='Voulez-vous vraiment désactiver le sociétaire \n';
                                            $msg2='';
                                            $msg3='fa-times';
											break;
										case 'B':
											$connexion = 'Activé';
                                            $msg="désactiver";
                                            $msg1='Voulez-vous vraiment désactiver le sociétaire \n';
                                            $msg2='';
                                            $msg3='fa-times';
											break;
                                        case 'D':
                                            $connexion = 'Désactivé';
                                            $msg="activer";
                                            $msg1 = 'Voulez-vous vraiment envoyer un mail au sociétaire\n';
                                            $msg2 = '\npour activer son compte: ';
                                            $msg3='fa-envelope';
                                            break;
										default:
											break;
									}
									if ($soc['sta_nom']==='Physique') {
										$nom = $soc['soc_nom'];
										$prenom = $soc['soc_prenom'];
									} else {
										$nom =$soc['soc_nomUsuel'];
										$prenom = $soc['soc_nom'].' '.$soc['soc_prenom'];
									}
                                    if ($siteInfos['structure']) {
                                        $quitter = 'Quitter le GFA';
                                    } else {
                                        $quitter = 'Quitter la SCI';
                                    }

									echo	'<tr>'.
												'<td>'. $soc['soc_login'] .'</td>'.
												'<td>'. $soc['sta_nom'] .'</td>'.
												'<td>'. $nom .'</td>'.
												'<td>'. $prenom .'</td>'.
												'<td>'. date('d-m-Y', strtotime($soc['ass_dateEntree'])) .'</td>'.
												'<td>'. $connexion .'</td>'.
												'<td><a title="modifier" href="'. base_url() .'index.php/profil/informations/'. $soc['soc_login'] .'"><i class="fas fa-edit"></i></a></td>'.
												'<td><a title="'. $msg .'" onclick="return confirm(\''.$msg1.$soc['soc_login'].$msg2.'\');" href="'. base_url() .'index.php/gestionSocietaires/activation/'. $soc['soc_login'] .'"><i class="fas '.$msg3.'"></i></a></td>';
                                    if ($soc['soc_etat']==='D') {
										echo	'<td><a title="'. $quitter .'" href="'. base_url() .'index.php/gestionSocietaires/quitter/'. $soc['soc_login'] .'"><i class="fas fa-sign-out-alt"></i></a></td>'.
												'<td><a title="Supprimer" onclick="return confirm(\'Voulez-vous vraiment supprimer le sociétaire : '. $soc['soc_login'] .'\');" href="'. base_url() .'index.php/gestionSocietaires/supprimer/'. $soc['soc_login'] .'"><i class="fas fa-trash-alt"></i></a></td>';
                                    } else {
                                        echo     '<td></td>'.
                                                 '<td></td>';
                                    }
                                    echo        '</tr>';
                                }
							}
						}
                    ?>
                </tbody>
            </table>
        </div>
		<table class="table table-striped display" style="width:100%" id="tableau-societaires">
                <thead>
                    <tr>
                        <th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
                        <th><label id="allDisabled" name="allDisabled" title="Désactiver les sociétaires sélectionnés"class="button"><i class="fas fa-times"></i></span></th>
                        <th><label id="allEnabled" name="allEnabled" title="Activer les sociétaires sélectionnés"class="button"><i class="fas fa-envelope"></i></span></th>
                        <th><label id="allSupprimer" name="allSupprimer" title="Supprimer les sociétaires sélectionnés"class="button"><i class="fas fa-trash-alt"></i></span></th>
                    </tr>
                </thead>
		</table>
		<div class="row justify-content-center import-export">
			<label title="Importer des sociétaires" for="import" class="button"><i class="fas fa-file-import fa-3x"></i></span>
			<?php echo form_open_multipart('gestionSocietaires/importer', 'name="form-importer" hidden');?>
				<input type="file" name="import" id="import">
			</form>
			<a title="Exporter des sociétaires" target="_blank" href="<?php echo base_url().'index.php/gestionSocietaires/exporter';?>"><i class="fas fa-file-export fa-3x"></i></a>
		</div>
    </div>
</div>
</div>
</section>