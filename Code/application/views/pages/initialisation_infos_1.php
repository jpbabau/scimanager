<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- JQuery V.3.6.0 -->
	<script defer src="<?php echo base_url();?>style/jquery/js/jquery-3.6.0.min.js"></script>

    <!-- Bootstrap V.4.6 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/bootstrap/css/bootstrap.min.css">
    <script defer src="<?php echo base_url();?>style/bootstrap/js/bootstrap.min.js"></script>

    <!-- FontAwesome V.5.15.3 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/fontawesome/css/all.min.css">

	<!-- DataTables V.1.10.24 -->
	<link rel="stylesheet" href="<?php echo base_url();?>style/datatables/css/datatables.min2.css">
	<script defer src="<?php echo base_url();?>style/datatables/js/datatables.min.js"></script>

	<!-- Typeahead -->
	<script defer src="<?php echo base_url();?>style/typeahead/js/bootstrap3-typeahead.min.js"></script>

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/core/css/main.css">

    <!-- Core JS -->
    <script defer src="<?php echo base_url();?>style/core/js/script.js"></script>


    <title>Initialisation</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-6 block">
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<?php echo form_open('initialisation/initialiserBDD', 'name="form-initialisationInfos"');?>
							<div class="form-header">
								<h2 class="font-weight-bold">Initialisation</h2>
								<p>Configuration des informations de base<br><small>3 / 3</small></p>
							</div>
							<div class="form-group">
								<label for="login">Login de l'administrateur <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="login" id="login">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="mdp1">Mot de passe <span class="text-danger">*</span></label>
								<input type="password" class="form-control" name="mdp1" id="mdp1">
								<span class="form-eye" data-input="mdp1">
									<i class="fas fa-eye-slash"></i>
								</span>
								<div class="form-regles-5 mdp">
									<div class="form-regle" id="mdp-car"><i class="fas fa-times"></i> Au moins 8 caractères.</div>
									<div class="form-regle" id="mdp-maj"><i class="fas fa-times"></i> Au moins 1 majuscule.</div>
									<div class="form-regle" id="mdp-min"><i class="fas fa-times"></i> Au moins 1 minuscule.</div>
									<div class="form-regle" id="mdp-num"><i class="fas fa-times"></i> Au moins 1 chiffre.</div>
									<div class="form-regle" id="mdp-spe"><i class="fas fa-times"></i> Au moins 1 caractère spécial.</div>
								</div>
							</div>
							<div class="form-group">
								<label for="mdp2">Confirmer le mot de passe <span class="text-danger">*</span></label>
								<input type="password" class="form-control" name="mdp2" id="mdp2">
								<span class="form-eye" data-input="mdp2">
									<i class="fas fa-eye-slash"></i>
								</span>
								<div class="form-regles-1 mdp">
									<div class="form-regle" id="mdp-equal"><i class="fas fa-times"></i> Les mots de passe ne sont pas identiques.</div>
								</div>
							</div>
							<div class="form-group">
								<label for="email">L'email de l'administrateur <span class="text-danger">*</span></label>
								<input type="email" class="form-control" name="email" id="email">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="nom">Le nom du site <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nom" id="nom">
								<div class="invalid-feedback">Champ obligatoire,<br>Le nom doit être compris entre 5 et 15 caractères.</div>
							</div>
							<div class="row justify-content-between align-items-center mt-4">
								<a href="<?php echo base_url() . 'index.php/initialisation/modifierEmail';?>" class="btn btn-lg btn-primary">Précédent</a>
								<button type="submit" class="btn btn-lg btn-primary">Suivant</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>