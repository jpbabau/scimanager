<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-6 block">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-header">
                        <h2 class="font-weight-bold">Première Connexion</h2>
                    </div>
                    <div class="text-center">
                        <p>Nous avons bien pris en compte la modification du mot de passe, veuillez-vous reconnecter.</p>
                    </div>
                    <div class="form-submit">
                        <a href="<?php echo base_url().'index.php/accueil/connexion';?>" class="btn btn-lg btn-primary">Continuer</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>