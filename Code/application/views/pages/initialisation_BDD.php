<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- JQuery V.3.6.0 -->
	<script defer src="<?php echo base_url();?>style/jquery/js/jquery-3.6.0.min.js"></script>

    <!-- Bootstrap V.4.6 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/bootstrap/css/bootstrap.min.css">
    <script defer src="<?php echo base_url();?>style/bootstrap/js/bootstrap.min.js"></script>

    <!-- FontAwesome V.5.15.3 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/fontawesome/css/all.min.css">

	<!-- DataTables V.1.10.24 -->
	<link rel="stylesheet" href="<?php echo base_url();?>style/datatables/css/datatables.min2.css">
	<script defer src="<?php echo base_url();?>style/datatables/js/datatables.min.js"></script>

	<!-- Typeahead -->
	<script defer src="<?php echo base_url();?>style/typeahead/js/bootstrap3-typeahead.min.js"></script>

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/core/css/main.css">

    <!-- Core JS -->
    <script defer src="<?php echo base_url();?>style/core/js/script.js"></script>


    <title>Initialisation</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-6 block">
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<?php echo form_open('initialisation/modifierBDD', 'name="form-initialisationBDD"');?>
							<div class="form-header">
								<h2 class="font-weight-bold">Initialisation</h2>
								<p>Configuration de la base de données<br><small>1 / 3</small></p>
							</div>
							<div class="form-group">
								<label for="hostname">Nom du serveur <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="hostname" id="hostname" value="<?php echo set_value('hostname', $db['hostname']);?>">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="username">Nom de l'utilisateur <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="username" id="username" value="<?php echo set_value('username', $db['username']);?>">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="password">Mot de passe</label>
								<input type="password" class="form-control" name="password" id="password" value="<?php echo set_value('password', $db['password']);?>">
								<span class="form-eye" data-input="password">
									<i class="fas fa-eye-slash"></i>
								</span>
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="database">Nom de la base de données <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="database" id="database" value="<?php echo set_value('database', $db['database']);?>">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<p><span class="text-danger">*</span> : Champs obligatoires</p>
							<?php
								if (isset($result)) {
									echo '<p class="text-danger text-center">Erreur lors de la connexion à la base de données.</p>';
								}
							?>
							<div class="row justify-content-between align-items-center mt-4">
								<a href="" class="btn btn-lg btn-primary disabled">Précédent</a>
								<button type="submit" class="btn btn-lg btn-primary">Suivant</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>