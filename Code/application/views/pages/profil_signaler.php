<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold">Signaler une erreur</h2>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">Un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">Un problème est survenu lors de l\'envoi du courriel.</p>';
						break;
					default:
						echo '<p class="text-success">Le courriel a été envoyé.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php echo form_open('profil/signaler/'. $societaireInfos['soc_login'], 'name="form-signaler" id="form-signaler"');?>
			<div class="col-lg-6">
				<div class="table-responsive">
					<label>Informations erronées <span class="text-danger">*</span></label>
					<table class="table table-bordered table-sm table-hover">
						<thead>
							<tr>
								<th scope="col">Champ</th>
								<th scope="col">Valeur</th>
								<th scope="col">Erreur ?</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Statut</td>
								<td><?php echo $societaireInfos['sta_nom'];?></td>
								<td class="text-center"><input type="checkbox" id="champ1" name="champ1" value="Statut"></td>
							</tr>
							<tr>
								<td>Porte-fort</td>
								<td><?php if ($societaireInfos['sta_indivision'] == 1) echo 'OUI'; else echo 'NON';?></td>
								<td class="text-center"><input type="checkbox" id="champ2" name="champ2" value="Porte-fort"></td>
							</tr>
							<tr>
								<td>Civilité</td>
								<td>
									<?php
										switch ($societaireInfos['soc_civilite']) {
											case 1:
												echo 'Madame';
												break;
											case 2:
												echo 'Non binaire';
												break;
											case 3:
												echo 'Non renseigné';
												break;
											case 4:
												echo 'Autre';
												break;
											default:
												echo 'Monsieur';
												break;
										}
									?>
								</td>
								<td class="text-center"><input type="checkbox" id="champ3" name="champ3" value="Civilité"></td>
							</tr>
							<tr>
								<td>Nom</td>
								<td><?php echo $societaireInfos['soc_nom'];?></td>
								<td class="text-center"><input type="checkbox" id="champ4" name="champ4" value="Nom"></td>
							</tr>
							<tr>
								<td>Prénom</td>
								<td><?php echo $societaireInfos['soc_prenom'];?></td>
								<td class="text-center"><input type="checkbox" id="champ5" name="champ5" value="Prénom"></td>
							</tr>
							<tr>
								<td>Nom usuel</td>
								<td><?php echo $societaireInfos['soc_nomUsuel'];?></td>
								<td class="text-center"><input type="checkbox" id="champ6" name="champ6" value="Nom usuel"></td>
							</tr>
							<tr>
								<td>Autres prénoms</td>
								<td><?php echo $societaireInfos['soc_autrePrenom'];?></td>
								<td class="text-center"><input type="checkbox" id="champ7" name="champ7" value="Autres prénoms"></td>
							</tr>
							<tr>
								<td>Représentants légaux</td>
								<td><?php echo $societaireInfos['soc_representantsLegaux'];?></td>
								<td class="text-center"><input type="checkbox" id="champ8" name="champ8" value="Représentants légaux"></td>
							</tr>
							<tr>
								<td>Lieu de naissance</td>
								<td><?php echo $societaireInfos['soc_lieuNaissance'];?></td>
								<td class="text-center"><input type="checkbox" id="champ9" name="champ9" value="Lieu de naissance"></td>
							</tr>
							<tr>
								<td>Siret</td>
								<td><?php echo $societaireInfos['soc_siret'];?></td>
								<td class="text-center"><input type="checkbox" id="champ10" name="champ10" value="Siret"></td>
							</tr>
							<tr>
								<td>Date de naissance</td>
								<td><?php echo date('d-m-Y', strtotime($soc['soc_dateNaissance']));?></td>
								<td class="text-center"><input type="checkbox" id="champ11" name="champ11" value="Date de naissance"></td>
							</tr>
							<tr>
								<td>Nationalité</td>
								<td><?php echo $societaireInfos['soc_nationalite'];?></td>
								<td class="text-center"><input type="checkbox" id="champ12" name="champ12" value="Nationalité"></td>
							</tr>
							<tr>
								<td>Adresse</td>
								<td><?php echo $societaireInfos['soc_adresse'];?></td>
								<td class="text-center"><input type="checkbox" id="champ13" name="champ13" value="Adresse"></td>
							</tr>
							<tr>
								<td>Code postal</td>
								<td><?php echo $societaireInfos['soc_codePostal'];?></td>
								<td class="text-center"><input type="checkbox" id="champ14" name="champ14" value="Code Postal"></td>
							</tr>
							<tr>
								<td>Ville</td>
								<td><?php echo $societaireInfos['soc_ville'];?></td>
								<td class="text-center"><input type="checkbox" id="champ15" name="champ15" value="Ville"></td>
							</tr>
							<tr>
								<td>Pays</td>
								<td><?php echo $societaireInfos['soc_pays'];?></td>
								<td class="text-center"><input type="checkbox" id="champ16" name="champ16" value="Pays"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="form-group">
					<label for="message">Message <span class="text-danger">*</span></label>
					<textarea class="form-control" name="message" id="message" form="form-signaler" cols="30" rows="10" placeholder="Écrire un message..." value="<?php echo set_value('message');?>"></textarea>
				</div>
				<p><span class="text-danger">*</span> : Champs obligatoires</p>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Envoyer</button>
			</div>
		</form>
    </div>
</div>
</div>
</section>