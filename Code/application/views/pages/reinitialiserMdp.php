<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-6 block">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <?php echo form_open('accueil/reinitialiserMdp', 'name="form-reinitialiserMdp"');?>
                        <div class="form-header">
                            <h2 class="font-weight-bold">Mot de passe oublié</h2>
                            <p>Veuillez entrer votre identifiant et votre email.</p>
                        </div>
						<div class="form-group">
							<label for="login">Identifiant</label>
							<input type="text" class="form-control" name="login" id="login" value="<?php echo set_value('login');?>">
						</div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email');?>">
                        </div>
                        <?php
                            if (isset($result)) {
                                echo '<div class="form-erreur">';
                                switch ($result) {
                                    case 1:
                                        echo '<p>L\'email est vide.</p>';
                                        break;
                                    case 2:
                                        echo '<p>L\'email ne correspond à aucun compte.</p>';
                                        break;
									case 3:
										echo '<p>L\'identifiant ne correspond pas à l\'email.<p>';
										break;
                                    default:
                                        break;
                                }
                                echo '</div>';
                            }
                        ?>
                        <div class="form-submit">
                            <button type="submit" class="btn btn-lg btn-primary">Envoyer un mail</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>