<div class="col-lg-9">
	<div class="block">
		<h2 class="font-weight-bold"><i class="fas fa-cogs"></i> Paramètres</h2>
		<small><a href="<?php echo base_url();?>index.php/parametres/avances">Paramètres avancés</a></small>
		<?php
			if (isset($siteNomResult)) {
				if ($siteNomResult === 0 && $siteImageResult === 0 && $siteIconeResult === 0) {
					echo '<p class="text-success">Les informations du site ont été modifiées.</p>';
				} else {
					switch ($siteNomResult) {
						case 1:
							echo '<p class="text-danger">Le nom du site est trop court.</p>';
							break;
						case 2:
							echo '<p class="text-danger">Le nom du site est trop long.</p>';
							break;
						default:
							break;
					}
					switch ($siteImageResult) {
						case 1:
							echo '<p class="text-danger">Le nom de l\'image est vide.</p>';
							break;
						case 2:
							echo '<p class="text-danger">Le nom de l\'image est trop long.</p>';
							break;
						case 3:
							echo '<p class="text-danger">Le format de l\'image n\'est pas valide.</p>';
							break;
						default:
							break;
					}
					switch ($siteIconeResult) {
						case 1:
							echo '<p class="text-danger">Le nom de l\'icône est vide.</p>';
							break;
						case 2:
							echo '<p class="text-danger">Le nom de l\'icône est trop long.</p>';
							break;
						case 3:
							echo '<p class="text-danger">Le format de l\'icône n\'est pas valide.</p>';
							break;
						default:
							break;
					}
				}
			}

			if (isset($adminLoginResult)) {
				if ($adminLoginResult === 0 && $adminEmailResult === 0) {
					echo '<p class="text-success">Les informations de l\'administrateur ont été modifiées.</p>';
				} else {
					switch ($adminLoginResult) {
						case 1:
							echo '<p class="text-danger">Le login est vide.</p>';
							break;
						case 2:
							echo '<p class="text-danger">Le login est trop long.</p>';
							break;
						case 3:
							echo '<p class="text-danger">Le login est déjà utilisé.</p>';
							break;
						default:
							break;
					}
					switch ($adminEmailResult) {
						case 1:
							echo '<p class="text-danger">L\'email du site est vide.</p>';
							break;
						case 2:
							echo '<p class="text-danger">L\'email du site est trop long.</p>';
							break;
						case 3:
							echo '<p class="text-danger">Le format de l\'email n\'est pas valide.</p>';
							break;
						default:
							break;
					}
				}
			}

			if (isset($mdpResult)) {
				switch ($mdpResult) {
					case 1:
						echo '<p class="text-danger">L\'un des champs est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">L\'un des champs est trop long.</p>';
						break;
					case 3:
						echo '<p class="text-danger">Le mot de passe actuel saisie est incorrect.</p>';
						break;
					case 4:
						echo '<p class="text-danger">Le format du nouveau mot de passe n\'est pas valide.</p>';
						break;
					default:
						echo '<p class="text-success">Le mot de passe de l\'administrateur a été modifié.</p>';
						break;
				}
			}
		?>
	</div>
	
	<div class="block">
		<h3>Configuration du Site</h3>
		<?php echo form_open_multipart('parametres/modifierSiteInfos', 'name="form-modifierSite" id="form-modifierSite"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="structure">Structure</label>
					<select class="custom-select" name="structure" id="structure">
						<option value="0" <?php if ($siteInfos['structure'] == 0) echo 'selected';?>>SCI</option>
						<option value="1" <?php if ($siteInfos['structure'] == 1) echo 'selected';?>>GFA</option>
					</select>
				</div>
				<div class="form-group">
					<label for="nom">Nom</label>
					<input type="text" class="form-control" name="nom" id="nom" autocomplete="off" value="<?php echo $siteInfos['nom'];?>">
					<div class="invalid-feedback">Champ obligatoire,<br>Le nom doit être compris entre 5 et 15 caractères.</div>
				</div>
				<div class="form-group">
					<label for="email">Email</label><br>
					<a href="<?php echo base_url();?>index.php/parametres/modifierSiteEmail">Configurer l'email de la SCI</a>
				</div>
				<div class="form-group">
					<label for="img">Image</label><br/>
					<img src="<?php echo base_url() . 'style/core/img/' . $siteInfos['image'];?>" alt="">
					<input type="file" class="form-control" name="img" id="img" accept=".jpg, .jpeg">
					<div class="invalid-feedback">Seuls les formats .jpg et .jpeg sont autorisés.</div>
				</div>
				<div class="form-group">
					<label for="ico">Icône</label><br/>
					<img src="<?php echo base_url() . 'style/core/img/' . $siteInfos['icone'];?>" alt="">
					<input type="file" class="form-control" name="ico" id="ico" accept=".jpg, .jpeg">
					<div class="invalid-feedback">Seuls les formats .jpg et .jpeg sont autorisés.</div>
				</div>
			</div>
			<?php
				if ($siteInfos['image'] === 'image.jpg' || $siteInfos['icone'] === 'icone.jpg') {
					echo	'<div class="form-submit">'.
								'<a href="'. base_url() .'index.php/parametres/imagesParDefaut">Remettre les images par défaut</a>'.
							'</div>';
				}
			?>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Enregistrer les modifications</button>
			</div>
		</form>
	</div>
	<div class="block">
		<h3>Type d'installation</h3>
		<br>
		<?php echo form_open('parametres/modifierInstallationLocal');?>
			<div class="col-lg-6">
				<div class="form-group">
					<input type="checkbox" class="form-check-input" id="local" name="local" <?php if($siteInfos['isLocal']) {echo checked;}?>>
					<label for="duree">Installation du site en local ? </label>
				</div>
				<div class="form-submit">
					<button type="submit" class="btn btn-lg btn-primary">Enregistrer les modifications</button>
				</div>
			</div>
		</form>
	</div>
	<div class="block">
		<h3>Administrateur</h3>
		<?php echo form_open('parametres/modifierAdminInfos', 'name="form-modifierAdmin" id="form-modifierAdmin"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="login">Identifiant</label>
					<input type="text" class="form-control" name="login" id="login" autocomplete="off" value="<?php echo $adminInfos['login'];?>">
					<div class="invalid-feedback">Champ obligatoire.</div>
				</div>
				<div class="form-group">
					<label for="mail">Email</label>
					<input type="email" class="form-control" name="mail" id="mail" autocomplete="off" value="<?php echo $adminInfos['email'];?>">
					<div class="invalid-feedback">Champ obligatoire,<br>Le format de l'email n'est pas valide.</div>
				</div>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Enregistrer les modifications</button>
			</div>
		</form>
	</div>

	<div class="block">
		<h3>Mot de passe</h3>
		<?php echo form_open('parametres/modifierAdminMdp', 'name="form-modifierMdp"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="oldmdp">Mot de passe actuel</label>
					<input type="password" class="form-control" name="oldmdp" id="oldmdp">
					<span class="form-eye" data-input="oldmdp">
						<i class="fas fa-eye-slash"></i>
					</span>
				</div>
				<div class="form-group">
					<label for="newmdp1">Nouveau mot de passe</label>
					<input type="password" class="form-control" name="newmdp1" id="newmdp1">
					<span class="form-eye" data-input="newmdp1">
						<i class="fas fa-eye-slash"></i>
					</span>
					<div class="form-regles-5 mdp">
						<div class="form-regle" id="mdp-car"><i class="fas fa-times"></i> Au moins 8 caractères.</div>
						<div class="form-regle" id="mdp-maj"><i class="fas fa-times"></i> Au moins 1 majuscule.</div>
						<div class="form-regle" id="mdp-min"><i class="fas fa-times"></i> Au moins 1 minuscule.</div>
						<div class="form-regle" id="mdp-num"><i class="fas fa-times"></i> Au moins 1 chiffre.</div>
						<div class="form-regle" id="mdp-spe"><i class="fas fa-times"></i> Au moins 1 caractère spécial.</div>
					</div>
				</div>
				<div class="form-group">
					<label for="newmdp2">Confirmer le mot de passe</label>
					<input type="password" class="form-control" name="newmdp2" id="newmdp2">
					<span class="form-eye" data-input="newmdp2">
						<i class="fas fa-eye-slash"></i>
					</span>
					<div class="form-regles-1 mdp">
						<div class="form-regle" id="mdp-equal"><i class="fas fa-times"></i> Les mots de passe ne sont pas identiques.</div>
					</div>
				</div>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Modifier le mot de passe</button>
			</div>
		</form>
	</div>
</div>
</div>
</section>