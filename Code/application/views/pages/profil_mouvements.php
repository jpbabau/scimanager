<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-exchange-alt"></i> Nombre de parts : <?php echo $societaireInfos['soc_nbParts'];?> parts</h2>
		<?php
		if ($this->session->statut != 'Admin') {
			echo '<FONT color="red" size="3pt"> Les informations concernant les mouvements de parts sont gérées par la co-gérance et ne peuvent être modifiées par le.a sociétaire.
			<br>Merci de signaler toute erreur en cliquant sur le menu « signaler une erreur »</FONT>';
		}
		?>
	</div>

    <div class="block">
		<?php
				if ($this->session->statut === 'Admin') {
					$txt = 'Les';
				} else {
					echo form_open('profil/signalerErreurMvt/' . $societaireInfos['soc_login'], 'name="form-modifierProfilMouvements"');
					$txt = 'Mes';
				}
		?>
        <h3><?php echo $txt?> mouvements</h3>
        <div class="table-responsive">
            <table class="table table-striped display" id="tableau-mouvements">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Type</th>
                        <th scope="col">Origine</th>
                        <th scope="col">Nombre de parts</th>
                        <th scope="col">Prix de la part</th>
                        <th scope="col">Paiements</th>
						<th scope="col">Destination</th>
						<th></th>
						<th></th>
                    </tr>
                </thead>
                <tbody>
					<?php
						if (isset($listeMouvements)) {
							$index = 0;
							while ($index < sizeof($listeMouvements)) {
								$titulaire = '';
								$beneficiaire = '';
								if ($listeMouvements[$index]['soc_loginTitulaire'] === $societaireInfos['soc_login'] || $listeMouvements[$index]['soc_loginBeneficiaire'] === $societaireInfos['soc_login']) {
									foreach ($listeSocietaires as $soc) {
										if ($listeMouvements[$index]['soc_loginTitulaire'] === $soc['soc_login']) {
											$titulaire = $soc['soc_prenom'].' '.$soc['soc_nom'];
										}
										if ($listeMouvements[$index]['soc_loginBeneficiaire'] === $soc['soc_login']) {
											$beneficiaire = $soc['soc_prenom'].' '.$soc['soc_nom'];
										}
									}
									$id = $listeMouvements[$index]['ope_id'];

									switch ($listeMouvements[$index]['ope_type']) {
										case 1:
											$opeType = 'Transmission par vifs';
											break;
										case 2:
											$opeType = 'Transmission par décès';
											break;
										case 3:
											$opeType = 'Vente';
											break;
										default:
											$opeType = 'Création';
											break;
									}

									$paiementType1	= $listeMouvements[$index]['moy_id'];
									$paiementNum1	= $listeMouvements[$index]['pai_numero'];
									$paiements = '';

									$i = $index;
									$recupere = TRUE;
									while ($i < sizeof($listeMouvements) && $listeMouvements[$i]['ope_id'] === $id) {
										if ($i !== $index && $listeMouvements[$i]['moy_id'] === $paiementType1 && $listeMouvements[$i]['pai_numero'] === $paiementNum1) {
											$recupere = FALSE;
										}

										if ($recupere) {
											$paiements = $paiements . $listeMouvements[$i]['moy_type'] . ' : ' . $listeMouvements[$i]['pai_numero'] . '<br>';
										}
										$i = $i + 1;
									}

									echo	'<tr>'.
												'<td>'. date('d-m-Y', strtotime($listeMouvements[$i - 1]['ass_date'])) .'</td>'.
												'<td>'. $opeType .'</td>'.
												'<td>'. $titulaire .'</td>'.
												'<td>'. $listeMouvements[$i - 1]['ope_nombrePart'] .'</td>'.
												'<td>'. $listeMouvements[$i - 1]['ope_valeurUnitaire'] .'</td>'.
												'<td>'. $paiements .'</td>'.
												'<td>'. $beneficiaire .'</td>'.
												'<td></td>'.
												'<td></td>'.
											'</tr>';

									$index = $i;
								} else {
									$index = $index + 1;
								}
							}
						}
                    ?>
                </tbody>
            </table>
        </div>
		<div class="form-submit">
			<?php
				if ($this->session->statut !== 'Admin') {
					echo '<button type="submit" class="btn btn-lg btn-primary">Signaler une erreur</button>';
				}
			?>
		</div>
    </div>
</div>
</div>
</section>