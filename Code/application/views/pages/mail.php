<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-envelope"></i> Mailing</h2>
		<FONT color="red" size="3pt"> Un seul fichier pdf peut etre joint au mail.</FONT>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger"><br> L\'un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger"><br> Une erreur s\'est produite lors de l\'envoi du courriel.</p>';
						break;
					case 3:
						echo '<p class="text-danger"><br> le fichier n\'a pas pu etre téléchargé, le courriel n\'a pas été envoyé.</p>';
						break;
					default:
						echo '<p class="text-success"><br> Le courriel a été envoyé.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
        <?php echo form_open_multipart('mail/envoyer', 'name="form-mail" id="form-mail"');?>
            <div class="col-lg-6">
				<div class="form-check ml-2">
					<input class="form-check-input" type="radio" name="type" id="important" value="0" checked>
					<label class="form-check-label" for="important">Important</label>
				</div>
				<div class="form-check ml-2">
					<input class="form-check-input" type="radio" name="type" id="information" value="1">
					<label class="form-check-label" for="information">Information</label>
				</div>
				<p><br>Envoyer à :</p>
				<div class="form-check ml-2">
					<input class="form-check-input" type="radio" name="destinataire" id="destinataire1" value="0" checked>
					<label class="form-check-label" for="destinataire1">Tous les sociétaires</label>
				</div>
				<div class="form-check ml-2">
					<input class="form-check-input" type="radio" name="destinataire" id="destinataire2" value="1" disabled>
					<label class="form-check-label" for="destinataire2">Tous les co-gérants</label>
				</div>
				<div class="form-check ml-2">
					<input class="form-check-input" type="radio" name="destinataire" id="destinataire3" value="2">
					<label class="form-check-label" for="destinataire3">Choisir :</label>
				</div>
				<div class="form-group ml-4 mt-2">
					<select name="email" id="email" class="custom-select">
						<option value="">Choisir...</option>
						<?php
							foreach ($listeSocietaires as $soc) {
								if ($soc['soc_pasEnvoyerEmail'] === '0' && isset($soc['soc_email']) && !isset($soc['ass_dateSortie'])) {
									echo '<option value="'. $soc['soc_email'] .'">'. $soc['soc_prenom'] . ' '. $soc['soc_nom'] .' ('. $soc['soc_login'] .')</option>';
								}
							}
						?>
					</select>
				</div>
				<div class="form-group mt-2">
					<label for="objet">Objet</label>
					<?php
						if ($siteInfos['structure']) {
							$structure = 'GFA';
						} else {
							$structure = 'SCI';
						}
					?>
					<input type="text" class="form-control" name="objet" id="objet" value="[<?php echo $structure .' '. $siteInfos['nom'];?>] - ">
				</div>
				<div class="form-group">
					<label for="message">Message</label>
					<textarea class="form-control" name="message" id="message" form="form-mail" cols="30" rows="10" placeholder="Écrire un message..." value="<?php echo set_value('message');?>"></textarea>
				</div>
				<div class="form-group">
                  <label for="attach">Fichier à télécharger</label>
                  <input type="file" name="attach" id="attach" accept=".pdf" />
				  <button id="rmvFile" type="button">Remove File</button>
                </div>
            </div>
            <div class="form-submit">
                <button id="send" type="submit" class="btn btn-lg btn-primary">Envoyer</button>
            </div>
        </form>
    </div>
</div>
</div>
</section>