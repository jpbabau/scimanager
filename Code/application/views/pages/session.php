<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-6 block">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-header">
                        <h2 class="font-weight-bold">Votre session a expiré</h2>
                    </div>
                    <div class="text-center">
                        <p>Vous avez été inactif(ve) pendant plus de 30 minutes, veuillez-vous reconnecter.</p>
                    </div>
                    <div class="form-submit">
                        <a href="<?php echo base_url();?>" class="btn btn-lg btn-primary">Continuer</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>