<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- JQuery V.3.6.0 -->
	<script defer src="<?php echo base_url();?>style/jquery/js/jquery-3.6.0.min.js"></script>

    <!-- Bootstrap V.4.6 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/bootstrap/css/bootstrap.min.css">
    <script defer src="<?php echo base_url();?>style/bootstrap/js/bootstrap.min.js"></script>

    <!-- FontAwesome V.5.15.3 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/fontawesome/css/all.min.css">

	<!-- DataTables V.1.10.24 -->
	<link rel="stylesheet" href="<?php echo base_url();?>style/datatables/css/datatables.min2.css">
	<script defer src="<?php echo base_url();?>style/datatables/js/datatables.min.js"></script>

	<!-- Typeahead -->
	<script defer src="<?php echo base_url();?>style/typeahead/js/bootstrap3-typeahead.min.js"></script>

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/core/css/main.css">

    <!-- Core JS -->
    <script defer src="<?php echo base_url();?>style/core/js/script.js"></script>


    <title>Initialisation</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-6 block">
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<div class="form-header">
							<h2 class="font-weight-bold">Initialisation</h2>
							<p>Configuration des informations de base<br><small>3 / 3</small></p>
						</div>
						<p class="text-center">La base de données a déjà été initialisée.</p>
						<div class="form-submit">
							<a href="<?php echo base_url();?>" class="btn btn-lg btn-primary">Terminer</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>