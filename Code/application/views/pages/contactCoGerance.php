<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold">Envoyer un mail à la co-gérance</h2>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">Un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">Un problème est survenu lors de l\'envoi du courriel.</p>';
						break;
					case 3:
						echo '<p class="text-danger">Le courriel n\'a pas été envoyé car vous n\'avez pas renseigné votre adresse mail.</p>';
						break;
					default:
						echo '<p class="text-success">Le courriel a été envoyé.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php echo form_open('profil/mailCogerance/'. $societaireInfos['soc_login'], 'name="form-contact-cogerance" id="form-contact-cogerance"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="message">Message <span class="text-danger">*</span></label>
					<textarea class="form-control" name="message" id="message" form="form-contact-cogerance" cols="30" rows="10" placeholder="Écrire un message..." value="<?php echo set_value('message');?>"></textarea>
				</div>
				<p><span class="text-danger">*</span> : Champs obligatoires</p>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Envoyer</button>
			</div>
		</form>
    </div>
</div>
</div>
</section>