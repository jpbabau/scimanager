<section class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<div class="block menu">
				<nav class="nav flex-column menu text-center">
					<a class="nav-link font-weight-bold active">Profil de 
					<?php 
					if ($societaireInfos['sta_id']== 0) {
						echo $societaireInfos['soc_prenom'].' '.$societaireInfos['soc_nomUsuel'];
					} else  {
						echo $societaireInfos['soc_nomUsuel'];
					}
					?></a>
				</nav>
			</div>

			<div class="block menu">
				<nav class="nav flex-column menu">
					<a href="<?php echo base_url() . 'index.php/profil/informations/'. $societaireInfos['soc_login'];?>"	class="nav-link font-weight-bold <?php if ($menuOnglet === 'informations') echo 'active';?>"><i class="fas fa-user"></i> Informations</a>
					<a href="<?php echo base_url() . 'index.php/profil/contact/'. $societaireInfos['soc_login'];?>"			class="nav-link font-weight-bold <?php if ($menuOnglet === 'contact') echo 'active';?>"><i class="fas fa-envelope"></i> Contact</a>
					<a href="<?php echo base_url() . 'index.php/profil/sci/'. $societaireInfos['soc_login'];?>"				class="nav-link font-weight-bold <?php if ($menuOnglet === 'sci') echo 'active';?>"><i class="fas fa-info-circle"></i> SCI</a>
					<a href="<?php echo base_url() . 'index.php/profil/mouvements/'. $societaireInfos['soc_login'];?>"		class="nav-link font-weight-bold <?php if ($menuOnglet === 'mouvements') echo 'active';?>"><i class="fas fa-exchange-alt"></i> Mouvements</a>
				</nav>
			</div>

			<div class="block menu">
				<nav class="nav flex-column menu text-center">
					<?php
						if (isset($societaireInfos['ass_dateSortie'])) {
							echo '<a href="'. base_url() .'index.php/gestionSocietaires/anciens" class="nav-link font-weight-bold">Retour</a>';
						} else {
							echo '<a href="'. base_url() .'index.php/gestionSocietaires" class="nav-link font-weight-bold">Retour</a>';
						}
					?>
				</nav>
			</div>

			<div class="block menu">
				<nav class="nav flex-column menu deconnexion">
					<a href="<?php echo base_url() . 'index.php/accueil/deconnexion';?>" class="nav-link font-weight-bold">Deconnexion</a>
				</nav>
			</div>
		</div>