<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-exchange-alt"></i> Ajouter un mouvement</h2>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">L\'un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">L\'un des champs de saisie est trop long.</p>';
						break;
					case 3:
						echo '<p class="text-danger">Le titulaire ou le bénéficiaire n\'existe pas.</p>';
						break;
					case 4:
						echo '<p class="text-danger">La liste des parts n\'est pas correcte, seuls des entiers sont autorisés.</p>';
						break;
					case 5:
						echo '<p class="text-danger">L\'une des parts existe déjà.</p>';
						break;
					case 6:
						echo '<p class="text-danger">Le titulaire ne possède pas l\'une des parts.</p>';
						break;
					default:
						echo '<p class="text-success">Le mouvement a été ajouté.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<br>
        <?php echo form_open('gestionMouvements/ajouter', 'name="form-mouvement"');?>
            <div class="col-lg-6">
				<div class="form-row">
					<div class="form-group col-6">
						<label for="ope-date">Date d'agrément <span class="text-danger">*</span></label>
						<select class="custom-select" name="ope-date" id="ope-date">
							<?php
								for ($i = 0; $i < sizeof($listeDatesAG); $i++) {
									$AG = $listeDatesAG[$i];
									if ($i === 0 || $AG['ass_id'] === set_value('ope-date')) {
										echo '<option value="'. $AG['ass_id'] .'" selected>'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
									} else {
										echo '<option value="'. $AG['ass_id'] .'">'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
									}
								}
							?>
						</select>
					</div>
					<div class="form-group col-6">
						<label for="ope-type">Type de l'opération</label>
						<select class="custom-select" name="ope-type" id="ope-type">
							<option value="0" <?php if (set_value('ope-type') == 0) echo 'selected';?>>Création</option>
							<option value="1" <?php if (set_value('ope-type') == 1) echo 'selected';?>>Transmission entre vifs</option>
							<option value="2" <?php if (set_value('ope-type') == 2) echo 'selected';?>>Transmission par décès</option>
							<option value="3" <?php if (set_value('ope-type') == 3) echo 'selected';?>>Vente</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="titulaire">Titulaire <span class="text-danger">*</span></label>
					<select class="custom-select" name="titulaire" id="titulaire">
						<option value="" selected>Choisir...</option>
						<?php
							foreach ($listeSocietaires as $soc) {
								if (!isset($soc['ass_dateSortie'])) {
									if ($soc['soc_login'] === set_value('titulaire')) {
										echo '<option value="'. $soc['soc_login'] .'" selected>'. $soc['soc_nom'] .' '. $soc['soc_prenom'] .'</option>';
									} else {
										echo '<option value="'. $soc['soc_login'] .'">'. $soc['soc_nom'] .' '. $soc['soc_prenom'] .'</option>';
									}
								}
							}
						?>
					</select>
					<div class="invalid-feedback">Champ obligatoire.</div>
				</div>
				<div class="form-group">
					<label for="beneficiaire">Bénéficiaire <span class="text-danger">*</span></label>
					<select class="custom-select" name="beneficiaire" id="beneficiaire">
						<option value="" selected>Choisir...</option>
						<?php
							foreach ($listeSocietaires as $soc) {
								if (!isset($soc['ass_dateSortie'])) {
									if ($soc['soc_login'] === set_value('beneficiaire')) {
										echo '<option value="'. $soc['soc_login'] .'" selected>'. $soc['soc_nom'] .' '. $soc['soc_prenom'] .'</option>';
									} else {
										echo '<option value="'. $soc['soc_login'] .'">'. $soc['soc_nom'] .' '. $soc['soc_prenom'] .'</option>';
									}
								}
							}
						?>
					</select>
					<div class="invalid-feedback">Champ obligatoire.</div>
				</div>
				<div class="form-group">
					<label>Nombre de parts</label>
					<input type="text" class="form-control" name="nb-parts" id="nb-parts" value="<?php echo set_value('nb-parts', '0');?>">
					<small class="form-text text-muted">Le nombre de parts est calculé automatiquement.</small>
				</div>
				<div class="form-group">
					<label for="parts">Numéro des parts <span class="text-danger">*</span></label>
					<input type="text" class="form-control" name="parts" id="parts" placeholder="Entrez le numéro des parts..." value="<?php echo set_value('parts');?>">
					<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des chiffres entiers.</div>
					<small class="form-text text-muted font-weight-bold">Exemples de saisies :</small>
					<small class="form-text text-muted">Les parts 1,2,3 : 1,2,3</small>
					<small class="form-text text-muted">Les parts de 1 à 5 : 1-5</small>
					<small class="form-text text-muted">Les parts 1,2 et de 5 à 10 : 1,2,5-10</small>
				</div>
				<div class="form-group">
					<label for="prix">Valeur d'une part (en euros) <span class="text-danger">*</span></label>
					<input type="text" class="form-control" name="prix" id="prix" placeholder="Entrez la valeur d'une part..." value="<?php echo set_value('prix');?>">
					<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir qu'un nombre.</div>
				</div>
				<input type="text" class="form-control" name="paiements-nb" id="paiements-nb" hidden>
				<div class="paiements">
					<div class="paiement">
						<label for="">Paiement n°1</label>
						<div class="form-row ml-4">
							<div class="form-group col-6">
								<label for="pai-type-1">Type <span class="text-danger">*</span></label>
								<select name="pai-type-1" id="pai-type-1" class="custom-select">
									<option value="">Choisir...</option>
									<?php
										foreach($listePaiements as $pai) {
											echo '<option value="'. $pai['moy_id'] .'">'. $pai['moy_type'] .'</option>';
										}
									?>
								</select>
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group col-6">
								<label for="pai-num-1">Information <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="pai-num-1" id="pai-num-1">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
						</div>
					</div>
				</div>
				<div class="text-center mt-2">
					<span class="button" name="plus"><i class="fas fa-plus fa-2x"></i></span>
				</div>
            </div>
            <p><span class="text-danger">*</span> : Champs obligatoires</p>
            <div class="form-submit">
                <button type="submit" class="btn btn-lg btn-primary">Ajouter</button>
            </div>
        </form>
    </div>
</div>
</div>
</section>