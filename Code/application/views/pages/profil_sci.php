<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-info-circle"></i> Informations utiles à la SCI</h2>
		<?php
		if ($this->session->statut != 'Admin') {
			echo '<FONT color="red" size="3pt"> Les informations concernant la SCI sont gérées par la co-gérance et ne peuvent être modifiées par le.a sociétaire.
			<br>Merci de signaler toute erreur en cliquant sur le menu « signaler une erreur »</FONT>';
		}
		?>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">L\'un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">L\'un des champs de saisie est trop long.</p>';
						break;
					default:
						echo '<p class="text-success">Les modifications ont été enregistrées.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php
				if ($this->session->statut === 'Admin') {
					echo form_open('profil/sci/' . $societaireInfos['soc_login'], 'name="form-modifierProfilSci"');
					$actionInfo = 'Enregistrer les modifications';
				} else {
					echo form_open('profil/signalerErreurSci/' . $societaireInfos['soc_login'], 'name="form-modifierProfilSci"');
					$actionInfo = 'Signaler une erreur';
				}
		?>
			<fieldset <?php if ($this->session->statut != 'Admin') echo 'disabled=disabled';?>>
			<div class="col-lg-6">
				<?php
					if ($societaireInfos['sta_nom'] == 0) {
						$nomRegime		= 'Régime matrimonial';
						$listeRegimes	= $listeRegimesMatrimoniaux;
					} else {
						$nomRegime		= 'Régime fiscal';
						$listeRegimes	= $listeRegimesFiscaux;
					}
				?>
				<div class="form-group" <?php if ($societaireInfos['sta_id'] != 0) echo 'hidden';?>>
                    <label for="regime"><?php echo $nomRegime;?></label>
                    <select class="custom-select" name="regime" id="regime">
						<?php
							foreach($listeRegimes as $reg) {
								if ($reg['reg_id'] === $societaireInfos['reg_id']) {
									echo '<option value="'. $reg['reg_id'] .'" selected>'. $reg['reg_nom'] .'</option>';
								} else {
									echo '<option value="'. $reg['reg_id'] .'">'. $reg['reg_nom'] .'</option>';
								}
							}
						?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="entree">Date d'entrée dans la SCI</label>
					<select class="custom-select" name="entree" id="entree">
						<?php
							foreach($listeDatesAG as $AG) {
								if ($AG['ass_date'] === $societaireInfos['ass_dateEntree']) {
									echo '<option value="'. $AG['ass_id'] .'" selected>'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
								} else {
									echo '<option value="'. $AG['ass_id'] .'">'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
								}
							}
						?>
                    </select>
                </div>
                <div class="form-group" <?php if ($this->session->statut === 'Sociétaire') echo 'hidden';?>>
                    <label for="entree">Date de sortie de la SCI</label>
					<select class="custom-select" name="sortie" id="sortie">
						<?php
							foreach($listeDatesAG as $AG) {
								if ($AG['ass_date'] === $societaireInfos['ass_dateSortie']) {
									echo '<option value="'. $AG['ass_id'] .'" selected>'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
								}
							}
						?>
                    </select>
                </div>
			</div>
			</fieldset>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary"><?php echo $actionInfo;?></button>
			</div>
		</form>
    </div>
</div>
</div>
</section>