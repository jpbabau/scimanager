<div class="col-lg-9">
	<div class="block">
        <div class="row justify-content-center align-items-center">
            <div class="col-auto mr-auto">
				<h2 class="font-weight-bold"><i class="fas fa-users"></i> Les indivisions représentées par <?php echo $societaireInfos['soc_prenom'] . ' ' . $societaireInfos['soc_nom'];?></h2>
            </div>
            <div class="col-auto">
                <a title="Ajouter un sociétaire" href="<?php echo base_url() . 'index.php/gestionIndivisions/ajouterSocietaire/'. $societaireInfos['soc_login'];?>"><i class="fas fa-user-plus fa-3x"></i></a>
            </div>
        </div>
    </div>

	<?php
		if (isset($listeSocietairesRepresentes) && sizeof($listeSocietairesRepresentes) !== 0) {

			$index = 0;
			while ($index < sizeof($listeSocietairesRepresentes)) {
				$indivision = $listeSocietairesRepresentes[$index]['ind_num'];
				echo	'<div class="block">'.
							'<h3>Indivision n°'. $indivision . '</h3>'.
							'<div class="table-responsive">'.
								'<table class="table table-striped display" name="tableau-representes">'.
									'<thead>'.
										'<tr>'.
											'<th scope="col">Civilite</th>'.
											'<th scope="col">Nom</th>'.
											'<th scope="col">Prénom</th>'.
											'<th scope="col">Adresse</th>'.
											'<th scope="col">Code postal</th>'.
											'<th scope="col">Ville</th>'.
											'<th scope="col">Pays</th>'.
											'<th></th>'.
											'<th></th>'.
										'</tr>'.
									'</thead>'.
									'<tbody>';
				
				$i = $index;
				while ($i < sizeof($listeSocietairesRepresentes) && $listeSocietairesRepresentes[$i]['ind_num'] === $indivision) {
					$socr = $listeSocietairesRepresentes[$i];

					$civilite = '';
					switch ($socr['socr_civilite']) {
						case 1:
							$civilite = 'Madame';
							break;
						case 2:
							$civilite = 'Non-binaire';
							break;
						case 3:
							$civilite = 'Non renseigné';
							break;
						case 4:
							$civilite = 'Autre';
							break;
						default:
							$civilite = 'Monsieur';
							break;
					}

					echo				'<tr>'.
											'<td>'. $civilite .'</td>'.
											'<td>'. $socr['socr_nom'] .'</td>'.
											'<td>'. $socr['socr_prenom'] .'</td>'.
											'<td>'. $socr['socr_adresse'] .'</td>'.
											'<td>'. $socr['socr_codePostal'] .'</td>'.
											'<td>'. $socr['socr_ville'] .'</td>'.
											'<td>'. $socr['socr_pays'] .'</td>'.
											'<td><a title="Modifier"  href="'. base_url() .'index.php/gestionIndivisions/modifierSocietaire/'. $societaireInfos['soc_login'] .'/'. $socr['socr_id'] .'"><i class="fas fa-edit"></i></a></td>'.
											'<td><a title="Supprimer" onclick="return confirm(\'Voulez-vous vraiment supprimer ce sociétaire ?\');" href="'. base_url() .'index.php/gestionIndivisions/supprimerSocietaire/'. $societaireInfos['soc_login'] .'/'. $socr['socr_id'] .'"><i class="fas fa-trash"></i></a></td>'.
										'</tr>';

					$i = $i + 1;
				}

				echo				'</tbody>'.
								'</table>'.
							'</div>'.
						'</div>';

				$index = $i;
			}
		} else {
			echo	'<div class="block text-center">'.
						'<h2 class="font-weight-bold">Aucune indivision</h2>'.
						'<br>'.
						'<p>Veuillez ajouter des sociétaires représentés</p>'.
					'</div>';
		}
	?>

</div>
</div>
</section>