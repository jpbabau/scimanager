<div class="col-lg-9">
	<input type="text" class="txt_csrfname" hidden name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>"><br>   
    <div class="block">
        <div class="row justify-content-center align-items-center">
            <div class="col-auto mr-auto">
				<?php
					$nbParts = 0;
					$nbCréation = 0;
					$nbTransmissioneEntreVifs = 0;
					$nbTransmissioneParDeces = 0;
					$nbVentes = 0;
					$nbDestructions = 0;
					foreach($listeMouvements as $mvt) {
						switch ($mvt['ope_type']) {
							case 0:
								$nbParts++;
								$nbCréation++;
								break;
							case 1:
								$nbTransmissioneEntreVifs++;
								break;
							case 2:
								$nbTransmissioneParDeces++;
								break;
							case 3:
								$nbVentes++;
								break;
							case 4:
								$nbParts--;
								$nbDestructions++;
								break;
							default:
								break;
						}
					}
				?>
                <h2 class="font-weight-bold"><i class="fas fa-exchange-alt"></i> Nombre de parts : <?php echo $nbParts.'('.$nbCréation.','.$nbTransmissioneEntreVifs.','.$nbTransmissioneParDeces.','.$nbVentes.','.$nbDestructions.')';?></h2>
            </div>
            <div class="col-auto">
                <a title="Ajouter un mouvement" href="<?php echo base_url() . 'index.php/gestionMouvements/ajouter';?>"><i class="fas fa-plus fa-3x"></i></a>
            </div>
        </div>
		<?php
			if (isset($result)) {
				if ($result === -1) {
					echo '<p class="text-danger">Le fichier n\'a pas pu être téléversé.</p>';
				} else {
					$i = 6;
					foreach($result as $ligne) {
						if ($ligne !== 0) {
							echo '<p class="text-danger">Il y a une erreur à la ligne '. $i .' : ';

							switch ($ligne) {
								case 1:
									echo 'L\'un des champs obligatoires est vide ou mal renseigné.</p>';
									break;
								case 2:
									echo 'L\'un des champs est trop long.</p>';
									break;
								case 3:
									echo 'Le titulaire ou le bénéficiaire n\'existe pas.</p>';
									break;
								case 4:
									echo 'La liste des parts n\'est pas correcte.</p>';
									break;
								case 5:
									echo 'Une des parts existe déjà, impossible de créer une part déjà existante.</p>';
									break;
								case 6:
									echo 'Le titulaire ne possède pas une des parts.</p>';
									break;
								case 7:
									echo 'La date d\'assemblée générale renseignée est incorrecte.</p>';
									break;
								case 8:
									echo 'Le type de mouvement est incorrect.</p>';
									break;
								case 9:
									echo 'Le mouvement est un doublon.</p>';
									break;
								default:
									break;
							}
						} else {
							if ($ligne === 0) {
								echo '<p class="text-success">le mouvement à la ligne '. $i . ' a été ajouté.</p>';
							}
						}

						$i = $i + 1;
					}
				}
			}
		?>
    </div>

    <div class="block">
        <h3>Mouvements</h3>
        <div class="table-responsive">
            <table class="table table-striped display" id="tableau-mouvements">
                <thead>
                    <tr>
						<th scope="col">Numéro</th>
                        <th scope="col">Date</th>
                        <th scope="col">Type</th>
                        <th scope="col">Titulaire</th>
                        <th scope="col">Nombre de parts</th>
                        <th scope="col">Prix par part</th>
                        <th scope="col">Paiements</th>
                        <th scope="col">Bénéficiaire</th>
						<th></th>
						<th></th>
                    </tr>
                </thead>
				<tbody>
					<?php
						if (isset($listeMouvements)) {

							$index = 0;
							while ($index < sizeof($listeMouvements)) {
								$titulaire = '';
								$beneficiaire = '';
								foreach ($listeSocietaires as $soc) {
									if ($listeMouvements[$index]['soc_loginTitulaire'] === $soc['soc_login']) {
										$titulaire = $soc['soc_prenom'].' '.$soc['soc_nom'];
									}
									if ($listeMouvements[$index]['soc_loginBeneficiaire'] === $soc['soc_login']) {
										$beneficiaire = $soc['soc_prenom'].' '.$soc['soc_nom'];
									}
								}
								$id = $listeMouvements[$index]['ope_id'];

								switch ($listeMouvements[$index]['ope_type']) {
									case 0:
										$opeType = 'Création';
										break;
									case 1:
										$opeType = 'Transmission par vifs';
										break;
									case 2:
										$opeType = 'Transmission par décès';
										break;
									case 3:
										$opeType = 'Vente';
										break;
									case 4:
										$opeType = 'Destruction';
										break;
									default:
										break;
								}

								$paiementType1	= $listeMouvements[$index]['moy_id'];
								$paiementNum1	= $listeMouvements[$index]['pai_numero'];
								$paiements = '';

								$i = $index;
								$recupere = TRUE;
								while ($i < sizeof($listeMouvements) && $listeMouvements[$i]['ope_id'] === $id) {
									if ($i !== $index && $listeMouvements[$i]['moy_id'] === $paiementType1 && $listeMouvements[$i]['pai_numero'] === $paiementNum1) {
										$recupere = FALSE;
									}

									if ($recupere) {
										$paiements = $paiements . $listeMouvements[$i]['moy_type'] . ' : ' . $listeMouvements[$i]['pai_numero'] . '<br>';
									}
									$i = $i + 1;
								}

								echo	'<tr>'.
											'<td>'. $listeMouvements[$i - 1]['ope_id'].'</td>'.
											'<td>'. date('d-m-Y', strtotime($listeMouvements[$i - 1]['ass_date'])) .'</td>'.
											'<td>'. $opeType .'</td>'.
											'<td>'. $titulaire .'</td>'.
											'<td>'. $listeMouvements[$i - 1]['ope_nombrePart'] .'</td>'.
											'<td>'. $listeMouvements[$i - 1]['ope_valeurUnitaire'] .'</td>'.
											'<td>'. $paiements .'</td>'.
											'<td>'. $beneficiaire .'</td>'.
											'<td><a title="modifier" href="'. base_url() .'index.php/gestionMouvements/modifier/'. $listeMouvements[$i - 1]['ope_id'].'"><i class="fas fa-edit"></i></a></td>'.
											'<td><a title="supprimer" onclick="return confirm(\'Voulez-vous vraiment supprimer ce mouvement\');" href="'. base_url() .'index.php/gestionMouvements/supprimer/'. $listeMouvements[$i - 1]['ope_id'].'"><i class="fas fa-trash-alt"></i></a></td>'.
										'</tr>';

								$index = $i;
							}
						}
					?>
				</tbody>
            </table>
        </div>
		<table class="table table-striped display" style="width:100%" id="tableau-mouvements">
                <thead>
                    <tr>
                        <th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>						
						<th></th>
						<th></th>
                       <th><label id="allMvtSupprimer" name="allMvtSupprimer" title="Supprimer les mouvements sélectionnés"class="button"><i class="fas fa-trash-alt"></i></span></th>
                    </tr>
                </thead>
		</table>
		<div class="row justify-content-center">
			<a target="_blank" href="<?php echo base_url();?>index.php/gestionMouvements/historiqueParts">Générer l'historique des parts</a>
		</div>
		<div class="row justify-content-center import-export">
			<label title="Importer des sociétaires" for="import" class="button"><i class="fas fa-file-import fa-3x"></i></span>
			<?php echo form_open_multipart('gestionMouvements/importer', 'name="form-importer" hidden');?>
				<input type="file" name="import" id="import">
			</form>
			<a title="Exporter des mouvements" target="_blank" href="<?php echo base_url()?>index.php/gestionMouvements/exporter"><i class="fas fa-file-export fa-3x"></i></a>
		</div>
    </div>
</div>
</div>
</section>