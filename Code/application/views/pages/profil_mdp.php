<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-lock"></i> Changer son mot de passe</h2>
		<?php
		if ($this->session->statut != 'Admin') {
			echo '<FONT color="red" size="3pt"> Il est conseillé de changer régulièrement son mot de passe</FONT>';
		}
		?>
		<?php
			if (isset($mdpResult)) {
				switch ($mdpResult) {
					case 1:
						echo '<p class="text-danger"> <br> L\'un des champs est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger"> <br> L\'un des champs est trop long.</p>';
						break;
					case 3:
						echo '<p class="text-danger"> <br> Le mot de passe actuel saisie est incorrect.</p>';
						break;
					case 4:
						echo '<p class="text-danger"> <br>  Le format du nouveau mot de passe n\'est pas valide.</p>';
						break;
					default:
						echo '<p class="text-success"> <br> Le mot de passe a été modifié.</p>';
						break;
				}
			}
		?>
	</div>

	<div class="block">
		<h3>Mot de passe</h3>
		<?php echo form_open('profil/modifierMdp/' . $societaireInfos['soc_login'], 'name="form-modifierMdp"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="oldmdp">Mot de passe actuel</label>
					<input type="password" class="form-control" name="oldmdp" id="oldmdp">
					<span class="form-eye" data-input="oldmdp">
						<i class="fas fa-eye-slash"></i>
					</span>
				</div>
				<div class="form-group">
					<label for="newmdp1">Nouveau mot de passe</label>
					<input type="password" class="form-control" name="newmdp1" id="newmdp1">
					<span class="form-eye" data-input="newmdp1">
						<i class="fas fa-eye-slash"></i>
					</span>
					<div class="form-regles-5 mdp">
						<div class="form-regle" id="mdp-car"><i class="fas fa-times"></i> Au moins 8 caractères.</div>
						<div class="form-regle" id="mdp-maj"><i class="fas fa-times"></i> Au moins 1 majuscule.</div>
						<div class="form-regle" id="mdp-min"><i class="fas fa-times"></i> Au moins 1 minuscule.</div>
						<div class="form-regle" id="mdp-num"><i class="fas fa-times"></i> Au moins 1 chiffre.</div>
						<div class="form-regle" id="mdp-spe"><i class="fas fa-times"></i> Au moins 1 caractère spécial.</div>
					</div>
				</div>
				<div class="form-group">
					<label for="newmdp2">Confirmer le mot de passe</label>
					<input type="password" class="form-control" name="newmdp2" id="newmdp2">
					<span class="form-eye" data-input="newmdp2">
						<i class="fas fa-eye-slash"></i>
					</span>
					<div class="form-regles-1 mdp">
						<div class="form-regle" id="mdp-equal"><i class="fas fa-times"></i> Les mots de passe ne sont pas identiques.</div>
					</div>
				</div>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Modifier le mot de passe</button>
			</div>
		</form>
	</div>
</div>
</div>
</section>