<div class="col-lg-9">
	<input type="text" class="txt_csrfname" hidden name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>"><br>   
    <div class="block">
		<?php
			$nbSocieteaires = 0;
			foreach($listeSocietaires as $soc) {
				if (isset($soc['ass_dateSortie'])) {
					$nbSocieteaires++;
				} 
			}
		?>
        <h2 class="font-weight-bold"><i class="fas fa-users"></i> Nombre d'ancien sociétaires : <?php echo $nbSocieteaires;?></h2>
    </div>

    <div class="block">
        <h3>Anciens sociétaires</h3>
        <div class="table-responsive">
            <table class="table table-striped display" id="tableau-societaires-a">
                <thead>
                    <tr>
                        <th scope="col">Identifiant</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Email</th>
                        <th scope="col">Date d'entrée</th>
                        <th scope="col">Date de sortie</th>
						<th scope="col">DateRGPD</th>
						<th></th>
						<th></th>
						<th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
						if (isset($listeSocietaires)) {
							foreach($listeSocietaires as $soc) {
								if (isset($soc['ass_dateSortie'])) {

									$dateRGPD = 'Aucune';
									$buttonRGPD = '';

									if (isset($soc['soc_adresse'])) {
										$dateRGPD = date('Y-m-d', strtotime('+'. $siteInfos['dureeRGPD'] .' months', strtotime($soc['ass_dateSortie'])));
										$aujourdhui = date('Y-m-d', strtotime('now'));

										if ($aujourdhui >= $dateRGPD) {
											$buttonRGPD = '<a onclick="return confirm(\'Voulez-vous vraiment supprimer les données du sociétaire : '. $soc['soc_login'] .'\');" href="'. base_url() .'index.php/gestionSocietaires/supprimerRGPD/'. $soc['soc_login'] .'" class="btn btn-danger blink_me"><i class="fas fa-exclamation-triangle"></i> Effacer données <i class="fas fa-exclamation-triangle"></i></a>';
										} else {
											$buttonRGPD = '<a onclick="return confirm(\'Voulez-vous vraiment supprimer les données du sociétaire : '. $soc['soc_login'] .'\');" href="'. base_url() .'index.php/gestionSocietaires/supprimerRGPD/'. $soc['soc_login'] .'" class="btn btn-primary">Effacer données</a>';
										}
									}

									echo	'<tr>'.
												'<td>'. $soc['soc_login'] .'</td>'.
												'<td>'. $soc['sta_nom'] .'</td>'.
												'<td>'. $soc['soc_nom'] .'</td>'.
												'<td>'. $soc['soc_prenom'] .'</td>'.
												'<td>'. $soc['soc_email'] .'</td>'.
												'<td>'. date('d-m-Y', strtotime($soc['ass_dateEntree'])) .'</td>'.
												'<td>'. date('d-m-Y', strtotime($soc['ass_dateSortie'])) .'</td>'.
												'<td>'. $dateRGPD . '</td>'.
												'<td><a title="Modifier" href="'. base_url() .'index.php/profil/informations/'. $soc['soc_login'] .'"><i class="fas fa-edit"></i></a></td>'.
												'<td>'. $buttonRGPD .'</td>'.
												'<td><a title="Supprimer" onclick="return confirm(\'Voulez-vous vraiment supprimer le sociétaire : '. $soc['soc_login'] .'\');" href="'. base_url() .'index.php/gestionSocietaires/supprimerAncien/'. $soc['soc_login'] .'"><i class="fas fa-trash-alt"></i></a></td>'.
											'</tr>';
								}
							}
						}
                    ?>
                </tbody>
            </table>
        </div>
		<table class="table table-striped display" style="width:100%" id="tableau-societaires-a">
                <thead>
                    <tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><label id="allErase" name="allErase" title="Effacer les données des sociétaires sélectionnés"class="button"><i class="fas fa-exclamation-triangle"></i></span></th>
                        <th><label id="allOldSupprimer" name="allOldSupprimer" title="Supprimer les sociétaires sélectionnés"class="button"><i class="fas fa-trash-alt"></i></span></th>
                    </tr>
                </thead>
		</table>
    </div>
</div>
</div>
</section>