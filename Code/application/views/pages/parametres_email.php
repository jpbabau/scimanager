<div class="col-lg-9">
	<div class="block">
		<h2 class="font-weight-bold"><i class="fas fa-cogs"></i> Paramètres</h2>
		<small><a href="<?php echo base_url();?>index.php/parametres/avances">Paramètres avancés</a></small>
		<?php
			if (isset($result) && !isset($testResult)) {
				echo '<p class="text-success">Les modifications ont été enregistrées.</p>';
			}

			if (isset($testResult)) {
				if ($testResult) {
					echo '<p class="text-success">Un courriel a été envoyé.</p>';
				} else {
					echo '<p class="text-danger">Une erreur s\'est produite lors de l\'envoi du courriel.</p>';
				}
			}
		?>
	</div>
	
	<div class="block">
		<h3>Configuration de l'email</h3>
		<?php echo form_open('parametres/modifierSiteEmail', 'name="form-initialisationEmail"');?>
			<div class="col-lg-6">
			<div class="form-group">
				<label for="host">Serveur SMTP <span class="text-danger">*</span></label>
				<input type="text" class="form-control" name="host" id="host" value="<?php echo set_value('host', $email['smtp_host']);?>">
				<div class="invalid-feedback">Champ obligatoire.</div>
			</div>
			<div class="form-group">
				<label for="port">Port <span class="text-danger">*</span></label>
				<input type="text" class="form-control" name="port" id="port" value="<?php echo set_value('port', $email['smtp_port']);?>">
				<div class="invalid-feedback">Champ obligatoire.</div>
			</div>
			<div class="form-group">
				<label for="email">Email <span class="text-danger">*</span></label>
				<input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email', $email['smtp_user']);?>">
				<div class="invalid-feedback">Champ obligatoire.</div>
			</div>
			<div class="form-group">
				<label for="password">Mot de passe <span class="text-danger">*</span></label>
				<input type="password" class="form-control" name="password" id="password" value="<?php echo set_value('password', $email['smtp_pass']);?>">
				<span class="form-eye" data-input="password">
					<i class="fas fa-eye-slash"></i>
				</span>
				<div class="invalid-feedback">Champ obligatoire.</div>
			</div>
			<div class="form-group">
				<label for="crypto">Crypto <span class="text-danger">*</span></label>
				<select name="crypto" id="crypto" class="custom-select">
					<option value="ssl" <?php if(set_value('crypto', $email['smtp_crypto']) === 'ssl') echo 'selected';?>>SSL</option>
					<option value="tls" <?php if(set_value('crypto', $email['smtp_crypto']) === 'tls') echo 'selected';?>>TLS</option>
				</select>
			</div>
			<div class="form-submit">
				<button type="submit" name="action" value="test" class="btn btn-primary">Test</button>
			</div>
			<div class="form-submit">
				<button type="submit" name="action" value="submit" class="btn btn-lg btn-primary">Enregistrer les modifications</button>
			</div>
		</form>
	</div>
</div>
</div>
</section>