<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold">Signaler une erreur</h2>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">Un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">Un problème est survenu lors de l\'envoi du courriel.</p>';
						break;
					case 3:
						echo '<p class="text-danger">Le courriel n\'a pas été envoyé car vous n\'avez pas renseigné votre adresse mail.</p>';
						break;
					default:
						echo '<p class="text-success">Le courriel a été envoyé.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php echo form_open('profil/signalerErreurSci/'. $societaireInfos['soc_login'], 'name="form-signaler" id="form-signaler"');?>
			<div class="col-lg-6">
				<div class="table-responsive">
					<?php
						if ($societaireInfos['sta_nom'] == 0) {
							$nomRegime		= 'Régime matrimonial';
							$listeRegimes	= $listeRegimesMatrimoniaux;
						} else {
							$nomRegime		= 'Régime fiscal';
							$listeRegimes	= $listeRegimesFiscaux;
						}
					?>
					<label>Informations erronées <span class="text-danger">*</span></label>
					<table class="table table-bordered table-sm table-hover">
						<thead>
							<tr>
								<th scope="col">Champ</th>
								<th scope="col">Valeur</th>
								<th scope="col">Erreur ?</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $nomRegime;?></td>
								<td><?php
									foreach($listeRegimes as $reg) {
										if ($reg['reg_id'] === $societaireInfos['reg_id']) {
											echo $reg['reg_nom'];
										} 
									}
								?></td>
								<td class="text-center"><input type="checkbox" id="champ17" name="champ17" value="Régime"></td>
							</tr>
							<tr>
								<td>Date d'entrée</td>
								<td><?php
									echo date('d-m-Y', strtotime($societaireInfos['ass_dateEntree']));
								?></td>
								<td class="text-center"><input type="checkbox" id="champ18" name="champ18" value="Date d'entrée"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="form-group">
					<label for="message">Message <span class="text-danger">*</span></label>
					<textarea class="form-control" name="message" id="message" form="form-signaler" cols="30" rows="10" placeholder="Écrire un message..." value="<?php echo set_value('message');?>"></textarea>
				</div>
				<p><span class="text-danger">*</span> : Champs obligatoires</p>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Envoyer</button>
			</div>
		</form>
    </div>
</div>
</div>
</section>