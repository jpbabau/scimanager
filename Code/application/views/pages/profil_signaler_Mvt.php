<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold">Signaler une erreur</h2>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">Un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">Un problème est survenu lors de l\'envoi du courriel.</p>';
						break;
					case 3:
						echo '<p class="text-danger">Le courriel n\'a pas été envoyé car vous n\'avez pas renseigné votre adresse mail.</p>';
						break;
					default:
						echo '<p class="text-success">Le courriel a été envoyé.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php echo form_open('profil/signalerErreurMvt/'. $societaireInfos['soc_login'], 'name="form-signalerS" id="form-signalerS"');?>
			<div class="col-lg-6">
			<div class="table-responsive">
				<label>Informations erronées <span class="text-danger">*</span></label>
				<table class="table table-bordered table-sm table-hover">
					<thead>
						<tr>
							<th scope="col">Mouvement</th>
							<th scope="col">Erreur ?</th>
						</tr>
					</thead>
					<tbody>
					<?php
						if (isset($listeMouvements)) {
							$index = 0;
							while ($index < sizeof($listeMouvements)) {
								$titulaire = '';
								$beneficiaire = '';
								if ($listeMouvements[$index]['soc_loginTitulaire'] === $societaireInfos['soc_login'] || $listeMouvements[$index]['soc_loginBeneficiaire'] === $societaireInfos['soc_login']) {
									foreach ($listeSocietaires as $soc) {
										if ($listeMouvements[$index]['soc_loginTitulaire'] === $soc['soc_login']) {
											$titulaire = $soc['soc_prenom'].' '.$soc['soc_nom'];
										}
										if ($listeMouvements[$index]['soc_loginBeneficiaire'] === $soc['soc_login']) {
											$beneficiaire = $soc['soc_prenom'].' '.$soc['soc_nom'];
										}
									}
									$id = $listeMouvements[$index]['ope_id'];

									switch ($listeMouvements[$index]['ope_type']) {
										case 1:
											$opeType = 'Transmission par vifs';
											break;
										case 2:
											$opeType = 'Transmission par décès';
											break;
										case 3:
											$opeType = 'Vente';
											break;
										default:
											$opeType = 'Création';
											break;
									}

									$paiementType1	= $listeMouvements[$index]['moy_id'];
									$paiementNum1	= $listeMouvements[$index]['pai_numero'];
									$paiements = '';

									$i = $index;
									$recupere = TRUE;
									while ($i < sizeof($listeMouvements) && $listeMouvements[$i]['ope_id'] === $id) {
										if ($i !== $index && $listeMouvements[$i]['moy_id'] === $paiementType1 && $listeMouvements[$i]['pai_numero'] === $paiementNum1) {
											$recupere = FALSE;
										}

										if ($recupere) {
											$paiements = $paiements . $listeMouvements[$i]['moy_type'] . ' : ' . $listeMouvements[$i]['pai_numero'] . '<br>';
										}
										$i = $i + 1;
									}

									echo	'<tr>'.
												'<td>'. date('d-m-Y', strtotime($listeMouvements[$i - 1]['ass_date'])) .' '
												. $opeType .' '
												. $titulaire .' '
												. $listeMouvements[$i - 1]['ope_nombrePart'] .' '
												. $listeMouvements[$i - 1]['ope_valeurUnitaire'] .' '
												. $paiements .' '
												. $beneficiaire .'</td>'
												.'<td class="text-center"><input type="checkbox" id="champM'.$i.'" name="champM'.$i.'" value="Régime"></td>'.
											'</tr>';
									$index = $i;
								} else {
									$index = $index + 1;
								}
							}
						}
                    ?>
					</tbody>
				</table>
			</div>
			<div class="form-group">
				<label for="message">Message <span class="text-danger">*</span></label>
				<textarea class="form-control" name="message" id="message" form="form-signalerS" cols="30" rows="10" placeholder="Écrire un message..." value="<?php echo set_value('message');?>"></textarea>
			</div>
				<p><span class="text-danger">*</span> : Champs obligatoires</p>
			</div>
			<div class="form-submit">
				<button type="submit" class="btn btn-lg btn-primary">Envoyer</button>
			</div>
		</form>
    </div>
</div>
</div>
</section>