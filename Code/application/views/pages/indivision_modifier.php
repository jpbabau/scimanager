<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold">
			<a href="<?php echo base_url() .'index.php/gestionIndivisions/indivision/'. $login;?>"><i class="fas fa-arrow-left"></i></a>
			<i class="fas fa-user"></i> Ajouter un sociétaire représenté
		</h2>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">L\'un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">L\'un des champs de saisie est trop long.</p>';
						break;
					default:
						echo '<p class="text-success">Le sociétaire représenté a été ajouté.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php echo form_open('gestionIndivisions/modifierSocietaire/'. $login .'/'. $societaireRepresenteInfos['socr_id'], 'name="form-SocietaireRepresente"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="indivision">Indivision</label>
					<select class="custom-select" name="indivision" id="indivision" disabled>
						<?php echo '<option value="'. $societaireRepresenteInfos['ind_num'] .'">Indivision n°'. $societaireRepresenteInfos['ind_num'] .'</option>';?>
					</select>
				</div>
				<div class="form-group">
					<label for="civilite">Civilité</label>
					<select class="custom-select" name="civilite" id="civilite">
						<option value="0" <?php if ($societaireRepresenteInfos['socr_civilite'] == 0) echo 'selected';?>>Monsieur</option>
						<option value="1" <?php if ($societaireRepresenteInfos['socr_civilite'] == 1) echo 'selected';?>>Madame</option>
						<option value="2" <?php if ($societaireRepresenteInfos['socr_civilite'] == 2) echo 'selected';?>>Non binaire</option>
						<option value="3" <?php if ($societaireRepresenteInfos['socr_civilite'] == 3) echo 'selected';?>>Non renseigné</option>
						<option value="4" <?php if ($societaireRepresenteInfos['socr_civilite'] == 4) echo 'selected';?>>Autre</option>
					</select>
				</div>
				<div class="form-row">
					<div class="form-group col-6">
						<label for="nom">Nom <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="nom" name="nom" value="<?php echo $societaireRepresenteInfos['socr_nom'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
					</div>
					<div class="form-group col-6">
						<label for="prenom">Prénom <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $societaireRepresenteInfos['socr_prenom'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
					</div>
				</div>
				<div class="form-group">
					<label for="lieu">Lieu de naissance <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="lieu" name="lieu" value="<?php echo $societaireRepresenteInfos['socr_lieuNaissance'];?>">
					<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
				</div>
				<div class="form-group">
					<label for="date">Date de naissance <span class="text-danger">*</span></label>
					<input type="date" class="form-control" id="date" name="date" value="<?php echo $societaireRepresenteInfos['socr_dateNaissance'];?>">
					<div class="invalid-feedback">Champ obligatoire.</div>
				</div><div class="form-group">
					<label for="adresse">Adresse <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="adresse" name="adresse" value="<?php echo $societaireRepresenteInfos['socr_adresse'];?>">
					<div class="invalid-feedback">Champ obligatoire.</div>
				</div>
				<div class="form-row">
					<div class="form-group col-6">
						<label for="code">Code postal <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="code" name="code" value="<?php echo $societaireRepresenteInfos['socr_codePostal'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des chiffres.</div>
					</div>
					<div class="form-group col-6">
						<label for="ville">Ville <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="ville" name="ville" value="<?php echo $societaireRepresenteInfos['socr_ville'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des lettres.</div>
					</div>
				</div>
				<div class="form-group">
					<label for="pays">Pays <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="pays" name="pays" value="<?php echo $societaireRepresenteInfos['socr_pays'];?>">
					<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des lettres.</div>
				</div>
			</div>
			<div class="form-submit">
                <button type="submit" class="btn btn-lg btn-primary">Enregistrer les modifications</button>
            </div>
		</form>
    </div>
</div>
</div>
</section>