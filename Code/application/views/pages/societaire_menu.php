<section class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<div class="block menu">
				<nav class="nav flex-column menu">
					<a href="<?php echo base_url() . 'index.php/profil/informations/'. $societaireInfos['soc_login'];?>"	class="nav-link font-weight-bold <?php if ($menuOnglet === 'informations') echo 'active';?>"><i class="fas fa-user"></i> Informations</a>
					<a href="<?php echo base_url() . 'index.php/profil/contact/'. $societaireInfos['soc_login'];?>"			class="nav-link font-weight-bold <?php if ($menuOnglet === 'contact') echo 'active';?>"><i class="fas fa-envelope"></i> Contact</a>
					<a href="<?php echo base_url() . 'index.php/profil/sci/'. $societaireInfos['soc_login'];?>"				class="nav-link font-weight-bold <?php if ($menuOnglet === 'sci') echo 'active';?>"><i class="fas fa-info-circle"></i> SCI</a>
					<a href="<?php echo base_url() . 'index.php/profil/mouvements/'. $societaireInfos['soc_login'];?>"		class="nav-link font-weight-bold <?php if ($menuOnglet === 'mouvements') echo 'active';?>"><i class="fas fa-exchange-alt"></i> Mouvements</a>
					<a href="<?php echo base_url() . 'index.php/profil/modifierMdp/'. $societaireInfos['soc_login'];?>"		class="nav-link font-weight-bold <?php if ($menuOnglet === 'mdp') echo 'active';?>"><i class="fas fa-lock"></i> Mot de passe</a>
				</nav>
			</div>

			<div class="block menu">
				<nav class="nav flex-column menu cogerance">
					<a href="<?php echo base_url() . 'index.php/profil/mailCogerance/'. $societaireInfos['soc_login'];?>" class="nav-link font-weight-bold">Contacter la co-gérance</a>
				</nav>
			</div>

			<div class="block menu">
				<nav class="nav flex-column menu deconnexion">
					<a href="<?php echo base_url() . 'index.php/accueil/deconnexion';?>" class="nav-link font-weight-bold">Deconnexion</a>
				</nav>
			</div>
		</div>