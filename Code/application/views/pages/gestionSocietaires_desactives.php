<div class="col-lg-9">
    <div class="block">
        <div class="row justify-content-center align-items-center">
            <div class="col-auto mr-auto">
				<?php
					$nbSocieteaires = 0;
					foreach($listeSocietaires as $soc) {
						if ($soc['soc_validite']!='D') {
							$nbSocieteaires++;
						} 
					}
					$nbSocieteairesD = 0;
					foreach($listeSocietaires as $soc) {
						if (($soc['soc_validite']==='D') && !(isset($soc['ass_dateSortie']))) {
							$nbSocieteairesD++;
						} 
					}
				?>
                <h2 class="font-weight-bold"><i class="fas fa-users"></i> Nombre de sociétaires désactivés : <?php echo $nbSocieteairesD;?></h2>
				<small><a href="<?php echo base_url()?>index.php/gestionSocietaires">Voir les societaires activés (<?php echo $nbSocieteaires;?>)</a></small>
            </div>
            <div class="col-auto">
                <a href="<?php echo base_url() . 'index.php/gestionSocietaires/ajouter';?>"><i class="fas fa-user-plus fa-3x"></i></a>
            </div>
        </div>
		<?php
			if (isset($result)) {
				if ($siteInfos['structure']) {
					$structure = 'le GFA';
				} else {
					$structure = 'la SCI';
				}
				echo '<p class="text-success">Le sociétaire '. $result .' a quitté '. $structure .'</p>';
			}
		?>
    </div>

    <div class="block">
        <h3>Sociétaires désactivés</h3>
        <div class="table-responsive">
            <table class="table table-striped display" id="tableau-societaires-d">
                <thead>
                    <tr>
                        <th scope="col">Identifiant</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Email</th>
                        <th scope="col">Date d'entrée</th>
						<th></th>
						<th></th>
						<th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php

						if ($siteInfos['structure']) {
							$quitter = 'Quitter le GFA';
						} else {
							$quitter = 'Quitter la SCI';
						}

						if (isset($listeSocietaires)) {
							foreach($listeSocietaires as $soc) {
								if ($soc['soc_validite'] === 'D' && !isset($soc['ass_dateSortie'])) {

									echo	'<tr>'.
												'<td>'. $soc['soc_login'] .'</td>'.
												'<td>'. $soc['sta_nom'] .'</td>'.
												'<td>'. $soc['soc_nom'] .'</td>'.
												'<td>'. $soc['soc_prenom'] .'</td>'.
												'<td>'. $soc['soc_email'] .'</td>'.
												'<td>'. date('d-m-Y', strtotime($soc['ass_dateEntree'])) .'</td>'.
												'<td><a title="Réactiver" onclick="return confirm(\'Voulez-vous vraiment réactiver le sociétaire : '. $soc['soc_login'] .'\');" href="'. base_url() .'index.php/gestionSocietaires/reactiver/'. $soc['soc_login'] .'"><i class="fas fa-check"></i></a></td>'.
												'<td><a title="'. $quitter .'" href="'. base_url() .'index.php/gestionSocietaires/quitter/'. $soc['soc_login'] .'"><i class="fas fa-sign-out-alt"></i></a></td>'.
												'<td><a title="Supprimer" onclick="return confirm(\'Voulez-vous vraiment supprimer le sociétaire : '. $soc['soc_login'] .'\');" href="'. base_url() .'index.php/gestionSocietaires/supprimer/desactives/'. $soc['soc_login'] .'"><i class="fas fa-trash-alt"></i></a></td>'.
											'</tr>';
								}
							}
						}
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</section>