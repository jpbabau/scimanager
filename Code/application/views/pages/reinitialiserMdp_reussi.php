<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-6 block">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="form-header">
                        <h2 class="font-weight-bold">Mot de passe oublié</h2>
                    </div>
                    <div class="text-center">
                        <p>Un courriel a été envoyé contenant un lien pour modifier votre mot de passe.</p>
                    </div>
                    <div class="form-submit">
                        <a href="<?php echo base_url();?>" class="btn btn-lg btn-primary">Continuer</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>