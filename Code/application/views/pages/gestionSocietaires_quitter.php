<div class="col-lg-9">
    <div class="block">
		<h2 class="font-weight-bold"><i class="fas fa-users"></i> Gestion des sociétaires</h2>
    </div>

    <div class="block">
		<?php echo form_open('gestionSocietaires/quitter/'. $societaireInfos['soc_login'], 'name="form-SocietaireRepresente"');?>
			<div class="row justify-content-center">
				<div class="col-lg-6">
					<div class="form-header">
						<h2>
							<?php
								if ($siteInfos['structure']) {
									$structure = 'le GFA';
								} else {
									$structure = 'la SCI';
								}

								echo $societaireInfos['soc_prenom'] .' '. $societaireInfos['soc_nom'] .' ('. $societaireInfos['soc_login'] .') quitte '. $structure;
							?>
						</h2>
					</div>
					<div class="form-group">
						<label>Date d'entrée</label>
						<input type="date" class="form-control" value="<?php echo $societaireInfos['ass_dateEntree'];?>" disabled>
					</div>
					<div class="form-group">
						<label for="entree">Date de sortie</label>
						<select class="custom-select" name="dateSortie" id="dateSortie">
							<?php
								for ($i = 0; $i < sizeof($listeDatesAG); $i++) {
									$AG = $listeDatesAG[$i];
									if ($i === 0) {
										echo '<option value="'. $AG['ass_id'] .'" selected>'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
									} else {
										echo '<option value="'. $AG['ass_id'] .'">'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
									}
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-submit">
                <button type="submit" class="btn btn-lg btn-primary">Valider</button>
            </div>
		</form>
    </div>
</div>
</div>
</section>