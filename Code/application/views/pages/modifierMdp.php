<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-6 block">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <?php echo form_open('accueil/modifierMdp', 'name="form-modifierMdp"');?>
                        <div class="form-header">
                            <h2 class="font-weight-bold">Première Connexion</h2>
                            <p>Veuillez modifier votre mot de passe.</p>
                        </div>
                        <div class="form-group">
                            <label for="oldmdp">Mot de passe actuel</label>
                            <input type="password" class="form-control" name="oldmdp" id="oldmdp">
                            <span class="form-eye" data-input="oldmdp">
                                <i class="fas fa-eye-slash"></i>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="newmdp1">Nouveau mot de passe</label>
                            <input type="password" class="form-control" name="newmdp1" id="newmdp1">
                            <span class="form-eye" data-input="newmdp1">
                                <i class="fas fa-eye-slash"></i>
                            </span>
                            <div class="form-regles-5 mdp">
                                <div class="form-regle" id="mdp-car"><i class="fas fa-times"></i> Au moins 8 caractères.</div>
                                <div class="form-regle" id="mdp-maj"><i class="fas fa-times"></i> Au moins 1 majuscule.</div>
                                <div class="form-regle" id="mdp-min"><i class="fas fa-times"></i> Au moins 1 minuscule.</div>
                                <div class="form-regle" id="mdp-num"><i class="fas fa-times"></i> Au moins 1 chiffre.</div>
                                <div class="form-regle" id="mdp-spe"><i class="fas fa-times"></i> Au moins 1 caractère spécial.</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newmdp2">Confirmer le mot de passe</label>
                            <input type="password" class="form-control" name="newmdp2" id="newmdp2">
                            <span class="form-eye" data-input="newmdp2">
                                <i class="fas fa-eye-slash"></i>
                            </span>
                            <div class="form-regles-1 mdp">
                                <div class="form-regle" id="mdp-equal"><i class="fas fa-times"></i> Les mots de passe ne sont pas identiques.</div>
                            </div>
                        </div>
                        <?php
                            if (isset($result)) {
                                echo '<div class="form-erreur">';
                                switch ($result) {
                                    case 1:
                                        echo '<p>L\'un des champs est vide.</p>';
                                        break;
                                    case 2:
                                        echo '<p>L\'un des champs est trop long.</p>';
                                        break;
                                    case 3:
                                        echo '<p>Le mot de passe actuel est incorrect.</p>';
                                        break;
                                    case 4:
                                        echo '<p>Le format du nouveau mot de passe n\'est pas valide.</p>';
                                        break;
                                    default:
                                        break;
                                }
                                echo '</div>';
                            }
                        ?>
                        <div class="form-submit">
                            <button type="submit" class="btn btn-lg btn-primary">Modifier le mot de passe</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>