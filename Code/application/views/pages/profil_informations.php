<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-user"></i> Informations personnelles <br></h2>
		<?php
		if ($this->session->statut != 'Admin') {
			echo '<FONT color="red" size="3pt"> Les informations personnelles sont gérées par la co-gérance et ne peuvent être modifiées par le.a sociétaire.
			<br>Merci de signaler toute erreur ou changement en cliquant sur le menu « signaler une erreur » en bas de cette page.</FONT>';
		}
		?>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">L\'un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">L\'un des champs de saisie est trop long.</p>';
						break;
					default:
						echo '<p class="text-success">Les modifications ont été enregistrées.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php
			if ($this->session->statut === 'Admin') {
				echo form_open('profil/informations/' . $societaireInfos['soc_login'], 'name="form-modifierProfilInfos"');
				$actionInfo = 'Enregistrer les modifications';
			} else {
				echo form_open('profil/signalerErreurInformations/' . $societaireInfos['soc_login'], 'name="form-modifierProfilInfos"');
				$actionInfo = 'Signaler une erreur';
			}
		?>
		<?php
			if ($societaireInfos['sta_id'] == 0) {
				$nomUsuel		= 'Nom usuel';
			} else {
				$nomUsuel		= 'Raison Sociale';
			}
		?>
			<fieldset <?php if ($this->session->statut != 'Admin') echo 'disabled=disabled';?>>
			<div class="col-lg-6">
				<br/>
				<div class="form-header">
					<h4>Statut</h4>
				</div>
				<div class="form-group"> 
					<select class="custom-select" name="statut" id="statut" >
						<?php
							foreach($listeStatuts as $sta) {
								if ($sta['sta_id'] === $societaireInfos['sta_id']) {
									echo '<option value="'. $sta['sta_id'] .'" selected>'. $sta['sta_nom'] .'</option>';
								} else {
									echo '<option value="'. $sta['sta_id'] .'">'. $sta['sta_nom'] .'</option>';
								}
							}
						?>
					</select>
				</div>
				<div class="form-check">
					<input type="checkbox" class="form-check-input" id="porte-fort" name="porte-fort" <?php if ($societaireInfos['soc_indivision'] == 1) echo 'checked';?>>
					<label for="porte-fort" class="form-check-label">Porte-fort (représentant d'une indivision)</label>
				</div>
				<br/>
				<div class="form-header">
					<h4>Informations personnelles</h4>
				</div>
				<div class="form-group">
					<label for="nom-usuel"><?php echo $nomUsuel;?></label>
					<input type="text" class="form-control" id="nom-usuel" name="nom-usuel" value="<?php echo $societaireInfos['soc_nomUsuel'];?>">
					<div class="invalid-feedback">Ne doit contenir que des lettres.</div>
				</div>
				<div class="form-group">
					<label for="civ">Civilité</label>
					<select class="custom-select" name="civilite" id="civilite">
						<option value="0" <?php if ($societaireInfos['soc_civilite'] == 0) echo 'selected';?>>Monsieur</option>
						<option value="1" <?php if ($societaireInfos['soc_civilite'] == 1) echo 'selected';?>>Madame</option>
						<option value="2" <?php if ($societaireInfos['soc_civilite'] == 2) echo 'selected';?>>Non binaire</option>
						<option value="3" <?php if ($societaireInfos['soc_civilite'] == 3) echo 'selected';?>>Non renseigné</option>
						<option value="4" <?php if ($societaireInfos['soc_civilite'] == 4) echo 'selected';?>>Autre</option>
					</select>
				</div>
				<div class="form-row">
					<div class="form-group col-6">
						<label for="nom">Nom <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="nom" name="nom" value="<?php echo $societaireInfos['soc_nom'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
					</div>
					<div class="form-group col-6">
						<label for="pre">Prénom <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $societaireInfos['soc_prenom'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
					</div>
				</div>
				<div class="form-group">
					<label for="autres-prenoms">Autres prénoms</label>
					<input type="text" class="form-control" id="autres-prenoms" name="autres-prenoms" value="<?php echo $societaireInfos['soc_autrePrenom'];?>">
					<div class="invalid-feedback">Ne doit contenir que des lettres.</div>
					<small class="form-text text-muted">Les prénoms doivent être séparés par une virgule.</small>
				</div>
				<div class="form-group">
					<label for="representants">Représentants légaux</label>
					<input type="text" class="form-control" id="representants" name="representants" value="<?php echo $societaireInfos['soc_representantsLegaux'];?>">
					<div class="invalid-feedback">Ne doit contenir que des lettres.</div>
					<small class="form-text text-muted">Les représentants doivent être séparés par une virgule.</small>
				</div>
				<div class="form-group">
					<label for="lieu">Lieu de naissance <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="lieu" name="lieu" value="<?php echo $societaireInfos['soc_lieuNaissance'];?>">
					<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
				</div>
				<div class="form-group">
					<label for="siret">Numéro de SIRET </span></label>
					<input type="text" class="form-control" id="siret" name="siret" value="<?php echo $societaireInfos['soc_siret'];?>">
					<div class="invalid-feedback">Un numéro de SIRET est composé de 14 chiffres maximum</div>
				</div>
				<div class="form-group">
					<label for="date">Date de naissance <span class="text-danger">*</span></label>
					<input type="date" class="form-control" id="date" name="date" value="<?php echo $societaireInfos['soc_naissance'];?>">
					<div class="invalid-feedback">Champ obligatoire.</div>
				</div>
				<div class="form-group">
					<label for="nationalite">Nationalite <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="nationalite" name="nationalite" value="<?php echo $societaireInfos['soc_nationalite'];?>">
				</div>
			</div>
			</fieldset>
			<div class="form-submit">
                <button type="submit" class="btn btn-lg btn-primary"><?php echo $actionInfo;?></button>
            </div>
		</form>
    </div>
</div>
</div>
</section>