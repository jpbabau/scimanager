<section class="container-fluid">
	<div class="row">
		<div class="col-lg-3">
			<div class="block menu">
				<nav class="nav flex-column menu" name="menu">
					<a class="nav-link font-weight-bold" role="button"><i class="fas fa-users"></i> Sociétaires</a>
					<nav class="nav flex-column sub-menu">
						<a href="<?php echo base_url() . 'index.php/gestionSocietaires';?>" class="nav-link font-weight-bold <?php if ($menuOnglet === 'gestionSocietaires') echo 'active';?>"> Les sociétaires</a>
						<a href="<?php echo base_url() . 'index.php/gestionIndivisions';?>" class="nav-link font-weight-bold <?php if ($menuOnglet === 'gestionIndivisions') echo 'active';?>"> Les indivisions</a>
						<a href="<?php echo base_url() . 'index.php/gestionSocietaires/anciens';?>" class="nav-link font-weight-bold <?php if ($menuOnglet === 'AnciensSocietaires') echo 'active';?>"> Les anciens sociétaires</a>
					</nav>
					<a href="<?php echo base_url() . 'index.php/gestionMouvements';?>" class="nav-link font-weight-bold	<?php if ($menuOnglet === 'gestionMouvements') echo 'active';?>"><i class="fas fa-exchange-alt"></i> Mouvements</a>
					<a href="<?php echo base_url() . 'index.php/mail/envoyer';?>" class="nav-link font-weight-bold	<?php if ($menuOnglet === 'mailing') echo 'active';?>"><i class="fas fa-envelope"></i> Mailing</a>
					<a href="<?php echo base_url() . 'index.php/configuration';?>" class="nav-link font-weight-bold		<?php if ($menuOnglet === 'configuration') echo 'active';?>"><i class="fas fa-cog"></i> Configuration</a>
					<a href="<?php echo base_url() . 'index.php/parametres';?>" class="nav-link font-weight-bold		<?php if ($menuOnglet === 'parametres') echo 'active';?>"><i class="fas fa-cogs"></i> Application</a>
				</nav>
			</div>

			<div class="block menu">
				<nav class="nav flex-column menu deconnexion">
					<a href="<?php echo base_url() . 'index.php/accueil/deconnexion';?>" class="nav-link font-weight-bold">Deconnexion</a>
				</nav>
			</div>
		</div>