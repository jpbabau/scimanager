<div class="col-lg-9">
	<div class="block">
		<h2 class="font-weight-bold"><i class="fas fa-users"></i> Ajouter un sociétaire</h2>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">L\'un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">L\'un des champs de saisie est trop long.</p>';
						break;
					case 3:
						echo '<p class="text-danger">Le format de l\'email n\'est pas valide.</p>';
						break;
					case 5:
						echo '<p class="text-danger">Le format d\'un des numéros de téléphone n\'est pas valide.</p>';
						break;
					case 6:
						echo '<p class="text-danger">Il faut au moins un mail ou un numéro de téléphone.</p>';
						break;
					case 7:
						echo '<p class="text-success">Le sociétaire a été ajouté.</p>';
						break;
					default:
						echo '<p class="text-success">Le sociétaire a été ajouté.</p>';
						break;
				}
			}
		?>
	</div>

	<div class="block">
		<?php echo form_open('gestionSocietaires/ajouter', 'name="form-ajouterSocietaire"');?>
			<div class="container">
				<div class="row justify-content-center" name="page">
					<div class="col-lg-6">
						<div class="form-header">
							<h2>Statut</h2>
						</div>
						<div class="form-group">
							<select class="custom-select" name="statut" id="statut">
								<?php
									foreach($listeStatuts as $sta) {
										if ($sta['sta_id'] == 0 || $sta['sta_id'] == set_value('statut')) {
											echo '<option value="'. $sta['sta_id'] .'" selected>'. $sta['sta_nom'] .'</option>';
										} else {
											echo '<option value="'. $sta['sta_id'] .'">'. $sta['sta_nom'] .'</option>';
										}
									}
								?>
							</select>
						</div>
						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="porte-fort" name="porte-fort" <?php if(set_value('porte-fort') == 'on') echo 'checked';?> >
							<label for="porte-fort" class="form-check-label">Porte-fort (représentant d'une indivision)</label>
						</div>

						<br>

						<div class="form-header">
							<h2>Informations personnelles</h2>
						</div>
						<div class="form-group">
							<label for="civilite">Civilité</label>
							<select class="custom-select" name="civilite" id="civilite">
								<option value="0" <?php if(set_value('civilite') == 0) echo 'selected';?>>Monsieur</option>
								<option value="1" <?php if(set_value('civilite') == 1) echo 'selected';?>>Madame</option>
								<option value="2" <?php if(set_value('civilite') == 2) echo 'selected';?>>Non binaire</option>
								<option value="3" <?php if(set_value('civilite') == 3) echo 'selected';?>>Non renseigné</option>
								<option value="4" <?php if(set_value('civilite') == 4) echo 'selected';?>>Autre</option>
							</select>
						</div>
						<div class="form-row">
							<div class="form-group col-6">
								<label for="nom">Nom <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="nom" name="nom" value="<?php echo set_value('nom');?>">
								<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
							</div>
							<div class="form-group col-6">
								<label for="prenom">Prénom <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo set_value('prenom');?>">
								<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
							</div>
						</div>
						<div class="form-group">
							<label for="nom-usuel">Nom usuel <span class="text-danger">*</span></label>
							<input type="text" class="form-control" id="nom-usuel" name="nom-usuel" value="<?php echo set_value('nom-usuel');?>">
							<div class="invalid-feedback">Ne doit contenir que des lettres.</div>
						</div>
						<div class="form-group">
							<label for="autres-prenoms">Autres prénoms</label>
							<input type="text" class="form-control" id="autres-prenoms" name="autres-prenoms" value="<?php echo set_value('autres-prenoms');?>">
							<div class="invalid-feedback">Ne doit contenir que des lettres.</div>
							<small class="form-text text-muted">Les prénoms doivent être séparés par une virgule.</small>
						</div>
						<div class="form-group">
							<label for="representants">Représentants légaux</label>
							<input type="text" class="form-control" id="representants" name="representants" value="<?php echo set_value('representants');?>">
							<div class="invalid-feedback">Ne doit contenir que des lettres.</div>
							<small class="form-text text-muted">Les représentants doivent être séparés par une virgule.</small>
						</div>
						<div class="form-group">
							<label for="lieu">Lieu de naissance <span class="text-danger">*</span></label>
							<input type="text" class="form-control" id="lieu" name="lieu" value="<?php echo set_value('lieu');?>">
							<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
						</div>
						<div class="form-group">
							<label for="siret">Numéro de SIRET </label>
							<input type="text" class="form-control" id="siret" name="siret" value="<?php echo set_value('siret');?>">
							<div class="invalid-feedback">Un numéro de SIRET est composé de 14 chiffres maximum</div>
						</div>
						<div class="form-group">
							<label for="date">Date de naissance <span class="text-danger">*</span></label>
							<input type="date" class="form-control" id="date" name="date" value="<?php echo set_value('date');?>">
							<div class="invalid-feedback">Champ obligatoire.</div>
						</div>
						<div class="form-group">
							<label for="nationalite">Nationalité <span class="text-danger">*</span></label>
							<input type="text" class="form-control" id="nationalite" name="nationalite" value="<?php echo set_value('nationalite');?>">
							<div class="invalid-feedback">Champ obligatoire,<br>Ne doit contenir que des lettres.</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-center" name="page" hidden>
					<div class="col-lg-6">
						<div class="form-header">
							<h2>Informations de contact</h2>
						</div>
						<div class="form-group">
							<label for="adresse">Adresse <span class="text-danger">*</span></label>
							<input type="text" class="form-control" id="adresse" name="adresse" value="<?php echo set_value('adresse');?>">
							<div class="invalid-feedback">Champ obligatoire.</div>
						</div>
						<div class="form-row">
							<div class="form-group col-6">
								<label for="code">Code postal <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="code" name="code" value="<?php echo set_value('code');?>">
								<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des chiffres.</div>
							</div>
							<div class="form-group col-6">
								<label for="ville">Ville <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="ville" name="ville" value="<?php echo set_value('ville');?>">
								<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des lettres.</div>
							</div>
						</div>
						<div class="form-group">
							<label for="pays">Pays <span class="text-danger">*</span></label>
							<input type="text" class="form-control" id="pays" name="pays" value="<?php echo set_value('pays', 'France');?>">
							<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des lettres.</div>
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email');?>">
							<div class="invalid-feedback">Le format n'est pas valide.</div>
						</div>
						<div class="form-group">
							<label for="fixe">Téléphone fixe</label>
							<input type="text" class="form-control" id="fixe" name="fixe" value="<?php echo set_value('fixe');?>">
							<div class="invalid-feedback">Le format n'est pas valide.</div>
						</div>
						<div class="form-group">
							<label for="portable">Téléphone portable</label>
							<input type="text" class="form-control" id="portable" name="portable" value="<?php echo set_value('portable');?>">
							<div class="invalid-feedback">Le format n'est pas valide.</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-center" name="page" hidden>
					<div class="col-lg-6">
						<div class="form-header">
							<h2><?php
									if ($siteInfos['structure']) {
										echo 'Informations GFA';
									} else {
										echo 'Informations SCI';
									}
							?></h2>
						</div>
						<div class="form-group">
							<label for="matrimonial">Régime matrimonial</label>
							<select class="custom-select" name="matrimonial" id="matrimonial">
								<?php
									foreach($listeRegimesMatrimoniaux as $reg) {
										if ($reg['reg_id'] === '0' || set_value('matrimonial') == $reg['reg_id']) {
											echo '<option value="'. $reg['reg_id'] .'" selected>'. $reg['reg_nom'] .'</option>';
										} else {
											echo '<option value="'. $reg['reg_id'] .'">'. $reg['reg_nom'] .'</option>';
										}
									}
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="fiscal">Régime fiscal</label>
							<select class="custom-select" name="fiscal" id="fiscal">
								<?php
									foreach($listeRegimesFiscaux as $reg) {
										if ($reg['reg_id'] === '0' || set_value('fiscal') == $reg['reg_id']) {
											echo '<option value="'. $reg['reg_id'] .'" selected>'. $reg['reg_nom'] .'</option>';
										} else {
											echo '<option value="'. $reg['reg_id'] .'">'. $reg['reg_nom'] .'</option>';
										}
									}
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="entree">
								<?php
									if ($siteInfos['structure']) {
										echo 'Date d\'entrée dans le GFA';
									} else {
										echo 'Date d\'entrée dans la SCI';
									}
								?>
							</label>
							<select class="custom-select" name="entree" id="entree">
								<?php
									for ($i = 0; $i < sizeof($listeDatesAG); $i++) {
										$AG = $listeDatesAG[$i];
										if ($i === 0 || set_value('entree') == $AG['ass_id']) {
											echo '<option value="'. $AG['ass_id'] .'" selected>'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
										} else {
											echo '<option value="'. $AG['ass_id'] .'">'. date('d-m-Y', strtotime($AG['ass_date'])) .'</option>';
										}
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<p><span class="text-danger">*</span> : Champs obligatoires</p>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<div class="row justify-content-between align-items-center">
							<span class="btn btn-lg btn-primary" name="prec">Précédent</span>
							<p name="num-page">1 / 3</p>
							<span class="btn btn-lg btn-primary" name="suiv">Suivant</span>

							<button type="submit" class="btn btn-lg btn-primary" hidden>Ajouter</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
</section>