<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-cog"></i> Configuration</h2>
		<?php
			if (isset($dateResult)) {
				switch ($dateResult) {
					case 1:
						echo '<p class="text-danger">Aucune date ne correspond à cette id</p>';
						break;
					case 2:
						echo '<p class="text-danger">Cette date est utilisée, vous ne pouvez pas la supprimer.</p>';
						break;
					default:
						break;
				}
			}

			if (isset($dureeResult)) {
				switch ($dureeResult) {
					case 1:
						echo '<p class="text-danger">La durée n\'est pas valide.</p>';
						break;
					default:
						break;
				}
			}
		?>
    </div>

	<div class="block">
        <h3>Les dates d'assemblées générales</h3>
		<div class="col-lg-6">
			<?php
				foreach($listeDatesAG as $date) {
					echo	form_open('configuration/modifierDateAG/', 'name="form-modifierDonnees"');
					echo 		'<div class="form-group form-inline mt-4">'.
									'<input type="text" name="id" value="'. $date['ass_id'] .'" readonly hidden>'.
									'<input type="date" class="form-control" name="date" value="'. $date['ass_date'] .'">'.
									'<span title="modifier" class="button" name="modifier"><i class="fas fa-edit"></i></span>'.
									'<a title="supprimer" class="button" name="supprimer" onclick="return confirm(\'Voulez-vous vraiment supprimer cette date ?\');" href="'. base_url() .'index.php/configuration/supprimerDateAG/'. $date['ass_id'] .'"><i class="fas fa-trash"></i></a>'.
									'<span title="valider" class="button" name="valider"><i class="fas fa-check"></i></span>'.
									'<span title="annuler" class="button" name="annuler"><i class="fas fa-times"></i></span>'.
									'<div class="invalid-feedback">Ne peut pas être vide.</div>'.
								'</div>'.
							'</form>';
				}
			?>
			<hr>
			<?php echo form_open('configuration/ajouterDateAG/', 'name="form-ajouterDonnees"');?>
				<div class="form-group form-inline mt-4">
					<input type="date" class="form-control" name="date">
					<span class="button" name="valider"><i class="fas fa-check"></i></span>
					<span class="button" name="annuler"><i class="fas fa-times"></i></span>
					<div class="invalid-feedback">Ne peut pas être vide.</div>
				</div>
				<div class="form-submit">
					<span class="button" name="plus"><i class="fas fa-plus fa-3x"></i></span>
				</div>
			</form>
		</div>
    </div>
</div>
</div>
</section>