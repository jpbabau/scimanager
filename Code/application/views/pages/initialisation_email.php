<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- JQuery V.3.6.0 -->
	<script defer src="<?php echo base_url();?>style/jquery/js/jquery-3.6.0.min.js"></script>

    <!-- Bootstrap V.4.6 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/bootstrap/css/bootstrap.min.css">
    <script defer src="<?php echo base_url();?>style/bootstrap/js/bootstrap.min.js"></script>

    <!-- FontAwesome V.5.15.3 -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/fontawesome/css/all.min.css">

	<!-- DataTables V.1.10.24 -->
	<link rel="stylesheet" href="<?php echo base_url();?>style/datatables/css/datatables.min2.css">
	<script defer src="<?php echo base_url();?>style/datatables/js/datatables.min.js"></script>

	<!-- Typeahead -->
	<script defer src="<?php echo base_url();?>style/typeahead/js/bootstrap3-typeahead.min.js"></script>

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>style/core/css/main.css">

    <!-- Core JS -->
    <script defer src="<?php echo base_url();?>style/core/js/script.js"></script>


    <title>Initialisation</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-6 block">
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<?php echo form_open('initialisation/modifierEmail', 'name="form-initialisationEmail"');?>
							<div class="form-header">
								<h2 class="font-weight-bold">Initialisation</h2>
								<p>Configuration de l'email<br><small>2 / 3</small></p>
							</div>
							<div class="form-group">
								<label for="host">Serveur SMTP <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="host" id="host" value="<?php echo set_value('host', $email['smtp_host']);?>">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="port">Port <span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="port" id="port" value="<?php echo set_value('port', $email['smtp_port']);?>">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="email">Email <span class="text-danger">*</span></label>
								<input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email', $email['smtp_user']);?>">
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="password">Mot de passe <span class="text-danger">*</span></label>
								<input type="password" class="form-control" name="password" id="password" value="<?php echo set_value('password', $email['smtp_pass']);?>">
								<span class="form-eye" data-input="password">
									<i class="fas fa-eye-slash"></i>
								</span>
								<div class="invalid-feedback">Champ obligatoire.</div>
							</div>
							<div class="form-group">
								<label for="crypto">Crypto <span class="text-danger">*</span></label>
								<select name="crypto" id="crypto" class="custom-select">
									<option value="ssl" <?php if(set_value('crypto', $email['smtp_crypto']) === 'ssl') echo 'selected';?>>SSL</option>
									<option value="tls" <?php if(set_value('crypto', $email['smtp_crypto']) === 'tls') echo 'selected';?>>TLS</option>
								</select>
							</div>
							<?php
								if (isset($result)) {
									if ($result) {
										echo '<p class="text-success text-center">Un courriel a été envoyé.</p>';
									} else {
										echo '<p class="text-danger text-center">Une erreur s\'est produite lors de l\'envoi du courriel.</p>';
									}
								}
							?>
							<div class="row justify-content-between align-items-center mt-4">
								<a href="<?php echo base_url() . 'index.php/initialisation/modifierBDD';?>" class="btn btn-lg btn-primary">Précédent</a>
								<button type="submit" name="action" value="test" class="btn btn-primary">Test</button>
								<button type="submit" name="action" value="submit" class="btn btn-lg btn-primary">Suivant</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>