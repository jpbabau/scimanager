<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-cogs"></i> Paramètres avancés</h2>
		<?php
			if (isset($statutResult)) {
				switch ($statutResult) {
					case 1:
						echo '<p class="text-danger">Aucun statut ne correspond à cet id</p>';
						break;
					case 2:
						echo '<p class="text-danger">Ce statut est utilisé, vous ne pouvez pas le supprimer.</p>';
						break;
					default:
						break;
				}
			}
		?>
		<small><a href="<?php echo base_url();?>index.php/parametres">Paramètres</a></small>
    </div>

    <div class="block">
        <h3>Les statuts des entreprises</h3>
		<div class="col-lg-6">
			<?php
				foreach($listeStatuts as $sta) {
					if ($sta['sta_id'] != 0) {
						echo	form_open('parametres/modifierStatut/', 'name="form-modifierDonnees"');
						echo 		'<div class="form-group form-inline mt-4">'.
										'<input type="text" name="id" value="'. $sta['sta_id'] .'" readonly hidden>'.
										'<input type="text" class="form-control" name="statut" value="'. $sta['sta_nom'] .'">'.
										'<span title="modifier" class="button" name="modifier"><i class="fas fa-edit"></i></span>'.
										'<a title="supprimer" class="button" name="supprimer" onclick="return confirm(\'Voulez-vous vraiment supprimer cet statut ?\');" href="'. base_url() .'index.php/parametres/supprimerStatut/'. $sta['sta_id'] .'"><i class="fas fa-trash"></i></a>'.
										'<span class="button" name="valider"><i class="fas fa-check"></i></span>'.
										'<span class="button" name="annuler"><i class="fas fa-times"></i></span>'.
										'<div class="invalid-feedback">Ne doit contenir que des lettres.</div>'.
									'</div>'.
								'</form>';
					}
				}
			?>
			<hr>
			<?php echo form_open('parametres/ajouterStatut/', 'name="form-ajouterDonnees"');?>
				<div class="form-group form-inline mt-4">
					<input type="text" class="form-control" name="statut" placeholder="Entrez un statut...">
					<span class="button" name="valider"><i class="fas fa-check"></i></span>
					<span class="button" name="annuler"><i class="fas fa-times"></i></span>
					<div class="invalid-feedback">Ne doit contenir que des lettres.</div>
				</div>
				<div class="form-submit">
					<span class="button" name="plus"><i class="fas fa-plus fa-3x"></i></span>
				</div>
			</form>
		</div>
    </div>
	<div class="block">
		<h3>La durée RGPD</h3>
		<br>
		<?php echo form_open('parametres/modifierDureeRGPD', 'name="form-modifierDureeRGPD"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="duree">Durée RGPD (en mois)</label>
					<input type="text" class="form-control" name="duree" id="duree" value="<?php echo $siteInfos['dureeRGPD'];?>">
					<div class="invalid-feedback">Champs obligatoire,<br>Ne peut pas être négatif ou égal à 0.</div>
				</div>
				<div class="form-submit">
					<button type="submit" class="btn btn-lg btn-primary">Enregistrer les modifications</button>
				</div>
			</div>
		</form>
	</div>
	<div class="block menu">
		<nav class="nav flex-column menu deconnexion">
			<a onclick="return confirm('Voulez-vous vraiment tout desinstaller ?');" href="<?php echo base_url() . 'index.php/parametres/desinstaller';?>" class="nav-link font-weight-bold">Desinstaller tout</a>
		</nav>
	</div>
</div>
</div>
</section>