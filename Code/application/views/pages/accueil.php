<section class="banner">
    <img src="<?php echo base_url() .'style/core/img/'. $siteInfos['image'];?>" alt="">
</section>
<?php
    if (!isset($_COOKIE['testcookie'])) {
        echo    '<section class="block cookie">'.
                    '<h1>Les cookies sont désactivés</h1>'.
                    '<p>Veuillez activer la gestion des cookies dans le navigateur, pour information aucun cookie n’est conservé par le site</p>'.
                    '<a href="'. base_url() .'" class="btn btn-lg btn-primary">Rafraîchir la page</a>'.
                '</section>';
    }
?>