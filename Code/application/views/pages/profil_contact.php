<div class="col-lg-9">
    <div class="block">
        <h2 class="font-weight-bold"><i class="fas fa-envelope"></i> Informations de contact</h2>
		<?php
		if ($this->session->statut != 'Admin') {
			echo '<FONT color="red" size="3pt"> Ces informations sont modifiables, merci de les tenir à jour.
			<br>Afin de permettre à la co-gérance de garder le contact, merci de renseigner au moins un numéro de téléphone ou un mail.</FONT>';
		}
		?>
		<?php
			if (isset($result)) {
				switch ($result) {
					case 1:
						echo '<p class="text-danger">L\'un des champs obligatoires est vide.</p>';
						break;
					case 2:
						echo '<p class="text-danger">L\'un des champs de saise est trop long.</p>';
						break;
					case 3:
						echo '<p class="text-danger">Le format de l\'email n\'est pas valide.</p>';
						break;
					case 5:
						echo '<p class="text-danger">Le format d\'un des numéros de téléphone n\'est pas valide.</p>';
						break;
					case 6:
							echo '<p class="text-danger">Il faut au moins un mail ou un numéro de téléphone.</p>';
							break;
					default:
						echo '<p class="text-success">Les modifications ont été enregistrées.</p>';
						break;
				}
			}
		?>
    </div>

    <div class="block">
		<?php echo form_open('profil/contact/' . $societaireInfos['soc_login'], 'name="form-modifierProfilContact"');?>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="adresse">Adresse <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="adresse" name="adresse" value="<?php echo $societaireInfos['soc_adresse'];?>">
					<div class="invalid-feedback">Champ obligatoire.</div>
				</div>
				<div class="form-row">
					<div class="form-group col-6">
						<label for="code">Code postal <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="code" name="code" value="<?php echo $societaireInfos['soc_codePostal'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des lettres ou des chiffres.</div>
					</div>
					<div class="form-group col-6">
						<label for="ville">Ville <span class="text-danger">*</span></label>
						<input type="text" class="form-control" id="ville" name="ville" value="<?php echo $societaireInfos['soc_ville'];?>">
						<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des lettres.</div>
					</div>
				</div>
				<div class="form-group">
					<label for="pays">Pays <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="pays" name="pays" value="<?php echo $societaireInfos['soc_pays'];?>">
					<div class="invalid-feedback">Champ obligatoire,<br> Ne doit contenir que des lettres.</div>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" class="form-control" id="email" name="email" value="<?php echo $societaireInfos['soc_email'];?>">
					<div class="invalid-feedback">Le format n'est pas valide.</div>
				</div>
				<div class="form-group">
					<label for="fixe">Téléphone fixe</label>
					<input type="text" class="form-control" id="fixe" name="fixe" value="<?php echo $societaireInfos['soc_telephoneFixe'];?>">
					<div class="invalid-feedback">Le format n'est pas valide.</div>
				</div>
				<div class="form-group">
					<label for="portable">Téléphone portable</label>
					<input type="text" class="form-control" id="portable" name="portable" value="<?php echo $societaireInfos['soc_telephonePortable'];?>">
					<div class="invalid-feedback">Le format n'est pas valide.</div>
				</div>
				<div class="form-check">
					<input type="checkbox" class="form-check-input" id="autoriser-email" name="autoriser-email" <?php if ($societaireInfos['soc_pasEnvoyerEmail'] == 0) echo 'checked';?>>
					<label for="autoriser-email" class="form-check-label">Autorisation d'envoi de mails d'information par la co-gérance</label>
				</div>
				<div class="form-check">
					<input type="checkbox" class="form-check-input" id="autoriser-fixe" name="autoriser-fixe" <?php if ($societaireInfos['soc_pasAppelerFixe'] == 0) echo 'checked';?>>
					<label for="autoriser-fixe" class="form-check-label">Autorisation d'appel sur le téléphone fixe par la co-gérance</label>
				</div>
				<div class="form-check">
					<input type="checkbox" class="form-check-input" id="autoriser-portable" name="autoriser-portable" <?php if ($societaireInfos['soc_pasAppelerPortable'] == 0) echo 'checked';?>>
					<label for="autoriser-portable" class="form-check-label">Autorisation d'appel sur le téléphone portable par la co-gérance</label>
				</div>
				<div class="form-check">
					<input type="checkbox" class="form-check-input" id="autoriser-sms" name="autoriser-sms" <?php if ($societaireInfos['soc_pasEnvoyerSms'] == 0) echo 'checked';?>>
					<label for="autoriser-sms" class="form-check-label">Autorisation d'envoi de sms par la co-gérance</label>
				</div>
				<div class="form-check">
					<input type="checkbox" class="form-check-input" id="autoriser-cloud" name="autoriser-cloud" <?php if ($societaireInfos['soc_autoriserCloud'] == 1) echo 'checked';?>>
					<label for="autoriser-cloud" class="form-check-label">Autorisation de la sauvegarde de vos données sur le cloud</label>
				</div>
			</div>
			<div class="form-submit">
                <button type="submit" class="btn btn-lg btn-primary">Enregistrer les modifications</button>
            </div>
		</form>
    </div>
</div>
</div>
</section>