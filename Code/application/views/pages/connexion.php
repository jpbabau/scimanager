<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-6 block">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <?php echo form_open('accueil/connexion', 'name="form-connexion"');?>
                        <div class="form-header">
                            <h2 class="font-weight-bold">Connexion</h2>
                            <p>Entrez vos identifiants pour vous connecter.</p>
                        </div>
                        <div class="form-group">
                            <label for="login">Identifiant</label>
                            <input type="text" class="form-control" name="login" id="login" autocomplete="off" value="<?php echo set_value('login');?>">
                        </div>
                        <div class="form-group">
                            <label for="mdp">Mot de passe</label>
                            <input type="password" class="form-control" name="mdp" id="mdp" autocomplete="off">
                            <span class="form-eye" data-input="mdp">
                                <i class="fas fa-eye-slash"></i>
                            </span>
                        </div>
                        <?php
                            if (isset($result)) {
                                echo '<div class="form-erreur">';
                                switch ($result) {
                                    case 1:
                                        echo '<p>Le login ou le mot de passe est vide.</p>';
                                        break;
                                    case 2:
                                        echo '<p>Les identifiants saisis sont incorrects.</p>';
                                        break;
                                    case 3:
                                        echo '<p>Le mot de passe est erroné.</p><p>Vous avez 5 tentatives pour vous connecter, avant que votre compte soit bloqué.</p>';
                                        break;
                                    case 4:
                                        echo '<p>Le compte est bloqué.</p>';
                                        break;
                                    default:
                                        break;
                                }
                                echo '</div>';
                            }
                        ?>
                        <div class="form-submit">
                            <button type="submit" class="btn btn-lg btn-primary">Se connecter</button>
                            <p><a href="<?php echo base_url() . 'index.php/accueil/reinitialiserMdp/';?>">Mot de passe oublié</a> ?</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>