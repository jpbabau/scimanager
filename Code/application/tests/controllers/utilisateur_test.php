<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;


final class Utilisateur_test extends TestCase
{

    protected $CI;

    function __construct()
    {
        $this->CI = &get_instance();
        parent::__construct();
    }
    
    /**
     * @dataProvider additionProvider
     */
    public function testAdd($a, $b, $expected)
    {
        $this->assertSame($expected, $a + $b);
    }

    public function additionProvider()
    {
        return [
            'adding zeros'  => [0, 0, 0],
            'zero plus one' => [0, 1, 1],
            'one plus zero' => [1, 0, 1],
            'one plus one'  => [1, 1, 3]
        ];
    }
}
