<?php

declare(strict_types=1);
require APPPATH . 'libraries/compte.php';

use PHPUnit\Framework\TestCase;


final class Compte_test extends TestCase
{

    protected $CI;
    protected $compte;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('Compte');
        parent::__construct();
    }

    /**
     * @before
     */
    public function init()
    {
        $this->compte = new Compte();
    }

    public function testConnexion(): void
    {
        $this->assertEquals(
            1,
            $this->compte->connexion("", "mdp")
        );

        $this->assertEquals(
            2,
            $this->compte->connexion("monLogin", "mdp")
        );

        $this->assertEquals(
            "AB",
            $this->compte->connexion("admin", "Password29!")
        );

        $this->assertEquals(
            "SA",
            $this->compte->connexion("monLoginUti", "mdputi29!")
        );

        $this->assertEquals(
            "SB",
            $this->compte->connexion("monLoginUti2", "mdputi29?")
        );

        $this->assertEquals(
            3,
            $this->compte->connexion("monLoginUti2", "Password1!")
        );

        $this->assertEquals(
            3,
            $this->compte->connexion("monLoginUti2", "Password2!")
        );

        $this->assertEquals(
            3,
            $this->compte->connexion("monLoginUti2", "Password3!")
        );

        $this->assertEquals(
            3,
            $this->compte->connexion("monLoginUti2", "Password4!")
        );

        $this->assertEquals(
            4,
            $this->compte->connexion("monLoginUti2", "Password5!")
        );
    }

    public function testReinitialiserMdp(): void
    {
        $this->assertEquals(
            1,
            $this->compte->reinitialiserMdp("")
        );

        $this->assertEquals(
            2,
            $this->compte->reinitialiserMdp("email56@sci.fr")
        );

        $this->assertEquals(
            0,
            $this->compte->reinitialiserMdp("votreemail@example.com")
        );

        $this->assertEquals(
            0,
            $this->compte->reinitialiserMdp("test@sci.fr")
        );

        $this->assertEquals(
            0,
            $this->compte->reinitialiserMdp("test@sci.fr")
        );

        $this->assertEquals(
            0,
            $this->compte->reinitialiserMdp("tristan.lp@live.fr")
        );
    }

    public function testObtenirLogin(): void
    {
        $this->assertEquals(
            null,
            $this->compte->obtenirLogin("Zqesrdhftghukkyhgfdszqefrg")
        );

        $this->assertEquals(
            "admin",
            $this->compte->obtenirLogin("b4gQru2KiWd885s5Y3sK7bTW63v5ZRRb")
        );
    }
}
