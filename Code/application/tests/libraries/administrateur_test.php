<?php

declare(strict_types=1);
require APPPATH . 'libraries/administrateur.php';

use PHPUnit\Framework\TestCase;


final class Administrateur_test extends TestCase
{

    protected $CI;
    protected $admin;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('Administrateur');
        parent::__construct();
    }

    /**
     * @before
     */
    public function init()
    {
        $this->admin = new Administrateur();
    }

    public function testModifierLogin(): void
    {
        $this->assertEquals(
            1,
            $this->admin->modifierLogin("")
        );

        $this->assertEquals(
            2,
            $this->admin->modifierLogin("azertyuiopazertyuiopazertyuiopazertyuiopazerty")
        );

        $this->assertEquals(
            0,
            $this->admin->modifierLogin("monLogin")
        );

        $this->assertEquals(
            0,
            $this->admin->modifierLogin("monLogin")
        );
    }

    public function testModifierEmail(): void
    {
        $this->admin = new Administrateur();
        $this->assertEquals(
            1,
            $this->admin->modifierLogin("")
        );

        $this->assertEquals(
            2,
            $this->admin->modifierEmail("zertyuiopazertyuiooazeropazertyuiopazerty@test.fr")
        );

        $this->assertEquals(
            3,
            $this->admin->modifierEmail("testemail")
        );

        $this->assertEquals(
            3,
            $this->admin->modifierEmail("testemail.fr")
        );

        $this->assertEquals(
            3,
            $this->admin->modifierEmail("test@emailfr")
        );

        $this->assertEquals(
            4,
            $this->admin->modifierEmail("testemail@sci.fr")
        );

        $this->assertEquals(
            0,
            $this->admin->modifierEmail("test@email.fr")
        );
    }

    public function testModifierMdp(): void
    {

        $this->admin = new Administrateur();
        $this->assertEquals(
            1,
            $this->admin->modifierMdp("@dminSCI29", "")
        );

        $this->assertEquals(
            2,
            $this->admin->modifierMdp("@dminSCI29", "azertyazertyazertyazertyazertyazertyazertyazertyaze")
        );

        $this->assertEquals(
            3,
            $this->admin->modifierMdp("testemail", "Admin29?")
        );

        $this->assertEquals(
            4,
            $this->admin->modifierMdp("@dminSCI29", "azertyuiop")
        );

        $this->assertEquals(
            4,
            $this->admin->modifierMdp("@dminSCI29", "azertyuiop29")
        );

        $this->assertEquals(
            4,
            $this->admin->modifierMdp("@dminSCI29", "Azertyuiop")
        );

        $this->assertEquals(
            4,
            $this->admin->modifierMdp("@dminSCI29", "Azertyuiop29")
        );

        $this->assertEquals(
            4,
            $this->admin->modifierMdp("@dminSCI29", "Azertyuiop!!")
        );

        $this->assertEquals(
            4,
            $this->admin->modifierMdp("@dminSCI29", "Azer!29")
        );

        $this->assertEquals(
            0,
            $this->admin->modifierMdp("@dminSCI29", "Azertyuiop29!!")
        );
    }

    public function testObtenirInformations(): void
    {
        $this->admin = new Administrateur();

        $infosTest = array(
            "login" => "monLogin",
            "email" => "test@email.fr",
        );
        $this->assertEquals(
            $infosTest,
            $this->admin->obtenirInformations()
        );
    }
}
