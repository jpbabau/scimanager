<?php

declare(strict_types=1);
require APPPATH . 'libraries/utilisateur.php';

use PHPUnit\Framework\TestCase;


final class Utilisateur_test extends TestCase
{

    protected $CI;
    protected $user;

    function __construct()
    {
        //$this->CI = &get_instance();
        //$this->CI->load->library('Utilisateur');
        parent::__construct();
    }

    /**
     * @before
     */
    public function init()
    {
        $this->user = new Utilisateur();
    }

    public function testAjouterUtilisateur(): void
    {

        $this->assertEquals(
            0,
            $this->user->ajouterUtilisateur("Azert29!", "Maël", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "email2@sci.fr", "0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );

        $this->assertEquals(
            1,
            $this->user->ajouterUtilisateur("Azert29!", "", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "email@sci.fr", "0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );

        $this->assertEquals(
            2,
            $this->user->ajouterUtilisateur("Azert29!", "Maël", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "testeazerazetyazeeazertyrtyumail@scimanager.fr", "0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );

        $this->assertEquals(
            3,
            $this->user->ajouterUtilisateur("Azert29!", "Maël", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "email@scifr", "0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );

        $this->assertEquals(
            4,
            $this->user->ajouterUtilisateur("Azert29!", "Maël", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "email2@sci.fr", "0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );

        $this->assertEquals(
            5,
            $this->user->ajouterUtilisateur("Azert29!", "Maël", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "email@sci.fr", "a0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );

        $this->assertEquals(
            6,
            $this->user->ajouterUtilisateur("Azert", "Maël", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "email@sci.fr", "0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );

        $this->assertEquals(
            0,
            $this->user->ajouterUtilisateur("Azert29!", "Maël", "tugdual, alain", "L'Helgouarc'h", "dupont", 1, 0, null, "1999-12-25", "Quimper", "email@sci.fr", "0123456789", "98 76 54 32 10", "13 rue bidon", 29000, "Quimper", "France", 0, "2021-05-27")
        );
    }

    public function testModifierMdp(): void
    {

        $this->assertEquals(
            1,
            $this->user->modifierMdp("", "", "")
        );

        $this->assertEquals(
            2,
            $this->user->modifierMdp("mlhelgouarch991", "azertyuiopazertyuiopazertyuiopazertyuiopazertaazertyazertyazertyazerty!", "-Scimanager29-")
        );

        $this->assertEquals(
            3,
            $this->user->modifierMdp("mlhelgouarch991", "Azert2", "-Scimanager29-")
        );

        $this->assertEquals(
            4,
            $this->user->modifierMdp("mlhelgouarch991", "Azert29!", "Scimanager29")
        );

        $this->assertEquals(
            0,
            $this->user->modifierMdp("mlhelgouarch991", "Azert29!", "-Scimanager29-")
        );
    }

    public function testObtenirInfos(): void
    {        
        $this->assertEquals(
            "mlhelgouarch99",
            $this->user->obtenirInfos("mlhelgouarch99")->uti_login
        );

        $this->assertEquals(
            "mlhelgouarch99",
            $this->user->obtenirInfos("email2@sci.fr")->uti_email
        );

        $this->assertEquals(
            "mlhelgouarch99",
            $this->user->obtenirInfos("Maël")->uti_prenom
        );

        $this->assertEquals(
            "mlhelgouarch99",
            $this->user->obtenirInfos("L'Helgouarc'h")->uti_nom
        );

        $this->assertEquals(
            "mlhelgouarch99",
            $this->user->obtenirInfos("1999-12-25")->uti_naissance
        );

        $this->assertEquals(
            "mlhelgouarch99",
            $this->user->obtenirInfos("B")->uti_validite
        );
    }

    public function testSupprimerUtilisateur(): void
    {

        $this->assertEquals(
            1,
            $this->user->supprimerUtilisateur("")
        );

        $this->assertEquals(
            2,
            $this->user->supprimerUtilisateur("testeazerazetyazeeazertyrtyumailascimanagertfr")
        );

        $this->assertEquals(
            3,
            $this->user->supprimerUtilisateur("qdsfghyh")
        );

        $this->assertEquals(
            0,
            $this->user->supprimerUtilisateur("mlhelgouarch991")
        );
    }
}
