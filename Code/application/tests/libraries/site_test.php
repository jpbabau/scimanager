<?php

declare(strict_types=1);
require APPPATH . 'libraries/site.php';

use PHPUnit\Framework\TestCase;


final class Site_test extends TestCase
{

    protected $CI;
    protected $site;

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('Site');
        parent::__construct();
    }

    /**
     * @before
     */
    public function init()
    {
        $this->site = new Site();
    }

    public function testModifierNom(): void
    {
        $this->site = new Site();
        $this->assertEquals(
            1,
            $this->site->modifierNom("")
        );

        $this->assertEquals(
            2,
            $this->site->modifierNom("azertyuiopazertyuiopazertyuiopazertyuiopazerty")
        );

        $this->assertEquals(
            2,
            $this->site->modifierNom("site")
        );

        $this->assertEquals(
            0,
            $this->site->modifierNom("SCIKervel")
        );
    }

    public function testModifierIcone(): void
    {
        $this->assertEquals(
            1,
            $this->site->modifierIcone("")
        );

        $this->assertEquals(
            2,
            $this->site->modifierIcone("zertyuiopazertyzertyuiopazertyzertyuiopazertyzertyuiopazertyopazery/uiopazertyuiopazert/yuiopazert.jpg")
        );

        $this->assertEquals(
            3,
            $this->site->modifierIcone("style/img/icone2.png")
        );

        $this->assertEquals(
            0,
            $this->site->modifierIcone("style/img/monIcone.jpg")
        );
    }

    public function testModifierImage(): void
    {
        $this->assertEquals(
            1,
            $this->site->modifierImage("")
        );

        $this->assertEquals(
            2,
            $this->site->modifierImage("zertyuiopazertyzertyuiopazertyzertyuiopazertyzertyuiopazertyopazery/uiopazertyuiopazert/yuiopazert.jpg")
        );

        $this->assertEquals(
            3,
            $this->site->modifierImage("style/img/image2.gif")
        );

        $this->assertEquals(
            0,
            $this->site->modifierImage("style/img/monImage.jpg")
        );
    }

    public function testObtenirInfos(): void
    {
        $infosTest = array(
            "nom" => "SCIKervel",
            "icone" => "icone.jpg",
            "image" => "image.jpg",
            "email" => "votreemaildusite@exemple.fr"
        );
        $this->assertEquals(
            $infosTest,
            $this->site->obtenirInfos()
        );

        $this->assertEquals(
            null,
            $this->site->obtenirInfos()
        );
    }
}
