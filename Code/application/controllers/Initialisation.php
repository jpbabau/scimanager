<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'libraries/installation.php';

/**
 * Contrôleur de la page d'initialisation
 */
class Initialisation extends CI_Controller {
	
	/**
	 * L'installation
	 *
	 * @access protected
	 * @var Installation
	 */
	protected $installation;
	
	/**
	 * Le constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->installation = new Installation();
	}
	
	/**
	 * Modifie les paramètres de la base de données
	 * 
	 * @return void
	 */
	public function modifierBDD() {
		$this->form_validation->set_rules('hostname', 'hostname', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('database', 'database', 'required');

		if ($this->form_validation->run()) {
			$hostname		= (string) $this->input->post('hostname');
			$username		= (string) $this->input->post('username');
			$password		= (string) $this->input->post('password');
			$databaseName	= (string) $this->input->post('database');

			$this->installation->modifierBD($hostname, $username, $password, $databaseName);
			$result = $this->installation->testerConnexionBD();

			if (!$result) {
				$data['result'] = $result;
				$this->load->view('pages/initialisation_BDD', $data);
			} else {
				redirect(base_url() . 'index.php/initialisation/modifierEmail');
			}
		} else {
			if (file_exists($file_path = APPPATH.'config/database.php')) {
				include($file_path);
			}
			$data['db'] = $db[$active_group];

			$this->load->view('pages/initialisation_BDD', $data);
		}
	}
	
	/**
	 * Modifie les paramètres d'envois de mails
	 * 
	 * @return void
	 */
	public function modifierEmail() {
		$this->form_validation->set_rules('host', 'host', 'required');
		$this->form_validation->set_rules('port', 'port', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('crypto', 'crypto', 'required');

		if ($this->form_validation->run()) {
			$host		= (string) $this->input->post('host');
			$port		= (string) $this->input->post('port');
			$email		= (string) $this->input->post('email');
			$password	= (string) $this->input->post('password');
			$crypto		= (string) $this->input->post('crypto');

			$action		= (string) $this->input->post('action');

			$this->installation->modifierEmail($host, $port, $email, $password, $crypto);

			if ($action === 'submit') {
				redirect(base_url() . 'index.php/initialisation/initialiserBDD');
			} else {
				$result = $this->installation->envoyerEmail($email);
				$data['result'] = $result;
				$this->load->view('pages/initialisation_email', $data);
			}
		} else {
			if (file_exists($file_path = APPPATH.'config/email.php')) {
				include($file_path);
			}
			$data['email'] = $config;

			$this->load->view('pages/initialisation_email', $data);
		}
	}

	/**
	 * Génére la base de données
	 * 
	 * @return void
	 */
	public function initialiserBDD() {
		$this->form_validation->set_rules('login', 'login', 'required');
		$this->form_validation->set_rules('mdp1', 'mdp1', 'required');
		$this->form_validation->set_rules('mdp2', 'mdp2', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('nom', 'nom', 'required');

		if ($this->form_validation->run()) {
			$login	= (string) $this->input->post('login');
			$mdp1	= (string) $this->input->post('mdp1');
			$mdp2	= (string) $this->input->post('mdp2');
			$email	= (string) $this->input->post('email');
			$nom	= (string) $this->input->post('nom');

			if ($mdp1 === $mdp2) {
				$result = $this->installation->initBD($login, $mdp1, $email, $nom);

				if ($result !== 0) {
					$data['result'] = $result;
					$this->load->view('pages/initialisation_infos_1', $data);
				} else {
					redirect(base_url());
				}
			} else {
				$this->load->view('pages/initialisation_infos_1');
			}
		} else {
			$result = $this->installation->testerVideBD();
			if ($result) {
				$this->load->view('pages/initialisation_infos_2');
			} else {
				$this->load->view('pages/initialisation_infos_1');
			}
		}
	}
}