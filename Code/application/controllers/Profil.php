<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de vue de la page profil
 */
class Profil extends CI_Controller {
	
	/**
	 * Le contrôleur principal de l'application
	 *
	 * @access protected
	 * @var Controleur
	 */
	protected $controleur;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		if (!$this->session->has_userdata('login') || ($this->session->statut !== 'Admin' && $this->session->statut !== 'Sociétaire')) {
			redirect(base_url());
		}

		$this->controleur = new Controleur();
	}
	
	/**
	 * Affiche / modifie les informations personnelles d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function informations(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}

		$this->form_validation->set_rules('statut', 'statut', 'required');
		$this->form_validation->set_rules('civilite', 'civilite', 'required');
		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('nationalite', 'nationalite', 'required');

		$statut = (int) $this->input->post('statut');

		if ($statut === 0) {
			$this->form_validation->set_rules('lieu', 'lieu', 'required');
		} else {
			$this->form_validation->set_rules('siret', 'siret', 'required');
		}

		if ($this->form_validation->run()) {
			$indivision		= (bool) ($this->input->post('porte-fort') == 'on');
			$civilite		= (int) $this->input->post('civilite');
			$nom			= (string) $this->input->post('nom');
			$nomUsuel		= (string) $this->input->post('nom-usuel');
			$prenom			= (string) $this->input->post('prenom');
			$autresPrenoms	= (string) $this->input->post('autres-prenoms');
			$representants	= (string) $this->input->post('representants');
			$lieuNaissance	= (string) $this->input->post('lieu');
			$dateNaissance	= (string) $this->input->post('date');
			$siret			= (string) $this->input->post('siret');
			$nationalite	= (string) $this->input->post('nationalite');

			if (empty($nomUsuel)) {
				$nomUsuel = NULL;
			}
			if (empty($autresPrenoms)) {
				$autresPrenoms = NULL;
			}
			if (empty($representants)) {
				$representants = NULL;
			}
			if (empty($lieuNaissance)) {
				$lieuNaissance = NULL;
			}
			if (empty($siret)) {
				$siret = NULL;
			}

			$this->controleur->profilModifierInfosPerso($login, $prenom, $autresPrenoms, $nom, $nomUsuel, $representants, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $indivision);
		} else {
			$this->controleur->profilModifierInfosPerso($login);
		}
	}
	
	/**
	 * Modifie le mot de passe d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function modifierMdp($login) {
		if ($login === NULL) {
			redirect(base_url());
		}

		$this->form_validation->set_rules('oldmdp', 'oldmdp', 'required');
		$this->form_validation->set_rules('newmdp1', 'newmdp1', 'required');
		$this->form_validation->set_rules('newmdp2', 'newmdp2', 'required');

		if ($this->form_validation->run()) {
			$ancienMdp			= (string) $this->input->post('oldmdp');
			$nouveauMdp			= (string) $this->input->post('newmdp1');
			$confirmationMdp	= (string) $this->input->post('newmdp2');

			if ($nouveauMdp === $confirmationMdp) {
				$this->controleur->profilModifierMdp($login, $ancienMdp, $nouveauMdp);
			} else {
				$this->controleur->profilModifierMdp($login);
			}
		} else {
			$this->controleur->profilModifierMdp($login);
		}
	}
	
	/**
	 * Affiche / modifie les informations de contact d'un sociétaire
	 *
	 * @param  string $login Le login du sociétaire
	 * @return void
	 */
	public function contact(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}

		$this->form_validation->set_rules('adresse', 'adresse', 'required');
		$this->form_validation->set_rules('code', 'code', 'required');
		$this->form_validation->set_rules('ville', 'ville', 'required');
		$this->form_validation->set_rules('pays', 'pays', 'required');

		if ($this->form_validation->run()) {
			$adresse		= (string) $this->input->post('adresse');
			$codePostal		= (string) $this->input->post('code');
			$ville			= (string) $this->input->post('ville');
			$pays			= (string) $this->input->post('pays');
			$email			= (string) $this->input->post('email');
			$telFixe		= (string) $this->input->post('fixe');
			$telPortable	= (string) $this->input->post('portable');

			$pasEmail		= (bool) ($this->input->post('autoriser-email')		!== 'on');
			$pasFixe		= (bool) ($this->input->post('autoriser-fixe')		!== 'on');
			$pasPortable	= (bool) ($this->input->post('autoriser-portable')	!== 'on');
			$pasSms			= (bool) ($this->input->post('autoriser-sms')		!== 'on');
			$cloud			= (bool) ($this->input->post('autoriser-cloud')		=== 'on');

			if (empty($email)) {
				$email = NULL;
			}
			if (empty($telFixe)) {
				$telFixe = NULL;
			}
			if (empty($telPortable)) {
				$telPortable = NULL;
			}

			$this->controleur->profilModifierInfosContact($login, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $pasFixe, $pasPortable, $pasSms, $pasEmail, $cloud);
		} else {
			$this->controleur->profilModifierInfosContact($login);
		}
	}
	
	/**
	 * Affiche / modifie les informations utiles à la SCI d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function sci(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}

		$this->form_validation->set_rules('regime', 'regime', 'required');
		$this->form_validation->set_rules('entree', 'entree', 'required');

		if ($this->form_validation->run()) {
			$regime		= (int) $this->input->post('regime');
			$agEntree	= (int) $this->input->post('entree');

			$this->controleur->profilModifierInfosSci($login, $regime, $agEntree);
		} else {
			$this->controleur->profilModifierInfosSci($login);
		}
	}
	
	/**
	 * Affiche les mouvements liés à un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function mouvements(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}

		$this->controleur->profilMouvements($login);
	}
	
	/**
	 * Envoie un mail pour signaler les erreurs sur le profil Information d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function signalerErreurInformations(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}
		
		$this->form_validation->set_rules('message', 'message', 'required');

		$champs = [];
		if ($this->form_validation->run()) {
			for($i = 1; $i <= 16; $i++) {
				$champ = (string) $this->input->post('champ'.$i);
				if ($champ !== '') {
					$champs[] = array($champ,$i);
				}
			}

			$message = (string) $this->input->post('message');

			$this->controleur->profilSignalerErreurInformations($login, $champs, $message);
		} else {
			$this->controleur->profilSignalerErreurInformations($login);
		}
	}

	/**
	 * Envoie un mail pour signaler les erreurs sur le profil SCI d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function signalerErreurSci(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}
		
		$this->form_validation->set_rules('message', 'message', 'required');

		$champs = [];
		if ($this->form_validation->run()) {
			for($i = 17; $i <= 18; $i++) {
				$champ = (string) $this->input->post('champ'.$i);
				if ($champ !== '') {
					$champs[] = array($champ,$i);
				}
			}

			$message = (string) $this->input->post('message');

			$this->controleur->profilSignalerErreurSci($login, $champs, $message);
		} else {
			$this->controleur->profilSignalerErreurSci($login);
		}
	}

		/**
	 * Envoie un mail pour signaler les erreurs sur les mouvements d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function signalerErreurMvt(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}
		
		$this->form_validation->set_rules('message', 'message', 'required');

		$champs = ["mouvements"];
		if ($this->form_validation->run()) {

			$message = (string) $this->input->post('message');

			$this->controleur->profilSignalerErreurMvt($login, $champs, $message);
		} else {
			$this->controleur->profilSignalerErreurMvt($login);
		}
	}
	
	/**
	 * Envoie un mail à la cogérance
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function mailCogerance(string $login = NULL) {
		if ($login === NULL) {
			redirect(base_url());
		}
		
		$this->form_validation->set_rules('message', 'message', 'required');

		$champs = [];
		if ($this->form_validation->run()) {

			$message = (string) $this->input->post('message');

			$this->controleur->mailCogerance($login, $message);
		} else {
			$this->controleur->mailCogerance($login);
		}
	}
}

?>