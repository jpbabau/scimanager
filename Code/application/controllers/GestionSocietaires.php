<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de la page gestion des sociétaires
 */
class GestionSocietaires extends CI_Controller {
	
	/**
	 * controleur
	 *
	 * @var mixed
	 */
	protected $controleur;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		if (!$this->session->has_userdata('login') || $this->session->statut !== 'Admin') {
			redirect(base_url());
		}

		$this->controleur = new Controleur();
	}
	
	/**
	 * Affiche la liste des sociétaires activés
	 *
	 * @return void
	 */
	public function index() {
		$this->controleur->gestionSocietaires();
	}
	
	/**
	 * Affiche la liste des anciens sociétaires
	 *
	 * @return void
	 */
	public function anciens() {
		$this->controleur->gestionSocietairesAnciens();
	}
	
	/**
	 * Désactive un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function activation(string $login) {
		$this->controleur->gestionSocietairesActivation($login);
	}

	/**
	 * activation d'une liste de sociétaires
	 *
	 * @param  array $logins les login des sociétaires
	 * @return void
	 */
	public function activationListe(array $logins=NULL) {
		$postData = $this->input->post();
		$logins = $postData['logins'];
		$this->controleur->gestionSocietairesListeActivation($logins);
   		$result = array("Status"=>"true");
		$result['token'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	/**
	 * Fait quitter un sociétaire de la SCI / GFA
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function quitter(string $login) {
		$this->form_validation->set_rules('dateSortie', 'dateSortie', 'required');

		if ($this->form_validation->run()) {
			$dateSortie = (string) $this->input->post('dateSortie');

			$this->controleur->gestionSocietairesQuitter($login, $dateSortie);
		} else {
			$this->controleur->gestionSocietairesQuitter($login);
		}
	}

	/**
	 * Supprime un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function supprimer(string $login) {
		$this->controleur->gestionSocietairesSupprimer($login);
	}

	/**
	 * supprimer une liste de sociétaires
	 *
	 * @param  array $logins les login des sociétaires
	 * @return void
	 */
	public function supprimerListe(array $logins=NULL) {
		$postData = $this->input->post();
		$logins = $postData['logins'];
		$this->controleur->gestionSocietairesListeSupprimer($logins);
   		$result = array("Status"=>"true");
		$result['token'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	/**
	 * Supprime un ancien sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function supprimerAncien( string $login) {
		$this->controleur->gestionSocietairesSupprimerAncien($login);
	}
	
	/**
	 * Supprime les données RGPD d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function supprimerRGPD(string $login) {
		$this->controleur->gestionSocietairesSupprimerRGPD($login);
	}

	/**
	 * supprime les données d'une liste de sociétaires
	 *
	 * @param  array $logins les login des sociétaires
	 * @return void
	 */
	public function effacerListe(array $logins=NULL) {
		$postData = $this->input->post();
		$logins = $postData['logins'];
		$this->controleur->gestionSocietairesListeEffacer($logins);
   		$result = array("Status"=>"true");
		$result['token'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}

	/**
	 * Ajoute un sociétaire
	 *
	 * @return void
	 */
	public function ajouter() {
		$this->form_validation->set_rules('statut', 'statut', 'required');
		$this->form_validation->set_rules('civilite', 'civilite', 'required');
		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('nationalite', 'nationalite', 'required');
		$this->form_validation->set_rules('adresse', 'adresse', 'required');
		$this->form_validation->set_rules('code', 'code', 'required');
		$this->form_validation->set_rules('ville', 'ville', 'required');
		$this->form_validation->set_rules('pays', 'pays', 'required');
		$this->form_validation->set_rules('entree', 'entree', 'required');

		$statut = (int) $this->input->post('statut');

		if ($statut === 0) {
			$this->form_validation->set_rules('lieu', 'lieu', 'required');
			$this->form_validation->set_rules('matrimonial', 'matrimonial', 'required');
		} else {
			$this->form_validation->set_rules('siret', 'siret', 'required');
			$this->form_validation->set_rules('fiscal', 'fiscal', 'required');
		}

		if ($this->form_validation->run()) {
			$indivision		= (bool) ($this->input->post('porte-fort') == 'on');
			$civilite		= (int) $this->input->post('civilite');
			$nom			= (string) $this->input->post('nom');
			$nomUsuel		= (string) $this->input->post('nom-usuel');
			$prenom			= (string) $this->input->post('prenom');
			$autresPrenoms	= (string) $this->input->post('autres-prenoms');
			$representants	= (string) $this->input->post('representants');
			$lieuNaissance	= (string) $this->input->post('lieu');
			$dateNaissance	= (string) $this->input->post('date');
			$siret			= (string) $this->input->post('siret');
			$nationalite	= (string) $this->input->post('nationalite');
			$adresse		= (string) $this->input->post('adresse');
			$codePostal		= (string) $this->input->post('code');
			$ville			= (string) $this->input->post('ville');
			$pays			= (string) $this->input->post('pays');
			$email			= (string) $this->input->post('email');
			$telFixe		= (string) $this->input->post('fixe');
			$telPortable	= (string) $this->input->post('portable');
			$entree			= (int) $this->input->post('entree');

			if ($statut === 0) {
				$regime = (int) $this->input->post('matrimonial');
			} else {
				$regime = (int) $this->input->post('fiscal');
			}

			if (empty($nomUsuel)) {
				$nomUsuel = NULL;
			}
			if (empty($autresPrenoms)) {
				$autresPrenoms = NULL;
			}
			if (empty($representants)) {
				$representants = NULL;
			}
			if (empty($lieuNaissance)) {
				$lieuNaissance = NULL;
			}
			if (empty($siret)) {
				$siret = NULL;
			}
			if (empty($email)) {
				$email = NULL;
			}
			if (empty($telFixe)) {
				$telFixe = NULL;
			}
			if (empty($telPortable)) {
				$telPortable = NULL;
			}

			$this->controleur->gestionSocietairesAjouter($prenom, $autresPrenoms, $nom, $nomUsuel, $representants, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $regime, $entree, $indivision);
		} else {
			$this->controleur->gestionSocietairesAjouter();
		}
	}
	
	/**
	 * Exporte les données de tous les sociétaires
	 *
	 * @return void
	 */
	public function exporter() {
		$this->controleur->gestionSocietairesExporter();
	}
	
	/**
	 * Importe les données de sociétaires via un fichier
	 *
	 * @return void
	 */
	public function importer() {
		$fichier = (string) $_FILES['import']['name'];
		$this->controleur->gestionSocietairesImporter($fichier);
	}
}