<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de la page de Mailing
 */
class Mail extends CI_Controller {
	
	/**
	 * Contrôleur principal
	 *
	 * @access protected
	 * @var Controleur
	 */
	protected $controleur;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		if (!$this->session->has_userdata('login') || $this->session->statut !== 'Admin') {
			redirect(base_url());
		}

		$this->controleur = new Controleur();
	}
	
	/**
	 * Envoie un mail à plusieurs destinataires
	 *
	 * @return void
	 */
	public function envoyer() {
		$this->form_validation->set_rules('destinataire', 'destinataire', 'required');
		$this->form_validation->set_rules('objet', 'objet', 'required');
		$this->form_validation->set_rules('message', 'message', 'required');

		if ($this->form_validation->run()) {
			$destinataire	= (int) $this->input->post('destinataire');
			$type	= (int) $this->input->post('type');
			$objet			= (string) $this->input->post('objet');
			$message		= (string) nl2br($this->input->post('message'));
			$attach 		= $this->input->post('attach');
			$file_name = $this->upload_file();
			$emails = NULL;
			if ($destinataire === 2) {
				$email = (string) $this->input->post('email');
				$emails = explode(' ', $email);
			}

			$this->controleur->mailEnvoyer($destinataire, $type, $emails, $objet, $message, $file_name);
		} else {
			$this->controleur->mailEnvoyer();
		}
	}

	function upload_file() {
  		$configUpload['upload_path'] = './uploads/';
  		$configUpload['allowed_types'] = 'pdf';
//		$configUpload['max_size'] = '100';
  		$this->load->library('upload', $configUpload);
  		if($this->upload->do_upload('attach')) {
			  print_r($this->upload->data("file_name"));
			return $this->upload->data("file_name");
  		} else {
			return 3;
  		}
 	}
}

?>
