<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de la page Accueil
 */
class Accueil extends CI_Controller {
	
	/**
	 * Le controleur principal de l'application
	 *
	 * @access protected
	 * @var Controleur
	 */
	protected $controleur;
	
	/**
	 * Le constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->controleur = new Controleur();
	}
	
	/**
	 * Vérifie si les cookies sont activés
	 *
	 * @return void
	 */
	public function index() {
		if ($this->session->statut === 'Admin') {
			redirect(base_url() . 'index.php/parametres');
		} elseif ($this->session->statut === 'Sociétaire') {
			redirect(base_url() . 'index.php/profil/informations/' . $this->session->login);
		}

		setcookie('testcookie', 'hello');
		redirect(base_url() . 'index.php/accueil/accueil');
	}
	
	/**
	 * Affiche la page d'accueil
	 *
	 * @return void
	 */
	public function accueil() {
		if ($this->session->statut === 'Admin') {
			redirect(base_url() . 'index.php/parametres');
		} elseif ($this->session->statut === 'Sociétaire') {
			redirect(base_url() . 'index.php/profil/informations/' . $this->session->login);
		}

		$this->controleur->accueil();
	}
	
	/**
	 * Connecte un utilisateur
	 *
	 * @return void
	 */
	public function connexion() {
		if ($this->session->statut === 'Admin') {
			redirect(base_url() . 'index.php/parametres');
		} elseif ($this->session->statut === 'Sociétaire') {
			redirect(base_url() . 'index.php/profil/informations/' . $this->session->login);
		}

		$this->form_validation->set_rules('login', 'login', 'required');
		$this->form_validation->set_rules('mdp', 'mdp', 'required');

		if ($this->form_validation->run()) {
			$login	= (string) $this->input->post('login');
			$mdp	= (string) $this->input->post('mdp');

			$this->controleur->accueilConnexion($login, $mdp);
		} else {
			$this->controleur->accueilConnexion();
		}
	}
	
	/**
	 * Déconnecte un utilisateur
	 *
	 * @return void
	 */
	public function deconnexion() {
		$this->controleur->accueilDeconnexion();
	}
	
	/**
	 * Affiche la page de session expirée
	 *
	 * @return void
	 */
	public function session() {
		$this->controleur->accueilSession();
	}
	
	/**
	 * Modifie le mot de passe d'un utilisateur, lors de sa première connexion
	 *
	 * @return void
	 */
	public function modifierMdp() {
		if ($this->session->statut === 'Admin') {
			redirect(base_url() . 'index.php/parametres');
		} elseif ($this->session->statut === 'Sociétaire') {
			redirect(base_url() . 'index.php/profil/informations/' . $this->session->login);
		}

		$this->form_validation->set_rules('oldmdp', 'oldmdp', 'required');
		$this->form_validation->set_rules('newmdp1', 'newmdp1', 'required');
		$this->form_validation->set_rules('newmdp2', 'newmdp2', 'required');

		if ($this->form_validation->run()) {
			$ancienMdp			= (string) $this->input->post('oldmdp');
			$nouveauMdp			= (string) $this->input->post('newmdp1');
			$confirmationMdp	= (string) $this->input->post('newmdp2');

			$login = (string) $this->session->login;

			if ($nouveauMdp === $confirmationMdp) {
				$this->controleur->accueilModifierMdp($login, $ancienMdp, $nouveauMdp);
			} else {
				$this->controleur->accueilModifierMdp();
			}
		} else {
			$this->controleur->accueilModifierMdp();
		}
	}
	
	/**
	 * Envoie un mail pour reinitialiser le mot de passe d'un utilisateur
	 *
	 * @return void
	 */
	public function reinitialiserMdp() {
		if ($this->session->statut === 'Admin') {
			redirect(base_url() . 'index.php/parametres');
		} elseif ($this->session->statut === 'Sociétaire') {
			redirect(base_url() . 'index.php/profil/informations/' . $this->session->login);
		}

		$this->form_validation->set_rules('login', 'login', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');

		if ($this->form_validation->run()) {
			$login = (string) $this->input->post('login');
			$email = (string) $this->input->post('email');

			$this->controleur->accueilReinitialiserMdp($login, $email);
		} else {
			$this->controleur->accueilReinitialiserMdp();
		}
	}
	
	/**
	 * Saisie d'un nouveau mot de passe, lorsque l'utilisateur a oublié le sien
	 *
	 * @param  string $token le token lié à la demande de réinitialisation de mot de passe
	 * @return void
	 */
	public function nouveauMdp($token = NULL) {
		if ($this->session->statut === 'Admin') {
			redirect(base_url() . 'index.php/parametres');
		} elseif ($this->session->statut === 'Sociétaire') {
			redirect(base_url() . 'index.php/profil/informations/' . $this->session->login);
		}
		
		$this->form_validation->set_rules('mdp1', 'mdp1', 'required');
		$this->form_validation->set_rules('mdp2', 'mdp2', 'required');

		if ($this->form_validation->run()) {
			$nouveauMdp			= (string) $this->input->post('mdp1');
			$confirmationMdp	= (string) $this->input->post('mdp2');

			if ($nouveauMdp === $confirmationMdp) {
				$this->controleur->accueilNouveauMdp($token, $nouveauMdp);
			} else {
				$this->controleur->accueilNouveauMdp($token);
			}
		} else {
			$this->controleur->accueilNouveauMdp($token);
		}
	}
}

?>