<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de la page gestion des indivisions
 */
class GestionIndivisions extends CI_Controller {
	
	/**
	 * controleur
	 *
	 * @var mixed
	 */
	protected $controleur;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		if (!$this->session->has_userdata('login') || $this->session->statut !== 'Admin') {
			redirect(base_url());
		}

		$this->controleur = new Controleur();
	}
	
	/**
	 * Affiche la liste des sociétaires porte-fort
	 *
	 * @return void
	 */
	public function index() {
		$this->controleur->gestionIndivisions();
	}
	
	/**
	 * Affiche la liste des sociétaires représentés par un porte-fort
	 *
	 * @param  string $porteFort le login du porte-fort
	 * @return void
	 */
	public function indivision(string $porteFort = NULL) {
		if ($porteFort === NULL) {
			redirect(base_url());
		}

		$this->controleur->gestionIndivisionsAfficherIndivision($porteFort);
	}
	
	/**
	 * Ajoute un sociétaire représenté
	 *
	 * @param  string 	$porteFort le login du porte-fort
	 * @return void
	 */
	public function ajouterSocietaire(string $porteFort = NULL) {
		if ($porteFort === NULL) {
			redirect(base_url());
		}

		$this->form_validation->set_rules('indivision', 'indivision', 'required');
		$this->form_validation->set_rules('civilite', 'civilite', 'required');
		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('lieu', 'lieu', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('adresse', 'adresse', 'required');
		$this->form_validation->set_rules('code', 'code', 'required');
		$this->form_validation->set_rules('ville', 'ville', 'required');
		$this->form_validation->set_rules('pays', 'pays', 'required');

		if ($this->form_validation->run()) {
			$indivision		= (int) $this->input->post('indivision');
			$civilite		= (int) $this->input->post('civilite');
			$nom			= (string) $this->input->post('nom');
			$prenom			= (string) $this->input->post('prenom');
			$lieuNaissance	= (string) $this->input->post('lieu');
			$dateNaissance	= (string) $this->input->post('date');
			$adresse		= (string) $this->input->post('adresse');
			$codePostal		= (int) $this->input->post('code');
			$ville			= (string) $this->input->post('ville');
			$pays			= (string) $this->input->post('pays');

			$this->controleur->gestionIndivisionsAjouterSocietaire($porteFort, $indivision, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
		} else {
			$this->controleur->gestionIndivisionsAjouterSocietaire($porteFort);
		}
	}

	/**
	 * Modifie un sociétaire représenté
	 *
	 * @param  string 	$porteFort le login du porte-fort
	 * @return void
	 */
	public function modifierSocietaire(string $porteFort = NULL, int $id = NULL) {
		if ($porteFort === NULL || $id === NULL) {
			redirect(base_url());
		}

		$this->form_validation->set_rules('civilite', 'civilite', 'required');
		$this->form_validation->set_rules('nom', 'nom', 'required');
		$this->form_validation->set_rules('prenom', 'prenom', 'required');
		$this->form_validation->set_rules('lieu', 'lieu', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('adresse', 'adresse', 'required');
		$this->form_validation->set_rules('code', 'code', 'required');
		$this->form_validation->set_rules('ville', 'ville', 'required');
		$this->form_validation->set_rules('pays', 'pays', 'required');

		if ($this->form_validation->run()) {
			$civilite		= (int) $this->input->post('civilite');
			$nom			= (string) $this->input->post('nom');
			$prenom			= (string) $this->input->post('prenom');
			$lieuNaissance	= (string) $this->input->post('lieu');
			$dateNaissance	= (string) $this->input->post('date');
			$adresse		= (string) $this->input->post('adresse');
			$codePostal		= (int) $this->input->post('code');
			$ville			= (string) $this->input->post('ville');
			$pays			= (string) $this->input->post('pays');

			$this->controleur->gestionIndivisionsModifierSocietaire($porteFort, $id, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
		} else {
			$this->controleur->gestionIndivisionsModifierSocietaire($porteFort, $id);
		}
	}

	/**
	 * Supprime un sociétaire représenté
	 *
	 * @param  string $porteFort le login du porte-fort
	 * @param  int $id l'id du sociétaire représenté
	 * @return void
	 */
	public function supprimerSocietaire(string $porteFort = NULL, int $id = NULL) {
		if ($porteFort === NULL) {
			redirect(base_url());
		}

		$this->controleur->gestionIndivisionsSupprimerSocietaire($porteFort, $id);
	}
}