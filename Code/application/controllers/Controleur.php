<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'models/ApiSciManager.php';
require_once APPPATH . 'libraries/installation.php';

/**
 * Contrôleur principal de l'application
 */
class Controleur {
	
	/**
	 * CodeIgniter super-object
	 *
	 * @access protected
	 * @var object
	 */
	protected $CI;

	protected $installation;
		
	/**
	 * L'API SciManager
	 *
	 * @access protected
	 * @var ApiSciManager
	 */
	protected $api;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		$this->CI = &get_instance();

		$this->installation = new Installation();
		if (!$this->installation->testerConnexionBD()) {
			redirect(base_url() . 'index.php/initialisation/modifierBDD');
		} elseif (!$this->installation->testerVideBD()) {
			redirect(base_url() . 'index.php/initialisation/modifierBDD');
		} else {
			$this->api = new ApiSciManager();
		}
	}
	
	/**
	 * Détruit le cookie de session
	 *
	 * @access private
	 * @return void
	 */
	private function _sessionDetruire() {
		if (isset($_COOKIE['ci_session'])) {
			setcookie('ci_session', '', time() - 42000, '/');
		}

		$_SESSION = array();
		session_unset();
		$this->CI->session->sess_destroy();
	}
	
	/**
	 * Regarde si l'utilisateur a été inactif pendant plus de 30 min, le déconnecte et le redirige vers la page de session expirée si il l'a été
	 *
	 * @return void
	 */
	private function _sessionExpire() {
		if (isset($_SESSION['temps']) && time() - $_SESSION['temps'] < 1800) {
			$_SESSION['temps'] = time();
		} else {
			$this->_sessionDetruire();
			redirect(base_url() . 'index.php/accueil/session');
		}
	}
	
	/**
	 * Affiche la page d'accueil
	 *
	 * @return void
	 */
	public function accueil() {
		$data['siteInfos'] = $this->api->siteObtenirInformations();

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/accueil', $data);
		$this->CI->load->view('pages/accueil', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Connecte un utilisateur en créant un cookie de session et le redirige vers sa page par défaut
	 *
	 * @param  string $login le login de l'utilisateur
	 * @param  string $mdp le mot de passe de l'utilisateur
	 * @return void
	 */
	public function accueilConnexion(string $login = NULL, string $mdp = NULL) {
		$data['siteInfos'] = $this->api->siteObtenirInformations();

		if ($login !== NULL && $mdp !== NULL) {
			$result = $this->api->connexion($login, $mdp);

			switch ($result) {
				case 'AA':
					$userdata = array('login' => $login, 'statut' => 'Admin', 'temps' => time());
					$this->CI->session->set_userdata($userdata);
					redirect(base_url() . 'index.php/parametres');
					break;
				case 'AB':
					$userdata = array('login' => $login, 'statut' => 'Admin Bloqué', 'temps' => time());
					$this->CI->session->set_userdata($userdata);
					redirect(base_url() . 'index.php/accueil/modifierMdp');
					break;
				case 'SA':
					$userdata = array('login' => $login, 'statut' => 'Sociétaire', 'temps' => time());
					$this->CI->session->set_userdata($userdata);
					redirect(base_url() . 'index.php/profil/informations/' . $login);
					break;
				case 'SB':
					$userdata = array('login' => $login, 'statut' => 'Sociétaire Bloqué', 'temps' => time());
					$this->CI->session->set_userdata($userdata);
					redirect(base_url() . 'index.php/accueil/modifierMdp');
					break;
				default:
					$data['result'] = $result;
					$this->CI->load->view('templates/haut', $data);
					$this->CI->load->view('bandeaux/home', $data);
					$this->CI->load->view('pages/connexion');
					$this->CI->load->view('templates/bas');
					break;
			}
		} else {
			$this->CI->load->view('templates/haut', $data);
			$this->CI->load->view('bandeaux/home', $data);
			$this->CI->load->view('pages/connexion');
			$this->CI->load->view('templates/bas');
		}
	}
	
	/**
	 * Déconnecte un utilisateur
	 *
	 * @return void
	 */
	public function accueilDeconnexion() {
		$this->_sessionDetruire();
		redirect(base_url());
	}
	
	/**
	 * Affiche la page de la session expirée
	 *
	 * @return void
	 */
	public function accueilSession() {
		$data['siteInfos'] = $this->api->siteObtenirInformations();

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/home', $data);
		$this->CI->load->view('pages/session');
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie le mot de passe de l'utilisateur, lors de sa première connexion
	 *
	 * @param  string $login le login de l'utilisateur
	 * @param  string $ancienMdp l'ancien mot de passe de l'utilisateur
	 * @param  string $nouveauMdp le nouveau mot de passe de l'utilisateur
	 * @return void
	 */
	public function accueilModifierMdp(string $login = NULL, string $ancienMdp = NULL, string $nouveauMdp = NULL) {
		$data['siteInfos'] = $this->api->siteObtenirInformations();

		if ($login !== NULL && $ancienMdp !== NULL && $nouveauMdp !== NULL) {
			if ($this->CI->session->statut === 'Admin Bloqué') {
				$result = $this->api->adminModifierMdp($ancienMdp, $nouveauMdp);
			} elseif ($this->CI->session->statut === 'Sociétaire Bloqué') {
				$result = $this->api->societaireModifierMdp($login, $ancienMdp, $nouveauMdp);
			}

			if ($result === 0) {
				$this->_sessionDetruire();
				$this->CI->load->view('templates/haut', $data);
				$this->CI->load->view('bandeaux/home', $data);
				$this->CI->load->view('pages/modifierMdp_reussi');
				$this->CI->load->view('templates/bas');
			} else {
				$data['result'] = $result;
				$this->CI->load->view('templates/haut', $data);
				$this->CI->load->view('bandeaux/home', $data);
				$this->CI->load->view('pages/modifierMdp', $data);
				$this->CI->load->view('templates/bas');
			}
		} else {
			$this->CI->load->view('templates/haut', $data);
			$this->CI->load->view('bandeaux/home', $data);
			$this->CI->load->view('pages/modifierMdp');
			$this->CI->load->view('templates/bas');
		}
	}
	
	/**
	 * Envoie un mail à l'utilisateur pour réinitialiser son mot de passe
	 *
	 * @param  string $login le login de l'utilisateur
	 * @param  string $email l'email de l'utilisateur
	 * @return void
	 */
	public function accueilReinitialiserMdp(string $login = NULL, string $email = NULL) {
		$data['siteInfos']	= $this->api->siteObtenirInformations();

		if ($login !== NULL && $email !== NULL) {
			$result = $this->api->reinitialiserMdp($login, $email);

			if ($result === 0) {
				$this->CI->load->view('templates/haut', $data);
				$this->CI->load->view('bandeaux/home', $data);
				$this->CI->load->view('pages/reinitialiserMdp_reussi');
				$this->CI->load->view('templates/bas');
			} else {
				$data['result'] = $result;
				$this->CI->load->view('templates/haut', $data);
				$this->CI->load->view('bandeaux/home', $data);
				$this->CI->load->view('pages/reinitialiserMdp', $data);
				$this->CI->load->view('templates/bas');
			}
		} else {
			$this->CI->load->view('templates/haut', $data);
			$this->CI->load->view('bandeaux/home', $data);
			$this->CI->load->view('pages/reinitialiserMdp');
			$this->CI->load->view('templates/bas');
		}
	}
	
	/**
	 * Crée un nouveau mot de passe pour l'utilisateur ayant demandé de réinitialiser son mot de passe
	 *
	 * @param  string $token le token correspondant à la demande de réinitialisation de mot de passe
	 * @param  string $nouveauMdp le nouveau mot de passe de l'utilisateur
	 * @return void
	 */
	public function accueilNouveauMdp(string $token = NULL, string $nouveauMdp = NULL) {
		$data['siteInfos'] = $this->api->siteObtenirInformations();

		if ($token === NULL) {
			redirect(base_url());
		}

		$data['token'] = $token;

		if ($nouveauMdp !== NULL) {
			$result = $this->api->modifierMdpOublie($token, $nouveauMdp);

			if ($result === 0) {
				$this->CI->load->view('templates/haut', $data);
				$this->CI->load->view('bandeaux/home', $data);
				$this->CI->load->view('pages/nouveauMdp_reussi');
				$this->CI->load->view('templates/bas');
			} else {
				$data['result'] = $result;
				$this->CI->load->view('templates/haut', $data);
				$this->CI->load->view('bandeaux/home', $data);
				$this->CI->load->view('pages/nouveauMdp', $data);
				$this->CI->load->view('templates/bas');
			}
		} else {
			$this->CI->load->view('templates/haut', $data);
			$this->CI->load->view('bandeaux/home', $data);
			$this->CI->load->view('pages/nouveauMdp', $data);
			$this->CI->load->view('templates/bas');
		}
	}
	
	/**
	 * Affiche la page paramètres
	 *
	 * @return void
	 */
	public function parametres() {
		$this->_sessionExpire();

		$data['siteInfos']	= $this->api->siteObtenirInformations();
		$data['adminInfos']	= $this->api->adminObtenirInformations();
		$data['menuOnglet'] = 'parametres';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametres', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie les informations du site et recharge la page
	 *
	 * @param  int		$structure le type de structure
	 * @param  string	$nom le nouveau nom du site
	 * @param  string	$image la nouvelle image du site
	 * @param  string	$icone la nouvelle icone du site
	 * @return void
	 */
	public function parametresModifierSiteInfos($structure = NULL, $nom = NULL, $image = NULL, $icone = NULL) {
		$this->_sessionExpire();

		if ($nom !== NULL) {
			$this->api->siteModifierStructure($structure);
			$nomResult		= $this->api->siteModifierNom($nom);

			$imageResult = 0;
			if (!empty($image)) {
				$imageResult = $this->api->siteModifierImage('image.jpg');
				if ($imageResult === 0) {
					$config['file_name']		= 'image.jpg';
					$config['upload_path']		= './style/core/img/';
					$config['overwrite']		= TRUE;
					$config['allowed_types']	= 'jpg|jpeg|JPG|JPEG';
					$config['max_size']			= '1000000';
					$this->CI->load->library('upload', $config);
					$this->CI->upload->do_upload('img');
					unset($this->CI->upload);
				}
			}

			$iconeResult = 0;
			if (!empty($icone)) {
				$iconeResult = $this->api->siteModifierIcone('icone.jpg');

				if ($iconeResult === 0) {
					$config = array();
					$config['file_name']		= 'icone.jpg';
					$config['upload_path']		= './style/core/img/';
					$config['overwrite']		= TRUE;
					$config['allowed_types']	= 'jpg|jpeg|JPG|JPEG';
					$config['max_size']			= '1000000';
					$this->CI->load->library('upload', $config);
					$this->CI->upload->do_upload('ico');

					$imageData = $this->CI->upload->data();
					$config = array();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $imageData['full_path'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 64;
					$this->CI->load->library('image_lib', $config);
					$this->CI->image_lib->resize();
					unset($this->CI->upload);
					unset($this->CI->image_lib);
				}
			}

			$data['siteNomResult']		= $nomResult;
			$data['siteImageResult']	= $imageResult;
			$data['siteIconeResult']	= $iconeResult;
		}

		$data['siteInfos']	= $this->api->siteObtenirInformations();
		$data['adminInfos']	= $this->api->adminObtenirInformations();
		$data['menuOnglet'] = 'parametres';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametres', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie / Teste le protocole SMTP
	 *
	 * @param  string $action l'action à réaliser (modifier / tester)
	 * @param  string $host adresse de l'hote
	 * @param  string $port numéro de port
	 * @param  string $email nom d'utilisateur (généralement l'adresse email)
	 * @param  string $password mot de passe (généralement le mot de passe du compte email)
	 * @param  string $crypto méthode de chiffrage (ssl ou tls)
	 * @return void
	 */
	public function parametresModifierSiteEmail(string $action = NULL, string $host = NULL, string $port = NULL, string $email = NULL, string $password = NULL, string $crypto = NULL) {
		$this->_sessionExpire();

		if ($action !== NULL && $host !== NULL && $port !== NULL && $email !== NULL && $password !== NULL && $crypto !== NULL) {
			$this->api->installationModifierEmail($host, $port, $email, $password, $crypto);

			if ($action === 'submit') {
				$data['result'] = 0;
			} else {
				$result = $this->api->installationTesterEmail($email);
				$data['testResult'] = $result;
			}
		}

		$data['siteInfos']	= $this->api->siteObtenirInformations();
		$data['menuOnglet']	= 'parametres';
		
		if (file_exists($file_path = APPPATH.'config/email.php')) {
			include($file_path);
		}
		$data['email'] = $config;

		$this->CI->load->view('templates/haut', $data);		
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametres_email', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie les informations de l'administrateur et recharge la page
	 *
	 * @param  string $login le nouveau login de l'administrateur
	 * @param  string $email le nouvel email de l'administrateur
	 * @return void
	 */
	public function parametresModifierAdminInfos($login = NULL, $email = NULL) {
		$this->_sessionExpire();

		if ($login !== NULL && $email !== NULL) {
			$loginResult = $this->api->adminModifierLogin($login);
			$emailResult = $this->api->adminModifierEmail($email);

			$data['adminLoginResult'] = $loginResult;
			$data['adminEmailResult'] = $emailResult;
		}

		$data['siteInfos']	= $this->api->siteObtenirInformations();
		$data['adminInfos']	= $this->api->adminObtenirInformations();
		$data['menuOnglet'] = 'parametres';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametres', $data);
		$this->CI->load->view('templates/bas');
	}
		
	/**
	 * Modifie le mot de passe de l'administrateur et recharge la page
	 *
	 * @param  string $ancienMdp l'ancien mot de passe de l'administrateur
	 * @param  string $nouveauMdp le nouveau mot de passe de l'administrateur
	 * @return void
	 */
	public function parametresModifierAdminMdp($ancienMdp = NULL, $nouveauMdp = NULL) {
		$this->_sessionExpire();

		if ($ancienMdp !== NULL && $nouveauMdp !== NULL) {
			$result = $this->api->adminModifierMdp($ancienMdp, $nouveauMdp);

			$data['mdpResult'] = $result;
		}

		$data['siteInfos']	= $this->api->siteObtenirInformations();
		$data['adminInfos']	= $this->api->adminObtenirInformations();
		$data['menuOnglet'] = 'parametres';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametres', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Remet les images par defaut du site, supprime celles qui ne sont plus utilisées et recharge la page
	 *
	 * @return void
	 */
	public function parametresImagesParDefaut() {
		$this->_sessionExpire();

		$this->api->siteModifierImage('image_defaut.jpg');
		$this->api->siteModifierIcone('icone_defaut.jpg');

		if (file_exists(FCPATH . 'style/core/img/image.jpg')) {
			unlink(FCPATH . 'style/core/img/image.jpg');
		}

		if (file_exists(FCPATH . 'style/core/img/icone.jpg')) {
			unlink(FCPATH . 'style/core/img/icone.jpg');
		}

		redirect(base_url() . 'index.php/parametres');
	}
	
	/**
	 * Affiche la page des paramètres avancés
	 *
	 * @return void
	 */
	public function parametresAvances() {
		$this->_sessionExpire();

		$data['siteInfos']		= $this->api->siteObtenirInformations();
		$data['listeStatuts']	= $this->api->parametresAvancesObtenirListeStatuts();
		$data['menuOnglet']		= 'parametres';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametresAvances', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Ajoute un statut
	 *
	 * @param  string $statut le nom du statut
	 * @return void
	 */
	public function ajouterStatut(string $statut = NULL) {
		$this->_sessionExpire();

		if ($statut !== NULL) {
			$this->api->ajouterStatut($statut);
		}

		redirect(base_url() . 'index.php/parametres/avances');
	}
	
	/**
	 * Modifie un statut
	 *
	 * @param  int $id l'id du statut à modifier
	 * @param  string $statut le nouveau nom du statut
	 * @return void
	 */
	public function modifierStatut(int $id = NULL, string $statut = NULL) {
		$this->_sessionExpire();

		if ($id !== NULL && $statut !== NULL) {
			$this->api->modifierStatut($id, $statut);
		}

		redirect(base_url() . 'index.php/parametres/avances');
	}
	
	/**
	 * Supprime un statut d'entreprise
	 *
	 * @param  int $id l'id du statut à supprimer
	 * @return void
	 */
	public function supprimerStatut(int $id = NULL) {
		$this->_sessionExpire();

		if ($id !== NULL) {
			$result = $this->api->supprimerStatut($id);
			if ($result === 0) {
				redirect(base_url() . 'index.php/parametres/avances');
			}
			$data['statutResult'] = $result;
		}

		$data['siteInfos']		= $this->api->siteObtenirInformations();
		$data['listeStatuts']	= $this->api->parametresAvancesObtenirListeStatuts();
		$data['menuOnglet']		= 'parametres';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametresAvances', $data);
		$this->CI->load->view('templates/bas');
	}


	/**
	 * Désinstalle la base de données
	 *
	 * @return void
	 */
	public function parametresAvancesDesinstaller() {
		$this->_sessionExpire();

		$this->api->siteDesinstallation();

		$this->accueilDeconnexion();
	}
	
	/**
	 * Affiche la page configuration
	 *
	 * @return void
	 */
	public function configuration() {
		$this->_sessionExpire();

		$data['siteInfos']		= $this->api->siteObtenirInformations();
		$data['listeDatesAG']	= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['menuOnglet']		= 'configuration';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/configuration', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Ajoute une date d'assemblée générale
	 *
	 * @param  string $date la date d'AG
	 * @return void
	 */
	public function configurationAjouterDateAG(string $date = NULL) {
		$this->_sessionExpire();
		
		if ($date !== NULL) {
			$this->api->assembleeGeneraleAjouterDateAG($date);
		}

		redirect(base_url() . 'index.php/configuration');
	}
	
	/**
	 * Modifie une date d'assemblée générale
	 *
	 * @param  int		$id l'id de la date à modifier
	 * @param  string	$date la nouvelle date
	 * @return void
	 */
	public function configurationModifierDateAG(int $id = NULL, string $date = NULL) {
		$this->_sessionExpire();

		if ($id !== NULL && $date !== NULL) {
			$this->api->assembleeGeneraleModifierDateAG($id, $date);
		}

		redirect(base_url() . 'index.php/configuration');
	}
	
	/**
	 * Supprime une date d'assemblée générale
	 *
	 * @param  int $id l'id de la date à supprimer
	 * @return void
	 */
	public function configurationSupprimerDateAG(int $id = NULL) {
		$this->_sessionExpire();

		if ($id !== NULL) {
			$result = $this->api->assembleeGeneraleSupprimerDateAG($id);
			if ($result === 0) {
				redirect(base_url() . 'index.php/configuration');
			}
			$data['dateResult'] = $result;
		}

		$data['siteInfos']		= $this->api->siteObtenirInformations();
		$data['listeDatesAG']	= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['menuOnglet']		= 'configuration';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/configuration', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie la durée RGPD
	 *
	 * @param  int $dureeRGPD la nouvelle durée en mois
	 * @return void
	 */
	public function configurationModifierDureeRGPD(int $dureeRGPD = NULL) {
		$this->_sessionExpire();

		if ($dureeRGPD !== NULL) {
			$result = $this->api->siteModifierDureeRGPD($dureeRGPD);
			if ($result === 0) {
				redirect(base_url() . 'index.php/parametres/avances');
			}
			$data['dureeResult'] = $result;
		}

		$data['siteInfos']		= $this->api->siteObtenirInformations();
		$data['listeDatesAG']	= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['menuOnglet']		= 'configuration';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametresAvances', $data);
		$this->CI->load->view('templates/bas');
	}

	/**
	 * Modifie le type d'installation
	 *
	 * @param  bool $local installation locale
	 * @return void
	 */
	public function configurationInstallationLocal(bool $local= NULL) {
		$this->_sessionExpire();

		if ($local !== NULL) {
			$result = $this->api->siteModifierLocal($local);
		}

		$data['siteInfos']		= $this->api->siteObtenirInformations();
		$data['listeDatesAG']	= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['menuOnglet']		= 'configuration';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/parametres', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Affiche la liste des sociétaires 
	 *
	 * @return void
	 */
	public function gestionSocietaires() {
		$this->_sessionExpire();

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['menuOnglet']			= 'gestionSocietaires';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionSocietaires', $data);
		$this->CI->load->view('templates/bas');
	}

	/**
	 * Affiche la liste des anciens sociétaires
	 *
	 * @return void
	 */
	public function gestionSocietairesAnciens() {
		$this->_sessionExpire();

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['menuOnglet']			= 'AnciensSocietaires';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionSocietaires_anciens', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Ajoute un sociétaire
	 *
	 * @param  string		$prenom prénom du nouveau societaire
	 * @param  string|null	$autrePrenom autres prénoms du nouveau societaire
	 * @param  string		$nom nom du nouveau societaire
	 * @param  string|null	$nomUsuel nom du nouveau societaire
	 * @param  string|null	$representantsLegaux réprésentants légaux
	 * @param  int 			$civilite civilité du nouveau societaire
	 * @param  int 			$statut statut du nouveau societaire
	 * @param  string|null	$siret numéro de siret du nouveau societaire si personne morale
	 * @param  string		$dateNaissance date de naissance du nouveau societaire
	 * @param  string|null	$lieuNaissance lieu de naissance du nouveau societaire
	 * @param  string|null	$email courriel du nouveau societaire
	 * @param  string|null	$telFixe téléphone fixe du nouveau societaire
	 * @param  string|null	$telPortable téléphone portable du nouveau societaire
	 * @param  string		$adresse adresse du nouveau societaire
	 * @param  string		$codePostal code postal du nouveau societaire
	 * @param  string		$ville ville du nouveau societaire
	 * @param  string		$pays pays du nouveau societaire
	 * @param  int 			$regime régime matrimonial ou fiscal du nouveau societaire
	 * @param  int 			$agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
	 * @return void
	 */
	public function gestionSocietairesAjouter(string $prenom = NULL, string $autrePrenom = NULL, string $nom = NULL, string $nomUsuel = NULL, string $representantsLegaux = NULL, int $civilite = NULL, int $statut = NULL, string $siret = NULL, string $dateNaissance = NULL, string $lieuNaissance = NULL, string $nationalite = NULL, string $email = NULL, string $telFixe = NULL, string $telPortable = NULL, string $adresse = NULL, string $codePostal = NULL, string $ville = NULL, string $pays = NULL, int $regime = NULL, int $agEntree = NULL, bool $indivision = NULL) {
		$this->_sessionExpire();

		if ($prenom !== NULL && $nom !== NULL && $civilite !== NULL && $statut !== NULL && $dateNaissance && $nationalite !== NULL && $adresse !== NULL && $codePostal !== NULL && $ville !== NULL &&  $pays !== NULL && $regime !== NULL && $agEntree !== NULL && $indivision !== NULL) {
			$result = $this->api->societaireAjouter($prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret,$dateNaissance, $lieuNaissance, $nationalite, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays, $regime, $agEntree, $indivision);
			$data['result'] = $result;
		}

		$data['siteInfos']					= $this->api->siteObtenirInformations();
		$data['listeStatuts']				= $this->api->parametresAvancesObtenirListeStatuts();
		$data['listeRegimesMatrimoniaux']	= $this->api->parametresAvancesObtenirListeRegimesParType(false);
		$data['listeRegimesFiscaux']		= $this->api->parametresAvancesObtenirListeRegimesParType(true);
		$data['listeDatesAG']				= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['menuOnglet']					= 'gestionSocietaires';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionSocietaires_ajouter', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Désactive un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function gestionSocietairesActivation(string $login) {
		$this->_sessionExpire();

		$result = $this->api->societaireActivation($login);
		$data['result'] = $result;
		
		redirect(base_url() . 'index.php/gestionSocietaires');
	}

	/**
	 * Active une liste de sociétaires
	 *
	 * @param  array $logins le login du sociétaire
	 * @return void
	 */
	public function gestionSocietairesListeActivation(array $logins) {
		$this->_sessionExpire();

		foreach($logins as $login) {
            $this->api->societaireActivation($login);
        }
	}
	
	/**
	 * Fait quitter un sociétaire de la SCI / GFA
	 *
	 * @param  string	$login le login du sociétaire
	 * @param  int		$dateSortie l'id de la date de sortie
	 * @return void
	 */
	public function gestionSocietairesQuitter(string $login = NULL, int $dateSortie = NULL) {
		$this->_sessionExpire();

		if ($dateSortie !== NULL) {
			$this->api->societaireModifierDateSortie($login, $dateSortie);

			redirect(base_url() . 'index.php/gestionSocietaires');
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['listeDatesAG']		= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['menuOnglet']			= 'gestionSocietaires';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionSocietaires_quitter', $data);
		$this->CI->load->view('templates/bas');
	}

	/**
	 * Supprime définitivement un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function gestionSocietairesSupprimer(string $login) {
		$this->_sessionExpire();

		$this->api->societaireSupprimer($login);
		redirect(base_url() . 'index.php/gestionSocietaires/');
	}

	/**
	 * supprimer une liste de sociétaires
	 *
	 * @param  array $logins le login du sociétaire
	 * @return void
	 */
	public function gestionSocietairesListeSupprimer(array $logins) {
		$this->_sessionExpire();

		foreach($logins as $login) {
            $this->api->societaireSupprimer($login);
        }
	}


		/**
	 * Supprime définitivement un ancien sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function gestionSocietairesSupprimerAncien(string $login) {
		$this->_sessionExpire();

		$this->api->societaireSupprimer($login);
		redirect(base_url() . 'index.php/gestionSocietaires/anciens');
	}

	/**
	 * Supprime les informations RGPD d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function gestionSocietairesSupprimerRGPD(string $login) {
		$this->_sessionExpire();

		$this->api->societaireSupprimerRGPD($login);
		redirect(base_url() . 'index.php/gestionSocietaires/anciens');
	}
	
	/**
	 * supprime les informations RGPD d'une liste de sociétaires
	 *
	 * @param  array $logins le login du sociétaire
	 * @return void
	 */
	public function gestionSocietairesListeEffacer(array $logins) {
		$this->_sessionExpire();

		foreach($logins as $login) {
            $this->api->societaireSupprimerRGPD($login);
        }
	}


	/**
	 * Exporte les données de tous les sociétaires
	 *
	 * @return void
	 */
	public function gestionSocietairesExporter() {
		$this->_sessionExpire();

		$this->api->societaireIOExporter();
	}
	
	/**
	 * Importe les données de sociétaires
	 *
	 * @param  mixed $fichier
	 * @return void
	 */
	public function gestionSocietairesImporter(string $fichier = NULL) {
		$this->_sessionExpire();

		if ($fichier !== NULL) {
			$config['file_name']		= 'registreSocietaires';
			$config['upload_path']		= './registres/import/';
			$config['overwrite']		= TRUE;
			$config['allowed_types']	= 'cvs|xlsx';
			$config['max_size']			= '1000000';
			$this->CI->load->library('upload', $config);
			$test = $this->CI->upload->do_upload('import');

			if ($test) {
				$result = $this->api->societaireIOImporter($this->CI->upload->data()['file_name']);
				$data['result'] = $result;
			} else {
				$data['result'] = -1;
			}
			unset($this->CI->upload);
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['menuOnglet']			= 'gestionSocietaires';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionSocietaires', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Affiche la liste des sociétaires porte-forts, représentants d'une indivision
	 *
	 * @return void
	 */
	public function gestionIndivisions() {
		$this->_sessionExpire();

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['menuOnglet']			= 'gestionIndivisions';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionIndivisions', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Affiche la liste des sociétaires représentés par un porte-fort
	 *
	 * @param  string $login le login du porte-fort
	 * @return void
	 */
	public function gestionIndivisionsAfficherIndivision(string $login = NULL) {
		$this->_sessionExpire();

		$data['siteInfos']						= $this->api->siteObtenirInformations();
		$data['societaireInfos']				= $this->api->societaireObtenirInformations($login);
		$data['listeSocietairesRepresentes']	= $this->api->indivisionObtenirListeSocietaireRepresente($login);
		$data['menuOnglet']						= 'gestionIndivisions';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/indivision', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Ajoute un sociétaire représenté
	 *
	 * @param  string 	$porteFort le login du porte-fort
	 * @param  int 		$indivision le numéro de l'indivision
	 * @param  string 	$prenom le prénom du sociétaire représenté
	 * @param  string 	$nom le nom du sociétaire représenté
	 * @param  int 		$civilite la civilité du sociétaire représenté
	 * @param  string 	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string 	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string 	$adresse l'adresse du sociétaire représenté
	 * @param  string	$codePostal le code postal du sociétaire représenté
	 * @param  string 	$ville la ville du sociétaire représenté
	 * @param  string 	$pays le pays du sociétaire représenté
	 * @return void
	 */
	public function gestionIndivisionsAjouterSocietaire(string $porteFort = NULL, int $indivision = NULL, string $prenom = NULL, string $nom = NULL, int $civilite = NULL, string $dateNaissance = NULL, string $lieuNaissance = NULL, string $adresse = NULL, string $codePostal = NULL, string $ville = NULL, string $pays = NULL) {
		$this->_sessionExpire();

		if ($indivision !== NULL && $prenom !== NULL && $nom !== NULL && $civilite !== NULL && $dateNaissance !== NULL && $lieuNaissance !== NULL && $adresse !== NULL && $codePostal !== NULL && $ville !== NULL && $pays !== NULL) {
			$result = $this->api->indivisionAjouterSocietaireRepresente($porteFort, $indivision, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeIndivisions']	= $this->api->indivisionObtenirListeIndivisions($porteFort);
		$data['menuOnglet']			= 'gestionIndivisions';
		$data['login']				= $porteFort;

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/indivision_ajouter', $data);
		$this->CI->load->view('templates/bas');

	}
	
	/**
	 * Modifie un sociétaire représenté
	 *
	 * @param  string 	$porteFort le login du porte-fort
	 * @param  int		$id l'id du sociétaire représenté
	 * @param  int 		$indivision le numéro de l'indivision
	 * @param  string 	$prenom le prénom du sociétaire représenté
	 * @param  string 	$nom le nom du sociétaire représenté
	 * @param  int 		$civilite la civilité du sociétaire représenté
	 * @param  string 	$dateNaissance la date de naissance du sociétaire représenté
	 * @param  string 	$lieuNaissance le lieu de naissance du sociétaire représenté
	 * @param  string 	$adresse l'adresse du sociétaire représenté
	 * @param  string 		$codePostal le code postal du sociétaire représenté
	 * @param  string 	$ville la ville du sociétaire représenté
	 * @param  string 	$pays le pays du sociétaire représenté
	 * @return void
	 */
	public function gestionIndivisionsModifierSocietaire(string $porteFort = NULL, int $id = NULL, string $prenom = NULL, string $nom = NULL, int $civilite = NULL, string $dateNaissance = NULL, string $lieuNaissance = NULL, string $adresse = NULL, string $codePostal = NULL, string $ville = NULL, string $pays = NULL) {
		$this->_sessionExpire();

		if ($prenom !== NULL && $nom !== NULL && $civilite !== NULL && $dateNaissance !== NULL && $lieuNaissance !== NULL && $adresse !== NULL && $codePostal !== NULL && $ville !== NULL && $pays !== NULL) {
			$result = $this->api->indivisionModifierSocietaireRepresente($id, $prenom, $nom, $civilite, $dateNaissance, $lieuNaissance, $adresse, $codePostal, $ville, $pays);
			$data['result'] = $result;
		}

		$data['siteInfos']					= $this->api->siteObtenirInformations();
		$data['societaireRepresenteInfos']	= $this->api->indivisionObtenirInformationsSocietaireRepresente($id);
		$data['menuOnglet']					= 'gestionIndivisions';
		$data['login']						= $porteFort;

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/indivision_modifier', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Supprime un sociétaire représenté
	 *
	 * @param  string $porteFort le login du porte-fort
	 * @param  int $id l'id du sociétaire représenté
	 * @return void
	 */
	public function gestionIndivisionsSupprimerSocietaire(string $porteFort, int $id = NULL) {
		$this->_sessionExpire();

		$this->api->indivisionSupprimerSocietaireRepresente($id);

		redirect(base_url() . 'index.php/gestionIndivisions/indivision/'. $porteFort);
	}
	
	/**
	 * Modifie les informations personnelles d'un sociétaire
	 *
	 * @param  string 		$login login du societaire
	 * @param  string 		$prenom prénom du societaire
	 * @param  string|null	$autrePrenom autres prénoms du societaire
	 * @param  string 		$nom nom du societaire
	 * @param  string|null	$nomUsuel nom du societaire
	 * @param  string|null	$representantsLegaux réprésentants légaux
	 * @param  int 			$civilite civilité du societaire
	 * @param  int 			$statut statut du societaire
	 * @param  string|null	$siret numéro de siret du nouveau sociétaire si personne morale
	 * @param  string 		$dateNaissance date de naissance du societaire
	 * @param  string|null	$lieuNaissance lieu de naissance du societaire
	 * @return void
	 */
	public function profilModifierInfosPerso(string $login = NULL, string $prenom = NULL, string $autrePrenom = NULL, string $nom = NULL, string $nomUsuel = NULL, string $representantsLegaux = NULL, int $civilite = NULL, int $statut = NULL, string $siret = NULL, string $dateNaissance = NULL, string $lieuNaissance = NULL, string $nationalite = NULL, bool $indivision = NULL) {
		$this->_sessionExpire();

		if ($login !== NULL && $prenom !== NULL && $nom !== NULL && $civilite !== NULL && $statut !== NULL && $dateNaissance !== NULL && $nationalite !== NULL && $indivision !== NULL) {
			$result = $this->api->societaireModifierInfosPerso($login, $prenom, $autrePrenom, $nom, $nomUsuel, $representantsLegaux, $civilite, $statut, $siret, $dateNaissance, $lieuNaissance, $nationalite, $indivision);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['listeStatuts']		= $this->api->parametresAvancesObtenirListeStatuts();
		$data['menuOnglet']			= 'informations';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$statut = $this->CI->session->statut;
		if ($statut === 'Admin') {
			$this->CI->load->view('pages/admin_menu_profil', $data);
		} else {
			$this->CI->load->view('pages/societaire_menu', $data);
		}
		$this->CI->load->view('pages/profil_informations', $data);
		$this->CI->load->view('templates/bas');
	}
		
	/**
	 * Modifie le mot de passe d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @param  string $ancienMdp l'ancien mot de passe du sociétaire
	 * @param  string $nouveauMdp le nouveau mot de passe du sociétaire
	 * @return void
	 */
	public function profilModifierMdp(string $login = NULL, string $ancienMdp = NULL, string $nouveauMdp = NULL) {
		$this->_sessionExpire();

		if ($login !== NULL && $ancienMdp !== NULL && $nouveauMdp !== NULL) {
			$result = $this->api->societaireModifierMdp($login, $ancienMdp, $nouveauMdp);
			$data['mdpResult'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['listeStatuts']		= $this->api->parametresAvancesObtenirListeStatuts();
		$data['menuOnglet']			= 'mdp';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$statut = $this->CI->session->statut;
		if ($statut === 'Admin') {
			$this->CI->load->view('pages/admin_menu_profil', $data);
		} else {
			$this->CI->load->view('pages/societaire_menu', $data);
		}
		$this->CI->load->view('pages/profil_mdp', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie les informations de contact, ainsi que les autorisations, d'un sociétaire
	 *
	 * @param  string		$login login du societaire
	 * @param  string|null	$email courriel du societaire
	 * @param  string|null	$telFixe téléphone fixe du societaire
	 * @param  string|null	$telPortable téléphone portable du societaire
	 * @param  string		$adresse adresse du societaire
	 * @param  string		$codePostal code postal du societaire
	 * @param  string		$ville ville du societaire
	 * @param  string		$pays pays du societaire
	 * @param  bool 		$pasFixe ne pas appeler sur le téléphone fixe
	 * @param  bool 		$pasPortable ne pas appeler sur le téléphone portable
	 * @param  bool 		$pasSms ne pas envoyer d'sms
	 * @param  bool 		$pasEmail ne pas envoyer d'email
	 * @param  bool 		$cloud autoriser le stockage dans le cloud
	 * @return void
	 */
	public function profilModifierInfosContact(	string $login = NULL, string $email = NULL, string $telFixe = NULL, string $telPortable = NULL, string $adresse = NULL, string $codePostal = NULL, string $ville = NULL, string $pays = NULL, bool $pasFixe = NULL, bool $pasPortable = NULL, bool $pasSms = NULL, bool $pasEmail = NULL, bool $cloud = NULL) {
		$this->_sessionExpire();

		if ($login !== NULL && $adresse !== NULL && $codePostal !== NULL && $ville !== NULL && $pays !== NULL) {
			$result = $this->api->societaireModifierInfosContact($login, $email, $telFixe, $telPortable, $adresse, $codePostal, $ville, $pays);
			$this->api->societaireModifierAutorisation($login, $pasFixe, $pasPortable, $pasSms, $pasEmail, $cloud);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['menuOnglet']			= 'contact';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$statut = $this->CI->session->statut;
		if ($statut === 'Admin') {
			$this->CI->load->view('pages/admin_menu_profil', $data);
		} else {
			$this->CI->load->view('pages/societaire_menu', $data);
		}
		$this->CI->load->view('pages/profil_contact', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie les informations utiles à la SCI d'un sociétaire
	 *
	 * @param  string	$login login du societaire
	 * @param  int		$regime régime matrimonial ou fiscal du societaire (Communauté, Contrat)
	 * @param  int		$agEntree assemblée générale pendant laquelle le sociétaire est entré dans la sci
	 * @return void
	 */
	public function profilModifierInfosSci(string $login = NULL, int $regime = NULL, int $agEntree = NULL) {
		$this->_sessionExpire();

		if ($login !== NULL && $regime !== NULL && $agEntree !== NULL) {
			$result = $this->api->societaireModifierInfosSci($login, $regime, $agEntree);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['listeRegimesMatrimoniaux']	= $this->api->parametresAvancesObtenirListeRegimesParType(false);
		$data['listeRegimesFiscaux']		= $this->api->parametresAvancesObtenirListeRegimesParType(true);
		$data['listeDatesAG']				= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['menuOnglet']			= 'sci';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$statut = $this->CI->session->statut;
		if ($statut === 'Admin') {
			$this->CI->load->view('pages/admin_menu_profil', $data);
		} else {
			$this->CI->load->view('pages/societaire_menu', $data);
		}
		$this->CI->load->view('pages/profil_sci', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Affiche les mouvements d'un sociétaire
	 *
	 * @param  string $login le login du sociétaire
	 * @return void
	 */
	public function profilMouvements(string $login = NULL) {
		$this->_sessionExpire();

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['listeMouvements']	= $this->api->mouvementObtenirListe();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['menuOnglet']			= 'mouvements';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$statut = $this->CI->session->statut;
		if ($statut === 'Admin') {
			$this->CI->load->view('pages/admin_menu_profil', $data);
		} else {
			$this->CI->load->view('pages/societaire_menu', $data);
		}
		$this->CI->load->view('pages/profil_mouvements', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Envoie un mail pour signaler les erreurs sur le profil Information d'un sociétaire
	 *
	 * @param  mixed $login
	 * @return void
	 */
	public function profilSignalerErreurInformations(string $login = NULL, array $champs = NULL, string $message = NULL) {
		$this->_sessionExpire();

		if ($champs !== NULL && $message !== NULL) {
			$result = $this->api->societaireSignalerProbleme($login, $champs, $message);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/societaire_menu', $data);
		$this->CI->load->view('pages/profil_signaler_Info', $data);
		$this->CI->load->view('templates/bas');
	}

	/**
	 * Envoie un mail pour signaler les erreurs sur le profil d'un sociétaire
	 *
	 * @param  mixed $login
	 * @return void
	 */
	public function profilSignalerErreurSci(string $login = NULL, array $champs = NULL, string $message = NULL) {
		$this->_sessionExpire();

		if ($champs !== NULL && $message !== NULL) {
			$result = $this->api->societaireSignalerProbleme($login, $champs, $message);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['listeRegimesMatrimoniaux']	= $this->api->parametresAvancesObtenirListeRegimesParType(false);
		$data['listeRegimesFiscaux']		= $this->api->parametresAvancesObtenirListeRegimesParType(true);
		$data['listeDatesAG']				= $this->api->assembleeGeneraleObtenirListeDatesAG();

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/societaire_menu', $data);
		$this->CI->load->view('pages/profil_signaler_Sci', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Envoie un mail pour signaler les erreurs sur le profil d'un sociétaire
	 *
	 * @param  mixed $login
	 * @return void
	 */
	public function profilSignalerErreurMvt(string $login = NULL, array $champs = NULL, string $message = NULL) {
		$this->_sessionExpire();

		if ($champs !== NULL && $message !== NULL) {
			$result = $this->api->societaireSignalerProbleme($login, $champs, $message);
			$data['result'] = $result;
		}

		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);
		$data['listeMouvements']	= $this->api->mouvementObtenirListe();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/societaire_menu', $data);
		$this->CI->load->view('pages/profil_signaler_Mvt', $data);
		$this->CI->load->view('templates/bas');
	}

	/**
	 * Envoie un mail à la cogérance
	 *
	 * @param  mixed $login
	 * @return void
	 */
	public function mailCogerance(string $login = NULL, string $message = NULL) {
		$this->_sessionExpire();

		if ($message !== NULL) {
			$result = $this->api->mailCogerance($login, $message);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['societaireInfos']	= $this->api->societaireObtenirInformations($login);

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/societaire_menu', $data);
		$this->CI->load->view('pages/contactCoGerance', $data);
		$this->CI->load->view('templates/bas');
	}

	/**
	 * Affiche la liste des mouvements
	 *
	 * @return void
	 */
	public function gestionMouvements() {
		$this->_sessionExpire();

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeMouvements']	= $this->api->mouvementObtenirListe();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['menuOnglet']			= 'gestionMouvements';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionMouvements', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Ajoute un mouvement
	 *
	 * @param  string	$titulaire titulaire de la part
	 * @param  string	$beneficiaire acheteur d'une part
	 * @param  array	$listePart liste des parts du mouvement
	 * @param  string	$date date du mouvement
	 * @param  float	$valeurUnitaire valeur d'une part
	 * @param  int		$type type de mouvement 0, 1 ou 2
	 * @param  int		$typePaiement type de paiement
	 * @param  string	$numeroPaiement numéro de paiement
	 * @return void
	 */
	public function gestionMouvementsAjouter(string $titulaire = NULL, string $beneficiaire = NULL, array $listePart = NULL, int $ag = NULL, float $valeurUnitaire = NULL, int $type = NULL, array $paiement = NULL) {
		$this->_sessionExpire();

		if ($beneficiaire !== NULL && $listePart !== NULL && $ag !== NULL && $valeurUnitaire !== NULL && $type !== NULL && $paiement !== NULL) {
			$result = $this->api->mouvementAjouter($titulaire, $beneficiaire, $listePart, $ag, $valeurUnitaire, $type, $paiement);
			$data['result'] = $result;
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['listeDatesAG']		= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['listePaiements']		= $this->api->parametresAvancesObtenirListePaiements();
		$data['menuOnglet']			= 'gestionMouvements';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionMouvements_ajouter', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Modifie un mouvement
	 *
	 * @param  int		$mouvementId
	 * @param  string	$titulaire titulaire de la part
	 * @param  string	$beneficiaire acheteur d'une part
	 * @param  array	$listePart liste des parts du mouvement
	 * @param  string	$date date du mouvement
	 * @param  float	$valeurUnitaire valeur d'une part
	 * @param  int		$typePaiement type de paiement
	 * @param  string	$numeroPaiement numéro de paiement
	 * @return void
	 */
	public function gestionMouvementsModifier(int $mouvementId = NULL, string $titulaire = NULL, string $beneficiaire = NULL, array $listePart = NULL, int $ag = NULL, float $valeurUnitaire = NULL, array $paiement = NULL) {
		$this->_sessionExpire();

		if ($mouvementId !== NULL && $beneficiaire !== NULL && $listePart !== NULL && $ag !== NULL && $valeurUnitaire !== NULL && $paiement!== NULL) {
			$result = $this->api->mouvementModifier($mouvementId, $titulaire, $beneficiaire, $listePart, $ag, $valeurUnitaire, $paiement);
			$data['result'] = $result;
		} 

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['mouvementInfos']		= $this->api->mouvementObtenirInformations($mouvementId);
		$data['listeSocietaires']	= $this->api->societaireObtenirListe();
		$data['listeDatesAG']		= $this->api->assembleeGeneraleObtenirListeDatesAG();
		$data['listePaiements']		= $this->api->parametresAvancesObtenirListePaiements();
		$data['menuOnglet']			= 'gestionMouvements';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionMouvements_modifier', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Supprime un mouvement
	 *
	 * @param  string $id l'id du mouvement à supprimer
	 * @return void
	 */
	public function gestionMouvementsSupprimer(string $id = NULL) {
		$this->_sessionExpire();

		if ($id !== NULL) {
			$this->api->mouvementSupprimer($id);
		}

		redirect(base_url() . 'index.php/gestionMouvements');
	}
	
	/**
	 * supprimer une liste de mouvements
	 *
	 * @param  array $ids les ids des mouvements
	 * @return void
	 */
	public function gestionMouvementsListeSupprimer(array $ids) {
		$this->_sessionExpire();

		foreach($ids as $id) {
            $this->api->mouvementSupprimer($id);
        }
	}

	/**
	 * Exporte les informations de tous les mouvements
	 *
	 * @return void
	 */
	public function gestionMouvementsExporter() {
		$this->_sessionExpire();
		$this->api->mouvementIOExporter();
	}
	
	/**
	 * Importe les informations des mouvements via un fichier
	 *
	 * @param  string $fichier le nom du fichier
	 * @return void
	 */
	public function gestionMouvementsImporter(string $fichier = NULL) {
		$this->_sessionExpire();

		if ($fichier !== NULL) {
			$config['file_name']		= 'registreMouvements';
			$config['upload_path']		= './registres/import/';
			$config['overwrite']		= TRUE;
			$config['allowed_types']	= 'cvs|xlsx';
			$config['max_size']			= '1000000';
			$this->CI->load->library('upload', $config);
			$test = $this->CI->upload->do_upload('import');

			if ($test) {
				$result = $this->api->mouvementIOImporter($this->CI->upload->data()['file_name']);
				$data['result'] = $result;
			} else {
				$data['result'] = -1;
			}
			unset($this->CI->upload);
		}

		$data['siteInfos']			= $this->api->siteObtenirInformations();
		$data['listeMouvements']	= $this->api->mouvementObtenirListe();
		$data['menuOnglet']			= 'gestionMouvements';

		$this->CI->load->view('templates/haut', $data);
		$this->CI->load->view('bandeaux/utilisateur', $data);
		$this->CI->load->view('pages/admin_menu', $data);
		$this->CI->load->view('pages/gestionMouvements', $data);
		$this->CI->load->view('templates/bas');
	}
	
	/**
	 * Exporte l'historique des parts
	 *
	 * @return void
	 */
	public function gestionMouvementsHistoriqueParts() {
		$this->api->partOExporter();
	}
	
	/**
	 * Envoie un mail à plusieurs destinataires
	 *
	 * @param  int $destinataire un entier permettant de savoir si il faut envoyer le mail à tous les sociétaires
	 * @param  array $emails les emails destinataires
	 * @param  string $objet l'objet du mail
	 * @param  string $message le message du mail
	 * @param  $file_name le nom du fichier attaché
	 * @return void
	 */
	public function mailEnvoyer(int $destinataire = NULL, int $type = NULL, array $emails = NULL, string $objet = NULL, string $message = NULL, $file_name = NULL) {
		$this->_sessionExpire();

		$listeSocietaires = $this->api->societaireObtenirListe();

		if ($destinataire !== NULL && $objet !== NULL && $message !== NULL) {
			if ($destinataire === 0) {
				$emails = [];
				foreach($listeSocietaires as $soc) {
					if ($type==0){
						if (isset($soc['soc_email']) && !isset($soc['ass_dateSortie'])) {
							$emails[] = $soc['soc_email'];
						}
					} else {
						if ($soc['soc_pasEnvoyerEmail'] === '0' && $soc['soc_etat']!=='D' && isset($soc['soc_email']) && !isset($soc['ass_dateSortie'])) {
							$emails[] = $soc['soc_email'];
						}
					}
				}
			}

			$result = $this->api->mailEnvoyer($emails, $objet, $message, $file_name);
			$data['result'] = $result;
		}

		$data['siteInfos'] = $this->api->siteObtenirInformations();
		$data['listeSocietaires'] = $listeSocietaires;
		$data['menuOnglet'] = 'mailing';

		$this->CI->load->view('templates/haut',$data);
		$this->CI->load->view('bandeaux/utilisateur',$data);
		$this->CI->load->view('pages/admin_menu',$data);
		$this->CI->load->view('pages/mail',$data);
		$this->CI->load->view('templates/bas');
	}
}

?>