<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de la page gestion des mouvements
 */
class GestionMouvements extends CI_Controller {
	
	/**
	 * Le contrôleur principal de l'application
	 *
	 * @var Controleur
	 */
	protected $controleur;
	
	/**
	 * Le constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		if (!$this->session->has_userdata('login') || $this->session->statut !== 'Admin') {
			redirect(base_url());
		}

		$this->controleur = new Controleur();
	}
	
	/**
	 * Affiche la liste des mouvements
	 *
	 * @return void
	 */
	public function index() {
		$this->controleur->gestionMouvements();
	}
	
	/**
	 * Ajoute un mouvement
	 *
	 * @return void
	 */
	public function ajouter() {
		$this->form_validation->set_rules('ope-date', 'ope-date', 'required');
		$this->form_validation->set_rules('ope-type', 'ope-type', 'required');
		$this->form_validation->set_rules('beneficiaire', 'beneficiaire', 'required');
		$this->form_validation->set_rules('parts', 'parts', 'required');
		$this->form_validation->set_rules('prix', 'prix', 'required');
		$this->form_validation->set_rules('paiements-nb', 'paiements-nb', 'required');
		$this->form_validation->set_rules('pai-type-1', 'pai-type-1', 'required');
		$this->form_validation->set_rules('pai-num-1', 'pai-num-1', 'required');

		$type = (int) $this->input->post('ope-type');
		if ($type !== 0) {
			$this->form_validation->set_rules('titulaire', 'titulaire', 'required');
		}

		if ($this->form_validation->run()) {
			$date			= (int) $this->input->post('ope-date');
			$type			= (int) $this->input->post('ope-type');
			$titulaire		= (string) $this->input->post('titulaire');
			$beneficiaire	= (string) $this->input->post('beneficiaire');
			$valeur			= (float) $this->input->post('prix');

			if (empty($titulaire)) {
				$titulaire = NULL;
			}

			$partsString	= (string) $this->input->post('parts');
			$parts			= array_map('intval', explode(',', $partsString));

			$paiement = array();
			$nombrePaiements = (int) $this->input->post('paiements-nb');
			for($i = 1; $i <= $nombrePaiements; $i++) {
				$paiType	= (int) $this->input->post('pai-type-'. $i);
				$paiNum		= (string) $this->input->post('pai-num-'. $i);

				$paiement[] = array($paiType => $paiNum);
			}

			$this->controleur->gestionMouvementsAjouter($titulaire, $beneficiaire, $parts, $date, $valeur, $type, $paiement);
		} else {
			$this->controleur->gestionMouvementsAjouter();
		}
	}
	
	/**
	 * Modifie un mouvement
	 *
	 * @param  string $id l'id du mouvement à modifier
	 * @return void
	 */
	public function modifier(int $id) {
		$this->form_validation->set_rules('ope-date', 'ope-date', 'required');
		$this->form_validation->set_rules('beneficiaire', 'beneficiaire', 'required');
		$this->form_validation->set_rules('parts', 'parts', 'required');
		$this->form_validation->set_rules('prix', 'prix', 'required');
		$this->form_validation->set_rules('paiements-nb', 'paiements-nb', 'required');
		$this->form_validation->set_rules('pai-type-1', 'pai-type-1', 'required');
		$this->form_validation->set_rules('pai-num-1', 'pai-num-1', 'required');

		if ($this->form_validation->run()) {
			$date			= (int) $this->input->post('ope-date');
			$titulaire		= (string) $this->input->post('titulaire');
			$beneficiaire	= (string) $this->input->post('beneficiaire');
			$valeur			= (float) $this->input->post('prix');

			if (empty($titulaire)) {
				$titulaire = NULL;
			}

			$partsString	= (string) $this->input->post('parts');
			$parts			= array_map('intval', explode(',', $partsString));

			$paiement = array();
			$nombrePaiements = (int) $this->input->post('paiements-nb');
			for($i = 1; $i <= $nombrePaiements; $i++) {
				$paiType	= (int) $this->input->post('pai-type-'. $i);
				$paiNum		= (string) $this->input->post('pai-num-'. $i);

				$paiement[] = array($paiType => $paiNum);
			}
			
			$this->controleur->gestionMouvementsModifier($id, $titulaire, $beneficiaire, $parts, $date, $valeur, $paiement);
		} else {
			$this->controleur->gestionMouvementsModifier($id);
		}
	}
	
	/**
	 * Supprime un mouvement
	 *
	 * @param  string $id l'id du mouvement à supprimer
	 * @return void
	 */
	public function supprimer(int $id) {
		$this->controleur->gestionMouvementsSupprimer($id);
	}

		/**
	 * supprimer une liste de mouvements
	 *
	 * @param  array $ids les ids des mouvements
	 * @return void
	 */
	public function supprimerListeMouvements(array $ids=NULL) {
		$postData = $this->input->post();
		$ids = $postData['ids'];
		$this->controleur->gestionMouvementsListeSupprimer($ids);
   		$result = array("Status"=>"true");
		$result['token'] = $this->security->get_csrf_hash();
		echo json_encode($result);
	}
	
	/**
	 * Exporte les informations de tous les mouvements
	 *
	 * @return void
	 */
	public function exporter() {
		$this->controleur->gestionMouvementsExporter();
	}
	
	/**
	 * Importe les informations de mouvements via un fichier excel
	 *
	 * @return void
	 */
	public function importer() {
		$fichier = (string) $_FILES['import']['name'];
		$this->controleur->gestionMouvementsImporter($fichier);
	}
	
	/**
	 * Exporte l'historique de toutes les parts
	 *
	 * @return void
	 */
	public function historiqueParts() {
		$this->controleur->gestionMouvementsHistoriqueParts();
	}
}
?>