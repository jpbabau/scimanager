<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de la page paramètres
 */
class Parametres extends CI_Controller {
	
	/**
	 * Le controleur principal de l'application
	 *
	 * @access protected
	 * @var Controleur
	 */
	protected $controleur;
	
	/**
	 * Constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		if (!$this->session->has_userdata('login') || $this->session->statut !== 'Admin') {
			redirect(base_url());
		}

		$this->controleur = new Controleur();
	}
	
	/**
	 * Affiche la page paramètres
	 *
	 * @return void
	 */
	public function index() {
		$this->controleur->parametres();
	}
	
	/**
	 * Affiche la page paramètres avancés
	 *
	 * @return void
	 */
	public function avances() {
		$this->controleur->parametresAvances();
	}
	
	/**
	 * Modifie les informations du site
	 *
	 * @return void
	 */
	public function modifierSiteInfos() {
		$this->form_validation->set_rules('structure', 'structure', 'required');
		$this->form_validation->set_rules('nom', 'nom', 'required');

		if ($this->form_validation->run()) {
			$structure	= (int) $this->input->post('structure');
			$nom		= (string) $this->input->post('nom');
			$image		= (string) $_FILES['img']['name'];
			$icone		= (string) $_FILES['ico']['name'];

			$this->controleur->parametresModifierSiteInfos($structure, $nom, $image, $icone);
		} else {
			$this->controleur->parametresModifierSiteInfos();
		}
	}

	/**
	 * Modifie le type d'installation
	 *
	 * @return void
	 */
	public function modifierInstallationLocal() {
		$local		= (bool) ($this->input->post('local') == 'on');
		$this->controleur->configurationInstallationLocal($local);
	}

	/**
	 * Modifie le protocole SMTP
	 *
	 * @return void
	 */
	public function modifierSiteEmail() {
		$this->form_validation->set_rules('host', 'host', 'required');
		$this->form_validation->set_rules('port', 'port', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('crypto', 'crytpo', 'required');

		if ($this->form_validation->run()) {
			$host		= (string) $this->input->post('host');
			$port		= (string) $this->input->post('port');
			$email		= (string) $this->input->post('email');
			$password	= (string) $this->input->post('password');
			$crypto		= (string) $this->input->post('crypto');

			$action		= (string) $this->input->post('action');

			$this->controleur->parametresModifierSiteEmail($action, $host, $port, $email, $password, $crypto);
		} else {
			$this->controleur->parametresModifierSiteEmail();
		}
	}
	
	/**
	 * Modifie les informations de l'administrateur
	 *
	 * @return void
	 */
	public function modifierAdminInfos() {
		$this->form_validation->set_rules('login', 'login', 'required');
		$this->form_validation->set_rules('mail', 'mail', 'required');

		if ($this->form_validation->run()) {
			$login = (string) $this->input->post('login');
			$email = (string) $this->input->post('mail');

			$this->controleur->parametresModifierAdminInfos($login, $email);
		} else {
			$this->controleur->parametresModifierAdminInfos();
		}
	}
	
	/**
	 * Modifie le mot de passe de l'administrateur
	 *
	 * @return void
	 */
	public function modifierAdminMdp() {
		$this->form_validation->set_rules('oldmdp', 'oldmdp', 'required');
		$this->form_validation->set_rules('newmdp1', 'newmdp1', 'required');
		$this->form_validation->set_rules('newmdp2', 'newmdp2', 'required');

		if ($this->form_validation->run()) {
			$ancienMdp			= (string) $this->input->post('oldmdp');
			$nouveauMdp			= (string) $this->input->post('newmdp1');
			$confirmationMdp	= (string) $this->input->post('newmdp2');

			if ($nouveauMdp === $confirmationMdp) {
				$this->controleur->parametresModifierAdminMdp($ancienMdp, $nouveauMdp);
			} else {
				$this->controleur->parametresModifierAdminMdp();
			}
		} else {
			$this->controleur->parametresModifierAdminMdp();
		}
	}
	
	/**
	 * Remet les images par défaut du site
	 *
	 * @return void
	 */
	public function imagesParDefaut() {
		$this->controleur->parametresImagesParDefaut();
	}
	
	/**
	 * Ajoute un statut
	 *
	 * @return void
	 */
	public function ajouterStatut() {
		$this->form_validation->set_rules('statut', 'statut', 'required');

		if ($this->form_validation->run()) {
			$statut = (string) $this->input->post('statut');

			$this->controleur->ajouterStatut($statut);
		} else {
			$this->controleur->ajouterStatut();
		}
	}

	/**
	 * Supprime un statut
	 *
	 * @param  int $id l'id du statut à supprimer
	 * @return void
	 */
	public function supprimerStatut(int $id = NULL) {
		$this->controleur->supprimerStatut($id);
	}
	
	/**
	 * Modifie un statut
	 *
	 * @return void
	 */
	public function modifierStatut() {
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('statut', 'statut', 'required');

		if ($this->form_validation->run()) {
			$id		= (int) $this->input->post('id');
			$statut	= (string) $this->input->post('statut');

			$this->controleur->modifierStatut($id, $statut);
		} else {
			$this->controleur->modifierStatut();
		}
	}

		/**
	 * Modifie la durée RGPD
	 *
	 * @return void
	 */
	public function modifierDureeRGPD() {
		$this->form_validation->set_rules('duree', 'duree', 'required');

		if ($this->form_validation->run()) {
			$dureeRGPD = (int) $this->input->post('duree');

			$this->controleur->configurationModifierDureeRGPD($dureeRGPD);
		} else {
			$this->controleur->configurationModifierDureeRGPD();
		}
	}
	
	/**
	 * Désinstalle la base de données
	 *
	 * @return void
	 */
	public function desinstaller() {
		$this->controleur->parametresAvancesDesinstaller();
	}
}

?>