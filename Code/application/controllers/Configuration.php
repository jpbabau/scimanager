<?php
defined('BASEPATH') || exit('No direct script access allowed');

require_once APPPATH . 'controllers/Controleur.php';

/**
 * Contrôleur de la page configuration
 */
class Configuration extends Ci_Controller {
	
	/**
	 * Le contrôleur principal de l'application
	 *
	 * @access protected
	 * @var mixed
	 */
	protected $controleur;
	
	/**
	 * Le constructeur de la classe
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');

		if (!$this->session->has_userdata('login') || $this->session->statut !== 'Admin') {
			redirect(base_url());
		}

		$this->controleur = new Controleur;
	}
	
	/**
	 * Affiche la page configuration
	 *
	 * @return void
	 */
	public function index() {
		$this->controleur->configuration();
	}
	
	/**
	 * Ajoute une date d'assemblée générale
	 *
	 * @return void
	 */
	public function ajouterDateAG() {
		$this->form_validation->set_rules('date', 'date', 'required');

		if ($this->form_validation->run()) {
			$date = (string) $this->input->post('date');

			$this->controleur->configurationAjouterDateAG($date);
		} else {
			$this->controleur->configurationAjouterDateAG();
		}
	}
	
	/**
	 * Modifie une date d'assemblée générale
	 *
	 * @return void
	 */
	public function modifierDateAG() {
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');

		if ($this->form_validation->run()) {
			$id		= (int) $this->input->post('id');
			$date	= (string) $this->input->post('date');

			$this->controleur->configurationModifierDateAG($id, $date);
		} else {
			$this->controleur->configurationModifierDateAG();
		}
	}
	
	/**
	 * Supprime une date d'assemblée générale
	 *
	 * @param  int $id l'id de la date à supprimer
	 * @return void
	 */
	public function supprimerDateAG(int $id = NULL) {
		$this->controleur->configurationSupprimerDateAG($id);
	}
}