# Projet

Application Web support pour la gestion d'une SCI solidaire.

## Description

Cette première version permet de gérer les informations concernant les sociétaires, la SCI ou le GFA et les mouvements de parts.

## Getting Started

### Dépendances

- WAMP server ou un serveur sur un hébergeur Web
- PHP Objet 7.2
- MySQL 5.6
- plus d'informations dans le guide d'installation

## Auteurs

- [@Jean-Philippe Babau](Jean-Philippe.Babau@univ-brest.fr)
- Pierre Cornen
- Tristan Le pape
- Vincent Bouillon

## Version

- 1.0
  - Initial Release

## License

This project is licensed under the [gpl-3.0] License- see the [LICENSE](/LICENSE) file for details.

